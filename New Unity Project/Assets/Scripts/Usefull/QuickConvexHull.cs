﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Usefull
{
    static class QuickConvexHull
    {

        public static ConvexHull find2DConvexHull(List<Vector2> input, out Vector2 mostLeft, out Vector2 mostRight, out List<Vector2> above, out List<Vector2> below)
        {
            //Debug.Log("find2DConvexHull: " + input.Count);
                                                                                                                                                                                                                                                                                                                    
            List<Vector2> output = new List<Vector2>();
            List<Vector2> inputCopy = new List<Vector2>();
            inputCopy.AddRange(input);

            mostLeft = inputCopy[0];
            mostRight = inputCopy[0];

            for (int i = 1; i < inputCopy.Count; i++)
            {
                if (inputCopy[i].x < mostLeft.x)
                {
                    mostLeft = inputCopy[i];
                }
                if (inputCopy[i].x > mostRight.x)
                {
                    mostRight = inputCopy[i];
                }   
            }

            above = new List<Vector2>();
            below = new List<Vector2>();

            /*Debug.Log("Input VERTICES");
            for (int i = 0; i < inputCopy.Count; i++)
            {
                Debug.Log(inputCopy[i]);
            }
*/
            inputCopy.Remove(mostLeft);
            inputCopy.Remove(mostRight);

            splitArea(inputCopy, mostLeft, mostRight, out above, out below);

            //Debug.Log("ConvexHull Vertices:");
            output.Add(mostLeft);
            //Debug.Log(mostLeft);
            
            convexHullDepth(above, mostLeft, mostRight, output);

            output.Add(mostRight);
            //Debug.Log(mostRight);

            convexHullDepth(below, mostLeft, mostRight, output);

            //splitAndSearch(mostLeft, mostRight,input, output);
            
            /*Debug.Log("CONVEX:");
            for (int i = 0; i < output.Count; i++)
            {
                Debug.Log(output[i]);
            }*/
            return new ConvexHull(output, input);
        }

        static void splitArea(List<Vector2> input, Vector2 A, Vector2 B, out List<Vector2> above, out List<Vector2> below)
        {
            above = new List<Vector2>();
            below = new List<Vector2>();

            for (int i = 0; i < input.Count; i++)
            {
                if (pointLocation(A, B, input[i]) > 0)
                {
                    above.Add(input[i]);
                }
                else
                {
                    below.Add(input[i]);
                }
            }
        }
        static Vector2 findFurthestelement(List<Vector2> input, Vector2 A, Vector2 B)
        {
            if (input.Count > 0)
            {
                Vector2 furthest = input[0];
                float dist = Assets.Scripts.Functions.distance(A,B,furthest);
                for (int i = 0; i < input.Count; i++)
                {
                    float tempDist = Assets.Scripts.Functions.distance(A, B, input[i]);
                    if (tempDist > dist)
                    {
                        dist = tempDist;
                        furthest = input[i];
                    }
                }
                return furthest;
            }
            else
            {
                return Vector2.zero;
            }
        }
        static void findEdges(List<Vector2> input, out Vector2 A, out Vector2 B)
        {
            if (input.Count > 1)
            {
                A = input[0];
                B = input[0];

                for (int i = 1; i < input.Count; i++)
                {
                    if (input[i].x < A.x)
                    {
                        A = input[i];
                    }
                    if (input[i].x > B.x)
                    {
                        B = input[i];
                    }
                }
            }
            else
            {
                A = Vector2.zero;
                B = Vector2.zero;
            }
        }
        static void convexHullDepth(List<Vector2> input, Vector2 A, Vector2 B, List<Vector2> output)
        {
            if (input.Count == 1)
            {
                output.Add(input[0]);
                //Debug.Log(input[0]);
                return;
            }
            if (input.Count > 0)
            {
                //ZNAJDZ NAJDALSZY ELEMENT ZBIORU
                Vector2 furthest = findFurthestelement(input, A, B);

                if (furthest != Vector2.zero)
                {
                    input.Remove(furthest);

                    //USUN WSZYSTKIE PUNKTY WERWNEATRZ TROJKATA
                    int i = 0;
                    do
                    {
                        if (isPointInTriangle(A, B, furthest, input[i]))
                        {
                            input.Remove(input[i]);
                        }
                        else
                        {
                            i++;
                        }
                    } while (i < input.Count);

                    if (input.Count > 0)
                    {
                        List<Vector2> above = new List<Vector2>();
                        List<Vector2> below = new List<Vector2>();

                        splitArea(input, A, furthest, out above, out below);

                        convexHullDepth(above, A, furthest, output);

                        output.Add(furthest);
                        //Debug.Log(furthest);

                        convexHullDepth(below, furthest, B, output);

                    }
                }
            }
        }
        static void splitAndSearch(Vector2 A, Vector2 B, List<Vector2> input, List<Vector2> output)
        {
            if (input.Count > 1)
            {
                List<Vector2> above = new List<Vector2>();
                List<Vector2> below = new List<Vector2>();

                float furthestAbove = 0;
                float furthestBelow = 0;

                Vector2 furthestAbovePoint = new Vector2();
                Vector2 furthestBelowPoint = new Vector2();

                splitArea(input, A, B,out above, out below);
                /*
                 * for (int i = 0; i < input.Count; i++)
                {
                    if (pointLocation(A, B, input[i]) > 0)
                    {
                        above.Add(input[i]);
                        //float tempDist = distance(A, B, input[i]);
                        if (tempDist > furthestAbove)
                        {
                            furthestAbove = tempDist;
                            furthestAbovePoint = input[i];
                        }
                    }
                    else
                    {
                        below.Add(input[i]);
                        /*float tempDist = distance(A, B, input[i]);

                        if (tempDist > furthestBelow)
                        {
                            furthestBelow = tempDist;
                            furthestBelowPoint = input[i];

                        }/
                    }
                }
                 * */

                if (above.Count > 0)
                {
                    if (above.Count == 1)
                    {
                        output.Add(above[0]);
                    }
                    else
                    {
                        furthestAbovePoint = above[0];
                        furthestAbove = Assets.Scripts.Functions.distance(A, B, above[0]);

                        for (int i = 0; i < above.Count; i++)
                        {
                            float dist = Assets.Scripts.Functions.distance(A, B, above[i]);
                            if (dist > furthestAbove)
                            {
                                furthestAbovePoint = above[i];
                                furthestAbove = dist;
                            }
                        }
                        above.Remove(furthestAbovePoint);

                        //USUWAM WSZYSTKIE PUNKTY ZE SRODKA TROJKATA GORNEGO
                        for (int i = 0; i < above.Count; i++)
                        {
                            if (isPointInTriangle(A, furthestAbovePoint, B, above[i]))
                            {
                                above.RemoveAt(i);
                            }
                        }
                        if (above.Count > 0)
                        {

                            //RECURSIVLY SEARCH LEFT PART FOR CONVEX HULL
                            splitAndSearch(A, furthestAbovePoint, above, output);
                        }

                        //DODAJE SRODKOWY PUNKT
                        output.Add(furthestAbovePoint);
                    }
                }

                    if (below.Count > 0)
                    {
                        if (below.Count == 1)
                        {
                            output.Add(below[0]);
                        }
                        else
                        {

                            furthestBelowPoint = below[0];
                            furthestBelow = Assets.Scripts.Functions.distance(A, B, below[0]);

                            for (int i = 0; i < below.Count; i++)
                            {
                                float dist = Assets.Scripts.Functions.distance(A, B, below[i]);
                                if (dist > furthestBelow)
                                {
                                    furthestBelowPoint = below[i];
                                    furthestBelow = dist;
                                }
                            }

                            below.Remove(furthestBelowPoint);

                            if (below.Count > 0)
                            {
                                //USUWAM WSZYSTKIE PUNKTY ZE SRODKA TROJKATA DOLNEGO
                                for (int i = 0; i < below.Count; i++)
                                {
                                    if (isPointInTriangle(A, furthestBelowPoint, B, below[i]))
                                    {
                                        below.RemoveAt(i);

                                    }
                                }


                                //RECURSIVLY SEARCH RIGHT PART FOR CONVEX HULL
                                splitAndSearch(A, furthestBelowPoint, below, output);
                            }
                            output.Add(furthestBelowPoint);
                        }
                    }
            }
            else if(input.Count == 1)
            {
                output.Add(input[0]);
            }
        }

        static int pointLocation(Vector2 A, Vector2 B, Vector2 P)
        {
            float cp1 = (B.x - A.x) * (P.y - A.y) - (B.y - A.y) * (P.x - A.x);
            return (cp1 > 0) ? 1 : -1;
        }
        static bool isPointInTriangle(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p)
        {
            float alpha = ((p2.y - p3.y) * (p.x - p3.x) + (p3.x - p2.x) * (p.y - p3.y)) /((p2.y - p3.y) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.y - p3.y));
            float beta = ((p3.y - p1.y) * (p.x - p3.x) + (p1.x - p3.x) * (p.y - p3.y)) /((p2.y - p3.y) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.y - p3.y));
            float gamma = 1.0f - alpha - beta;

            if (alpha >= 0 && beta >= 0 && gamma >= 0)
            {
                return true;
            }
            return false;
        }

        

    }
}
