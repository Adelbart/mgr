﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Assets.Scripts.Usefull
{
    static class RotatingCalipers
    {
        public static void findMinSurroundingRect(ConvexHull inputConvex, out int[] supportPoints, out Vector2[] cornerPoints, out float outputRotation)
        {
            List<Vector2> convexVertices = new List<Vector2>();
            convexVertices.AddRange(inputConvex.getConvexHullPoints());
            Vector2 polygonCentroid = inputConvex.getConvexCentroid();
            supportPoints = new int[5];
            cornerPoints = new Vector2[4];

            /*Vector2[] supportingEdgePoints = new Vector2[2];
            supportingEdgePoints[0] = convexVertices[0];
            supportingEdgePoints[1] = convexVertices[1];

            Vector2 supportingEdgeVector = supportingEdgePoints[1] - supportingEdgePoints[0];

            float angle = Vector2.Angle(supportingEdgeVector, Vector2.up);
            Debug.Log(angle);

            Vector2 furthest = convexVertices[3];
            float dist = Assets.Scripts.Functions.distance(supportingEdgePoints[0], supportingEdgePoints[1], furthest);
            for (int i = 4; i < convexVertices.Count; i++)
            {
                float tempDist = Assets.Scripts.Functions.distance(supportingEdgePoints[0], supportingEdgePoints[1], convexVertices[i]);
                if (tempDist > dist)
                {
                    furthest = convexVertices[i];
                    dist = tempDist;
                }
            }*/
            
            float rotation = 0;
            rotateAndFindMin(convexVertices, polygonCentroid, out supportPoints, out cornerPoints, out rotation);

           /* Debug.Log("Minimal rect found");
            for (int i = 0; i < cornerPoints.Length; i++)
            {
                Debug.Log("Final Corner " + i + " : " + cornerPoints[i]);
            }*/
            for (int i = 0; i < cornerPoints.Length; i++)
            {
                cornerPoints[i] = Assets.Scripts.Functions.RotatePoint(cornerPoints[i], polygonCentroid, rotation);
            }
            outputRotation = rotation;

        }
        private static void rotateAndFindMin(List<Vector2> vertices, Vector2 centroid, out int[] minSupEdgePoints, out Vector2[] minCornerPoints, out float rectAngle)
        {
            for (int z = 0; z < vertices.Count; z++)
            {
                //Debug.Log("Vertice " + z + " : " + vertices[z]);
            }
           // Debug.Log("Centroid: " + centroid);
                minSupEdgePoints = new int[5];
            minCornerPoints = new Vector2[4];
            Vector2[] supportingEdgePoints = new Vector2[2];

            

            rectAngle = 0;
            float angleAccumulation = 0;
            float minArea = 10000000;

            for (int edgeCount = 0; edgeCount < vertices.Count; edgeCount++)
            {
                Vector2 furthest = vertices[0];
                Vector2 minY = vertices[0];
                Vector2 maxY = vertices[0];
                Vector2 maxX = vertices[0];
                Vector2 minX = vertices[0];

                if (edgeCount < vertices.Count - 1)
                {
                    supportingEdgePoints[0] = vertices[edgeCount];
                    supportingEdgePoints[1] = vertices[edgeCount+1];
                }
                else
                {
                    supportingEdgePoints[0] = vertices[edgeCount];
                    supportingEdgePoints[1] = vertices[0];
                }

                //Debug.Log("Supporting edge vertices indexes: " + vertices.IndexOf(supportingEdgePoints[0]) + "   " + vertices.IndexOf(supportingEdgePoints[1]));

                float angle = Vector2.Angle(supportingEdgePoints[1] - supportingEdgePoints[0], Vector2.up);

               // Debug.Log("Rotation Angle: " + angle);
                angleAccumulation += angle;
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i] = Assets.Scripts.Functions.RotatePoint(vertices[i], centroid, -angle);
                }
                //float dist = Assets.Scripts.Functions.distance(supportingEdgePoints[0], supportingEdgePoints[1], furthest);

                for (int i = 0; i < vertices.Count; i++)
                {
                    /*float tempDist = Assets.Scripts.Functions.distance(supportingEdgePoints[0], supportingEdgePoints[1], vertices[i]);
                    if (tempDist > dist)
                    {
                        furthest = vertices[i];
                        dist = tempDist;
                    }*/
                    if (vertices[i].x > maxX.x)
                    {
                        maxX = vertices[i];
                    }
                    if (vertices[i].x < minX.x)
                    {
                        minX = vertices[i];
                    }
                    if (vertices[i].y < minY.y)
                    {
                        minY = vertices[i];
                    }
                    if (vertices[i].y > maxY.y)
                    {
                        maxY = vertices[i];
                    }
                }
                float newRectArea = (maxX.x - minX.x) * (maxY.y - minY.y);
                //Debug.Log("Width: " + (maxX.x - minX.x));
                //Debug.Log("Height: " + (maxY.y - minY.y));
                if (minArea > newRectArea)
                {
                    //Debug.Log("New Minimal Enclosing Rectangle with are of: "+newRectArea);

                    minArea = newRectArea;
                    rectAngle = angleAccumulation;

                    minSupEdgePoints[0] = vertices.IndexOf(supportingEdgePoints[0]);
                    minSupEdgePoints[1] = vertices.IndexOf(supportingEdgePoints[1]);
                    minSupEdgePoints[2] = vertices.IndexOf(maxY);
                    minSupEdgePoints[3] = vertices.IndexOf(furthest);
                    minSupEdgePoints[4] = vertices.IndexOf(minY);

                    minCornerPoints[0] = new Vector2(minX.x,maxY.y);
                    //Debug.Log("Upper Left: " + minCornerPoints[0]);
                    minCornerPoints[1] = new Vector2(maxX.x,maxY.y);
                   //Debug.Log("Upper Right: " + minCornerPoints[1]);
                    minCornerPoints[2] = new Vector2(maxX.x, minY.y);
                    //Debug.Log("Lower Right: " + minCornerPoints[2]);
                    minCornerPoints[3] = new Vector2(minX.x, minY.y);
                    //Debug.Log("Lower Left: " + minCornerPoints[3]);
                }
            }
        }
        static Vector2[] findStartingAxisAlignedRect(List<Vector2> inputPoints)
        {
            Vector2[] corners = new Vector2[4];

            for (int j = 0; j < 4;j++) {
                corners[j] = inputPoints[0];
            }

            for (int i = 0; i < inputPoints.Count; i++)
            {
                if (inputPoints[i].x < corners[0].x)
                {
                    corners[0].x = inputPoints[i].x;
                    corners[3].x = inputPoints[i].x;
                }
                if (inputPoints[i].x > corners[1].x)
                {
                    corners[0].x = inputPoints[i].x;
                    corners[3].x = inputPoints[i].x;
                }
                if (inputPoints[i].y < corners[0].y)
                {
                    corners[0].y = inputPoints[i].y;
                    corners[1].y = inputPoints[i].y;
                }
                if (inputPoints[i].y > corners[2].y)
                {
                    corners[3].y = inputPoints[i].y;
                    corners[2].y = inputPoints[i].y;
                }
            }
            return corners;

        }
    }

}

