﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace Assets.Scripts
{

    static class Functions
    {
        public const float PI = 3.14f;

        static int index =0;
        private static bool testDirCreated = false;

        public static int rotationIndex = 0;
        public static List<Vector2> sortByX(List<Vector2> input)
        {
            int n = input.Count;
            while (n > 1)
            {
                for (int i = 0; i < input.Count - 1; i++)
                {
                    if (input[i].x > input[i + 1].x)
                    {
                        Vector2 temp = input[i];
                        input[i] = input[i + 1];
                        input[i + 1] = temp;
                    }
                }
                n--;
            }
            return input;
        }
        public static List<Vector2> sortByY(List<Vector2> input)
        {
            int n = input.Count;
            while (n > 1)
            {
                for (int i = 0; i < input.Count - 1; i++)
                {
                    if (input[i].y > input[i + 1].y)
                    {
                        Vector2 temp = input[i];
                        input[i] = input[i + 1];
                        input[i + 1] = temp;
                    }
                }
                n--;
            }
            return input;
        }
        public static double distanceAB(Vector2 A, Vector2 B)
        {
            return Math.Sqrt(Math.Pow(A.x - B.x, 2) + Math.Pow(A.y - B.y, 2));
        }
        public static float distanceAB_float(Vector2 A, Vector2 B)
        {
            return Mathf.Sqrt(Mathf.Pow(A.x - B.x, 2) + Mathf.Pow(A.y - B.y, 2));
        }
        public static double Abs(double a)
        {
            if (a < 0)
            {
                return -a;
            }
            return a;
        }

        public static float RectArea(Rect A)
        {
            return A.width * A.height;
        }
        public static float CircleArea(Assets.Scripts.Structures.MyCircleCollider C)
        {
            return PI * Mathf.Pow(C.getRadius(), 2);

        }
        public static double BoundsArea(Bounds B)
        {
            if (B.size.z == 0)
            {
                return B.size.x * B.size.y;
            }
            return B.size.x * B.size.y * B.size.z;
        }
        public static float getAngleBetweenVectors(Vector2 A, Vector2 B)
        {
            return Vector2.Angle(A, B);
        }
        public static float distance(Vector2 A, Vector2 B, Vector2 P)
        {
            float ABx = B.x - A.x;
            float ABy = B.y - A.y;
            float num = ABx * (A.y - P.y) - ABy * (A.x - P.x);
            if (num < 0) num = -num;
            return num;
        }

        public static Color32 getColorAroundPixel(Texture2D sample, int x, int y)
        {
            float r = 0;
            float g = 0;
            float b = 0;

            for(int i=-1;i<2;i++){
                for(int j=-1;j<2;j++){
                    r += sample.GetPixel(x+i,y+j).r;
                    g += sample.GetPixel(x+i,y+j).g;
                    b += sample.GetPixel(x+i,y+j).b;
                }
            }
            r/=9;
            g/=9;
            b/=9;

            //Debug.Log(new Color(r, g, b));
            return new Color(r,g,b);
        }
        /*public static Vector2 findPolygonCentroid(List<Vector2> inputVertices)
        {
            return Vector2.zero;
        }*/
        public static Vector2 findPolygonCentroid(this List<Vector2> path)
        {
            Vector2 result = path.Aggregate(Vector2.zero, (current, point) => current + point);
            result /= path.Count;

            return result;
        }
        public static Vector2 RotatePoint(Vector2 pointToRotate, Vector2 centerPoint, double angleInDegrees)
        {
            double angleInRadians = angleInDegrees * (Math.PI / 180);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);
            return new Vector2
            {
                x =
                    (int)
                    (cosTheta * (pointToRotate.x - centerPoint.x) -
                    sinTheta * (pointToRotate.y - centerPoint.y) + centerPoint.x),
                y =
                    (int)
                    (sinTheta * (pointToRotate.x - centerPoint.x) +
                    cosTheta * (pointToRotate.y - centerPoint.y) + centerPoint.y)
            };
        }
        public static Vector2[] getPolygonEdgesNormals(List<Vector2> points)
        {
            //List<Vector2> polygonVertices = polygon.getConvexHullPoints();
            Vector2[] resultNormals = new Vector2[points.Count];

            for (int i = 0; i < points.Count; i++)
            {
                if (i < points.Count - 1)
                {
                    Vector2 edge = (points[i + 1] - points[i]).normalized;
                    Vector2 edgeNormal = new Vector2(-edge.y, edge.x);
                    resultNormals[i] = edgeNormal.normalized;
                }
                else
                {
                    Vector2 edge = (points[0] - points[i]).normalized;
                    Vector2 edgeNormal = new Vector2(-edge.y, edge.x);
                    resultNormals[i] = edgeNormal.normalized;
                }

            }
            return resultNormals;
        }
        public static Vector2 projectPointOnLine(Vector2 line1, Vector2 line2, Vector2 toProject)
        {
            float m = (line2.y - line1.y) / (line2.x - line1.x);
            float b = line1.y - (m * line1.x);

            float x = (m * toProject.y + toProject.x - m * b) / (m * m + 1);
            float y = (m * m * toProject.y + m * toProject.x + b) / (m * m + 1);

            return new Vector2(x,y);
        }
        
        public static Vector2 projectPolygonOnAxis(Vector2 axis, List<Vector2> convexPolygonPoints)
        {
            float min = Vector2.Dot(axis, convexPolygonPoints[0]);
            float max = min;
            for (int i = 1; i < convexPolygonPoints.Count; i++)
            {
                // NOTE: the axis must be normalized to get accurate projections
                float p = Vector2.Dot(axis, convexPolygonPoints[i]);

                if (p < min)
                {
                    min = p;
                }
                else if (p > max)
                {
                    max = p;
                }
            }
            Vector2 proj = new Vector2(min, max);
            return proj;
        }
        public static bool vectorsOverlap(Vector2 A, Vector2 B)
        {
            return (!(B.y < A.x || A.y < B.x));
            /*
            if (
                (A.x >= B.x && A.x <= B.y) ||
                (A.y >= B.x && A.y <= B.y) ||
                (B.x >= A.x && B.x <= A.y) ||
                (B.y >= A.x && B.y <= A.y)
                )
            {
                return true;
            }
            else
            {
                return false;
            }*/
        }
        public static bool testRectsForIntersectionNew(MinEnclosingRect rectA, MinEnclosingRect rectB)
        {
            return false;
        }
        public static bool testRectsForIntersection(MinEnclosingRect rectA, MinEnclosingRect rectB)
        {
            List<Vector2> rectAPoints = rectA.getMinRectCornersAsList();
            List<Vector2> rectBPoints = rectB.getMinRectCornersAsList();

            Vector2[] polygonAEdgesNormal = getPolygonEdgesNormals(rectAPoints);
            Vector2[] polygonBEdgesNormal = getPolygonEdgesNormals(rectBPoints);

            List<Vector2> polygonAPoints = rectAPoints;
            List<Vector2> polygonBPoints = rectBPoints;

            for (int i = 0; i < polygonAEdgesNormal.Length; i++)
            {
                Vector2 projPolA = projectPolygonOnAxis(polygonAEdgesNormal[i], polygonAPoints);
                Vector2 projPolB = projectPolygonOnAxis(polygonAEdgesNormal[i], polygonBPoints);

                if (!vectorsOverlap(projPolA, projPolB))
                {
                    //Debug.Log("VECTORS DO NOT OVERLAP");
                    return false;
                }
            }

            for (int i = 0; i < polygonBEdgesNormal.Length; i++)
            {
                Vector2 projPolA = projectPolygonOnAxis(polygonBEdgesNormal[i], polygonAPoints);
                Vector2 projPolB = projectPolygonOnAxis(polygonBEdgesNormal[i], polygonBPoints);

                if (!vectorsOverlap(projPolB, projPolA))
                {
                    //Debug.Log("VECTORS DO NOT OVERLAP");
                    return false;
                }
            }
            //Debug.Log("VECTORS DO OVERLAP");
            return true;
        }
        public static bool testPolygonsForIntersection(ConvexHull polygonA, ConvexHull polygonB)
        {
            Vector2[] polygonAEdgesNormal = getPolygonEdgesNormals(polygonA.getConvexHullPoints());

            Vector2[] polygonBEdgesNormal = getPolygonEdgesNormals(polygonB.getConvexHullPoints());

            List<Vector2> polygonAPoints = polygonA.getConvexHullPoints();
            List<Vector2> polygonBPoints = polygonB.getConvexHullPoints();

            for (int i = 0; i < polygonAEdgesNormal.Length; i++)
            {
                Vector2 projPolA = projectPolygonOnAxis(polygonAEdgesNormal[i], polygonAPoints);
                Vector2 projPolB = projectPolygonOnAxis(polygonAEdgesNormal[i], polygonBPoints);

                if (!vectorsOverlap(projPolA, projPolB))
                {
                    //Debug.Log("VECTORS DO NOT OVERLAP");
                    return false;
                }
            }

            for (int i = 0; i < polygonBEdgesNormal.Length; i++)
            {
                Vector2 projPolA = projectPolygonOnAxis(polygonBEdgesNormal[i], polygonAPoints);
                Vector2 projPolB = projectPolygonOnAxis(polygonBEdgesNormal[i], polygonBPoints);

                if (!vectorsOverlap(projPolB, projPolA))
                {
                    //Debug.Log("VECTORS DO NOT OVERLAP");
                    return false;
                }
            }
            //Debug.Log("VECTORS DO OVERLAP");
            return true;
        }
        public static Enums.SlopeDirection slopeDirection(Vector2 startPoint, Vector2 endpoint)
        {
            float dx = endpoint.x-startPoint.x;
            float dy = endpoint.y - startPoint.y;
            if (dx == 0)
            {
                return Enums.SlopeDirection.Vertical;
            }
            float m = dy / dx;

            if (m == 0)
            {
                return Enums.SlopeDirection.Horizontal;
            }
            else if (Math.Abs(m) > 1)
            {
                return Enums.SlopeDirection.Steep;
            }
            else
            {
                return Enums.SlopeDirection.Flat;
            }
        }
        public static List<Vector2> detectPixelsOnLine(Vector2 v1, Vector2 v2)
        {
            //Bresenham's line algorithm
            float x1 = v1.x;
            float y1 = v1.y;
            float x2 = v2.x;
            float y2 = v2.y;


            bool steep = (Math.Abs(y2 - y1) > Math.Abs(x2 - x1));
            if(steep)
            {
                float t = x1;
                x1 = y1;
                y1=t;
                t=x2;
                x2=y2;
                y2=t;
            }
 
            if(x1 > x2)
            {
                float t = x1;
                x1=x2;
                x2=t;
                t=y1;
                y1=y2;
                y2=t;
            }
 
            float dx = x2 - x1;
            float dy = Math.Abs(y2 - y1);
 
            float error = dx / 2.0f;
            int ystep = (y1 < y2) ? 1 : -1;
            int y = (int)y1;
 
            int maxX = (int)x2;
            List<Vector2> result = new List<Vector2>();
            for(int x=(int)x1; x<maxX; x++)
            {
                if(steep)
                {
                    result.Add(new Vector2(y, x));
                }
                else
                {
                    result.Add(new Vector2(x, y));
                }
 
                error -= dy;
                if(error < 0)
                {
                    y += ystep;
                    error += dx;
                }
            }
            return result;
        }

        public static List<Vector2> getPointsOnLineWithBresham(Vector2 startPoint, Vector2 endpoint,Enums.SlopeDirection m)
        {

            
            float dx = endpoint.x - startPoint.x;
            float dy = endpoint.y - startPoint.y;
            float dy2 = 2*dy;
            float dx2 = 2 * dx;

            float dy2_dx2 = dy2 - dx2;
            float dx2_dy2 = dx2 - dy2;

            int Increment = 0;
            List<Vector2> result = new List<Vector2>();
            result.Add(startPoint);
            if (m == Enums.SlopeDirection.Steep)
            {
                float p0 = dy2 - dx;
                float decisionParameter = p0;

                for (int i = 1; i < dx; i++)
                {
                    if (decisionParameter < 0)
                    {
                        result.Add(new Vector2(startPoint.x + i, startPoint.y));
                        decisionParameter = decisionParameter + dx2;
                    }
                    else
                    {
                        result.Add(new Vector2(startPoint.x + i, startPoint.y + Increment));
                        Increment++;
                        decisionParameter = decisionParameter + dx2_dy2;
                    }
                }
            }
            else
            {
                float p0 = dy2 - dx;
                float decisionParameter = p0;

                for (int i = 1; i < dx; i++)
                {
                    if(decisionParameter < 0)
                    {
                        result.Add(new Vector2(startPoint.x + i, startPoint.y));
                        decisionParameter = decisionParameter + dy2;
                    }
                    else
                    {
                        result.Add(new Vector2(startPoint.x + i, startPoint.y+Increment));
                        Increment++;
                        decisionParameter = decisionParameter + dy2_dx2;
                    }
                }
            }
            result.Add(endpoint);
            return result;
        }
        public static Texture2D rotateTexture90DegreesLeft(Texture2D input)
        {
            Texture2D result = new Texture2D(input.height, input.width);

            for (int i = 0; i < input.width; i++)
            {
                for (int k = 0; k < input.height; k++)
                {
                    result.SetPixel(k, i, input.GetPixel(i, input.height - k));
                }
            }
            result.Apply();
            
            saveTexture(result, ""+index, "Rotated");
            index++;

            return result;
        }
        public static Texture2D cropTextureFromImageByCornerPoints(Texture2D imageCaptured, Vector2[] inputCorners)
        {
            Texture2D result = new Texture2D(100, 100);

            return result;
        }
        static DirectoryInfo checkFolder(string path)
        {

            if (Directory.Exists(path))
            {
                return new DirectoryInfo(path);
            }
            else
            {
                return Directory.CreateDirectory(path);
            }

        }
        public static void saveTexture(Texture2D textureToSave, string fileName, string optionalFolder = "")
        {
            byte[] textureByteData = textureToSave.EncodeToPNG();

#if UNITY_EDITOR

            if (!string.IsNullOrEmpty(optionalFolder))
            {
                DirectoryInfo dir = checkFolder(Application.dataPath + "/../" + optionalFolder);
                File.WriteAllBytes(Application.dataPath + "/../" + optionalFolder + "/" + fileName + ".png", textureByteData);
            }
            else
            {
                File.WriteAllBytes(Application.dataPath + "/../" + fileName + ".png", textureByteData);

            }

#elif UNITY_STANDALONE
            if (!string.IsNullOrEmpty(optionalFolder))
            {

                DirectoryInfo dir = checkFolder(Application.dataPath + "/" + optionalFolder);
                File.WriteAllBytes(Application.dataPath + "/" + optionalFolder + "/" + fileName + ".png", textureByteData);
            }
            else
            {
                File.WriteAllBytes(Application.dataPath + "/" + fileName + ".png", textureByteData);

            }
           
#endif
        }
        public static void saveTestTexture(Texture2D textureToSave)
        {
            #if UNITY_EDITOR
            DirectoryInfo folder = new DirectoryInfo(Application.dataPath + "/Models/Test/SavedTestScreens");
            int numberOfDirectories = folder.GetDirectories().Length;
            if (!testDirCreated)
            {
                testDirCreated = true;
                DirectoryInfo tempFolder = Directory.CreateDirectory(Application.dataPath + "/Models/Test/SavedTestScreens/Test"+numberOfDirectories); 
            }
            else
            {
                numberOfDirectories--;
            }

            DirectoryInfo folder2 = new DirectoryInfo(Application.dataPath + "/Models/Test/SavedTestScreens/Test" + numberOfDirectories);
            byte[] textureByteData = textureToSave.EncodeToPNG();

            int numberOfFiles = folder2.GetFiles().Length;

            File.WriteAllBytes(Application.dataPath + "/Models/Test/SavedTestScreens/Test" + numberOfDirectories+"/test" +numberOfFiles+ ".png", textureByteData);
            #elif UNITY_STANDALONE
            if (Directory.Exists(Application.dataPath + "/Test/"))
            {
                DirectoryInfo folder3 = new DirectoryInfo(Application.dataPath + "/Test/");
                int numberOfDirectories2 = folder3.GetFiles().Length;
                byte[] textureByteData2 = textureToSave.EncodeToPNG();
                File.WriteAllBytes(Application.dataPath + "/Test/" + numberOfDirectories2 + ".png", textureByteData2);
            }
            else
            {
                DirectoryInfo folder3 = Directory.CreateDirectory(Application.dataPath + "/Test/");
                int numberOfDirectories2 = folder3.GetFiles().Length;
                byte[] textureByteData2 = textureToSave.EncodeToPNG();
                File.WriteAllBytes(Application.dataPath + "/Test/" + numberOfDirectories2 + ".png", textureByteData2);
            }
            #endif
        }
        public static float isLeft(Vector2 P0, Vector2 P1, Vector2 P2)
        {
            return ((P1.x - P0.x) * (P2.y - P0.y) - (P2.x - P0.x) * (P1.y - P0.y));
        }
        public static bool PointInRectangle(Vector2 X, Vector2 Y, Vector2 Z, Vector2 W, Vector2 P)
        {
            return (isLeft(X, Y, P) > 0 && isLeft(Y, Z, P) > 0 && isLeft(Z, W, P) > 0 && isLeft(W, X, P) > 0);
        }
        public static bool PointInRectangle(MinEnclosingRect rect, Vector2 P)
        {
            Vector2 X = rect.getCorner(0);
            Vector2 Y = rect.getCorner(1);
            Vector2 Z = rect.getCorner(2);
            Vector2 W = rect.getCorner(3);

            return (isLeft(X, Y, P) > 0 && isLeft(Y, Z, P) > 0 && isLeft(Z, W, P) > 0 && isLeft(W, X, P) > 0);
        }
        public static int maxIntOf3(int A, int B, int C)
        {
            if (A >= B && A >= C)
            {
                return A;
            }
            else if (B >= A && B >= C)
            {
                return B;
            }
            else
            {
                return C;
            }
        }
        public static Texture2D cutSpriteFromAnimatedTexture(Texture2D inputTexture, int noColumns, int noRows, int indexOfTexture)
        {
            Texture2D result = new Texture2D(inputTexture.width/noColumns, inputTexture.height/noRows);
            //Debug.Log(inputTexture.width +"     "+inputTexture.height);
            //Debug.Log(inputTexture.width + "    " + "/" + "    " + noColumns + "    " + inputTexture.height + "    " + " /" + "    " + noRows);
            //Debug.Log(result.width + "    " + result.height);
            //Debug.Log(result.width + " " + result.height);
            /*for (int x = 0; x < result.width; x++)
            {
                for (int y = 0; y < result.height; y++)
                {*/
            //Debug.Log(result.width * (indexOfTexture % noColumns) + " " + result.height * (indexOfTexture / noColumns));
            result.SetPixels(inputTexture.GetPixels(result.width*(indexOfTexture%noColumns),result.height*(indexOfTexture/noColumns),result.width,result.height));
                /*}
            }*/
            result.Apply();
            //saveTexture(result, "przerobione" + indexOfTexture);
            return result;
        }
    }
}
