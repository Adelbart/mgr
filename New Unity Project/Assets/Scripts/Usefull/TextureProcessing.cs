﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Structures;

namespace Assets.Scripts.Usefull
{
    public static class TextureProcessing
    {

        public static Texture2D convertToGreyscale(Texture2D inputTexture)
        {
            Texture2D result = new Texture2D(inputTexture.width, inputTexture.height);

            Color[] inputcolors = inputTexture.GetPixels();
            for (int i = 0; i < inputcolors.Length; i++)
            {
                int y=i/inputTexture.width;
                int x=i%inputTexture.width;
                float color = inputcolors[i].r*0.299f+inputcolors[i].g*0.587f+inputcolors[i].b*0.114f;
                result.SetPixel(x, y, new Color(color, color, color));
            }
            result.Apply();
            return result;
        }
        public static Texture2D cropTextureToSize(Texture2D input, int desiredwidth, int desiredHeight)
        {
            Texture2D result = new Texture2D(desiredwidth, desiredHeight);
            int startX = (input.width - desiredwidth) / 2;
            int startY = (input.height - desiredHeight) / 2;

            for (int i = 0; i < desiredwidth; i++)
            {
                for (int j = 0; j < desiredHeight; j++)
                {
                    result.SetPixel(i, j, input.GetPixel(startX + i, startY + j));
                }
            }
            result.Apply();
            return result;
        }
        public static Texture2D createImageHistogramRGBTexture(Texture2D inputImage)
        {

            int[] bluePixels = new int[230];
            int[] greenPixels = new int[230];
            int[] redPixels = new int[230];

            Color32[] colors = inputImage.GetPixels32();

            for (int i = 0; i < colors.Length; i++)
            {
                if (!colorCloseToWhite(colors[i]))
                {
                    bluePixels[colors[i].b] += 1;
                    greenPixels[colors[i].g] += 1;
                    redPixels[colors[i].r] += 1;
                }
            }
            int max = 0;
            for (int i = 0; i < bluePixels.Length; i++)
            {
                if (bluePixels[i] > max)
                {
                    max = bluePixels[i];
                }
                if (redPixels[i] > max)
                {
                    max = redPixels[i];
                }
                if (greenPixels[i] > max)
                {
                    max = greenPixels[i];
                }
            }

            Texture2D outputTexture = new Texture2D(230, max+5);


            for (int i = 0; i < bluePixels.Length; i++)
            {
                outputTexture.SetPixel(i, redPixels[i], Color.red);
                outputTexture.SetPixel(i, greenPixels[i], Color.green);
                outputTexture.SetPixel(i, bluePixels[i], Color.blue);
            }
            outputTexture.Apply();

            return outputTexture;
        }

        public static Texture2D createImageHistogram64ColorTexture(Texture2D inputImage)
        {
            int[] colorsPixels = new int[64];

            Color32[] colors = inputImage.GetPixels32();

            for (int i = 0; i < colors.Length; i++)
            {
                colorsPixels[convertColorBy2MostSignificantBytes(colors[i])]+=1;
            }
            int max = 0;
            for (int i = 0; i < colorsPixels.Length; i++)
            {
                if (colorsPixels[i] > max)
                {
                    max = colorsPixels[i];
                }
               
            }

            Texture2D outputTexture = new Texture2D(64, max + 5);


            for (int i = 0; i < colorsPixels.Length; i++)
            {
                outputTexture.SetPixel(i, colorsPixels[i], Color.black);
            }
            outputTexture.Apply();

            return outputTexture;
        } 
        public static int[] getImageHistogramFullColors(Texture2D inputImage)
        {
            int[] result = new int[8388608];
            Color32[] colors = inputImage.GetPixels32();

            for (int i = 0; i < colors.Length; i++)
            {

                result[(int)convertColorBy8Bytes(colors[i])] += 1;
            }

            return result;
        }

        public static int[] getImageHistogram32768Colors(Texture2D inputImage)
        {
            int[] result = new int[32768];
            Color32[] colors = inputImage.GetPixels32();

            for (int i = 0; i < colors.Length; i++)
            {
               
                result[(int)convertColorBy5MostSignificantBytes(colors[i])] += 1;
            }

            return result;
        }

        public static int[] getImageHistogram64Colors(Texture2D inputImage)
        {
            int[] result = new int[64];
            Color32[] colors = inputImage.GetPixels32();
            
            for (int i = 0; i < colors.Length;i++)
            {
                //Debug.Log(colors[i]);
                //Debug.Log(convertColorBy2MostSignificantBytes(colors[i]));
                result[(int)convertColorBy2MostSignificantBytes(colors[i])] += 1;
            }

            return result;
        }
        public static float createAndCompareHistograms(Texture2D textureA, Texture2D textureB)
        {
            float result = 0;

            result = ColorHistogram.compareColorHistograms(new ColorHistogram(textureA), new ColorHistogram(textureB));
            return result;
        }
        public static int compareColorHistograms(int[] first, int[] second)
        {
            int result = 0;
            
            for (int i = 1; i < first.Length; i++)
            {
                result +=  Math.Abs( first[i]-second[i]);
            }
                return result;
        }

        public static byte convertColorBy2MostSignificantBytes(Color32 inputColor)
        {
            if (colorCloseToWhite(inputColor))
            {
                return 0;
            }
            byte result = 0;

            byte color = (byte)(inputColor.b);
            //Debug.Log(color + "   " + inputColor.b);
            color >>= 6;
            result |= color;
            //Debug.Log(result);
            
            color = (byte)(inputColor.g);
            color >>= 4;
            result |= color;
            //Debug.Log(result);

            color = (byte)(inputColor.r);
            color >>= 2;
            result |= color;

            //Debug.Log(result);


            return result;
        }

        public static byte convertColorBy5MostSignificantBytes(Color32 inputColor)
        {

            if (colorCloseToWhite(inputColor))
            {
                return 0;
            }
            byte result = 0;

            byte color = (byte)inputColor.b;
            color >>= 3;
            result |= color;
            result <<= 5;

            color = (byte)(inputColor.g);
            color >>= 3;
            result |= color;
            result <<= 5;

            color = (byte)(inputColor.r);
            color >>= 3;
            result |= color;

            //Debug.Log(result);


            return result;
        }

        public static byte convertColorBy8Bytes(Color32 inputColor)
        {

            if (colorCloseToWhite(inputColor))
            {
                return 0;
            }
            byte result = 0;

            byte color = (byte)inputColor.b;
            result |= color;
            result <<= 8;

            color = (byte)(inputColor.g);
            result |= color;
            result <<= 8;

            color = (byte)(inputColor.r);
            result |= color;

            //Debug.Log(result);


            return result;
        }

        public static bool colorCloseToWhite(Color32 inputColor)
        {
            if ((inputColor.r > 240 && inputColor.g > 240 && inputColor.b > 240) || (inputColor.r < 20 && inputColor.g < 20 && inputColor.b  < 20))
            {
                return true;
            }
            return false;
        }

        public static Texture2D rescaleTexture(Texture2D input, int desiredwidth, int desiredHeight)
        {
            Texture2D result = new Texture2D(input.width, input.height);

            for (int i = 0; i < input.width; i++)
            {
                for (int j = 0; j < input.height; j++)
                {
                    result.SetPixel(i, j, input.GetPixel(i, j));
                }
            }
            result.Apply();

            result.Resize(desiredwidth, desiredHeight);
            result.Apply();

            Assets.Scripts.Functions.saveTexture(result, "asaa", "");
            return result;
        }
        public static void calibrateRaceTextures()
        {

        }
     public static Texture2D scaled(Texture2D src, int width, int height, FilterMode mode = FilterMode.Trilinear)
        {
                Rect texR = new Rect(0,0,width,height);
                _gpu_scale(src,width,height,mode);
               
                //Get rendered data back to a new texture
                Texture2D result = new Texture2D(width, height, TextureFormat.ARGB32, true);
                result.Resize(width, height);
                result.ReadPixels(texR,0,0,true);


                return result;                 
        }
       
        /// <summary>
        /// Scales the texture data of the given texture.
        /// </summary>
        /// <param name="tex">Texure to scale</param>
        /// <param name="width">New width</param>
        /// <param name="height">New height</param>
        /// <param name="mode">Filtering mode</param>
        public static void scale(Texture2D tex, int width, int height, FilterMode mode = FilterMode.Trilinear)
        {
                Rect texR = new Rect(0,0,width,height);
                _gpu_scale(tex,width,height,mode);
               
                // Update new texture
                tex.Resize(width, height);
                tex.ReadPixels(texR,0,0,true);
                tex.Apply(true);        //Remove this if you hate us applying textures for you :)
        }
               
        // Internal unility that renders the source texture into the RTT - the scaling method itself.
        static void _gpu_scale(Texture2D src, int width, int height, FilterMode fmode)
        {
                //We need the source texture in VRAM because we render with it
                src.filterMode = fmode;
                src.Apply(true);       
                               
                //Using RTT for best quality and performance. Thanks, Unity 5
                RenderTexture rtt = new RenderTexture(width, height, 32);
               
                //Set the RTT in order to render to it
                Graphics.SetRenderTarget(rtt);
               
                //Setup 2D matrix in range 0..1, so nobody needs to care about sized
                GL.LoadPixelMatrix(0,1,1,0);
               
                //Then clear & draw the texture to fill the entire RTT.
                GL.Clear(true,true,new Color(0,0,0,0));
                Graphics.DrawTexture(new Rect(0,0,1,1),src);
        }
    }
}