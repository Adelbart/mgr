﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Assets;
using UnityEngine.UI;
using UnityEngine.Events;
using Emgu.CV;
using Emgu.CV.VideoSurveillance;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Assets.Scripts.Structures;
using System.Runtime.InteropServices;
using MyLib;

public class cameraObscura3 : MonoBehaviour {

    public Text winText;
    public bool cauntingRegionsInBestResultSearch = true;
    int numberOfSetsTrainedHistograms = 0;
    public SpriteRenderer testTexture;
    public int _uvTieX = 3;
    public int _uvTieY = 1;
    private Vector2 _size;
    private int _lastIndex = -1;
    public int[] histogramsMapColorValues;
    public int _fps = 30;
    CameraCapture dllCamera = new CameraCapture();
    CommunicationController comController = new CommunicationController();
    private static string calibrationFilePath = "calibrationConfig.txt";
    public bool dllCameraInUse = true;
    public bool HandTestEnabled = false;
    public AssetManager assetManager;
    bool hasCamera = false;
    private bool isLiveDetectionEnabled = false;
    public GameObject menu;
    public PanelManager panelManager;
    int numberOfPawnsInLastFrame = 0;
    int[] pawnsDistributionInLastUpdate = new int[(int)Enums.RaceType.numberOfTypes];
    Pawn calibrationPawnAquired = null;

    private float photoDelayTime = 0.2f;
    public Text photoDelayTimeText;

    public Image handTexture;
    List<Texture2D> raceTemplateTextures = new List<Texture2D>();
    List<Region> desiredScenarioSetup = new List<Region>();
    public static int racesTemplatesPerDetection = 9;
    public int calirationStep;
    int currentRacePattern = 0;

    ColorHistogram[] templateHistograms;

    private bool isCalibTextureReady = false;

    private float handScreenTimer = 0;
    private MapControler mapControler;
    private CameraControler cameraControler;
    Texture2D extractedImage;
    public int pawnIndex = 0;
    public InputField inputField;

    public bool isDebug;
    public Text debug;
    public Slider slider;
    public bool didUpdatePawnInfo = false;
    public bool motionDetectionVariant = true; //True - detect motion with emgu  False - only compare if Camera input is identical
    Enums.HandDetectionState prevCaptureState = Enums.HandDetectionState.CameraClear;
    float handInCameraTime = 0;

    private MotionHistory motionHistory;
    private Mat forgroundMask = new Mat();
    private BackgroundSubtractor background;
    private Texture2D lastFrameCameraImage;

    Enums.HandDetectionState state = Enums.HandDetectionState.CameraClear;
    private int imageExtractionWidening = 2;

    bool cornersDetected = false;
    float cornersTime = 0;

    int imageCounter = 0;
    double contourSize = 0;
    WebCamTexture cameraUsed;
    int trainCounter = 0;
    //List<Pawn> results = new List<Pawn>();

    Pawn calibrationPawn;
    bool drawCalibrationPawn = false;
    float drawCalibrationPawnTimer = 0;
    List<Vector2> calibrationPixels = new List<Vector2>();

    //List<Rect> results = new List<Rect>();
    List<Rect> results2 = new List<Rect>();

    List<Vector2> pixels = new List<Vector2>();

    int topWidthOfCameraTextureCrop;
    int bottomWidthOfCameraTextureCrop;
    int leftHeightOfCameraTextureCrop;
    int rightHeightOfCameraTextureCrop;

    List<Vector2> cornersofCameraTexture = new List<Vector2>(); //corners are [0]-upperleft, [1] -upperright, [2] -lowerright, [3] -lowerleft
   
    public GUITexture textureHolder;
    public GameObject regionsParent;
    public GameObject extraFParent;
    public GameObject linesParent;
    public GameObject uiElementsParent;
    public Dropdown scenarioDropDownList;

    List<Pawn> debugPawnList = new List<Pawn>();
    List<int> debugRegionPawnList = new List<int>();
    public GameObject AmazonPawnAdder;
    public GameObject AmazonPawnRemover;
    public GameObject OrcPawnAdder;
    public GameObject OrcPawnRemover;
    public GameObject SkeletonPawnAdder;
    public GameObject SkeletonPawnRemover;
    bool moviePlayed = false;
    Texture2D cameraInput;
    float updatetimer = 4.0f;
    Vector2[] r = new Vector2[0];
    // Use this for initialization


    Image<Bgr, byte> Image1 = null;
    Image<Bgr, byte> Image2 = null;

    void Awake()
    {
        Application.targetFrameRate = 120;
    }
    void OnApplicationQuit()
    {
        if(dllCamera.isCameraRunning)
        {
            dllCamera.stopCameraCapture();
        }
    }
	void Start () {

        photoDelayTimeText.text = "Delay: " + photoDelayTime.ToString();
        templateHistograms = new ColorHistogram[racesTemplatesPerDetection * ((int)Enums.RaceType.numberOfTypes-1)];
        _size = new Vector2(1.0f / _uvTieX, 1.0f / _uvTieY);

        /*float width = testTexture.sprite.texture.width/3;
        float height = testTexture.sprite.texture.height;

        float worldScreenHeight = Screen.height;
        float worldScreenWidth = Screen.width;

        Vector3 xWidth = testTexture.transform.localScale;
        xWidth.x = worldScreenWidth / width;
        testTexture.transform.localScale = xWidth;
        //transform.localScale.x = worldScreenWidth / width;
        Vector3 yHeight = testTexture.transform.localScale;
        yHeight.y = worldScreenHeight / height;
        testTexture.transform.localScale = yHeight;
        //transform.localScale.y = worldScreenHeight / height;*/

        if (!isDebug)
        {
            OrcPawnAdder.SetActive(false);
            AmazonPawnAdder.SetActive(false);
            SkeletonPawnAdder.SetActive(false);
            OrcPawnRemover.SetActive(false);
            AmazonPawnRemover.SetActive(false);
            SkeletonPawnRemover.SetActive(false);
        }
        for (int i = 0; i < pawnsDistributionInLastUpdate.Length; i++)
        {
            pawnsDistributionInLastUpdate[i] = 0;
        }
            HideHand();
        if (!dllCameraInUse)
        {
            InitiateCamera();
        }
        else
        {
            startDLLCamera();
        }
        textureHolder.texture = assetManager.mapTexture;

        raceTemplateTextures = new List<Texture2D>();
        /*templateHistograms = new ColorHistogram[raceTemplateTextures.Length];
        for (int i = 0; i < templateHistograms.Length; i++)
        {
            templateHistograms[i] = new ColorHistogram(raceTemplateTextures[i]);
            //templateHistograms[i + 1] = new ColorHistogram(Assets.Scripts.Usefull.TextureProcessing.scaled(raceTemplateTextures[i % 2], 100, 100));
        }*/

       


        int numberOfRegionsHoldersToSpawn = assetManager.regionsConquarableTextures.Length;
        GUITexture[] regionsTextures = new GUITexture[numberOfRegionsHoldersToSpawn];
        GUITexture[] lineTextures = new GUITexture[numberOfRegionsHoldersToSpawn];
        GUITexture[] regionsExtraF = new GUITexture[5];
        for (int i = 0; i < numberOfRegionsHoldersToSpawn; i++)
        {
            GameObject tG = Instantiate(assetManager.regionTextureHolderInstance);
            tG.transform.SetParent(regionsParent.transform, false);
            regionsTextures[i] = tG.GetComponent<GUITexture>();

            tG = Instantiate(assetManager.lineTextureHolderInstance);
            tG.transform.SetParent(linesParent.transform, false);
            lineTextures[i] = tG.GetComponent<GUITexture>();
        }
        for (int i = 0; i < regionsExtraF.Length; i++)
        {
            GameObject tG = Instantiate(assetManager.regionTextureHolderInstance);
            tG.transform.SetParent(extraFParent.transform, false);
            tG.transform.localPosition+=new Vector3(0,0,1);
            regionsExtraF[i] = tG.GetComponent<GUITexture>();
        }
            mapControler = new MapControler(assetManager.mapRegionColor, panelManager, assetManager, regionsTextures, lineTextures, regionsExtraF, uiElementsParent);
       
        cameraControler = new CameraControler();
        initScenarioDropDownList();
	}
    void initScenarioDropDownList()
    {
        DirectoryInfo dirInfo = new DirectoryInfo(Application.dataPath + "/MapsInfo");
        if (dirInfo.Exists)
        {
            //Debug.Log(dirInfo.GetFiles().Length/2);
            for (int i = 0; i < dirInfo.GetFiles().Length/2-1; i++)
            {
                scenarioDropDownList.options.Add(new Dropdown.OptionData((i+1).ToString()));
            }
        }
        else
        {
            debug.text = "SCENARIO FOLDER DOES NOT EXIST";
        }

    }
    public void startDLLCamera()
    {
        dllCamera.startCamera(CameraControler.cameraCaptureWidth,CameraControler.cameraCaptureHeight);
        DirectoryInfo dir = new DirectoryInfo(Application.dataPath);
        FileInfo[] files =  dir.GetFiles();
        int numberOfHandCascades = 0;
        for(int i=0;i < files.Length; i++)
        {
            if (files[i].Name.Contains("Hand.Cascade") && !files[i].Name.Contains("meta"))
            {
                numberOfHandCascades++;
                Debug.Log("New handcascade file: " + files[i].Name+"   "+numberOfHandCascades);
            }
        }
        dllCamera.setCascadeClassifier(numberOfHandCascades, Application.dataPath + "/Hand.Cascade.");
        dllCameraInUse = true;
    }
    public void ShowSetup()
    {
        if (panelManager.isActiveAndEnabled)
        {
            panelManager.gameObject.SetActive(false);
        }
        else
        {
            panelManager.gameObject.SetActive(true);
            mapControler.showSetupPanels(assetManager);
        }
    }
    void InitiateCamera()
    {
        if (!isDebug)
        {
            if (WebCamTexture.devices.Length > 1)
            {
                foreach (WebCamDevice device in WebCamTexture.devices)
                {
                    if (device.name.Contains("Logitech"))
                    {
                        hasCamera = true;
                        Debug.Log(device.name);
                        cameraUsed = new WebCamTexture(device.name);
                        cameraUsed.requestedWidth = CameraControler.cameraCaptureWidth;
                        cameraUsed.requestedHeight = CameraControler.cameraCaptureHeight;
                        cameraUsed.requestedFPS = 120;
                        cameraUsed.Play();

                        /*cameraInput = new Texture2D(cameraUsed.width, cameraUsed.height);
                        lastFrameCameraImage = new Texture2D(cameraUsed.width, cameraUsed.height);
                        */
                        cameraInput = new Texture2D(cameraUsed.width, cameraUsed.height);
                        lastFrameCameraImage = new Texture2D(cameraUsed.width, cameraUsed.height);

                        //textureHolder.texture = cameraInput;
                        motionHistory = new MotionHistory(
                    1.0, //in second, the duration of motion history you wants to keep
                    0.05, //in second, maxDelta for cvCalcMotionGradient
                    0.5); //in second, minDelta for cvCalcMotionGradient
                    }
                }
            }
            else if (WebCamTexture.devices.Length == 1)
            {
                WebCamDevice device = WebCamTexture.devices[0];
                hasCamera = true;
                Debug.Log(device.name);
                cameraUsed = new WebCamTexture(device.name);
                cameraUsed.requestedWidth = CameraControler.cameraCaptureWidth;
                cameraUsed.requestedHeight = CameraControler.cameraCaptureHeight;
                cameraUsed.requestedFPS = 120;
                cameraUsed.Play();



                /*cameraInput = new Texture2D(cameraUsed.width, cameraUsed.height);
                lastFrameCameraImage = new Texture2D(cameraUsed.width, cameraUsed.height);
                */

                cameraInput = new Texture2D(cameraUsed.width, cameraUsed.height);
                lastFrameCameraImage = new Texture2D(cameraUsed.width, cameraUsed.height);

                //textureHolder.texture = cameraInput;
                motionHistory = new MotionHistory(
            1.0, //in second, the duration of motion history you wants to keep
            0.05, //in second, maxDelta for cvCalcMotionGradient
            0.5); //in second, minDelta for cvCalcMotionGradient
            }
            else
            {
                isDebug = true;
            }
            CameraControler.cameraCaptureWidth = cameraUsed.width;
            CameraControler.cameraCaptureHeight = cameraUsed.height;
            Debug.Log(CameraControler.cameraCaptureWidth + "    " + CameraControler.cameraCaptureHeight);
            Debug.Log(cameraUsed.requestedFPS);
        }
        else
        {
            CameraControler.cameraCaptureWidth = assetManager.colorTexture.width;
            CameraControler.cameraCaptureHeight = assetManager.colorTexture.height;
            Debug.Log(CameraControler.cameraCaptureWidth + "    " + CameraControler.cameraCaptureHeight);
        }
    }
    /*void FixedUpdate()
    {
        Debug.Log("FixedUpdate:  " + cameraUsed.didUpdateThisFrame);
    }
    void LateUpdate()
    {
        Debug.Log("LateUpdate:  " + cameraUsed.didUpdateThisFrame);
    }*/
	// Update is called once per frame
    bool frameCounter = false;
    float snapcounter = 0;
    int imgCounter = 301;

    private void saveUserScore()
    {
        StreamWriter playerScores;
        if (File.Exists(Application.dataPath + "/" + assetManager.playersScoreFile))
        {
            playerScores = new StreamWriter(Application.dataPath + "/" + assetManager.playersScoreFile, true);
        }
        else
        {
            playerScores = File.CreateText(Application.dataPath + "/" + assetManager.playersScoreFile);
        }
        Debug.Log("New Player Score. Scenario Played: " + mapControler.getScenario());
        playerScores.WriteLine("New Player Score. Scenario Played: " + mapControler.getScenario());
        Debug.Log("Conquare order:");
        playerScores.Write("Conquare order:");
        List<int> playerRegions = mapControler.getPlayer().getConqauredRegions();
        string s = "";
        for (int i = 0; i < playerRegions.Count; i++)
        {
            s += " " + playerRegions[i];
        }
        playerScores.WriteLine(" " + s);
        Debug.Log(s);
        Debug.Log("Number of moves:" + mapControler.getPlayer().getNumberOfMoves());
        playerScores.WriteLine ("Number of moves:" + mapControler.getPlayer().getNumberOfMoves());
        List<float> playerMoves = mapControler.getPlayer().getMoveSTimes();

        playerScores.WriteLine("Pawns used: " + (mapControler.getPlayer().playerMaxPawns() - mapControler.getPlayer().pawnsLeft()));
        Debug.Log("PawnsUsed: " + (mapControler.getPlayer().playerMaxPawns() - mapControler.getPlayer().pawnsLeft()));
        
        playerScores.WriteLine("Pawns left : " + mapControler.getPlayer().pawnsLeft());
        Debug.Log("Pawns left : " + mapControler.getPlayer().pawnsLeft());

        playerScores.Write("Moves times:");
        Debug.Log("Moves times:");
        s = "";
        for (int i = 0; i < playerMoves.Count; i++)
        {
            s += " " + playerMoves[i];
        }
        playerScores.WriteLine(" " + s);
        Debug.Log(s);
        if (scenarioDropDownList.options[scenarioDropDownList.value].text == "2")
        {

            playerScores.WriteLine("Player Score: " + mapControler.getPlayerIncome().ToString());
            Debug.Log("Player Score: " + mapControler.getPlayerIncome().ToString());
        }
        else
        {

            Debug.Log("Player Score: " + mapControler.getPlayerRegionsCount().ToString());
            playerScores.WriteLine("Player Score: " +mapControler.getPlayerRegionsCount().ToString());
        }
        playerScores.WriteLine("Conquared regions: "+mapControler.getPlayer().getConqauredRegions().Count);
            Debug.Log("Conquared regions: " + mapControler.getPlayer().getConqauredRegions().Count);
        playerScores.WriteLine("Gained income: "+mapControler.getPlayer().getIncome());
            Debug.Log("Gained income: " + mapControler.getPlayer().getIncome());
        playerScores.WriteLine();

        playerScores.Close();

    }
    public void addUpdateTimerDueToEvents(float extraTime)
    {
        updatetimer = 3.0f+ extraTime;
    }
	void Update ()
    {
        /*if (index != _lastIndex)
        {
            // split into horizontal and vertical index
            int uIndex = index % _uvTieX;
            int vIndex = index / _uvTieY;

            // build offset
            // v coordinate is the bottom of the image in opengl so we need to invert.
            Vector2 offset = new Vector2(uIndex * _size.x, 1.0f - _size.y - vIndex * _size.y);

            testTexture.material.SetTextureOffset("_MainTex", offset);
            testTexture.material.SetTextureScale("_MainTex", _size);

            _lastIndex = index;
        }*/
        /*if (moviePlayed)
        {
            dllCamera.getFrameAsImage().Save(Application.dataPath+"/../TestReki/img"+imgCounter+".png");
            imgCounter++;
        }*/
        /*showHand();
        textureHolder.texture = assetManager.whiteTexture;
        //Debug.Log(Application.targetFrameRate);
        if (moviePlayed)
        {
            if (snapcounter < 4.0f)
            {
                snapcounter += Time.deltaTime;
            }
            else
            {
                StartCoroutine(saveScreenCorutine());
                snapcounter = 0.0f;

            }
        }*/
                //c.getFrameAsImage();
                //c.getFrameAsImage().Save(Application.dataPath + "/../Frame/EmguCameraImg2.png");
                //textureHolder.texture = assetManager.mapTexture;
                //Debug.Log("changing texture at frame: " + imgCounter);
                //Debug.Log("screen");
            /*}
            
            //cameraInput.SetPixels32(cameraUsed.GetPixels32());
            //cameraInput.Apply();
            //Assets.Scripts.Functions.saveTexture(cameraInput, "cameraInput" + imgCounter, "Frame");
            //assetManager.movieTexture.
           imgCounter++;
        }*/
        /*if (frameCounter)
        {
            textureHolder.texture = assetManager.whiteTexture;
            cameraInput.SetPixels32( cameraUsed.GetPixels32());
            cameraInput.Apply();
            Assets.Scripts.Functions.saveTexture(cameraInput, "cameraInput" + snapcounter, "Frame");
            snapcounter++;
        }
        else
        {
            textureHolder.texture = assetManager.mapTexture;
        }*/
        //frameCounter = !frameCounter;

        #region showCalibrationResults
        if (drawCalibrationPawn) 
        {
            if (drawCalibrationPawnTimer < 6.0f)
            {
                drawCalibrationPawnTimer += Time.deltaTime;
            }
            else
            {
                drawCalibrationPawn = false;
                drawCalibrationPawnTimer = 0;
            }
        }
        if (cornersDetected)
        {
            if (cornersTime < 6.0f)
            {
                cornersTime += Time.deltaTime;
            }
            else
            {
                cornersTime = 0;
                cornersDetected = false;
            }
        }
        #endregion

        //Debug.Log("Update:  " + cameraUsed.didUpdateThisFrame);

        //Debug.Log(PanelManager.getPanelMAnagerSingleton().getPanelsCount());
        /*if (HandTestEnabled)
        {
            cameraInput.SetPixels32(cameraUsed.GetPixels32());
            cameraInput.Apply();
            Image<Rgb, byte> inputImage = Assets.Scripts.Emgu.ImageRecognition.convertTextureToEmguImage(cameraInput);
            if (background != null)
            {
                background.Apply(inputImage, forgroundMask);
                state = Assets.Scripts.Emgu.ImageRecognition.detectMotion(inputImage, motionHistory, forgroundMask);
            }
            else
            {
                background = new BackgroundSubtractorMOG2();
                forgroundMask = new Mat();
                background.Apply(inputImage, forgroundMask);
            }
        }*/

        if (isLiveDetectionEnabled)
        {
            /*if (mapControler.isEventBeingShown())
             {
                 mapControler.progressEventResult(Time.deltaTime);
             }
             else
             {
                 if (mapControler.hadThePresentationOfEventsEndedInLastFrame())
                 {
                     mapControler.showTipsAfterEventsPresentation(uiElementsParent);
                 }
                 else
                 {*/

            mapControler.updateExtraTextures();
            bool thereAreEventsToShow = false;
            if (mapControler.shouldEventbeShowed() && !mapControler.isEventBeingShown())
            {
                //addUpdateTimerDueToEvents(3.0f * mapControler.getEventsCount());
                thereAreEventsToShow = mapControler.proceedToNextEvent();
            }
            if (thereAreEventsToShow || mapControler.isEventBeingShown())
            {
                mapControler.progressEventResult(Time.deltaTime);
            }
            else
            {
                if (mapControler.isScenarioFinished())
                {
                    winText.transform.parent.gameObject.SetActive(!winText.IsActive());
                    if (scenarioDropDownList.options[scenarioDropDownList.value].text == "2")
                    {
                        winText.text = mapControler.getPlayerIncome().ToString();
                    }
                    else
                    {
                        winText.text = mapControler.getPlayerRegionsCount().ToString();
                    }
                    enableLiveDetection();
                    saveUserScore();
                }
                else
                {
                    mapControler.updatePanels(Time.deltaTime);

                    if (!isDebug)
                    {
                        if ((cameraUsed != null && hasCamera) || dllCameraInUse)
                        {
                            /*if (!awaitingCorutineEnd)
                            {
                                if (newDataFromCorutine)
                                {

                                }
                                else
                                {*/
                            if (snapcounter < updatetimer)
                            {
                                snapcounter += Time.deltaTime;
                            }
                            else
                            {
                                Debug.Log("TimeStamp Start:" + Time.deltaTime);
                                StartCoroutine(getNewDllCameraImage(Enums.ImageProcessFunction.detectHand));
                                snapcounter = 0.0f;
                            }
                            /*{
                         }
                         else
                         {

                         }*/


                            /*if (motionDetectionVariant)
                            {
                                cameraInput.SetPixels32(cameraUsed.GetPixels32());
                                cameraInput.Apply();
                                //textureHolder.texture = cameraInput;
                                if (background != null)
                                {
                                    Image<Rgb, byte> inputImage = Assets.Scripts.Emgu.ImageRecognition.convertTextureToEmguImage(cameraInput);
                                    background.Apply(inputImage, forgroundMask);
                                    state = Assets.Scripts.Emgu.ImageRecognition.detectMotion(inputImage, motionHistory, forgroundMask);
                                    //Debug.Log(state);
                                    if (state == Enums.HandDetectionState.CameraClear && didUpdatePawnInfo)
                                    {
                                        //Debug.Log("Camera Clear");
                                        didUpdatePawnInfo = false;
                                    }
                                    if (state == Enums.HandDetectionState.HandInCamera)
                                    {
                                        showHand();

                                        handInCameraTime += Time.deltaTime;
                                        //Debug.Log("Hand in Camera");
                                        if (handInCameraTime > 1.5f)
                                        {


                                            //double matchResult = Assets.Scripts.Emgu.ImageRecognition.findMatchWithEmgu(cameraInput, lastFrameCameraImage);
                                            //double matchResult = Assets.Scripts.Emgu.ImageRecognition.findMatchWithAForge(cameraInput, lastFrameCameraImage);

                                            //double matchResult = 1;
                                            double matchResult = Assets.Scripts.Emgu.ImageRecognition.findMatchWithEmguNoScale(cameraInput, lastFrameCameraImage);
                                            if (matchResult > 0.99f)
                                            {
                                                HideHand();
                                                Debug.Log("Images are the same");
                                                didUpdatePawnInfo = true;
                                                invokeCalibrationFunction(Enums.ImageProcessFunction.detectPawns);

                                                //StartCoroutine(findContoursFunction());

                                            }
                                            else
                                            {
                                                Debug.Log("Images are not the same");
                                                didUpdatePawnInfo = false;
                                            }

                                            lastFrameCameraImage.SetPixels(cameraInput.GetPixels());
                                            lastFrameCameraImage.Apply();
                                        }
                                    }
                                    else
                                    {
                                        if (prevCaptureState == Enums.HandDetectionState.HandInCamera && !didUpdatePawnInfo)
                                        {
                                            HideHand();
                                            didUpdatePawnInfo = true;
                                            //StartCoroutine(findContoursFunction());                            
                                            invokeCalibrationFunction(Enums.ImageProcessFunction.detectPawns);

                                        }
                                    }

                                    prevCaptureState = state;

                                }
                                else
                                {
                                    background = new BackgroundSubtractorMOG2();
                                    forgroundMask = new Mat();
                                    Image<Rgb, byte> inputImage = Assets.Scripts.Emgu.ImageRecognition.convertTextureToEmguImage(cameraInput);
                                    background.Apply(inputImage, forgroundMask);
                                }
                            }
                            else
                            {
                                //Debug.Log(handInCameraTime);
                                cameraInput.SetPixels32(cameraUsed.GetPixels32());
                                cameraInput.Apply();

                                double matchResult = Assets.Scripts.Emgu.ImageRecognition.findMatchWithEmguNoScale(cameraInput, lastFrameCameraImage);
                                bool imagesAreIdentical = matchResult > 0.85f;
                                if (state == Enums.HandDetectionState.CameraClear)
                                {
                                    if (!imagesAreIdentical)
                                    {
                                        handInCameraTime += Time.deltaTime;
                                        Debug.Log("Hand in Camera");
                                        showHand();
                                        state = Enums.HandDetectionState.HandInCamera;
                                    }
                                }
                                else
                                {
                                    if (imagesAreIdentical && handInCameraTime > 1.0f)
                                    {
                                        Debug.Log("Hand Removed");
                                        state = Enums.HandDetectionState.CameraClear;

                                        invokeCalibrationFunction(Enums.ImageProcessFunction.detectPawns);
                                        handInCameraTime = 0;
                                        //StartCoroutine(findContoursFunction(cameraInput));
                                    }
                                    else
                                    {
                                        handInCameraTime += Time.deltaTime;
                                    }
                                }

                                lastFrameCameraImage.SetPixels(cameraInput.GetPixels());
                                lastFrameCameraImage.Apply();
                            }*/

                        }
                    }
                    else
                    {
                        if (assetManager.colorTexture != assetManager.lastColorTexture)
                        {
                            float t = Time.realtimeSinceStartup;
                            //Debug.Log("TimeStamp Start: " + (Time.realtimeSinceStartup-t));
                            assetManager.lastColorTexture = assetManager.colorTexture;
                            List<Pawn> temporaryResults = RectangleDetection.myDetectFromTextureFunction(assetManager.colorTexture, cameraControler, true);
                            //Debug.Log("TimeStamp pawns detected: "+(Time.realtimeSinceStartup-t));
                            temporaryResults = detectPawnsRace(assetManager.colorTexture, temporaryResults);
                            //Debug.Log("TimeStamp races detected: " + (Time.realtimeSinceStartup-t));
                            Debug.Log("PAWNS FOUND: " + temporaryResults.Count);

                            updateMapInfo(temporaryResults);
                            //Debug.Log("TimeStamp map updated: " + (Time.realtimeSinceStartup-t));
                        }
                        if (Input.GetKeyDown(KeyCode.F1))
                        {

                            if (!string.IsNullOrEmpty(AmazonPawnAdder.GetComponent<InputField>().text))
                            {
                                debugRegionPawnList.Add(int.Parse(AmazonPawnAdder.GetComponent<InputField>().text));
                                debugPawnList.Add(new Pawn(Enums.RaceType.Amazon));
                            }
                            if (!string.IsNullOrEmpty(OrcPawnAdder.GetComponent<InputField>().text))
                            {
                                debugRegionPawnList.Add(int.Parse(OrcPawnAdder.GetComponent<InputField>().text));
                                debugPawnList.Add(new Pawn(Enums.RaceType.Orc));
                            }
                            if (!string.IsNullOrEmpty(SkeletonPawnAdder.GetComponent<InputField>().text))
                            {
                                debugRegionPawnList.Add(int.Parse(SkeletonPawnAdder.GetComponent<InputField>().text));
                                debugPawnList.Add(new Pawn(Enums.RaceType.Skeleton));
                            }
                            Debug.Log("Total no of Pawns:" + debugPawnList.Count);
                            //mapControler.printPawns();
                        }
                        if (Input.GetKeyDown(KeyCode.F2))
                        {
                            if (!string.IsNullOrEmpty(AmazonPawnRemover.GetComponent<InputField>().text))
                            {
                                int indexOfRemoved = debugRegionPawnList.IndexOf(int.Parse(AmazonPawnRemover.GetComponent<InputField>().text));
                                debugRegionPawnList.RemoveAt(indexOfRemoved);
                                debugPawnList.RemoveAt(indexOfRemoved);
                            }
                            if (!string.IsNullOrEmpty(OrcPawnRemover.GetComponent<InputField>().text))
                            {
                                int indexOfRemoved = debugRegionPawnList.IndexOf(int.Parse(OrcPawnRemover.GetComponent<InputField>().text));
                                debugRegionPawnList.RemoveAt(indexOfRemoved);
                                debugPawnList.RemoveAt(indexOfRemoved);
                            }
                            if (!string.IsNullOrEmpty(SkeletonPawnRemover.GetComponent<InputField>().text))
                            {
                                int indexOfRemoved = debugRegionPawnList.IndexOf(int.Parse(SkeletonPawnRemover.GetComponent<InputField>().text));
                                debugRegionPawnList.RemoveAt(indexOfRemoved);
                                debugPawnList.RemoveAt(indexOfRemoved);
                            }
                            Debug.Log("Total no of Pawns:" + debugPawnList.Count);
                        }
                        if (Input.GetKeyDown(KeyCode.F3))
                        {
                            Debug.Log("Total no of Pawns:" + debugPawnList.Count);
                            for (int i = 0; i < debugPawnList.Count; i++)
                            {
                                mapControler.addPawnToRegion(debugPawnList[i], uiElementsParent, debugRegionPawnList[i]);
                            }
                            //mapControler.printPawns();
                            mapControler.updateRegionsInfo();
                        }
                    }
                }
            }
            // }
            //}
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                enableLiveDetection();
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                saveScreen();
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                mapControler.finishScenario();
            }
        }
        else
        {
            #region KEYS
            if (Input.GetKeyDown(KeyCode.H))
            {
                menu.SetActive(!menu.activeSelf);
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

            if (Input.GetKeyDown(KeyCode.T))
            {
                cameraInput.SetPixels32(cameraUsed.GetPixels32());
                cameraInput.Apply();

                //SAVE DEFAULT CAMERA IMAGE
                byte[] imageData = cameraInput.EncodeToPNG();
                File.WriteAllBytes(Application.dataPath + "/../Image" + imageCounter + ".png", imageData);
                Debug.Log("Camera image saved");
                imageCounter++;
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                debug.text = "";
                textureHolder.texture = assetManager.mapTexture;
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                saveScreen();
            }
            if (Input.GetKeyDown(KeyCode.V))
            {
                trainHistograms();
            }
            #endregion
            #region DrawLinesInSceneView

            /*if (results.Count > 0)
            {
                //Debug.Log("Number of pavns dravn: " + results.Count);

                results[0].getMinRectArea();
                foreach (Pawn pawn in results)
                {
                    //float colorVal = Random.value * 255;
                    //Debug.Log(colorVal);
                    Color tempColor = new Color(0, 255, 0);

                    //pawn.getMinRectArea();
                    //Debug.Log(pawn.positionOfCentre);
                    //pawn.getConvexHullPoints();]
                    //Debug.Log("Pawn ConvexPoints " + pawn.getConvexHull().getConvexHullPoints().Count);
                    
                    
                    //DRAW CONVEX HULL LINES
                    List<Vector2> pawnPoints = pawn.getConvexHullPoints();
                    
                    for (int j = 0; j < pawnPoints.Count; j++)
                    {
                        //Debug.Log("Convex point " + j + " :" + pawn.getConvexHullPoints()[j]);
                        if (j < pawn.getConvexHullPoints().Count - 1)
                        {
                            Debug.DrawLine(new Vector2(pawnPoints[j].x, Screen.height - pawnPoints[j].y), new Vector2(pawnPoints[j+1].x, Screen.height - pawnPoints[j+1].y), tempColor);
                        }
                        else
                        {
                            Debug.DrawLine(new Vector2(pawnPoints[j].x, Screen.height - pawnPoints[j].y), new Vector2(pawnPoints[0].x, Screen.height - pawnPoints[0].y), tempColor);
                        }
                    }
                   

                    //DRAW CONVEX HULL FIRST LINE AND POINT

                    //Debug.DrawLine(new Vector2(pawnPoints[j].x, Screen.height - pawnPoints[j].y), new Vector2(pawnPoints[j + 1].x, Screen.height - pawnPoints[j + 1].y), tempColor);

                    /*tempColor = new Color(255, 0, 255);
                    Debug.DrawLine(new Vector2(pawnPoints[0].x, Screen.height - pawnPoints[0].y) + new Vector2(-2, -2), new Vector2(pawnPoints[0].x, Screen.height - pawnPoints[0].y) + new Vector2(2, -2),tempColor);
                    Debug.DrawLine(new Vector2(pawnPoints[0].x, Screen.height - pawnPoints[0].y) + new Vector2(2, -2), new Vector2(pawnPoints[0].x, Screen.height - pawnPoints[0].y) + new Vector2(2, 2), tempColor);
                    Debug.DrawLine(new Vector2(pawnPoints[0].x, Screen.height - pawnPoints[0].y) + new Vector2(2, 2), new Vector2(pawnPoints[0].x, Screen.height - pawnPoints[0].y) + new Vector2(-2, 2), tempColor);
                    Debug.DrawLine(new Vector2(pawnPoints[0].x, Screen.height - pawnPoints[0].y) + new Vector2(-2, 2), new Vector2(pawnPoints[0].x, Screen.height - pawnPoints[0].y) + new Vector2(-2, -2), tempColor);
                    */





            /* Vector2[] minrectCornerPoints = pawn.getMinEnclosingRect().getMinRectCorners();

             tempColor = new Color(255, 0, 255);
             Debug.DrawLine(new Vector2(minrectCornerPoints[0].x, Screen.height - minrectCornerPoints[0].y) + new Vector2(-2, -2), new Vector2(minrectCornerPoints[0].x, Screen.height - minrectCornerPoints[0].y) + new Vector2(2, -2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[0].x, Screen.height - minrectCornerPoints[0].y) + new Vector2(2, -2), new Vector2(minrectCornerPoints[0].x, Screen.height - minrectCornerPoints[0].y) + new Vector2(2, 2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[0].x, Screen.height - minrectCornerPoints[0].y) + new Vector2(2, 2), new Vector2(minrectCornerPoints[0].x, Screen.height - minrectCornerPoints[0].y) + new Vector2(-2, 2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[0].x, Screen.height - minrectCornerPoints[0].y) + new Vector2(-2, 2), new Vector2(minrectCornerPoints[0].x, Screen.height - minrectCornerPoints[0].y) + new Vector2(-2, -2), tempColor);


             tempColor = new Color(0, 0, 255);
             Debug.DrawLine(new Vector2(minrectCornerPoints[1].x, Screen.height - minrectCornerPoints[1].y) + new Vector2(-2, -2), new Vector2(minrectCornerPoints[1].x, Screen.height - minrectCornerPoints[1].y) + new Vector2(2, -2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[1].x, Screen.height - minrectCornerPoints[1].y) + new Vector2(2, -2), new Vector2(minrectCornerPoints[1].x, Screen.height - minrectCornerPoints[1].y) + new Vector2(2, 2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[1].x, Screen.height - minrectCornerPoints[1].y) + new Vector2(2, 2), new Vector2(minrectCornerPoints[1].x, Screen.height - minrectCornerPoints[1].y) + new Vector2(-2, 2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[1].x, Screen.height - minrectCornerPoints[1].y) + new Vector2(-2, 2), new Vector2(minrectCornerPoints[1].x, Screen.height - minrectCornerPoints[1].y) + new Vector2(-2, -2), tempColor);

             tempColor = new Color(0, 255, 255);
             Debug.DrawLine(new Vector2(minrectCornerPoints[2].x, Screen.height - minrectCornerPoints[2].y) + new Vector2(-2, -2), new Vector2(minrectCornerPoints[2].x, Screen.height - minrectCornerPoints[2].y) + new Vector2(2, -2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[2].x, Screen.height - minrectCornerPoints[2].y) + new Vector2(2, -2), new Vector2(minrectCornerPoints[2].x, Screen.height - minrectCornerPoints[2].y) + new Vector2(2, 2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[2].x, Screen.height - minrectCornerPoints[2].y) + new Vector2(2, 2), new Vector2(minrectCornerPoints[2].x, Screen.height - minrectCornerPoints[2].y) + new Vector2(-2, 2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[2].x, Screen.height - minrectCornerPoints[2].y) + new Vector2(-2, 2), new Vector2(minrectCornerPoints[2].x, Screen.height - minrectCornerPoints[2].y) + new Vector2(-2, -2), tempColor); 

             tempColor = new Color(255, 255, 00);
             Debug.DrawLine(new Vector2(minrectCornerPoints[3].x, Screen.height - minrectCornerPoints[3].y) + new Vector2(-2, -2), new Vector2(minrectCornerPoints[3].x, Screen.height - minrectCornerPoints[3].y) + new Vector2(2, -2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[3].x, Screen.height - minrectCornerPoints[3].y) + new Vector2(2, -2), new Vector2(minrectCornerPoints[3].x, Screen.height - minrectCornerPoints[3].y) + new Vector2(2, 2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[3].x, Screen.height - minrectCornerPoints[3].y) + new Vector2(2, 2), new Vector2(minrectCornerPoints[3].x, Screen.height - minrectCornerPoints[3].y) + new Vector2(-2, 2), tempColor);
             Debug.DrawLine(new Vector2(minrectCornerPoints[3].x, Screen.height - minrectCornerPoints[3].y) + new Vector2(-2, 2), new Vector2(minrectCornerPoints[3].x, Screen.height - minrectCornerPoints[3].y) + new Vector2(-2, -2), tempColor);
             tempColor = new Color(255, 0, 0);

             for (int k = 0; k < minrectCornerPoints.Length; k++)
             {
                 if (k < minrectCornerPoints.Length - 1)
                 {
                     Debug.DrawLine(new Vector2(minrectCornerPoints[k].x,Screen.height-minrectCornerPoints[k].y), new Vector2(minrectCornerPoints[k + 1].x,Screen.height-minrectCornerPoints[k + 1].y), tempColor);
                 }
                 else
                 {
                     Debug.DrawLine(new Vector2(minrectCornerPoints[k].x, Screen.height - minrectCornerPoints[k].y), new Vector2(minrectCornerPoints[0].x, Screen.height - minrectCornerPoints[0].y), tempColor);

                 }
             }

             /*
                 * Debug.DrawLine(
                 pawn.myBounds.center - pawn.myBounds.extents,
                 pawn.myBounds.center + new Vector3(-pawn.myBounds.extents.x, pawn.myBounds.extents.y, pawn.myBounds.extents.z), tempColor);
         Debug.DrawLine(
             pawn.myBounds.center + new Vector3(-pawn.myBounds.extents.x, pawn.myBounds.extents.y, pawn.myBounds.extents.z),
             pawn.myBounds.center + pawn.myBounds.extents, tempColor);
         Debug.DrawLine(
             pawn.myBounds.center + pawn.myBounds.extents,
             pawn.myBounds.center - new Vector3(pawn.myBounds.extents.x, -pawn.myBounds.extents.y, pawn.myBounds.extents.z), tempColor);
         Debug.DrawLine(
             pawn.myBounds.center - new Vector3(pawn.myBounds.extents.x, -pawn.myBounds.extents.y, pawn.myBounds.extents.z),
             pawn.myBounds.center - pawn.myBounds.extents, tempColor);
                 */
            /*
                }
            }
            if (r.Length > 0)
            {
                Color tempColor = new Color(0, 255, 0);

                Debug.DrawLine(r[0], r[1], tempColor);
                Debug.DrawLine(r[1], r[2], tempColor);
                Debug.DrawLine(r[2], r[3], tempColor);
                Debug.DrawLine(r[3], r[0], tempColor);
            }*/
            #endregion
        }
	}
    public void testScenarioForBestResult()
    {
        if (isDebug)
        {
            mapControler.LookForBestResult(cauntingRegionsInBestResultSearch, desiredScenarioSetup);
        }
        else
        {
            mapControler.LookForBestResult(cauntingRegionsInBestResultSearch);
        }
    }
    public void destroyUIElementsHints()
    {
        foreach (Image comp in uiElementsParent.GetComponentsInChildren<Image>())
        {
            //Debug.Log(comp);
            Destroy(comp.gameObject);
        }
    }
    public void hideUIElements()
    {
        uiElementsParent.SetActive(false);
        extraFParent.SetActive(false);
    }
    public void showUIElements()
    {
        uiElementsParent.SetActive(true);
        extraFParent.SetActive(true);
    }
    public void saveScreen2()
    {
        Assets.Scripts.Functions.saveTestTexture(cameraInput);
    }
    public void saveScreen()
    {
        if (hasCamera)
        {
            invokeCalibrationFunction(Enums.ImageProcessFunction.saveScreen);
        }
        else if(dllCameraInUse)
        {
            invokeCalibrationFunction(Enums.ImageProcessFunction.saveScreen);

        }
        else
        {
            cameraInput.SetPixels(cameraUsed.GetPixels());
            cameraInput.Apply();
            Assets.Scripts.Functions.saveTestTexture(cameraInput);
        }
    }
    public void showGearBox()
    {
        //Debug.Log("showingGear");
        handTexture.gameObject.SetActive(true);
        handTexture.sprite = assetManager.gearbox;
    }
    public void hideGearBox()
    {
        if (handTexture.sprite != assetManager.hand)
        {
            handTexture.gameObject.SetActive(false);
        }
    }
    public void hideMenu()
    {

    }
    public void showMenu()
    {

    }
    public void HideHand()
    {
        Debug.Log("Hiding Hand");
        handTexture.sprite = assetManager.gearbox;
        handTexture.gameObject.SetActive(false);
    }
    public void showHand()
    {
        Debug.Log("Showing Hand");
        handTexture.gameObject.SetActive(true);
        handTexture.sprite = assetManager.hand;
    }
    public void showRegionsTextures()
    {
        Debug.Log("Showing regions textures");
        regionsParent.SetActive(true);
        linesParent.SetActive(true);
    }
    public void hideRegionsTextures()
    {
        Debug.Log("Hiding regions textures");
        regionsParent.SetActive(false);
        linesParent.SetActive(false);
    }
    public void enableLiveDetection(){
        isLiveDetectionEnabled = !isLiveDetectionEnabled;
        Debug.Log("LiveDetection: " + isLiveDetectionEnabled);
        
        menu.SetActive(!isLiveDetectionEnabled);
        if (isLiveDetectionEnabled)
        {
            showRegionsTextures();
            destroyUIElementsHints();
            panelManager.showPanel();
            mapControler.clearHints();
            mapControler.showHintsForNextUser();
            mapControler.startScenario();
        }
        else
        {
            panelManager.hidePanel();
        }
        textureHolder.texture = assetManager.mapTexture;
    }
    public void detectShapes()
    {
        if (!isDebug)
        {
            textureHolder.texture = assetManager.whiteTexture;
            invokeCalibrationFunction(Enums.ImageProcessFunction.detectPawns);
            //StartCoroutine(startLiveFunction(Enums.ImageProcessFunction.detectPawns));
        }
        else
        {
            List<Pawn> temporaryResults = RectangleDetection.myDetectFromTextureFunction(assetManager.colorTexture, cameraControler, true);

            Debug.Log("PAWNS FOUND: " + temporaryResults.Count);
            temporaryResults = detectPawnsRace(assetManager.colorTexture, temporaryResults);
            updateMapInfo( temporaryResults);
            
        }

       
    }
    private void updatePhotoDelayTimeText()
    {
        photoDelayTimeText.text = "Delay: " + photoDelayTime.ToString();
    }
    public void addPhotoDelayTime()
    {
        if (!isDebug)
        {
            photoDelayTime += 0.01f;
            invokeCalibrationFunction(Enums.ImageProcessFunction.testPhotoDelay);
        }
        else
        {
            photoDelayTime += 0.01f;
        }
        updatePhotoDelayTimeText();
    }
    public void substractPhotoDelayTime()
    {
        if (!isDebug)
        {
            photoDelayTime -= 0.01f;
            invokeCalibrationFunction(Enums.ImageProcessFunction.testPhotoDelay);
        }
        else
        {
            photoDelayTime -= 0.01f;
        }
        updatePhotoDelayTimeText();
    }

    public void detectCorners()
    {
         if (!isDebug)
        {
            textureHolder.texture = assetManager.whiteTexture;
             invokeCalibrationFunction(Enums.ImageProcessFunction.detectCorners);
             //StartCoroutine(startLiveFunction(Enums.ImageProcessFunction.detectCorners));
        }
        else
        {
            //RectangleDetection.cameraCalibration(calibTexture);
            cornersDetected = cameraControler.setCameraCorners(RectangleDetection.cameraCalibration(assetManager.calibTexture));
            textureHolder.texture = assetManager.calibTexture;
            //cornersofCameraTexture = RectangleDetection.cameraCalibration(calibTexture);
            //calibrateCorners();
        }

       
    }
    public void test()
    {
        Texture2D t = Assets.Scripts.Usefull.TextureProcessing.scaled(assetManager.raceTexture, 100, 100);
        Assets.Scripts.Functions.saveTexture(t, "safasf", "");
    }
    public void detectPawnSize()
    {
        if (!isDebug)
        {
            invokeCalibrationFunction(Enums.ImageProcessFunction.detectPawnSize);
            //StartCoroutine(startLiveFunction(Enums.ImageProcessFunction.detectPawnSize));
        }
        else
        {
            findPawnSize(assetManager.pawnTexture);
            /*contourSize = RectangleDetection.findContourSize(Assets.Scripts.Usefull.TextureProcessing.cropTextureToSize(assetManager.pawnTexture, CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2), cameraControler, out calibrationPawnAquired);
            Debug.Log("Pavn size: " + contourSize);
            if (contourSize != 0)
            {
                drawCalibrationPawn = true;
                //calculatePawnSizeAreaToDraw(Assets.Scripts.Usefull.TextureProcessing.cropTextureToSize(pawnTexture, CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2));
                textureHolder.texture = assetManager.pawnTexture;
            }*/
        }
     
    }
    public void checkPreStartSetupOfPawns()
    {
        if (mapControler.isScenarioLoaded())
        {
            destroyUIElementsHints();
            if (isDebug)
            {
                for (int i = 0; i < debugPawnList.Count; i++)
                {
                    mapControler.addPawnToRegion(debugPawnList[i], uiElementsParent, debugRegionPawnList[i]);
                }
                debug.text = mapControler.checkRegionsPreStartReadiness().ToString();
            }
            else
            {
                StartCoroutine(getNewDllCameraImage(Enums.ImageProcessFunction.testSetup));
            }
        }
    }
    public void loadScenario()
    {
        if (mapControler.isScenarioLoaded())
        {
            mapControler.clearScenarioInfo();
        }
        if (mapControler.loadMapInfo("mapInfo.data") && mapControler.loadScenarioInfo("scenario0" + scenarioDropDownList.options[scenarioDropDownList.value].text + ".data",out desiredScenarioSetup))
        {
            mapControler.setScenario(int.Parse(scenarioDropDownList.options[scenarioDropDownList.value].text));
            foreach (Region r in desiredScenarioSetup)
            {
                for (int i = 0; i < r.getCurrentNumberOfPawnsInRegion(); i++)
                {
                    debugRegionPawnList.Add(r.getRegionIndex());
                    debugPawnList.Add(new Pawn(r.getCurrentPawn(i).getRaceType()));
                }
            }
            Debug.Log("Total no of Pawns:" + debugPawnList.Count);
            debug.text = "Scenario " + scenarioDropDownList.options[scenarioDropDownList.value].text + " Loaded";
        }
        else
        {
            debug.text = "Loading ERROR";
        }
        //mapControler.showHintsForNextUser();
    }
    public void detectRaces()
    {
        if (!isDebug)
        {
            textureHolder.texture = assetManager.whiteTexture;
            StartCoroutine(findRacesMatch());
        }
        else
        {
            /*
       int templateIndex = 0;
       foreach(Texture2D template in raceTemplateTextures)
       {
           Debug.Log("Mathing with template: " + templateIndex);
           templateIndex++;
           Texture2D temp = raceTexture;
           for (int i = 0; i < 4; i++)
           {
               Assets.Scripts.Emgu.ImageRecognition.findMatchWithEmgu(template, temp);
               temp = Assets.Scripts.Functions.rotateTexture90DegreesLeft(temp);
           }
       }
        * */


            /*
            int templateIndex = 0;
            double matchValue = 0;
            int matchedTemplateIndex = 0;
            Debug.Log("RaceTemplates: "+raceTemplateTextures.Length);
            foreach (Texture2D template in raceTemplateTextures)
            {
                Debug.Log("Mathing with template: " + (Enums.RaceType)templateIndex);
                Texture2D temp = extractedImage;
                for (int i = 0; i < 4; i++)
                {
                    //double matchValueT = Assets.Scripts.Emgu.ImageRecognition.findMatchWithEmgu(template, temp);
                    double matchValueT = Assets.Scripts.Emgu.ImageRecognition.findMatchWithAForge(template, temp);
                    Debug.Log(matchValueT);

                    if (matchValue < matchValueT)
                    {
                        matchedTemplateIndex = templateIndex;
                        matchValue = matchValueT;
                    }
                    temp = Assets.Scripts.Functions.rotateTexture90DegreesLeft(temp);
                }
                templateIndex++;
            }
            Debug.Log("Best Match: "+(Enums.RaceType)matchedTemplateIndex);

            */
            /*if (results.Count > 0)
            {
                Pawn t = results[pawnIndex];

                Vector2 topLeft = t.getMinEnclosingRect().getCorner(0);
                Vector2 topRight = t.getMinEnclosingRect().getCorner(1);
                Vector2 bottomRight = t.getMinEnclosingRect().getCorner(2);
                Vector2 bottomLeft = t.getMinEnclosingRect().getCorner(3);

                //Debug.Log(topLeft + "     " + topRight + "       " + bottomRight + "     " + bottomLeft);

                List<Vector2> top = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, topRight);
                List<Vector2> left = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, bottomLeft);
                List<Vector2> right = Assets.Scripts.Functions.detectPixelsOnLine(topRight, bottomRight);

                //Debug.Log(topLeft+"     "+ topRight+"       "+bottomRight+"     "+bottomLeft);

                Debug.Log(left.Count + "   " + right.Count);

                extractedImage = new Texture2D(top.Count, left.Count);

                pixels = new List<Vector2>();
                int pixelNum = left.Count <= right.Count ? left.Count : right.Count;
                for (int i = 0; i < pixelNum; i++)
                {

                    Vector2 startPixel = left[i];
                    Vector2 endPixel = right[i];

                    List<Vector2> linePixels = Assets.Scripts.Functions.detectPixelsOnLine(startPixel, endPixel);

                    //Debug.Log("Linepixels count: " + linePixels.Count);
                    for (int j = 0; j < linePixels.Count; j++)
                    {
                        pixels.Add(linePixels[j]);
                        /*Debug.Log(linePixels[j]);
                        Debug.Log(colorTexture.GetPixel((int)linePixels[j].x, (int)linePixels[j].y));
                        Debug.Log("");*/
                        //extractedImage.SetPixel(i, j, Assets.Scripts.Functions.getColorAroundPixel(colorTexture, (int)linePixels[j].x, Screen.height - (int)linePixels[j].y));
                        //extractedImage.SetPixel(i, j, assetManager.colorTexture.GetPixel((int)linePixels[j].x, assetManager.colorTexture.height - (int)linePixels[j].y));
                        //extractedImage.Apply();
            /*
                    }
                }

                Assets.Scripts.Functions.saveTexture(extractedImage, "extractedImage", "");

                t.detectPawnRace(cameraInput, raceTemplateTextures);
            }*/
            /*
            int[] sourceHistogram = Assets.Scripts.Usefull.TextureProcessing.getImageHistogram64Colors(extractedImage);
            Texture2D sourceHistogramTexture = Assets.Scripts.Usefull.TextureProcessing.createImageHistogram64ColorTexture(extractedImage);
            Assets.Scripts.Functions.saveTexture(sourceHistogramTexture, "sourceHistogramTexture", "");


            int templateIndex = 0;
            int matchValue = 100000000;
            int matchedTemplateIndex = 0;

            foreach (Texture2D template in raceTemplateTextures)
            {

                Texture2D resized = Assets.Scripts.Usefull.TextureProcessing.scaled(template, extractedImage.width, extractedImage.height);
                //Debug.Log(extractedImage.width + "    " + extractedImage.height);

                int[] templateHistogram = Assets.Scripts.Usefull.TextureProcessing.getImageHistogram64Colors(resized);
                Texture2D templateHistogramTexture = Assets.Scripts.Usefull.TextureProcessing.createImageHistogram64ColorTexture(extractedImage);

                Assets.Scripts.Functions.saveTexture(templateHistogramTexture, "templateHistogramTexture" + templateIndex, "");

                int matchValueT = Assets.Scripts.Usefull.TextureProcessing.compareColorHistograms(sourceHistogram, templateHistogram);

                Debug.Log("match with template: " + templateIndex + "     " + matchValueT);

                if (matchValue > matchValueT)
                {
                    matchedTemplateIndex = templateIndex;
                    matchValue = matchValueT;
                }
                templateIndex++;
            }
            Debug.Log("Best Match: " + (Enums.RaceType)matchedTemplateIndex);

            /*Assets.Scripts.Emgu.ImageRecognition.FindMatch(cameraInput);
            Debug.Log("Pavn size: " + contourSize);*/
        }
        
    }
    
    public void testRotatingCalipers(){

    }
    public void changePawnIndex(){
        Debug.Log(inputField.text);
        pawnIndex = int.Parse(inputField.text);
    }

   /* bool calculatePawnSizeAreaToDraw(Texture2D inputTexture)
    {
        List<Pawn> temp = RectangleDetection.myDetectFromTextureFunction(inputTexture, cameraControler,false);
        Debug.Log("Pawns found: "+temp.Count);
        if (temp.Count == 1)
        {
            calibrationPawn = temp[0];
            Vector2 topLeft = calibrationPawn.getMinEnclosingRect().getCorner(0) - new Vector2(imageExtractionWidening, imageExtractionWidening);
            Vector2 topRight = calibrationPawn.getMinEnclosingRect().getCorner(1) + new Vector2(imageExtractionWidening, -imageExtractionWidening);
            Vector2 bottomRight = calibrationPawn.getMinEnclosingRect().getCorner(2) + new Vector2(imageExtractionWidening, imageExtractionWidening);
            Vector2 bottomLeft = calibrationPawn.getMinEnclosingRect().getCorner(3) + new Vector2(-imageExtractionWidening, imageExtractionWidening);


            List<Vector2> top = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, topRight);
            List<Vector2> left = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, bottomLeft);
            List<Vector2> right = Assets.Scripts.Functions.detectPixelsOnLine(topRight, bottomRight);


            int pixelNum = left.Count <= right.Count ? left.Count : right.Count;
            extractedImage = new Texture2D(top.Count, pixelNum);
            calibrationPixels = new List<Vector2>();
            int pixelsAdded = 0;
            for (int i = 0; i < pixelNum; i++)
            {

                Vector2 startPixel = left[i];
                Vector2 endPixel = right[i];

                List<Vector2> linePixels = Assets.Scripts.Functions.detectPixelsOnLine(startPixel, endPixel);
                
                //Debug.Log("Linepixels count: " + linePixels.Count);
                for (int j = 0; j < linePixels.Count; j++)
                {
                    calibrationPixels.Add(new Vector2(CameraControler.cameraCaptureWidth/4+ linePixels[j].x, CameraControler.cameraCaptureHeight/4+linePixels[j].y));
                    pixelsAdded++; 
                    if (!isDebug)
                    {
                        extractedImage.SetPixel(j, i, cameraInput.GetPixel((int)linePixels[j].x, (int)linePixels[j].y));
                    }
                    else
                    {
                        extractedImage.SetPixel(j, i, assetManager.colorTexture.GetPixel((int)linePixels[j].x, (int)linePixels[j].y));
                    }
                }
                extractedImage.Apply();
            }
            Debug.Log("PxielsAdded: " + pixelsAdded);
            Debug.Log("Texture SAved");
            Assets.Scripts.Functions.saveTexture(extractedImage, "observedTexture", "");

            Debug.Log("Calibration Pawn calculated");
            drawCalibrationPawn = true;
            StreamWriter fileDebug = File.CreateText(Application.dataPath + "/debug.txt");
            foreach (Vector2 p in calibrationPixels)
            {
                fileDebug.WriteLine(p.ToString());
            }
            return true;
        }
        else
        {
            Debug.Log("Calibration Pawn calculation error");
            return false;
        }
    }*/

    public void testRotatingTextured()
    {
        /*pixels = new List<Vector2>();
        if (results.Count > 0)
        {
            Pawn t = results[pawnIndex];

            Vector2 topLeft = t.getMinEnclosingRect().getCorner(0) - new Vector2(imageExtractionWidening, imageExtractionWidening);
            Vector2 topRight = t.getMinEnclosingRect().getCorner(1) + new Vector2(imageExtractionWidening, -imageExtractionWidening);
            Vector2 bottomRight = t.getMinEnclosingRect().getCorner(2) + new Vector2(imageExtractionWidening, imageExtractionWidening);
            Vector2 bottomLeft = t.getMinEnclosingRect().getCorner(3) + new Vector2(-imageExtractionWidening, imageExtractionWidening);

            //Debug.Log(topLeft + "     " + topRight + "       " + bottomRight + "     " + bottomLeft);

            List<Vector2> top = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, topRight);
            List<Vector2> left = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, bottomLeft);
            List<Vector2> right = Assets.Scripts.Functions.detectPixelsOnLine(topRight, bottomRight);


            //Texture2D cutPiece = new Texture2D(colorTexture.width, colorTexture.height);
            Texture2D cutPiece = new Texture2D(assetManager.colorTexture.width, assetManager.colorTexture.height);

            if (!isDebug) 
            {
                cutPiece = new Texture2D(cameraInput.width, cameraInput.height);
                cutPiece.SetPixels32(cameraInput.GetPixels32());
            }
            else
            {
                cutPiece.SetPixels32(assetManager.colorTexture.GetPixels32());
            }

            for (int i = 0; i < top.Count; i++)
            {
                cutPiece.SetPixel((int)top[i].x, (int)top[i].y, Color.black);

                Vector2 tempVector = new Vector2(top[i].x, CameraControler.cameraCaptureHeight - top[i].y);
                cutPiece.SetPixel((int)tempVector.x, (int)tempVector.y, Color.red);

                tempVector = new Vector2(top[i].x, top[i].y);
                cutPiece.SetPixel((int)tempVector.x, (int)tempVector.y, Color.magenta);
            }
            for (int i = 0; i < left.Count; i++)
            {
                cutPiece.SetPixel((int)left[i].x, (int)left[i].y, Color.black);

                Vector2 tempVector = new Vector2(left[i].x, CameraControler.cameraCaptureHeight - left[i].y);
                cutPiece.SetPixel((int)tempVector.x, (int)tempVector.y, Color.red);

                tempVector = new Vector2(left[i].x, left[i].y);
                cutPiece.SetPixel((int)tempVector.x, (int)tempVector.y, Color.magenta);
            }
            for (int i = 0; i < right.Count; i++)
            {
                cutPiece.SetPixel((int)right[i].x, (int)right[i].y, Color.black);

                Vector2 tempVector = new Vector2(right[i].x, CameraControler.cameraCaptureHeight - right[i].y);
                cutPiece.SetPixel((int)tempVector.x, (int)tempVector.y, Color.red);

                tempVector = new Vector2(right[i].x, right[i].y);
                cutPiece.SetPixel((int)tempVector.x, (int)tempVector.y, Color.magenta);
            }
            cutPiece.Apply();

            Assets.Scripts.Functions.saveTexture(cutPiece, "cutPiece", "");

                //Debug.Log(topLeft+"     "+ topRight+"       "+bottomRight+"     "+bottomLeft);

                Debug.Log(left.Count + "   " + right.Count);


            /*for (int z = 0; z < left.Count; z++)
            {
                Debug.Log(left[z] + "    " + right[z]);

            }*/
            /*int pixelNum = left.Count <= right.Count ? left.Count : right.Count;
            extractedImage = new Texture2D(top.Count, pixelNum);

            for (int i = 0; i < pixelNum; i++)
                {

                    Vector2 startPixel = left[i];
                    Vector2 endPixel = right[i];

                    List<Vector2> linePixels = Assets.Scripts.Functions.detectPixelsOnLine(startPixel, endPixel);

                    //Debug.Log("Linepixels count: " + linePixels.Count);
                    for (int j = 0; j < linePixels.Count; j++)
                    {
                        pixels.Add(linePixels[j]);
                        /*Debug.Log(linePixels[j]);
                        Debug.Log(colorTexture.GetPixel((int)linePixels[j].x, (int)linePixels[j].y));
                        Debug.Log("");*/
                        //extractedImage.SetPixel(i, j, Assets.Scripts.Functions.getColorAroundPixel(colorTexture, (int)linePixels[j].x, Screen.height - (int)linePixels[j].y));
                        /*if (!isDebug)
                        {
                            extractedImage.SetPixel(j, i, cameraInput.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y));
                        }
                        else
                        {
                            extractedImage.SetPixel(j, i, assetManager.colorTexture.GetPixel((int)linePixels[j].x, assetManager.colorTexture.height - (int)linePixels[j].y));
                        }
                    }
                    extractedImage.Apply();
                }
            Debug.Log("Texture SAved");
            Assets.Scripts.Functions.saveTexture(extractedImage, "observedTexture", "");
        }
       */
    }
    void OnGUI()
    {
        if (drawCalibrationPawn)
        {
            /*if (isDebug)
            {
                GUI.DrawTexture(new Rect(cameraControler.castPointFromCameraToScreen(calibrationPawnAquired.getPawnPosition()), new Vector2(1, 1)), assetManager.textureTemp);
            }
            else
            {
                GUI.DrawTexture(new Rect(cameraControler.castPositionFromMapToScreen(calibrationPawnAquired.getPawnPosition()), new Vector2(1, 1)), assetManager.textureTemp);
            }*/
            //Debug.Log(calibrationPixels.Count);
            /*foreach (Vector2 pixel in calibrationPixels)
            {
                if (isDebug)
                {
                    GUI.DrawTexture(new Rect(cameraControler.castPointFromCameraToScreen(pixel), new Vector2(1, 1)), assetManager.textureTemp);
                }
                else
                {
                    GUI.DrawTexture(new Rect(cameraControler.castPositionFromMapToScreen(pixel), new Vector2(1, 1)), assetManager.textureTemp);
                }
            }*/
        }
        if (cornersDetected)
        {
            //Debug.Log("PrintedCorners: " + cornersofCameraTexture.Count);
            List<Vector2> cornersTemp = cameraControler.getCorners();
            for (int i = 0; i < cornersTemp.Count; i++)
            {
                if (i < 2)
                {
                        GUI.DrawTexture(new Rect(cameraControler.castPointFromCameraToScreen(cornersTemp[i]), new Vector2(5, 5)), assetManager.textureTemp);
                }
                else
                {
                   
                        GUI.DrawTexture(new Rect(cameraControler.castPointFromCameraToScreen(cornersTemp[i]), new Vector2(15, 15)), assetManager.textureTemp);
                }
                //Debug.Log("Casting Point at: " +cornersTemp[i]+"    to:  "+ cameraControler.castPointFromCameraToScreen(cornersTemp[i]));
            }
        }
        if (pixels.Count > 0)
        {
            //Debug.Log("Pixels dravn: " + pixels.Count);
            foreach (Vector2 pixel in pixels)
            {
                if (isDebug)
                {
                    GUI.DrawTexture(new Rect(cameraControler.castPointFromCameraToScreen(pixel), new Vector2(1, 1)), assetManager.textureTemp);
                }
                else
                {
                    GUI.DrawTexture(new Rect(pixel, new Vector2(1, 1)), assetManager.textureTemp);
                }
            }
        }
        /*if (results.Count > 0)
        {
            //Debug.Log("Number of pavns dravn: " + results.Count);
            int w=0;
            foreach (Pawn pawn in results)
            {
                Rect temp2;
                if (isDebug)
                {
                    temp2 = new Rect(cameraControler.castPointFromCameraToScreen(pawn.getPawnPosition()), new Vector2(10, 10));
                }
                else
                {
                    temp2 = new Rect(pawn.getPawnPosition(), new Vector2(10, 10));
                }
                GUI.Label(temp2, ""+w);
                if (pawn.getPawnState() == Enums.PawnState.acceptedNew)
                {
                    GUI.DrawTexture(temp2, assetManager.tickTexture);
                }
                else if (pawn.getPawnState() == Enums.PawnState.toRemove)
                {
                    GUI.DrawTexture(temp2, assetManager.errorTexture);
                }
                /*if (pawn.getRaceType() == Enums.RaceType.Amazon)
                {
                    GUI.DrawTexture(temp2, textureTemp);
                }
                else if (pawn.getRaceType() == Enums.RaceType.Orc)
                {
                    GUI.DrawTexture(temp2, textureTemp2);
                }*/

                /*w++;
            }
        }*/
    }
    public void saveCalibrationConfig()
    {

        using (StreamWriter sw = File.CreateText(Application.dataPath + "/" + calibrationFilePath))
        {

            sw.WriteLine(RectangleDetection.contourAreaMax);
            sw.WriteLine(RectangleDetection.contourAreaMin);
            sw.WriteLine(RectangleDetection.colorRange);
            sw.WriteLine(cameraControler.CornersToStringFormat());
            Debug.Log(templateHistograms.Length);
            for (int i = 0; i < templateHistograms.Length; i++)
            {
                sw.WriteLine(templateHistograms[i].HistogramToString());
                Debug.Log("Saved histogram: " + i);
            }
            sw.Close();
        }
        Debug.Log("Calib Config Saved");
        debug.text = "Calib Config Saved";
    }

    public void loadCalibrationConfig()
    {
        if (File.Exists(Application.dataPath + "/" + calibrationFilePath))
        {
            string[] fileLines = File.ReadAllLines(Application.dataPath + "/" + calibrationFilePath);

            RectangleDetection.contourAreaMax = float.Parse(fileLines[0]);
            RectangleDetection.contourAreaMin = float.Parse(fileLines[1]);
            RectangleDetection.colorRange = int.Parse(fileLines[2]);
            cameraControler.CornersFromStringFormat(fileLines[3]);
            templateHistograms = new ColorHistogram[fileLines.Length - 4];
            for (int i = 4; i < fileLines.Length; i++)
            {
                templateHistograms[i - 4] = new ColorHistogram(fileLines[i]);
                Debug.Log("loaded histogram: " + (i - 3));
            }

            numberOfSetsTrainedHistograms = 3;
            debug.text = "Calib Config Loaded";
            Debug.Log("Calib Config Loaded");
        }
        else
        {
            debug.text = "Calib Config Loading Error";
            Debug.Log("Calib Config Loading Error");
        }

    }
    public void trainHistograms()
    {
        debug.text = "";
        if (!isDebug)
        {
            cameraInput.SetPixels32(cameraUsed.GetPixels32());
            cameraInput.Apply();
            List<Pawn> pawns = RectangleDetection.myDetectFromTextureFunction(cameraInput, cameraControler, true);
            if (pawns.Count > 0)
            {
                pawns[0].cutPawnFromImage(cameraInput);
            }
            else
            {
                Debug.Log("Error");
            }
            //templateHistograms[trainCounter] = new ColorHistogram(pawn.cutPawnFromImage(cameraInput, cameraControler));
        }
        else
        {
            List<Pawn> pawns = RectangleDetection.myDetectFromTextureFunction(assetManager.colorTexture, cameraControler, true);
            if (pawns.Count > 0)
            {
                pawns[0].cutPawnFromImage(assetManager.colorTexture);
            }
            else
            {
                Debug.Log("Error");
            }
            //templateHistograms[trainCounter] = new ColorHistogram(pawn.cutPawnFromImage(colorTexture, cameraControler));
        }
        trainCounter++;

        debug.text = "Done";
    }
    void prepareCalibTexture(Enums.ImageProcessFunction newCalibFunction)
    {
        isCalibTextureReady = true;

        if (newCalibFunction == Enums.ImageProcessFunction.detectPawnSize)
        {
            textureHolder.texture = assetManager.calibrationTextures[1];
        }
        else if (newCalibFunction == Enums.ImageProcessFunction.trainPatterns || newCalibFunction == Enums.ImageProcessFunction.detectTreshold || newCalibFunction == Enums.ImageProcessFunction.manualTreshold)
        {
            textureHolder.texture = assetManager.calibrationTextures[2];
        }
    }
    void invokeCalibrationFunction(Enums.ImageProcessFunction newCalibFunction)
    {
        if (isCalibTextureReady ||
            (   newCalibFunction != Enums.ImageProcessFunction.detectPawnSize &&
                newCalibFunction != Enums.ImageProcessFunction.trainPatterns &&
                newCalibFunction != Enums.ImageProcessFunction.detectTreshold &&
                newCalibFunction != Enums.ImageProcessFunction.manualTreshold
            )
           )
        {
            StartCoroutine(getNewDllCameraImage(newCalibFunction));
        }
        else
        {
            prepareCalibTexture(newCalibFunction);
        }
    }
    /*void processDataFromDLLCamera(Enums.ImageProcessFunction functionToStart) 
    {
        if (functionToStart == Enums.ImageProcessFunction.detectCorners)
            {
                List<Vector2> tempCorners = RectangleDetection.cameraCalibration(Image1);
                completedSucessfuly = cornersDetected = cameraControler.setCameraCorners(tempCorners);
                debug.text = tempCorners.Count.ToString();
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectPawns)
            {
                findContoursFunction(Image1);
                CvInvoke.CvtColor(Image1, assetManager.whiteBackgroundEmguImage, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectPawnSize)
            {
                completedSucessfuly = findPawnSize(Image1);
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectTreshold)
            {
                completedSucessfuly = Assets.RectangleDetection.searchForDetectionTreshold(Image1, calirationStep, (int)racesTemplatesPerDetection, cameraControler);
                debug.text = RectangleDetection.colorRange.ToString();
            }
            else if (functionToStart == Enums.ImageProcessFunction.trainPatterns)
            {
                completedSucessfuly = findRacesPatterns(Image1);
            }
            else if (functionToStart == Enums.ImageProcessFunction.manualTreshold)
            {
                textureHolder.texture =  Assets.RectangleDetection.detectTreshold(Image1, (int)slider.value);
                List<Pawn> tempResults = RectangleDetection.myDetectFromTextureFunction(Image1, cameraControler, true, false);
                debug.text = RectangleDetection.colorRange.ToString();
                if (tempResults.Count != (int)racesTemplatesPerDetection)
                {
                    completedSucessfuly = false;
                }
            }
            else if (functionToStart == Enums.ImageProcessFunction.saveScreen)
            {
                Image1.Save(Application.dataPath + "/" + "image" + imgCounter + ".png");
                imgCounter++;
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectHand)
            {
                Enums.HandDetectionState currentState = Enums.HandDetectionState.CameraClear;
                
                bool handDetection1 = dllCamera.lookForHandBooleanFromImage(Image1);
                bool handDetection2 = dllCamera.lookForHandBooleanFromImage(Image2);

                Debug.Log(handDetection1 + "   " + handDetection2);

                /*if (handDetection1 && handDetection2)
                {
                    currentState = Enums.HandDetectionState.HandInCamera;
                    Debug.Log("HandInCamera");
                    showHand();
                }
                else
                {

                    if (prevCaptureState == Enums.HandDetectionState.HandInCamera)
                    {
                        Debug.Log("HandRemoved");
                        destroyUIElementsHints();
                        findContoursFunction(Image1);
                        prevCaptureState = Enums.HandDetectionState.CameraClear;

                        //CvInvoke.CvtColor(Image1, assetManager.whiteBackgroundEmguImage, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
                    }
                    else
                    {
                        Debug.Log("Checking for pawn difference:");
                        //JEZELI TERAZ NIE MA REKI I POPRZEDNIO TEZ NIE BYLO TO SPRAWDZ CZY ZMIENIL SIE UKLAD PIONKOW NA STOLE
                        List<Pawn> temporaryResults = RectangleDetection.myDetectFromTextureFunction(Image1, cameraControler, true, true, assetManager.whiteBackgroundEmguImage);
                        List<Pawn> temporaryResults2 = RectangleDetection.myDetectFromTextureFunction(Image2, cameraControler, true, true, assetManager.whiteBackgroundEmguImage);
                        if (temporaryResults.Count == temporaryResults2.Count)
                        {
                            if (numberOfPawnsInLastFrame == 0 && temporaryResults.Count == 0)
                            {

                            }
                            else
                            {
                                Texture2D tempConvertedTextue = RectangleDetection.convertImageToTexture(Image1);
                                temporaryResults = detectPawnsRace(tempConvertedTextue, temporaryResults);
                                if (numberOfPawnsInLastFrame == temporaryResults.Count)
                                {
                                    Debug.Log("Same number of pawns checking for difference in races");

                                    //IF RODZAJ PIONKOW ULEGL ZMIANIE TO ZROBU UPDATE MAPY
                                    int[] pawnsInCurrentFrame = calculatePawnDistribution(temporaryResults);
                                    if (!areTheSamePawnsInCurrentFrame(pawnsInCurrentFrame))
                                    {
                                        Debug.Log("Change in races detected");
                                        destroyUIElementsHints();
                                        updateMapInfo(temporaryResults);
                                    } 
                                    Debug.Log("No Change in races detected");
                                }
                                else
                                {
                                    Debug.Log("Different number of pawns");
                                    destroyUIElementsHints();
                                    updateMapInfo(temporaryResults);
                                    //CvInvoke.CvtColor(Image1, assetManager.whiteBackgroundEmguImage, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
                                }
                            }
                        }
                    //}
                }
                prevCaptureState = currentState;
                Debug.Log("Setting cameraState to: " + prevCaptureState);

            }
            else if (functionToStart == Enums.ImageProcessFunction.testTreshold)
            {
                textureHolder.texture = Assets.RectangleDetection.detectTreshold(Image1, (int)slider.value);
            }
            if (!completedSucessfuly)
            {
                textureHolder.texture = assetManager.errorCalibTexture;
            }
            else
            {
                if (functionToStart != Enums.ImageProcessFunction.manualTreshold && functionToStart != Enums.ImageProcessFunction.testTreshold)
                {
                    if (
                        functionToStart == Enums.ImageProcessFunction.detectCorners || functionToStart == Enums.ImageProcessFunction.detectPawnSize ||
                        functionToStart == Enums.ImageProcessFunction.trainPatterns || functionToStart == Enums.ImageProcessFunction.detectTreshold
                        )
                    {
                        
                        Debug.Log("Converting Image; image length: "+Image1.Bytes);
                        Texture2D tempConvertedTextue = RectangleDetection.convertImageToTexture(Image1);
                        textureHolder.texture = tempConvertedTextue;
                    }
                    else
                    {
                        textureHolder.texture = assetManager.mapTexture;
                    }
                }
            } 
    }*/
    void processDataFromUnityCamera() { }

    IEnumerator getNewDllCameraImage(Enums.ImageProcessFunction newCalibFunction)
    {
        debug.text = "";
        isCalibTextureReady = false;
        HideHand();
        menu.SetActive(false);
        //regionsParent.SetActive(false);
        hideRegionsTextures();
        Cursor.visible = false;
        bool panelVasVisible = false;
        if (panelManager.isVisible())
        {
            panelVasVisible = true;
            panelManager.hidePanel();
        }
        //destroyUIElementsHints();
        hideUIElements();
        //SET TEXTURE TO WHITE
        textureHolder.texture = assetManager.whiteTexture;


        if (!dllCameraInUse)
        {
            #region UnityCameraUsage

            yield return new WaitForSeconds(photoDelayTime);


            //GET NEW CAMERA IMAGE
            cameraInput.SetPixels32(cameraUsed.GetPixels32());
            cameraInput.Apply();


            #endregion
        }
        else
        {
            #region DllCameraUsage
            yield return new WaitForSeconds(photoDelayTime);
            float timeStamp = Time.realtimeSinceStartup;

            //GET NEW CAMERA IMAGE
            Image2 = dllCamera.getFrameAsImage();

            yield return new WaitForFixedUpdate();
            Image1 = dllCamera.getFrameAsImage();

            //Debug.Log("Take photo time: " + (Time.realtimeSinceStartup - timeStamp)); 
            timeStamp = Time.realtimeSinceStartup;
            Image1.Save("Img1.png");
            Image2.Save("Img2.png");

            //Debug.Log("Save photo time: " + (Time.realtimeSinceStartup - timeStamp));

        }
        //RESET MAP TEXTURE
        textureHolder.texture = assetManager.mapTexture;
        showGearBox();
        if (isLiveDetectionEnabled)
        {
            showUIElements();
            //regionsParent.SetActive(true);
            showRegionsTextures();
        }
        if (panelVasVisible)
        {
            panelManager.showPanel();
        }

        if (!isLiveDetectionEnabled && !menu.activeSelf)
        {
            menu.SetActive(true);
        }
        StartCoroutine(startImageProcessingFunction(newCalibFunction));
    }
    IEnumerator startImageProcessingFunction(Enums.ImageProcessFunction newCalibFunction)
    {


        yield return new WaitForFixedUpdate();
        Enums.ImageProcessFunction functionToStart = newCalibFunction;
        bool completedSucessfuly = true;

        if (!dllCameraInUse)
        {
            #region unityCamera
            if (functionToStart == Enums.ImageProcessFunction.detectCorners)
            {
                List<Vector2> tempCorners = RectangleDetection.cameraCalibration(cameraInput);
                completedSucessfuly = cornersDetected = cameraControler.setCameraCorners(tempCorners);
                debug.text = tempCorners.Count.ToString();
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectPawns)
            {
                findContoursFunction(cameraInput);
                assetManager.whiteBackground = Assets.Scripts.Usefull.TextureProcessing.convertToGreyscale(cameraInput);
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectPawnSize)
            {
                completedSucessfuly = findPawnSize(cameraInput);
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectTreshold)
            {
                completedSucessfuly = Assets.RectangleDetection.searchForDetectionTreshold(cameraInput, calirationStep, (int)racesTemplatesPerDetection, cameraControler);
                debug.text = RectangleDetection.colorRange.ToString();
            }
            else if (functionToStart == Enums.ImageProcessFunction.trainPatterns)
            {
                //completedSucessfuly = findRacesPatterns(cameraInput);
            }
            else if (functionToStart == Enums.ImageProcessFunction.manualTreshold)
            {
                textureHolder.texture = Assets.RectangleDetection.detectTreshold(cameraInput, (int)slider.value);
                List<Pawn> tempResults = RectangleDetection.myDetectFromTextureFunction(cameraInput, cameraControler, true, false);
                debug.text = RectangleDetection.colorRange.ToString();
                if (tempResults.Count != (int)racesTemplatesPerDetection)
                {
                    completedSucessfuly = false;
                }
            }
            else if (functionToStart == Enums.ImageProcessFunction.saveScreen)
            {
                saveScreen2();
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectHand)
            {
                processHandDetection(RectangleDetection.detectHandInCamera(cameraInput, assetManager.backgroundImageCaptured));

            }
            else if (functionToStart == Enums.ImageProcessFunction.testTreshold)
            {
                textureHolder.texture = Assets.RectangleDetection.detectTreshold(cameraInput, (int)slider.value);
            }
            if (!completedSucessfuly)
            {
                textureHolder.texture = assetManager.errorCalibTexture;
            }
            else
            {
                if (functionToStart != Enums.ImageProcessFunction.manualTreshold && functionToStart != Enums.ImageProcessFunction.testTreshold)
                {
                    if (
                        functionToStart == Enums.ImageProcessFunction.detectCorners || functionToStart == Enums.ImageProcessFunction.detectPawnSize ||
                        functionToStart == Enums.ImageProcessFunction.trainPatterns || functionToStart == Enums.ImageProcessFunction.detectTreshold
                        )
                    {
                        textureHolder.texture = cameraInput;
                    }
                    else
                    {
                        textureHolder.texture = assetManager.mapTexture;
                    }
                }
            }
            #endregion
        }
        else{

            if (functionToStart == Enums.ImageProcessFunction.testPhotoDelay)
            {
                Texture2D tempConvertedTextue = RectangleDetection.convertImageToTexture(Image2);
                textureHolder.texture = tempConvertedTextue;
            }
            else if (functionToStart == Enums.ImageProcessFunction.testSetup)
            {
                if (isDebug)
                {

                }
                else
                {


                    float timeStamp = Time.realtimeSinceStartup;

                    List<Pawn> temporaryResults;
                    RectangleDetection.myDetectFromTextureFunction(Image1, cameraControler, true, out temporaryResults, true, false, "", assetManager.whiteBackgroundEmguImage);

                    //Debug.Log("Pawns detection time: " + (Time.realtimeSinceStartup - timeStamp));
                    //Debug.Log("PAWNS FOUND: " + temporaryResults.Count);
                    timeStamp = Time.realtimeSinceStartup;

                    temporaryResults = detectPawnsRace(Image1, temporaryResults);

                    //Debug.Log("Race detection2 time: " + (Time.realtimeSinceStartup - timeStamp));
                    timeStamp = Time.realtimeSinceStartup;

                    //updateMapInfo(temporaryResults);

                    foreach (Pawn p in temporaryResults)
                    {
                        mapControler.addPawnToRegion(p, uiElementsParent);
                    }

                    bool testResult = mapControler.checkRegionsPreStartReadiness();
                    debug.text = testResult.ToString();
                }
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectCorners)
            {
                List<Vector2> tempCorners = RectangleDetection.cameraCalibration(Image1);
                completedSucessfuly = cornersDetected = cameraControler.setCameraCorners(tempCorners);
                if (completedSucessfuly)
                {
                    cornersDetected = true;
                }
                debug.text = tempCorners.Count.ToString();
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectPawns)
            {
                findContoursFunction(Image1);
                //CvInvoke.CvtColor(Image1, assetManager.whiteBackgroundEmguImage, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectPawnSize)
            {
                completedSucessfuly = findPawnSize(Image1);
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectTreshold)
            {
                completedSucessfuly = Assets.RectangleDetection.searchForDetectionTreshold(Image1, calirationStep, (int)racesTemplatesPerDetection, cameraControler);
                debug.text = RectangleDetection.colorRange.ToString();
            }
            else if (functionToStart == Enums.ImageProcessFunction.trainPatterns)
            {
                completedSucessfuly = findRacesPatterns(Image1);
            }
            else if (functionToStart == Enums.ImageProcessFunction.manualTreshold)
            {
                textureHolder.texture =  Assets.RectangleDetection.detectTreshold(Image1, (int)slider.value);
                List<Pawn> tempResults;
                RectangleDetection.myDetectFromTextureFunction(Image1, cameraControler, true, out tempResults, false);
                debug.text = RectangleDetection.colorRange.ToString();
                if (tempResults.Count != (int)racesTemplatesPerDetection)
                {
                    completedSucessfuly = false;
                }
            }
            else if (functionToStart == Enums.ImageProcessFunction.saveScreen)
            {
                Image1.Save(Application.dataPath + "/" + "image" + imgCounter + ".png");
                imgCounter++;
            }
            else if (functionToStart == Enums.ImageProcessFunction.detectHand)
            {
                #region pawnUpdateByHandDetection
                float timeStamp = Time.realtimeSinceStartup;

                bool handDetection1 = dllCamera.lookForHandBooleanFromImage(Image1);
                //bool handDetection1 = false;
                //bool handDetection2 = false;
                bool handDetection2 = dllCamera.lookForHandBooleanFromImage(Image2);

                Debug.Log("Hand detection time: " + (Time.realtimeSinceStartup - timeStamp));
                //Debug.Log(handDetection1 + "   " + handDetection2);

                if (handDetection1 || handDetection2)
                {
                    Debug.Log("HandInCamera");
                    showHand();
                }
                else
                {

                    /*if (prevCaptureState == Enums.HandDetectionState.HandInCamera)
                    {
                        //Debug.Log("HandRemoved");
                        destroyUIElementsHints();
                        findContoursFunction(Image1);
                        prevCaptureState = Enums.HandDetectionState.CameraClear;

                        //CvInvoke.CvtColor(Image1, assetManager.whiteBackgroundEmguImage, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
                    }
                    else
                    {*/
                    //Debug.Log("Checking for pawn difference:");
                    //JEZELI TERAZ NIE MA REKI I POPRZEDNIO TEZ NIE BYLO TO SPRAWDZ CZY ZMIENIL SIE UKLAD PIONKOW NA STOLE
                    timeStamp = Time.realtimeSinceStartup;
                    List<Pawn> temporaryResults;
                    bool bigObjectDetected = RectangleDetection.myDetectFromTextureFunction(Image1, cameraControler, true, out temporaryResults, true,false,"1", assetManager.whiteBackgroundEmguImage);

                    Debug.Log("Pawn detection1 time: " + (Time.realtimeSinceStartup - timeStamp));

                    timeStamp = Time.realtimeSinceStartup;
                    List<Pawn> temporaryResults2;
                    bool bigObjectDetected2 = RectangleDetection.myDetectFromTextureFunction(Image2, cameraControler, true, out temporaryResults2, true,false,"2", assetManager.whiteBackgroundEmguImage);
                    Debug.Log("Pawn detection2 time: " + (Time.realtimeSinceStartup - timeStamp));
                    if (!bigObjectDetected || !bigObjectDetected2 || Mathf.Abs(temporaryResults.Count - temporaryResults2.Count) > 1 ) 
                    {
                        Debug.Log("Big object detected");
                        showHand();
                    }
                    else
                    {
                        /*if (temporaryResults.Count == temporaryResults2.Count)
                        {
                            timeStamp = Time.realtimeSinceStartup;

                            temporaryResults = detectPawnsRace(Image1, temporaryResults);
                            Debug.Log("Race detection time: " + (Time.realtimeSinceStartup - timeStamp));
                            destroyUIElementsHints();
                            timeStamp = Time.realtimeSinceStartup;
                            updateMapInfo(temporaryResults);
                            Debug.Log("Map update time: " + (Time.realtimeSinceStartup - timeStamp));
                        }
                        else if (Mathf.Abs(temporaryResults.Count - temporaryResults2.Count) == 1)
                        {*/
                            timeStamp = Time.realtimeSinceStartup;
                            /*Texture2D tempConvertedTextue = RectangleDetection.convertImageToTexture(Image1);
                            Debug.Log("ImageConvertion time: " + (Time.realtimeSinceStartup - timeStamp));
                            timeStamp = Time.realtimeSinceStartup;*/

                            temporaryResults = temporaryResults.Count > temporaryResults2.Count ? detectPawnsRace(Image1, temporaryResults) : detectPawnsRace(Image2, temporaryResults2);
                            Debug.Log("Race detection time: " + (Time.realtimeSinceStartup - timeStamp));

                            /*if (numberOfPawnsInLastFrame == temporaryResults.Count)
                            {
                                Debug.Log("Same number of pawns checking for difference in races");

                                //IF RODZAJ PIONKOW ULEGL ZMIANIE TO ZROBU UPDATE MAPY
                                int[] pawnsInCurrentFrame = calculatePawnDistribution(temporaryResults);
                                if (!areTheSamePawnsInCurrentFrame(pawnsInCurrentFrame))
                                {
                                    Debug.Log("Change in races detected");
                                    timeStamp = Time.realtimeSinceStartup;
                                    destroyUIElementsHints();
                                    updateMapInfo(temporaryResults);
                                    Debug.Log("Map update time: " + (Time.realtimeSinceStartup - timeStamp));
                                } 
                                Debug.Log("No Change in races detected");
                            }
                            else
                            {*/
                            //Debug.Log("Different number of pawns");
                            destroyUIElementsHints();
                            timeStamp = Time.realtimeSinceStartup;
                            updateMapInfo(temporaryResults);
                            Debug.Log("Map update time: " + (Time.realtimeSinceStartup - timeStamp));
                        /*}
                        else
                        { 
                            Debug.Log("Diff Number of pawns");
                            showHand();
                        }*/
                    }
                }
                //Debug.Log("Setting cameraState to: " + prevCaptureState);
                #endregion
            }
            else if (functionToStart == Enums.ImageProcessFunction.testTreshold)
            {
                textureHolder.texture = Assets.RectangleDetection.detectTreshold(Image1, (int)slider.value);
            }
            if (!completedSucessfuly)
            {
                textureHolder.texture = assetManager.errorCalibTexture;
            }
            else
            {
                if (functionToStart != Enums.ImageProcessFunction.manualTreshold && functionToStart != Enums.ImageProcessFunction.testTreshold && functionToStart != Enums.ImageProcessFunction.testPhotoDelay)
                {
                    if (
                        functionToStart == Enums.ImageProcessFunction.detectCorners || functionToStart == Enums.ImageProcessFunction.detectPawnSize ||
                        functionToStart == Enums.ImageProcessFunction.trainPatterns || functionToStart == Enums.ImageProcessFunction.detectTreshold
                        )
                    {
                        
                        Debug.Log("Converting Image; image length: "+Image1.Bytes);
                        Texture2D tempConvertedTextue = RectangleDetection.convertImageToTexture(Image1);
                        //textureHolder.texture = tempConvertedTextue;
                    }
                    else
                    {
                        textureHolder.texture = assetManager.mapTexture;
                    }
                }
            }
            #endregion
        }
            //textureHolder.texture = mapTexture;
       
        hideGearBox();
        showUIElements();
        Cursor.visible = true;
        
    }
    public void testTreshold()
    {
        StartCoroutine(getNewDllCameraImage(Enums.ImageProcessFunction.testTreshold));
    }
    bool areTheSamePawnsInCurrentFrame(int[] pawnsInCurrentFrame)
    {
        for (int i = 0; i < pawnsInCurrentFrame.Length; i++)
        {
            if (pawnsInCurrentFrame[i] != pawnsDistributionInLastUpdate[i])
            {
                return false;
            }
        }
        return true;
    }
    void processHandDetection(Enums.HandDetectionState currentState)
    {
        Debug.Log(currentState + "    " + prevCaptureState);
        if (currentState != Enums.HandDetectionState.HandInCamera && prevCaptureState != Enums.HandDetectionState.CameraClear)
        {
            invokeCalibrationFunction(Enums.ImageProcessFunction.detectPawns);
        }
        else
        {
            //JEZELI TERAZ NIE MA REKI I POPRZEDNIO TEZ NIE BYLO TO SPRAWDZ CZY ZMIENIL SIE UKLAD PIONKOW NA STOLE
        }
        prevCaptureState = currentState;
    }

    bool findPawnSize(Texture2D inputTexture)
    {

        //SAVE DEFAULT CAMERA IMAGE
        RectangleDetection.findContourSize(Assets.Scripts.Usefull.TextureProcessing.cropTextureToSize(inputTexture, CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2), cameraControler, out calibrationPawnAquired);
        contourSize = RectangleDetection.contourAreaMax;
        Debug.Log("Pawn size set: " + contourSize);
        debug.text = contourSize.ToString();
        if (contourSize != 0)
        {
            drawCalibrationPawn = true;
            textureHolder.texture = inputTexture;
            return true;
            //return calculatePawnSizeAreaToDraw(Assets.Scripts.Usefull.TextureProcessing.cropTextureToSize(inputTexture, CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2));
        }
        return false;
    }
    bool findPawnSize(Image<Bgr,byte> inputTexture)
    {

        //SAVE DEFAULT CAMERA IMAGE 
        Pawn p = RectangleDetection.findContourSize(inputTexture, cameraControler, out calibrationPawnAquired);
        
        if (p != null)
        {
            //p.castPawnToScreen(cameraControler);
            contourSize += RectangleDetection.contourAreaMax;
            contourSize /= 2;
            Debug.Log("Pawn size set: " + contourSize);
            //Debug.Log("Pawn size set: " + contourSize / 2);
            debug.text = contourSize.ToString();

            MapControler.showPawnHint(p, assetManager.UIElementPrefab, uiElementsParent, assetManager.whiteSprite);
            MapControler.showPawnHint(p, assetManager.UIElementPrefab, uiElementsParent, assetManager.blueSprite,true);
            drawCalibrationPawn = true;
            //textureHolder.texture = inputTexture;
            return true;
            //return calculatePawnSizeAreaToDraw(Assets.Scripts.Usefull.TextureProcessing.cropTextureToSize(inputTexture, CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2));
        }
        return false;
    }
    IEnumerator findRacesMatch()
    {
        yield return new WaitForSeconds(photoDelayTime);

        //GET NEW CAMERA IMAGE
        cameraInput.SetPixels32(cameraUsed.GetPixels32());
        cameraInput.Apply();

        //textureHolder.texture = cameraInput;




        /*if (results.Count > 0)
        {
            Pawn t = results[pawnIndex];

            Vector2 topLeft = t.getMinEnclosingRect().getCorner(0);
            Vector2 topRight = t.getMinEnclosingRect().getCorner(1);
            Vector2 bottomRight = t.getMinEnclosingRect().getCorner(2);
            Vector2 bottomLeft = t.getMinEnclosingRect().getCorner(3);

            //Debug.Log(topLeft + "     " + topRight + "       " + bottomRight + "     " + bottomLeft);

            List<Vector2> top = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, topRight);
            List<Vector2> left = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, bottomLeft);
            List<Vector2> right = Assets.Scripts.Functions.detectPixelsOnLine(topRight, bottomRight);

            //Debug.Log(topLeft+"     "+ topRight+"       "+bottomRight+"     "+bottomLeft);

            Debug.Log(left.Count + "   " + right.Count);

            extractedImage = new Texture2D(top.Count, left.Count);

            pixels = new List<Vector2>();
            int pixelNum = left.Count <= right.Count ? left.Count : right.Count;
            for (int i = 0; i < pixelNum; i++)
            {

                Vector2 startPixel = left[i];
                Vector2 endPixel = right[i];

                List<Vector2> linePixels = Assets.Scripts.Functions.detectPixelsOnLine(startPixel, endPixel);

                //Debug.Log("Linepixels count: " + linePixels.Count);
                for (int j = 0; j < linePixels.Count; j++)
                {
                    pixels.Add(linePixels[j]);
                    /*Debug.Log(linePixels[j]);
                    Debug.Log(colorTexture.GetPixel((int)linePixels[j].x, (int)linePixels[j].y));
                    Debug.Log("");*/
                    //extractedImage.SetPixel(i, j, Assets.Scripts.Functions.getColorAroundPixel(colorTexture, (int)linePixels[j].x, Screen.height - (int)linePixels[j].y));
                    /*extractedImage.SetPixel(i, j, cameraInput.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y));
                    extractedImage.Apply();

                }
            }

            Assets.Scripts.Functions.saveTexture(extractedImage, "extractedImage", "");



            /*
            int templateIndex = 0;
            double matchValue = 0;
            int matchedTemplateIndex = 0;
            foreach (Texture2D template in raceTemplateTextures)
            {
                Debug.Log("Mathing with template: " + templateIndex);
                templateIndex++;
                Texture2D temp = extractedImage;
                for (int i = 0; i < 4; i++)
                {
                    double matchValueT = Assets.Scripts.Emgu.ImageRecognition.findMatchWithEmgu(template, temp);
                    if (matchValue < matchValueT)
                    {
                        matchedTemplateIndex = templateIndex;
                        matchValue = matchValueT;
                    }
                    temp = Assets.Scripts.Functions.rotateTexture90DegreesLeft(temp);
                }
            }
             * */
            //Debug.Log((Enums.RaceType)matchedTemplateIndex);
            /*t.detectPawnRace(cameraInput, raceTemplateTextures);
        }*/

    }
    /*bool findRacesPatterns(Texture2D inputTexture)
    {
        List<Pawn> patternResults = RectangleDetection.myDetectFromTextureFunction(inputTexture, cameraControler, true);
        debug.text = patternResults.Count.ToString();
        if (patternResults.Count == racesTemplatesPerDetection)
        {
            for (int i = 0; i < patternResults.Count; i++)
            {
                addPatterns(patternResults[i].cutPawnFromImage(inputTexture));
            }
            return true;
        }
        return false;
    }*/
    public int getHistogramIndexFromPosition(Vector2 inputPosition)
    {
        Color color = assetManager.histogramColorMap.GetPixel((int)inputPosition.x, Screen.height - (int)inputPosition.y);
        int colorValue = (int)(color.r * 255);
        //Debug.Log(colorValue);
        for (int i = 0; i < histogramsMapColorValues.Length; i++)
        {
            if (colorValue == histogramsMapColorValues[i])
            {
                return i;
            }
        }
        return 0;
    }
    bool findRacesPatterns(Image<Bgr,byte> inputTexture)
    {
        List<Pawn> patternResults;

        if (RectangleDetection.myDetectFromTextureFunction(inputTexture, cameraControler, true, out patternResults))
        {
            debug.text = patternResults.Count.ToString();
            if (patternResults.Count == racesTemplatesPerDetection)
            {
                //Texture2D tempConvertedTextue = RectangleDetection.convertImageToTexture(inputTexture);
                for (int i = 0; i < patternResults.Count; i++)
                {
                    //addPatterns(patternResults[i].cutPawnFromImage(inputTexture));
                    Debug.Log(getHistogramIndexFromPosition(patternResults[i].getPawnPositionOnScreen()));
                    templateHistograms[getHistogramIndexFromPosition(patternResults[i].getPawnPositionOnScreen()) + 9 * numberOfSetsTrainedHistograms] = patternResults[i].cutPawnHistogram(inputTexture);
                    templateHistograms[getHistogramIndexFromPosition(patternResults[i].getPawnPositionOnScreen()) + 9 * numberOfSetsTrainedHistograms].print4Buckets(); 
                    templateHistograms[getHistogramIndexFromPosition(patternResults[i].getPawnPositionOnScreen()) + 9 * numberOfSetsTrainedHistograms].print6Buckets(); 
                    templateHistograms[getHistogramIndexFromPosition(patternResults[i].getPawnPositionOnScreen()) + 9 * numberOfSetsTrainedHistograms].print12Buckets();
                    MapControler.showPawnHint(patternResults[i], assetManager.UIElementPrefab, uiElementsParent, assetManager.whiteSprite);
                    MapControler.showPawnHint(patternResults[i], assetManager.UIElementPrefab, uiElementsParent, assetManager.textureTempSprite,true);
                    //patternResults[i].castLightOnPawn(assetManager.UIElementPrefab, uiElementsParent, assetManager);
                }
                numberOfSetsTrainedHistograms++;
                return true;
            }
        }
            return false;
    }
    List<Pawn> detectPawnsRace(Image<Bgr, byte> observedTexture, List<Pawn> inputList)
    {
        for (int i = 0; i < inputList.Count; i++)
        {
            //inputList[i].detectPawnRace2(observedTexture, raceTemplateTextures, templateHistograms);
            //Debug.Log("Screen: " +(Screen.height - inputList[i].getPawnPositionOnScreen().y));
            inputList[i].setPawnRegionIndex(mapControler.getRegionFromPosition(new Vector2(inputList[i].getPawnPositionOnScreen().x, inputList[i].getPawnPositionOnScreen().y)));

            /*int[] tempList = mapControler.getRegionHistogramTemplates(inputList[i].getPawnRegionIndex());
            ColorHistogram[] tempHistograms  = new ColorHistogram[tempList.Length*numberOfSetsTrainedHistograms];
            //Debug.Log("Prepared Histograms length: "+tempHistograms.Length);
            for (int j = 0; j < tempList.Length; j++)
            {
                tempHistograms[j] = templateHistograms[tempList[j]];

                //Debug.Log("Prepared Histograms[" + j + "] : " + tempList[j]);
                tempHistograms[j + tempList.Length] = templateHistograms[tempList[j] + racesTemplatesPerDetection];
                //Debug.Log("Prepared Histograms[" + (j + tempList.Length) + "] : " + (tempList[j] + racesTemplatesPerDetection));

                tempHistograms[j + (tempList.Length * 2)] = templateHistograms[tempList[j] + racesTemplatesPerDetection * 2];
                //Debug.Log("Prepared Histograms[" + (j + (tempList.Length * 2)) + "] : " + (tempList[j] + racesTemplatesPerDetection * 2));

            }*/
            inputList[i].detectPawnRace4(observedTexture, raceTemplateTextures, templateHistograms /*tempHistograms*//* templateHistograms*/,racesTemplatesPerDetection /*tempList.Length*/);
            //inputList[i].detectPawnRace4(observedTexture, raceTemplateTextures, tempHistograms, tempList.Length);
        }
        return inputList;
    }
    List<Pawn> detectPawnsRace(Texture2D observedTexture, List<Pawn> inputList)
    {
        for (int i = 0; i < inputList.Count; i++)
        {
            //inputList[i].detectPawnRace2(observedTexture, raceTemplateTextures, templateHistograms);
            inputList[i].detectPawnRace3(observedTexture, raceTemplateTextures, templateHistograms);
        }
        return inputList;
    }
    int[] calculatePawnDistribution(List<Pawn> inputList)
    {
        int[] result = new int[(int)Enums.RaceType.numberOfTypes];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = 0;
        }
            foreach (Pawn p in inputList)
            {
                result[(int)p.getRaceType()] += 1;
            }

        return result;
    }
    void updateMapInfo(List<Pawn> pawnsList)
    {
        numberOfPawnsInLastFrame = pawnsList.Count;
        pawnsDistributionInLastUpdate = calculatePawnDistribution(pawnsList);
        foreach (Pawn p in pawnsList)
        {
            mapControler.addPawnToRegion(p,uiElementsParent);
        }

        mapControler.updateRegionsInfo();
    }
    void findContoursFunction(Image<Bgr,byte> inputTexture)
    {

        //SAVE DEFAULT CAMERA IMAGE
        //byte[] imageData = cameraInput.EncodeToPNG();
        //File.WriteAllBytes(Application.dataPath + "/../defaultImage.png", imageData);
        float timeStamp = Time.realtimeSinceStartup;

        List<Pawn> temporaryResults;
        RectangleDetection.myDetectFromTextureFunction(inputTexture, cameraControler, true, out temporaryResults, true,true,"1", assetManager.whiteBackgroundEmguImage);
        Debug.Log("Pawns detection time: " + (Time.realtimeSinceStartup - timeStamp));
        Debug.Log("PAWNS FOUND: " + temporaryResults.Count);
        timeStamp = Time.realtimeSinceStartup;
        Texture2D tempConvertedTextue = RectangleDetection.convertImageToTexture(inputTexture);

        Debug.Log("Image convertion time: " + (Time.realtimeSinceStartup - timeStamp));
        timeStamp = Time.realtimeSinceStartup;

        temporaryResults = detectPawnsRace(tempConvertedTextue, temporaryResults);
        Debug.Log("Race detection time: " + (Time.realtimeSinceStartup - timeStamp));
        timeStamp = Time.realtimeSinceStartup;

        numberOfPawnsInLastFrame = temporaryResults.Count;
        pawnsDistributionInLastUpdate = calculatePawnDistribution(temporaryResults);
        foreach (Pawn p in temporaryResults)
        {
            mapControler.addPawnToRegion(p, uiElementsParent);
        }
        debug.text = mapControler.checkRegionsPreStartReadiness().ToString();
        //updateMapInfo(temporaryResults);
        Debug.Log("Map update time: " + (Time.realtimeSinceStartup - timeStamp));

    }
    void findContoursFunction(Texture2D inputTexture)
    {

        //SAVE DEFAULT CAMERA IMAGE
        //byte[] imageData = cameraInput.EncodeToPNG();
        //File.WriteAllBytes(Application.dataPath + "/../defaultImage.png", imageData);
        List<Pawn> temporaryResults = RectangleDetection.myDetectFromTextureFunction(inputTexture, cameraControler, true, assetManager.whiteBackground);
        Debug.Log("PAWNS FOUND: " + temporaryResults.Count);
        temporaryResults = detectPawnsRace(inputTexture, temporaryResults);
        updateMapInfo(temporaryResults);
        
    }
    /*void addPatterns(Texture2D inputPattern)
    {
        raceTemplateTextures.Add(inputPattern);
        templateHistograms.Add(new ColorHistogram(inputPattern));
        Assets.Scripts.Functions.saveTexture(inputPattern, "inputPattern" + raceTemplateTextures.Count, "");
    }*/
    public void detectRacesPatterns()
    {
        if(isDebug)
        {
            List<Pawn> resultPatterns = Assets.RectangleDetection.myDetectFromTextureFunction(assetManager.racePatternTextures[currentRacePattern], cameraControler, true);

            Debug.Log("Patterns Count: " + resultPatterns.Count + "     should be: "+ racesTemplatesPerDetection);
            if (resultPatterns.Count == racesTemplatesPerDetection)
            {
                for (int i = 0; i < resultPatterns.Count; i++)
                {
                    //addPatterns(resultPatterns[i].cutPawnFromImage(assetManager.racePatternTextures[currentRacePattern]));
                }
                currentRacePattern += 1;
            }
            
        }
        else
        {
            invokeCalibrationFunction(Enums.ImageProcessFunction.trainPatterns);
        }
    }
    public void detectTresholdManually()
    {
        if (!isDebug)
        {
            invokeCalibrationFunction(Enums.ImageProcessFunction.manualTreshold);
        }
        else
        {
            textureHolder.texture = Assets.RectangleDetection.detectTreshold(assetManager.racePatternTextures[0], (int)slider.value);
        }
    }
    IEnumerator captureBackgroundFunction( )
    {
        HideHand();
        menu.SetActive(false);
        //regionsParent.SetActive(false);
        hideRegionsTextures();
        Cursor.visible = false;
        textureHolder.texture = assetManager.whiteTexture;
        yield return new WaitForSeconds(photoDelayTime);
        dllCamera.setBackgroundImage();
        menu.SetActive(true);
        //regionsParent.SetActive(true);
        showRegionsTextures();
        Cursor.visible = true;
        textureHolder.texture = assetManager.mapTexture;
        //moviePlayed = true;

    }
    IEnumerator captureBackgroundFunction(bool mapBackground)
    {
        HideHand();
        menu.SetActive(false);
        //regionsParent.SetActive(false);
        hideRegionsTextures();
        Cursor.visible = false;
        if (mapBackground)
        {
            textureHolder.texture = assetManager.mapTexture;
        }
        else
        {
            textureHolder.texture = assetManager.whiteTexture;
        }

        yield return new WaitForSeconds(photoDelayTime);

        cameraInput.SetPixels(cameraUsed.GetPixels());
        cameraInput.Apply();

        if (mapBackground)
        {
            //assetManager.backgroundImageCaptured = Assets.Scripts.Usefull.TextureProcessing.convertToGreyscale(cameraInput);
            //Assets.Scripts.Functions.saveTexture(assetManager.backgroundImageCaptured, "mapBackground", "");
        }
        else
        {

            assetManager.whiteBackground = Assets.Scripts.Usefull.TextureProcessing.convertToGreyscale(cameraInput);
            Assets.Scripts.Functions.saveTexture(assetManager.whiteBackground, "whiteBackground", "");
        }

        menu.SetActive(true);
        //regionsParent.SetActive(true);
        showRegionsTextures();
        Cursor.visible = true;

    }
    public void saveBackground()
    {


        StartCoroutine(captureBackgroundFunction(true));
    }
    public void savewhiteBackground()
    {

        StartCoroutine(captureBackgroundFunction());
        //StartCoroutine(captureBackgroundFunction(false));
    }
    public void detectTreshold()
    {

        if (!isDebug)
        {
            invokeCalibrationFunction(Enums.ImageProcessFunction.detectTreshold);
            //StartCoroutine(startLiveFunction(Enums.ImageProcessFunction.detectTreshold));
        }
        else
        {
            //textureHolder.texture = Assets.RectangleDetection.detectTreshold(racePatternTextures[0], (int)slider.value);
            //Debug.Log((int)racesTemplatesPerDetection);
            bool detectionResult = Assets.RectangleDetection.searchForDetectionTreshold(assetManager.racePatternTextures[0], calirationStep, (int)racesTemplatesPerDetection, cameraControler);
        }
       // Debug.Log(Assets.RectangleDetection.searchForDetectionTreshold(colorTexture, calirationStep, numberOfImagesForCalibration));
        //Assets.Scripts.Usefull.TextureProcessing.convertColorBy2MostSignificantBytes(new Color(128, 128, 128));

        /*Texture2D resized1 = Assets.Scripts.Usefull.TextureProcessing.scaled(raceTemplateTextures[0], 20, 20);
        Texture2D resized2 = Assets.Scripts.Usefull.TextureProcessing.scaled(raceTemplateTextures[1], 20, 20);

        Debug.Log("EmguHist comparison: "+Assets.Scripts.Emgu.ImageRecognition.compareHistogramswithEmgu(resized1, resized1));

        int[] histogram1 = Assets.Scripts.Usefull.TextureProcessing.getImageHistogramFullColors(resized1);
        int[] histogram2 = Assets.Scripts.Usefull.TextureProcessing.getImageHistogramFullColors(resized2);

        Texture2D hist1 = Assets.Scripts.Usefull.TextureProcessing.createImageHistogramRGBTexture(resized1);
        Texture2D hist2 = Assets.Scripts.Usefull.TextureProcessing.createImageHistogramRGBTexture(resized2);
        
        Texture2D hist11 = Assets.Scripts.Usefull.TextureProcessing.createImageHistogram64ColorTexture(resized1);
        Texture2D hist22 = Assets.Scripts.Usefull.TextureProcessing.createImageHistogram64ColorTexture(resized2);

        Assets.Scripts.Functions.saveTexture(hist1, "histogram1", "");
        Assets.Scripts.Functions.saveTexture(hist2, "histogram2", "");
        
        Assets.Scripts.Functions.saveTexture(hist11, "histogram11", "");
        Assets.Scripts.Functions.saveTexture(hist22, "histogram22", "");

        for (int i = 0; i < histogram1.Length; i++)
        {
           // Debug.Log(histogram1[i] + "   " + histogram2[i]);
        }
            Assets.Scripts.Functions.saveTexture(resized1, "resize1", "");
        Assets.Scripts.Functions.saveTexture(resized2, "resize2", "");
        Debug.Log(Assets.Scripts.Usefull.TextureProcessing.compareColorHistograms(histogram1, histogram2));
       
        */
        //Debug.Log(histogram);
    }
    IEnumerator saveScreenCorutine()
    {
        textureHolder.texture = assetManager.whiteTexture;
        float timer = Time.time;
        yield return new WaitForSeconds(photoDelayTime);
        //Debug.Log(Time.time - timer);
        if (dllCamera.lookForHandBoolean())
        {
            showHand();
        }
        else
        {
            HideHand();
        }

        //c.getFrameAsImage();
        //c.getFrameAsImage().Save(Application.dataPath + "/../Frame/EmguCameraImg.png");
        textureHolder.texture = assetManager.mapTexture;


    }
    
    public void blankTest()
    {
        winText.transform.parent.gameObject.SetActive(!winText.IsActive());
        /*GameObject obj =  Instantiate(assetManager.UIElementPrefab);
        obj.transform.SetParent(uiElementsParent.transform,false);
        obj.transform.position = new Vector3(400, 300) + new Vector3(100, 100);
        Image objImg = obj.GetComponent<Image>();
        objImg.sprite = assetManager.UIElementsTextures[0];
        float imageSize = 15;
        objImg.rectTransform.sizeDelta = new Vector2(imageSize, imageSize);

        GameObject obj2 = Instantiate(assetManager.UIElementPrefab);
        obj2.transform.SetParent(uiElementsParent.transform, false);
        obj2.transform.position = new Vector3(100, 100);*/

        /*foreach (Image comp in uiElementsParent.GetComponentsInChildren<Image>())
        {
            Debug.Log(comp);
            Destroy(comp.gameObject);
        }*/

        //moviePlayed = true;
        /*
        if (panelManager.isVisible())
        {
            panelManager.hidePanel();
        }
        else
        {
            panelManager.showPanel();
        }*/
        /*if (dllCamera.isCameraRunning)
        {
          //StartCoroutine(captureBackgroundFunction());
            moviePlayed = true;
        }
        else
        {
            menu.SetActive(false);
            regionsParent.SetActive(false);
            Debug.Log(dllCamera.startCamera(CameraControler.cameraCaptureWidth,CameraControler.cameraCaptureHeight));
            menu.SetActive(true);
            dllCamera.setCascadeClassifier(9, Application.dataPath + "/Hand.Cascade.");
            
        }*/
        //assetManager.movieTexture.Play();
        //moviePlayed = true;
        ////invokeCalibrationFunction(Enums.ImageProcessFunction.detectHand);
        //textureHolder.texture = assetManager.whiteTexture;
       
        
        /*Debug.Log(cameraUsed.didUpdateThisFrame);
        textureHolder.texture = assetManager.whiteTexture;

        cameraInput.SetPixels(cameraUsed.GetPixels());
        cameraInput.Apply();
        //textureHolder.texture = assetManager.mapTexture;

        Assets.Scripts.Functions.saveTexture(cameraInput, "blankTestImage", "");*/

        //StartCoroutine(saveScreenCorutine());


        //RectangleDetection.substractImages2(cameraInput,assetManager.whiteBackground);
        //RectangleDetection.detectHandInCamera(cameraInput, assetManager.backgroundImageCaptured);
        //showGearBox();
        //HandTestEnabled = !HandTestEnabled;
        //trainHistograms();
        /*Texture2D tempTest = new Texture2D(assetManager.match2.width, assetManager.match2.height);
        Texture2D tempTest2 = new Texture2D(assetManager.match3.width, assetManager.match3.height);
        tempTest.SetPixels32(assetManager.match2.GetPixels32());
        tempTest2.SetPixels32(assetManager.match3.GetPixels32());
        tempTest.Apply();
        tempTest2.Apply();

        Assets.Scripts.Usefull.TextureProcessing.scale(tempTest, 40, 40);
        Assets.Scripts.Usefull.TextureProcessing.scale(tempTest2, 40, 40);
        //Debug.Log(Assets.Scripts.Emgu.ImageRecognition.FindMatch(match1, tempTest));
        //Debug.Log(Assets.Scripts.Emgu.ImageRecognition.FindMatch(match2, tempTest));
        Debug.Log(Assets.Scripts.Emgu.ImageRecognition.FindMatch(assetManager.match1, assetManager.match2));
        //Debug.Log(Assets.Scripts.Emgu.ImageRecognition.FindMatch(tempTest, match1));
        //Debug.Log(Assets.Scripts.Emgu.ImageRecognition.FindMatch(match2, match1));

        ColorHistogram histogram1 = new ColorHistogram(assetManager.match1);
        ColorHistogram histogram2 = new ColorHistogram(assetManager.match2);
        ColorHistogram histogram3 = new ColorHistogram(assetManager.match3);
        /*ColorHistogram match55 = new ColorHistogram(tempTest);
        ColorHistogram match66 = new ColorHistogram(tempTest2);*/

        //Debug.Log(ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistanceWithoutGreyscale(histogram1, histogram2));
        /*Debug.Log(ColorHistogram.compareColorFactorWithHalfChiSquaredDistance(ColorHistogram.get6BucketFactorHistogram(histogram1.greyScale),
            ColorHistogram.get6BucketFactorHistogram(histogram2.greyScale)));
        //Debug.Log(ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistanceWithoutGreyscale(histogram2, histogram3));
        Debug.Log(ColorHistogram.compareColorFactorWithHalfChiSquaredDistance(ColorHistogram.get6BucketFactorHistogram(histogram2.greyScale),
            ColorHistogram.get6BucketFactorHistogram(histogram3.greyScale)));

        //ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistanceWithoutGreyscale(histogram, raceTemplateHistograms[i]);
        /*Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy12BucketsWithChiSquaredDistance(match11, match22));
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy12BucketsWithChiSquaredDistance(match11, match33));
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy12BucketsWithChiSquaredDistance(match11, match55));
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy12BucketsWithChiSquaredDistance(match11, match66));
        Debug.Log("");
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistance(match11, match22));
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistance(match11, match33));
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistance(match11, match55));
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistance(match11, match66));
        Debug.Log("");
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy4BucketsWithChiSquaredDistance(match11, match22));
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy4BucketsWithChiSquaredDistance(match11, match33));
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy4BucketsWithChiSquaredDistance(match11, match55));
        Debug.Log(Assets.Scripts.Structures.ColorHistogram.compareColorHistogramsBy4BucketsWithChiSquaredDistance(match11, match66));*/
    }
}
