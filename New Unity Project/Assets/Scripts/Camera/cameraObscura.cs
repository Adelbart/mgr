﻿using UnityEngine;
using System.Collections;

public class cameraObscura : MonoBehaviour
{

    WebCamTexture textureCam;
    //public DeviceOrEmulator devOrEmu;
    //public DepthWrapper dw;
    //private Kinect.KinectInterface myKinect;
    public Texture2D basicTexture;

    // Use this for initialization
    void Start()
    {

        //Debug.Log(WebCamTexture.devices.Length);
        //this.myKinect = devOrEmu.getKinect();
        //GetComponent<Renderer>().material.mainTexture = tex;
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.A))
        {
            //if (myKinect.pollColor())
            {

                Texture2D tex = new Texture2D(640, 480, TextureFormat.ARGB32, false);
                //tex.SetPixels32(myKinect.getColor());
                //Color32[] temp = myKinect.getColor();
                // Debug.Log(temp[0]);
                for (int i = 0; i < temp.Length; i++)
                {
                    //Debug.Log("ZMIANA");
                    temp[i].a = 255;
                }
                //tex.SetPixels32(mipmapImg(myKinect.getColor(), 640, 480));
                tex.SetPixels32(temp);
                tex.Apply(false);
                GetComponentInChildren<GUITexture>().texture = tex;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                if (myKinect.pollDepth())
                {

                    Texture2D tex = new Texture2D(320, 240, TextureFormat.ARGB32, false);
                    //tex.SetPixels32(myKinect.getColor());
                    Color32[] temp = convertDepthToColor(myKinect.getDepth());
                    // Debug.Log(temp[0]);
                    for (int i = 0; i < temp.Length; i++)
                    {
                        //Debug.Log("ZMIANA");
                        temp[i].a = 255;
                    }
                    //tex.SetPixels32(mipmapImg(myKinect.getColor(), 640, 480));
                    //Debug.Log(temp[0]);
                    tex.SetPixels32(temp);
                    tex.Apply(false);
                    GetComponentInChildren<GUITexture>().texture = tex;
                }
            }
        }*/
    }
    private Color32[] convertDepthToColor(short[] depthBuf)
    {
        Color32[] img = new Color32[depthBuf.Length];
        for (int pix = 0; pix < depthBuf.Length; pix++)
        {
            img[pix].r = (byte)(depthBuf[pix] / 32);
            img[pix].g = (byte)(depthBuf[pix] / 32);
            img[pix].b = (byte)(depthBuf[pix] / 32);
        }
        return img;
    }


}
