﻿using UnityEngine;
using System.Collections;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Drawing;

namespace SURFFeatureExample
{
    public class vx : MonoBehaviour
    {
        long matchTime;
        Mat homography;
        VectorOfKeyPoint modelKeyPoints;
        VectorOfKeyPoint observedKeyPoints;
        VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch();
        Mat mask;

        // Use this for initialization
        void Start()
        {
            //Mat modelImage = CvInvoke.Imread("Assets/Models/ItsJustMe.jpg", LoadImageType.Grayscale);
            Mat modelImage = CvInvoke.Imread("Assets/Models/lena.jpg", LoadImageType.Grayscale);
            Mat observedImage = CvInvoke.Imread("Assets/Models/Rysunek 1.png", LoadImageType.Grayscale);
           

            DrawMatches.FindMatch(modelImage, observedImage, out matchTime, out modelKeyPoints, out observedKeyPoints, matches, out mask, out homography);
            //czas szukania
           /* Debug.Log(matchTime / 1000.0f);

            Debug.Log(modelKeyPoints.Size);
            Debug.Log(observedKeyPoints.Size);
            Debug.Log(matches.Size);*/

            //Draw the matched keypoints
            Mat result = new Mat();
            Features2DToolbox.DrawMatches(modelImage, modelKeyPoints, observedImage, observedKeyPoints,
               matches, result, new MCvScalar(255, 255, 255), new MCvScalar(255, 255, 255), mask);

            if (homography != null)
            {
                Debug.Log("Match Found");
                //draw a rectangle along the projected model
                Rectangle rect = new Rectangle(Point.Empty, modelImage.Size);
                PointF[] pts = new PointF[]
               {
                  new PointF(rect.Left, rect.Bottom),
                  new PointF(rect.Right, rect.Bottom),
                  new PointF(rect.Right, rect.Top),
                  new PointF(rect.Left, rect.Top)
               };
                pts = CvInvoke.PerspectiveTransform(pts, homography);

                /*Point[] points = Array.ConvertAll<PointF, Point>(pts, Point.Round);
                using (VectorOfPoint vp = new VectorOfPoint(points))
                {
                    CvInvoke.Polylines(result, vp, true, new MCvScalar(255, 0, 0, 255), 5);
                }*/

            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}