﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PanelManager : MonoBehaviour{

    List<PanelScript> infoPanels = new List<PanelScript>();

    void Start()
    {
        //Debug.Log("Number of Panels found: " +transform.childCount);
        for (int i = 0; i < transform.childCount; i++)
        {
            infoPanels.Add(new PanelScript());
            Text[] t = transform.GetChild(i).gameObject.GetComponentsInChildren<Text>();
            Image[] tt = transform.GetChild(i).gameObject.GetComponentsInChildren<Image>();
            infoPanels[i].setCoinTextComponent(t[0]);
            infoPanels[i].setTroopCountComponent(t[1]);
            infoPanels[i].setCoinImageComponent(tt[0]);
            infoPanels[i].setTroopImageComponent(tt[1]);
        } 
        hideAllPanels();
        hidePanel();
    }
    private PanelManager()
    {
        
    }
    public int getPanelsCount()
    {
        return infoPanels.Count;
    }
    public bool isVisible()
    {
        return gameObject.activeInHierarchy;
    }
    public void showPanel()
    {
        Debug.Log("Showing Regions Panel");
        gameObject.SetActive(true);
    }
    public void clearAllPanels()
    {
        foreach (PanelScript ps in infoPanels)
        {
            ps.updateInfo(0, 0);
            ps.hide();
        }
    }
    public void hideAllPanels()
    {
        foreach (PanelScript ps in infoPanels)
        {
            ps.hide();
        }
    }
    public void updatePanelInfo(int panelIndex, Enums.RegionEvents eventType, float deltaTime)
    {
        infoPanels[panelIndex].updateInfo(eventType,deltaTime);
    }
    public void hidePanel()
    {
        Debug.Log("Hiding Regions Panel");
        gameObject.SetActive(false);
    }
    public void showPanelInfo(int panelIndex, int coinValue, int troopValue)
    {
        //Debug.Log(panelIndex);
        infoPanels[panelIndex].show();
        infoPanels[panelIndex].updateInfo(coinValue, troopValue);
    }
    public void changePanelTexture(int panelIndex, Sprite newTexture)
    {
        infoPanels[panelIndex].changeTexture(newTexture);
    }
    public void hidePanelInfo(int panelIndex)
    {
        infoPanels[panelIndex].updateInfo(0, 0);
    }
}
