﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PanelScript{

    Image coinImage;
    Image troopImage;
    Text cointCount;
    Text troopCount;
    GameObject parent;
    bool growing = false;
    bool decreasing = false;
    float accumulativeTime = 0.0f;
    public PanelScript() { }

    public void setCoinTextComponent(Text t)
    {
        parent = t.transform.parent.gameObject;
        cointCount = t;
    }
    public void setTroopCountComponent(Text t)
    {
        troopCount = t;
    }
    public void setCoinImageComponent(Image t)
    {
        coinImage = t;
    }
    public void setTroopImageComponent(Image t)
    {
        troopImage = t;
    }
    public void hide()
    {
        parent.SetActive(false);
    }
    public void show()
    {
        parent.SetActive(true);
    }
    public void updateInfo(Enums.RegionEvents eventType, float deltaTime)
    {
        if(eventType == Enums.RegionEvents.regionBeingConqueredNotEnoughPawns || eventType == Enums.RegionEvents.regionBeingLeftButNotAllPawnsTaken)
        {
            accumulativeTime+=deltaTime;
            if (!decreasing && !growing)
            {
                growing = true;
                //troopCount.fontSize += 1;
                troopCount.color = Color.red;
                if (accumulativeTime > 0.1f)
                {
                    troopCount.fontSize += 1;
                    accumulativeTime = 0;
                }
            }
            else if (growing)
            {
                //troopCount.fontSize += 1;
                if (accumulativeTime > 0.1f)
                {
                    troopCount.fontSize += 1;
                    accumulativeTime = 0;
                }
                if (troopCount.fontSize == 28)
                {
                    growing = false;
                    decreasing = true;
                }
            }
            else if (decreasing)
            {
                //troopCount.fontSize -= 1;
                if (accumulativeTime > 0.1f)
                {
                    troopCount.fontSize -= 1;
                    accumulativeTime = 0;
                }
                if (troopCount.fontSize == 21)
                {
                    growing = true;
                    decreasing = false;
                }
            }

        }
    }
    private void clearEventsData()
    {
        growing = false;
        decreasing = false;
        accumulativeTime = 0;
        cointCount.color = Color.black;
        troopCount.color = Color.black;
    }
    public void changeTexture(Sprite newTexture)
    {
        troopImage.sprite = newTexture;
    }
    public void updateInfo(int coinValue, int troopValue)
    {
        clearEventsData();
        if (coinValue == 0)
        {
            cointCount.text = "";
            coinImage.gameObject.SetActive(false);
        }
        else
        {
            cointCount.text = coinValue.ToString();
            coinImage.gameObject.SetActive(true);
            if (coinValue > 1)
            {
                cointCount.color = Color.blue;
            }
        }

        if (troopValue == 0)
        {
            troopCount.text = "";
            troopImage.gameObject.SetActive(false);
        }
        else
        {
            troopCount.text = troopValue.ToString();
            troopImage.gameObject.SetActive(true);
            if (troopValue > 2)
            {
                troopCount.color = Color.red;
            }
            if (troopValue < 2)
            {
                troopCount.color = Color.blue;
            }
        }
    }
}
