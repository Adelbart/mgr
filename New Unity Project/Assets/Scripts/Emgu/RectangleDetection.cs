﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Assets.Scripts;
using Assets.Scripts.Structures;

namespace Assets
{
    static class RectangleDetection
    {
        public static int colorRange = 100;
        public static float contourAreaMax = 200;
        public static float contourAreaMin = 50;
        public static bool saveTextures = true;
        private static float contourDivider = 2.8f;
        private static float heightContourExtender = 0.5f;
        private static float mapArea = 0;
        public static Texture2D detectTreshold(Texture2D inputTexture, int newColorRange)
        {
            byte[] imageData = inputTexture.EncodeToPNG();

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }


            Image<Bgr, byte> img = new Image<Bgr, byte>(bitmapData);
            Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);

            CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);

            Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(newColorRange), new Gray(255));
            Img_Source_Gray.Save(Application.dataPath + "/../TresholdBinaryTest.png");
            

            colorRange = newColorRange;
            Texture2D newTemp = new Texture2D(inputTexture.width,inputTexture.height);
            
            Bitmap bitmapCurrentFrame = Img_Source_Gray.ToBitmap();
            MemoryStream m = new MemoryStream();
            bitmapCurrentFrame.Save(m, bitmapCurrentFrame.RawFormat);

            newTemp.LoadImage(m.ToArray());
            newTemp.Apply();

            Assets.Scripts.Functions.saveTexture(newTemp,"BinaryTexTest","");
            return newTemp;
        }
        public static Texture2D convertImageToTexture(Image<Bgr, byte> img)
        {
            Bitmap bitmapCurrentFrame = img.ToBitmap();
            MemoryStream m = new MemoryStream();
            bitmapCurrentFrame.Save(m, bitmapCurrentFrame.RawFormat);

            Texture2D tempConvertedTextue = new Texture2D(bitmapCurrentFrame.Width, bitmapCurrentFrame.Height);
            tempConvertedTextue.LoadImage(m.ToArray());
            tempConvertedTextue.Apply();
            return tempConvertedTextue;
        }
        public static Texture2D detectTreshold(Image<Bgr, byte> img, int newColorRange, bool changeTresholdValue = true)
        {
            Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);

            CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);

            Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(newColorRange), new Gray(255));
            Img_Source_Gray.Save(Application.dataPath + "/../TresholdBinaryTest.png");

            //CvInvoke.CvtColor(Img_Source_Gray, img, ColorConversion.Gray2Bgr);

            if (changeTresholdValue)
            {
                Debug.Log("Setting color range to: " + colorRange);
                colorRange = newColorRange;
            }

            Bitmap bitmapCurrentFrame = Img_Source_Gray.ToBitmap();
            MemoryStream m = new MemoryStream();
            bitmapCurrentFrame.Save(m, bitmapCurrentFrame.RawFormat);

            Texture2D newTemp = new Texture2D(bitmapCurrentFrame.Width, bitmapCurrentFrame.Height); 
            newTemp.LoadImage(m.ToArray());
            newTemp.Apply();
            return newTemp;
        }
        public static bool colorsAreNear(Color32 A, Color32 B, int closeValue)
        {
            if (Mathf.Abs(A.r - B.r) <= closeValue && Mathf.Abs(A.g - B.g) <= closeValue && Mathf.Abs(A.b - B.b) <= closeValue)
            {
                return true;
            }
            return false;
        }
        public static Texture2D substractImages2(Texture2D textureA, Texture2D textureB)
        {
            if (textureA.width == textureB.width && textureA.height == textureB.height)
            {
                Texture2D greyscaleA = Assets.Scripts.Usefull.TextureProcessing.convertToGreyscale(textureA);
                Texture2D result = new Texture2D(textureA.width, textureA.height);
                UnityEngine.Color[] pixelsA = greyscaleA.GetPixels();
                UnityEngine.Color[] pixelsB = textureB.GetPixels();
                for (int i = 0; i < pixelsA.Length; i++)
                {
                    UnityEngine.Color color = new UnityEngine.Color();
                    //Debug.Log(pixelsA[i]);
                    color.r = pixelsA[i].r - pixelsB[i].r > 0 ? 0 : 255;
                    color.g = pixelsA[i].g - pixelsB[i].g > 0 ? 0 : 255;
                    color.b = pixelsA[i].b - pixelsB[i].b > 0 ? 0 : 255;
                    color.a = 1;
                        int y = i / textureA.width;
                        int x = i % textureA.width;
                        result.SetPixel(x, y, color);
                    

                }
                result.Apply();

                saveTexture(result, "resultSubstract", "");
            }
            return null;

        }
       
        public static Texture2D substractImages(Texture2D textureA, Texture2D textureB, int tresholdValue)
        {
            if(textureA.width == textureB.width && textureA.height == textureB.height)
            {
                Texture2D greyscaleA = Assets.Scripts.Usefull.TextureProcessing.convertToGreyscale(textureA);
                Texture2D result = new Texture2D(textureA.width, textureA.height);


                Color32[] pixelsA = greyscaleA.GetPixels32();
                Color32[] pixelsB = textureB.GetPixels32();

                for (int i = 0; i < pixelsA.Length; i++)
                {
                    if (!colorsAreNear(pixelsA[i], pixelsB[i], tresholdValue))
                    {
                        int y=i/textureA.width;
                        int x=i%textureA.width;
                        result.SetPixel(x, y, UnityEngine.Color.black);
                    }
                    
                }
                result.Apply();

                saveTexture(result, "result", "");

                return result;
            
            }
            return null;
        }
        public static Enums.HandDetectionState detectHandInCamera2(Texture2D colorTexture, Texture2D backgroundTexture)
        {
            Texture2D substractedImage = substractImages(colorTexture, backgroundTexture, 40); 
            saveTexture(colorTexture, "handDetectionScreen2", "");

            return Enums.HandDetectionState.CameraClear;
        }
        public static Enums.HandDetectionState detectHandInCamera(Texture2D colorTexture, Texture2D backgroundTexture)
        {


            Texture2D substractedImage = substractImages(colorTexture, backgroundTexture,40);

            //saveTexture(colorTexture, "handDetectionScreen", "");

            byte[] imageData = substractedImage.EncodeToPNG();
            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }

            Image<Bgr, byte> img = new Image<Bgr, byte>(bitmapData);
            //Image<Bgr, byte> img2 = new Image<Bgr, byte>(bitmapData);
            Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);

            CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);

            //img2 = img2.ThresholdBinary(new Bgr(colorRange/2,colorRange,colorRange/2), new Bgr(255, 255, 255));
            Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(colorRange), new Gray(255));

            Img_Source_Gray.Save(Application.dataPath + "/../handConvertion1.png");
            ///img2.Save(Application.dataPath + "/../handConvertion2.png");

            UMat cannyEdges = new UMat();
            double cannyThreshold = 240.0;
            double cannyThresholdLinking = 120;
            CvInvoke.Canny(Img_Source_Gray, cannyEdges, cannyThreshold, cannyThresholdLinking);

            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple, new Point());
                //Debug.Log("Number of Contours: " + contours.Size);

                for (int i = 0; i < contours.Size; i++)
                {
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.025, true);

                        if (CvInvoke.ContourArea(approxContour, false) > 500)
                        {
                            return Enums.HandDetectionState.HandInCamera;
                        }
                    }
                }
            }
            return Enums.HandDetectionState.CameraClear;
        }
        public static Pawn findContourSize(Texture2D colorTexture, CameraControler camera, out Pawn calibrationPawnFound)
        {
            calibrationPawnFound = null;
            List<Pawn> foundPawns = new List<Pawn>();

            
            
            byte[] imageData = colorTexture.EncodeToPNG();

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }

            Image<Bgr, byte> img = new Image<Bgr, byte>(bitmapData);
            Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);

            CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);

            Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(colorRange), new Gray(255));
            Img_Source_Gray.Save(Application.dataPath + "/../FindSizeBinaryTest.png");
            UMat cannyEdges = new UMat();

            double cannyThreshold = 240.0;
            double cannyThresholdLinking = 120;
            CvInvoke.Canny(Img_Source_Gray, cannyEdges, cannyThreshold, cannyThresholdLinking);

            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple, new Point());
                Debug.Log("Number of Contours: "+contours.Size);

                //Save contours
                Texture2D contoursTexture = new Texture2D(colorTexture.width, colorTexture.height);

                for (int i = 0; i < contours.Size; i++)
                {
                    for (int j = 0; j < contours[i].Size; j++)
                    {
                        contoursTexture.SetPixel(contours[i][j].X, colorTexture.height - contours[i][j].Y, new UnityEngine.Color(0, 0, 0));

                    }
                }
                contoursTexture.Apply();
                saveTexture(contoursTexture, "contourSize", "");
                //byte[] contoursData = contoursTexture.EncodeToPNG();
                //File.WriteAllBytes(Application.dataPath + "/../contourSize.png", contoursData);


                Texture2D ApproxcontoursTexture = new Texture2D(colorTexture.width, colorTexture.height);
                for (int i = 0; i < contours.Size; i++)
                {
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.025, true);

                        if (approxContour.Size > 3)
                        {
                            Pawn testPawn = new Pawn(EmguPointToUnityPoint(approxContour,camera),camera);
                            if (testPawn.getMinRectArea() > 1500 && testPawn.getMinRectArea() < 7000)
                            {
                                if (foundPawns.Count > 0)
                                {

                                    //Debug.Log("PAWN FOUND. Size: " + testPawn.getMinRectArea());
                                    for (int z = 0; z < foundPawns.Count; z++)
                                    {
                                        if (Assets.Scripts.Functions.testRectsForIntersection(foundPawns[z].getMinEnclosingRect(), testPawn.getMinEnclosingRect())){
                                        
                                            Debug.Log("Found Intersecting Pawns. Widening PAwn are range");
                                            Pawn tempPawn = new Pawn(EmguPointToUnityPoint(approxContour,camera),camera);
                                            List<Vector2> newVErtices = tempPawn.getConvexHull().getAllConvexPoints();
                                            newVErtices.AddRange(foundPawns[z].getAllConvexHullPoints());
                                            tempPawn.setMinRectFromPoints(newVErtices,camera);
                                            foundPawns[z] = tempPawn;
                                        }
                                        else
                                        {
                                            Debug.Log("CALIBRATION ERROR: MORE THAN 1 PAWN FOUND");
                                            return null;
                                        }
                                    }

                                }
                                else
                                {

                                    //Debug.Log("PAWN FOUND. Size: " + testPawn.getMinRectArea());
                                    foundPawns.Add(testPawn);
                                }
                            }

                        }
                    }
                }
            }
            if (foundPawns.Count == 1)
            {
                float result = foundPawns[0].getMinRectArea();
                //double result = testPawn.boundsSize();
                contourAreaMax = result * 1.1f;
                contourAreaMin = result / contourDivider;
                Debug.Log("PAWN FOUND. Size: " + contourAreaMax);
                calibrationPawnFound = foundPawns[0];

                return foundPawns[0];
            }
            else
            {
                Debug.Log("CALIBRATION ERROR");
                return null;
            }
        }
        public static Pawn findContourSize(Image<Bgr, byte> img, CameraControler camera, out Pawn calibrationPawnFound)
        {
            calibrationPawnFound = null;
            List<Pawn> foundPawns = new List<Pawn>();

            Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);

            CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);

            Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(colorRange), new Gray(255));
            Img_Source_Gray.Save(Application.dataPath + "/../FindSizeBinaryTest.png");
            UMat cannyEdges = new UMat();

            double cannyThreshold = 240.0;
            double cannyThresholdLinking = 120;
            CvInvoke.Canny(Img_Source_Gray, cannyEdges, cannyThreshold, cannyThresholdLinking);

            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple, new Point());
                Debug.Log("Number of Contours: " + contours.Size);

                //Save contours
                
                //byte[] contoursData = contoursTexture.EncodeToPNG();
                //File.WriteAllBytes(Application.dataPath + "/../contourSize.png", contoursData);


                for (int i = 0; i < contours.Size; i++)
                {
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.025, true);

                        if (approxContour.Size > 3)
                        {
                            Pawn testPawn = new Pawn(EmguPointToUnityPoint(approxContour, camera),camera);
                            if (testPawn.getMinRectArea() > 1500 && testPawn.getMinRectArea() < 7000 && testPawn.isRectangular())
                            {
                                if (foundPawns.Count > 0)
                                {

                                    //Debug.Log("PAWN FOUND. Size: " + testPawn.getMinRectArea());
                                    for (int z = 0; z < foundPawns.Count; z++)
                                    {
                                        if (Assets.Scripts.Functions.testRectsForIntersection(foundPawns[z].getMinEnclosingRect(), testPawn.getMinEnclosingRect()))
                                        {

                                            Debug.Log("Found Intersecting Pawns. Widening PAwn are range");
                                            Pawn tempPawn = new Pawn(EmguPointToUnityPoint(approxContour, camera), camera);
                                            List<Vector2> newVErtices = tempPawn.getConvexHull().getAllConvexPoints();
                                            newVErtices.AddRange(foundPawns[z].getAllConvexHullPoints());
                                            tempPawn.setMinRectFromPoints(newVErtices, camera);
                                            foundPawns[z] = tempPawn;
                                        }
                                        else
                                        {
                                            Debug.Log("CALIBRATION ERROR: MORE THAN 1 PAWN FOUND");
                                            return null;
                                        }
                                    }

                                }
                                else
                                {

                                    //Debug.Log("PAWN FOUND. Size: " + testPawn.getMinRectArea());
                                    foundPawns.Add(testPawn);
                                }
                            }

                        }
                    }
                }
            }
            if (foundPawns.Count == 1)
            {
                float result = foundPawns[0].getMinRectArea();
                //double result = testPawn.boundsSize();
                contourAreaMax = result*1.25f;
                contourAreaMin = result / contourDivider;
                Debug.Log("PAWN FOUND. Size: " + contourAreaMax);
                calibrationPawnFound = foundPawns[0];

                Texture2D tempTexture = convertImageToTexture(img);
                Vector2[] corners = foundPawns[0].getMinEnclosingRect().getMinRectCorners();
                List<Vector2> line = new List<Vector2>();
                for(int z=0;z<corners.Length-1;z++)
                {
                    line = Assets.Scripts.Functions.detectPixelsOnLine(corners[z], corners[z+1]);
                    foreach (Vector2 pointOnLine in line)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                    }

                }
                line = Assets.Scripts.Functions.detectPixelsOnLine(corners[0], corners[corners.Length - 1]);
                foreach (Vector2 pointOnLine in line)
                {
                    tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                }

                List<Vector2> points = foundPawns[0].getMinEnclosingRect().getConvexHull().getConvexHullPoints();

                for (int z = 0; z < points.Count - 1; z++)
                {
                     line = Assets.Scripts.Functions.detectPixelsOnLine(points[z], points[z + 1]);
                    foreach (Vector2 pointOnLine in line)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.green);
                    }

                }
                line = Assets.Scripts.Functions.detectPixelsOnLine(points[0], points[points.Count - 1]);
                foreach (Vector2 pointOnLine in line)
                {
                    tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.green);
                }

                points = foundPawns[0].getMinEnclosingRect().getConvexHull().getAllConvexPoints();
                foreach (Vector2 pointOnLine in points)
                {
                    tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.red);
                }

                line = Assets.Scripts.Functions.detectPixelsOnLine(foundPawns[0].getMinEnclosingRect().debugMostLeft, foundPawns[0].getMinEnclosingRect().debugMostRight);
                foreach (Vector2 pointOnLine in line)
                {
                    tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.cyan);
                }

                tempTexture.Apply();
                
                saveTexture(tempTexture, "calibrationPawnTexture");
                return foundPawns[0];
            }
            else
            {
                Debug.Log("CALIBRATION ERROR");
                return null;
            }
        }
        public static List<Vector2> cameraCalibration(Image<Bgr, byte> img)
        {
            List<VectorOfPoint> contoursFound = new List<VectorOfPoint>();
            List<Vector2> result = new List<Vector2>();

            Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);

            CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);

            Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(colorRange), new Gray(255));

            Img_Source_Gray.Save(Application.dataPath + "/../cameraBinary0.png");
            UMat cannyEdges = new UMat();

            double cannyThreshold = 240.0;
            double cannyThresholdLinking = 140;
            CvInvoke.Canny(Img_Source_Gray, cannyEdges, cannyThreshold, cannyThresholdLinking);

            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple, new Point());

                //Debug.Log(contours.Size);
                for (int i = 0; i < contours.Size; i++)
                {

                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.025, true);

                        for (int j = 0; j < approxContour.Size; j++)
                        {
                            result.Add(new Vector2(approxContour[j].X, approxContour[j].Y));
                        }
                    }
                }
            }


            if (result.Count == 4)
            {
                result = Functions.sortByY(result);
                if (result[0].x > result[1].x)
                {
                    Vector2 temp = result[0];
                    result[0] = result[1];
                    result[1] = temp;

                }
                if (result[2].x < result[3].x)
                {
                    Vector2 temp = result[2];
                    result[2] = result[3];
                    result[3] = temp;
                }
            }
            else if (result.Count > 4)
            {
                Vector2 upperLeft = new Vector2(CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2);
                Vector2 upperRight = new Vector2(CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2);
                Vector2 lowerLeft = new Vector2(CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2);
                Vector2 lowerRight = new Vector2(CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2);
                for (int i = 0; i < result.Count; i++)
                {
                    //UPPER LEFT
                    if (Functions.distanceAB(result[i], new Vector2(0, 0)) < Functions.distanceAB(upperLeft, new Vector2(0, 0)))
                    {
                        upperLeft = result[i];
                    }
                    //UPPER RIGHT
                    if (Functions.distanceAB(result[i], new Vector2(CameraControler.cameraCaptureWidth, 0)) < Functions.distanceAB(upperRight, new Vector2(CameraControler.cameraCaptureWidth, 0)))
                    {
                        upperRight = result[i];
                    }
                    //LOWER RIGHT
                    if (Functions.distanceAB(result[i], new Vector2(CameraControler.cameraCaptureWidth, CameraControler.cameraCaptureHeight)) < Functions.distanceAB(lowerRight, new Vector2(CameraControler.cameraCaptureWidth, CameraControler.cameraCaptureHeight)))
                    {
                        lowerRight = result[i];
                    }
                    //LOWER LEFT
                    if (Functions.distanceAB(result[i], new Vector2(0, CameraControler.cameraCaptureHeight)) < Functions.distanceAB(lowerLeft, new Vector2(0, CameraControler.cameraCaptureHeight)))
                    {
                        lowerLeft = result[i];
                    }
                }
                result = new List<Vector2>();
                result.Add(upperLeft);
                result.Add(upperRight);
                result.Add(lowerRight);
                result.Add(lowerLeft);
            }

            mapArea = Functions.distanceAB_float(result[0], result[1]) * Functions.distanceAB_float(result[1],result[2]);
            // Img_Source_Gray.Save(Application.dataPath + "/../cameraBinary1.png");
            return result;
        }
        public static List<Vector2> cameraCalibration(Texture2D colorTexture)
        {
            List<VectorOfPoint> contoursFound = new List<VectorOfPoint>();
            List<Vector2> result = new List<Vector2>();
            byte[] imageData = colorTexture.EncodeToPNG();
            saveTexture(colorTexture, "calibrationScreen", "");
            //File.WriteAllBytes(Application.dataPath + "/../calibrationScreen.png", imageData);

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }

            Image<Bgr, byte> img = new Image<Bgr, byte>(bitmapData);
            Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);

            CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);

            Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(colorRange), new Gray(255));

            Img_Source_Gray.Save(Application.dataPath+"/../cameraBinary0.png");
            UMat cannyEdges = new UMat();

            double cannyThreshold = 240.0;
            double cannyThresholdLinking = 140;
            CvInvoke.Canny(Img_Source_Gray, cannyEdges, cannyThreshold, cannyThresholdLinking);

            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple, new Point());

                Texture2D ApproxcontoursTexture = new Texture2D(colorTexture.width, colorTexture.height);

                //Debug.Log(contours.Size);
                for (int i = 0; i < contours.Size; i++)
                {
                    
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.025, true);

                             for (int j = 0; j < approxContour.Size; j++)
                             {
                                 ApproxcontoursTexture.SetPixel(approxContour[j].X, ApproxcontoursTexture.height - approxContour[j].Y, new UnityEngine.Color(0, 0, 0));
                                 result.Add(new Vector2(approxContour[j].X, approxContour[j].Y));
                             }
                    }
                }
                ApproxcontoursTexture.Apply();
                saveTexture(ApproxcontoursTexture, "calibrationScreenAprox", "");
            }
            
            
            if (result.Count == 4)
            {
                result = Functions.sortByY(result);
                if (result[0].x > result[1].x)
                {
                    Vector2 temp = result[0];
                    result[0] = result[1];
                    result[1] = temp;

                }
                if (result[2].x < result[3].x)
                {
                    Vector2 temp = result[2];
                    result[2] = result[3];
                    result[3] = temp;
                }
            }
            else if (result.Count > 4)
            {
                Vector2 upperLeft = new Vector2(CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2);
                Vector2 upperRight = new Vector2(CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2);
                Vector2 lowerLeft = new Vector2(CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2);
                Vector2 lowerRight = new Vector2(CameraControler.cameraCaptureWidth / 2, CameraControler.cameraCaptureHeight / 2);
                for (int i = 0; i < result.Count; i++)
                {
                    //UPPER LEFT
                    if (Functions.distanceAB(result[i],new Vector2(0,0)) < Functions.distanceAB(upperLeft,new Vector2(0,0)))
                    {
                        upperLeft = result[i];
                    }
                    //UPPER RIGHT
                    if (Functions.distanceAB(result[i], new Vector2(CameraControler.cameraCaptureWidth, 0)) < Functions.distanceAB(upperRight, new Vector2(CameraControler.cameraCaptureWidth, 0)))
                    {
                        upperRight = result[i];
                    }
                    //LOWER RIGHT
                    if (Functions.distanceAB(result[i], new Vector2(CameraControler.cameraCaptureWidth, CameraControler.cameraCaptureHeight)) < Functions.distanceAB(lowerRight, new Vector2(CameraControler.cameraCaptureWidth, CameraControler.cameraCaptureHeight)))
                    {
                        lowerRight = result[i];
                    }
                    //LOWER LEFT
                    if (Functions.distanceAB(result[i], new Vector2(0, CameraControler.cameraCaptureHeight)) < Functions.distanceAB(lowerLeft, new Vector2(0, CameraControler.cameraCaptureHeight)))
                    {
                        lowerLeft = result[i];
                    }
                }
                result = new List<Vector2>();
                result.Add(upperLeft);
                result.Add(upperRight);
                result.Add(lowerRight);
                result.Add(lowerLeft);
            }
            Texture2D calibFound = new Texture2D(colorTexture.width, colorTexture.height);

            for (int i = 0; i < result.Count; i++)
            {
                calibFound.SetPixel((int)result[i].x, colorTexture.height-(int)result[i].y, UnityEngine.Color.black);
            }
            calibFound.Apply();
            saveTexture(calibFound,"CalibFound","");
                // Img_Source_Gray.Save(Application.dataPath + "/../cameraBinary1.png");
                return result;
        }

        public static bool searchForDetectionTreshold(Texture2D colorTexture, int step,   int numberOfPavnsToFind, CameraControler camera)
        {

            Debug.Log("DETECTING TRESHOLD");
            int numberOfIterations = 255 / step;

            int currentTreshold = 0;

            bool[] resultGraph = new bool[numberOfIterations];
            byte[] imageData = colorTexture.EncodeToPNG();

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }



            

            for (int q = 0; q < numberOfIterations; q++)
            {
                Image<Bgr, byte> img = new Image<Bgr, byte>(bitmapData);
                Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);
                CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);

                colorRange = currentTreshold + step;

                img = img.ThresholdBinary(new Bgr(colorRange, colorRange, colorRange), new Bgr(255, 255, 255));
                img.Save(Application.dataPath + "/../TresholdDetectionBinary"+q+".png");
                Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(colorRange), new Gray(255));
                Img_Source_Gray.Save(Application.dataPath + "/../TresholdDetectionGreyScaleBinary" + q + ".png");

                List<Pawn> result = myDetectFromTextureFunction(colorTexture, camera, true, false);

                if (result.Count == numberOfPavnsToFind)
                {
                    resultGraph[q] = true;
                }
                else
                {
                    resultGraph[q] = false;
                }
                currentTreshold += step;
            }

            int startingIndex = -1;
            int endIndex = -1;
            int maxLength   =  0;
            int tresholdResult  =   0;
            bool lastNode = false;
            for (int i = 0; i < numberOfIterations; i++)
            {
                Debug.Log(resultGraph[i]);
                if (!resultGraph[i])
                {
                    endIndex = i;
                    int length = i - startingIndex;
                    if (length > maxLength)
                    {
                        maxLength = length;
                        //tresholdResult = step * (startingIndex + maxLength / 2);
                        tresholdResult = step * (startingIndex + maxLength);
                    }
                    startingIndex = i;
                }
                else
                {
                    if (!lastNode)
                    {
                        startingIndex = i;
                    }
                }
                lastNode = resultGraph[i];
            }

            colorRange = tresholdResult;
            Debug.Log(colorRange);
            if (tresholdResult > 50 && tresholdResult < 200)
            {
                return true;
            }
            return false;
        }
        public static bool searchForDetectionTreshold(Image<Bgr, byte> img, int step, int numberOfPavnsToFind, CameraControler camera)
        {

            Debug.Log("DETECTING TRESHOLD");
            int numberOfIterations = 255 / step;

            int currentTreshold = 0;

            bool[] resultGraph = new bool[numberOfIterations];




            for (int q = 0; q < numberOfIterations; q++)
            {
                Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);
                CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);

                colorRange = currentTreshold + step;

                Image<Bgr, byte> tresholdImg = img.ThresholdBinary(new Bgr(colorRange, colorRange, colorRange), new Bgr(255, 255, 255));
                tresholdImg.Save(Application.dataPath + "/../TresholdDetectionBinary" + q + ".png");
                Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(colorRange), new Gray(255));
                Img_Source_Gray.Save(Application.dataPath + "/../TresholdDetectionGreyScaleBinary" + q + ".png");

                List<Pawn> result;
                myDetectFromTextureFunction(tresholdImg, camera, true, out result, false);

                if (result.Count == numberOfPavnsToFind)
                {
                    resultGraph[q] = true;
                }
                else
                {
                    resultGraph[q] = false;
                }
                currentTreshold += step;
            }

            int startingIndex = -1;
            int endIndex = -1;
            int maxLength = 0;
            int tresholdResult = 0;
            bool lastNode = false;
            for (int i = 0; i < numberOfIterations; i++)
            {
                Debug.Log(resultGraph[i]);
                if (!resultGraph[i])
                {
                    endIndex = i;
                    int length = i - startingIndex;
                    if (length > maxLength)
                    {
                        maxLength = length;
                        //tresholdResult = step * (startingIndex + maxLength / 2);
                        tresholdResult = step * (startingIndex + maxLength);
                    }
                    startingIndex = i;
                }
                else
                {
                    if (!lastNode)
                    {
                        startingIndex = i;
                    }
                }
                lastNode = resultGraph[i];
            }

            colorRange = tresholdResult;
            Debug.Log(colorRange);
            if (tresholdResult > 50 && tresholdResult < 200)
            {
                return true;
            }
            return false;
        }
        public static float getMinContourSizePropToHeight(float pawnHeight)
        {
            float pawnHDiv = pawnHeight / (float)CameraControler.cameraCaptureHeight;
            if (pawnHDiv < 0.4f)
            {
                return (1.0f / (float)CameraControler.cameraCaptureHeight) * pawnHeight - 0.5f;
            }
            return 0;
        }
            public static float getMaxContourSizePropToHeight(float pawnHeight)
        {

            float pawnHDiv = pawnHeight / (float)CameraControler.cameraCaptureHeight;
            if (pawnHDiv > 0.3f  )//&& pawnHDiv < 0.6f)
            {
                return (1.3f / (float)CameraControler.cameraCaptureHeight) * pawnHeight - pawnHDiv/2;
            }
            /*else if (pawnHDiv >= 0.6f)
            {
                return (1.0f / (float)CameraControler.cameraCaptureHeight) * pawnHeight - 0.4f;
            }*/
            else
            {
                return 0;
            }
            /*if (pawnHDiv < 0.4f)
            {
                return (1f / (float)CameraControler.cameraCaptureHeight) * pawnHeight + 0.5f;
            }
            else
            {
                return (0.6f / (float)CameraControler.cameraCaptureHeight) * pawnHeight + 0.3f;
            }*/
        }
        public static bool myDetectFromTextureFunction(Image<Bgr, byte> img, CameraControler camera, bool detectingPawns, out List<Pawn> result, bool castPawnPosition = true, bool savePawnsTextureToFile = false, string fileNameCount = "1",Image<Gray, byte> backgroundTexture = null)
        {
           result = new List<Pawn>();
            List<Pawn> suspiciousPieces = new List<Pawn>();
            //saveTexture(colorTexture, "SavedScreen3", "");
            //if(backgroundTexture!=null)
            //{
                //Texture2D substractedBackground = substractImages(colorTexture, backgroundTexture, 25);
               // imageData= substractedBackground.EncodeToPNG();
            //}
            //else
            //{
                //imageData = colorTexture.EncodeToPNG();
            //}
            /*File.WriteAllBytes(Application.dataPath + "/../SavedScreen3.png", imageData);*/
            Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);
            

            CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);
            //Img_Source_Gray.Save(Application.dataPath + "/../GreyScale.png");
            
            //double threshValue = 180;

            //img = img.ThresholdBinary(new Bgr(colorRange, colorRange, colorRange), new Bgr(255, 255, 255));
            /*if (detectingPawns)
            {
                img.Save(Application.dataPath + "/../ImageBinary.png");
            }*/


              Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(colorRange), new Gray(255));
            
            //Img_Source_Gray.Save(Application.dataPath + "/../GreyScaleBinary.png");


            
            UMat cannyEdges = new UMat();

            double cannyThreshold = 240;
            double cannyThresholdLinking = 120;
            CvInvoke.Canny(Img_Source_Gray, cannyEdges, cannyThreshold, cannyThresholdLinking);


            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple, new Point());



                Texture2D tooBigPawns = convertImageToTexture(img);
                for (int i = 0; i < contours.Size; i++)
                {
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.01, true);



                        //Debug.Log(colorVal);
                       
                        if (approxContour.Size > 2)
                        {
                           
                            Pawn newPawn = new Pawn(EmguPointToUnityPoint(approxContour,camera), camera);

                            //if (newPawn.getPawnPosition().x > 15 && newPawn.getPawnPosition().x < CameraControler.cameraCaptureWidth - 15 && newPawn.getPawnPosition().y > 15 && newPawn.getPawnPosition().y < CameraControler.cameraCaptureHeight - 15)
                            List<Vector2> cameraCorners = camera.getCorners();
                            bool allCornersInCameraView = true;
                            
                            /*foreach (Vector2 corner in newPawn.getMinEnclosingRect().getMinRectCorners())
                            {
                                if(!Functions.PointInRectangle(cameraCorners[0], cameraCorners[1], cameraCorners[2], cameraCorners[3], corner))
                                {
                                    allCornersInCameraView = false;
                                }
                            }*/
                            if (Functions.PointInRectangle(cameraCorners[0], cameraCorners[1], cameraCorners[2], cameraCorners[3], newPawn.getPawnPosition()) && allCornersInCameraView)
                            {
                                //Debug.Log((newPawn.getMinRectArea() > contourAreaMin) + " " + (newPawn.getMinRectArea() < contourAreaMax + contourAreaMax * getMaxContourSizePropToHeight(newPawn.getPawnPosition().y)));
                                //Debug.Log(newPawn.getPawnPosition().y + " " + (contourAreaMax * getMaxContourSizePropToHeight(newPawn.getPawnPosition().y)));

                                if (newPawn.getMinRectArea() >= contourAreaMin + contourAreaMin*getMinContourSizePropToHeight(newPawn.getPawnPosition().y) && newPawn.getMinRectArea() <= contourAreaMax + contourAreaMax * getMaxContourSizePropToHeight(newPawn.getPawnPosition().y))
                                //if (Functions.CircleArea(newPawn.myCollider) > contourAreaMin && Functions.CircleArea(newPawn.myCollider) < contourAreaMax*1.8f)
                                {
                                    if (!newPawn.isRectangular())
                                    {
                                        suspiciousPieces.Add(newPawn);
                                    }
                                    else
                                    {
                                        //Debug.Log("Contour: " + i + " has desired position");
                                        if (result.Count > 0)
                                        {
                                            float thisHeightPawnWidth = Mathf.Sqrt(contourAreaMax + contourAreaMax * getMaxContourSizePropToHeight(newPawn.getPawnPosition().y));

                                            bool flager = false;
                                            for (int z = 0; z < result.Count; z++)
                                            {
                                                if (Assets.Scripts.Functions.testRectsForIntersection(result[z].getMinEnclosingRect(), newPawn.getMinEnclosingRect())
                                                    ||
                                                    Functions.distanceAB(result[z].getPawnPosition(), newPawn.getPawnPosition()) < thisHeightPawnWidth
                                                    )
                                                {
                                                    //Debug.Log("NEW SPOT OVERLAPS PREAVIOUS PAWN: "+z);
                                                    //Pawn tempPawn = new Pawn(createBoundsFromPoints(approxContour));
                                                    List<Vector2> newVErtices = newPawn.getAllConvexHullPoints();
                                                    newVErtices.AddRange(result[z].getAllConvexHullPoints());
                                                    Pawn tempPawn = new Pawn(newVErtices, camera);
                                                    //tempPawn.setMinRectFromPoints(newVErtices);
                                                    //Debug.Log((tempPawn.getMinRectArea() > contourAreaMax + contourAreaMax * getMaxContourSizePropToHeight(tempPawn.getPawnPosition().y)) +" "+
                                                        //!tempPawn.isRectangular());
                                                    if (tempPawn.getMinRectArea() > contourAreaMax + contourAreaMax * getMaxContourSizePropToHeight(tempPawn.getPawnPosition().y))
                                                    {
                                                        //.Log(tempPawn.getPawnPosition().y + " " + (contourAreaMax * getMaxContourSizePropToHeight(tempPawn.getPawnPosition().y)));
                                                        //Debug.Log("Pawn Too Big!! Slicing");

                                                        if (savePawnsTextureToFile)
                                                        {
                                                            Vector2[] corners = tempPawn.getMinEnclosingRect().getMinRectCorners();
                                                            List<Vector2> line = new List<Vector2>();
                                                            for (int cornerIndexes = 0; cornerIndexes < corners.Length - 1; cornerIndexes++)
                                                            {
                                                                line = Assets.Scripts.Functions.detectPixelsOnLine(corners[cornerIndexes], corners[cornerIndexes + 1]);
                                                                foreach (Vector2 pointOnLine in line)
                                                                {
                                                                    tooBigPawns.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                                                                }

                                                            }
                                                            line = Assets.Scripts.Functions.detectPixelsOnLine(corners[0], corners[corners.Length - 1]);
                                                            tooBigPawns.Apply();
                                                        }
                                                        result.Add(newPawn);
                                                    }
                                                    else if (tempPawn.isRectangular())
                                                    {
                                                        //Debug.Log("Pawn isn't too BIG. ReWriting with new data");
                                                        result[z] = tempPawn;
                                                    }

                                                    flager = true;
                                                    break;
                                                }
                                            }
                                            if (!flager)
                                            {
                                                //Debug.Log("Adding to result List Contour: "+ i + " no overlaping item");
                                                result.Add(newPawn);
                                            }
                                        }
                                        else
                                        {
                                            //Debug.Log("Adding to result List Contour: " + i + ": first item");
                                            result.Add(newPawn);
                                        }
                                    }
                                }
                                else if (newPawn.getMinRectArea() > contourAreaMin * 0.05f && newPawn.getMinRectArea() < contourAreaMax + contourAreaMax * getMaxContourSizePropToHeight(newPawn.getPawnPosition().y))
                                {
                                    //Debug.Log("Possible Pawn");
                                    suspiciousPieces.Add(newPawn);
                                }
                                else if (newPawn.getMinRectArea() > contourAreaMax * 2.0f && newPawn.getMinRectArea() < mapArea / 2)
                                {
                                    if (savePawnsTextureToFile) {
                                        
                                        Texture2D tempTexture2 = convertImageToTexture(img);

                                        #region saveTexture

                                        Vector2[] corners = newPawn.getMinEnclosingRect().getMinRectCorners();
                                        List<Vector2> line = new List<Vector2>();
                                        //DRAW MINRECT
                                        for (int z = 0; z < corners.Length - 1; z++)
                                        {
                                            line = Assets.Scripts.Functions.detectPixelsOnLine(corners[z], corners[z + 1]);
                                            foreach (Vector2 pointOnLine in line)
                                            {
                                                tempTexture2.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                                            }

                                        }
                                        line = Assets.Scripts.Functions.detectPixelsOnLine(corners[0], corners[corners.Length - 1]);
                                        foreach (Vector2 pointOnLine in line)
                                        {
                                            tempTexture2.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                                        }

                                        //DRAW MIDLINE
                                        line = Assets.Scripts.Functions.detectPixelsOnLine(newPawn.getMinEnclosingRect().debugMostLeft, newPawn.getMinEnclosingRect().debugMostRight);
                                        foreach (Vector2 pointOnLine in line)
                                        {
                                            tempTexture2.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.cyan);
                                        }

                                        List<Vector2> points = newPawn.getMinEnclosingRect().getConvexHull().getConvexHullPoints();
                                        //DRAW CONVEX
                                        for (int z = 0; z < points.Count - 1; z++)
                                        {
                                            line = Assets.Scripts.Functions.detectPixelsOnLine(points[z], points[z + 1]);
                                            foreach (Vector2 pointOnLine in line)
                                            {
                                                tempTexture2.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.green);
                                            }

                                        }
                                        line = Assets.Scripts.Functions.detectPixelsOnLine(points[0], points[points.Count - 1]);
                                        foreach (Vector2 pointOnLine in line)
                                        {
                                            tempTexture2.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.green);
                                        }

                                        //DRAW ALL POINTS
                                        points = newPawn.getMinEnclosingRect().getConvexHull().getAllConvexPoints();
                                        foreach (Vector2 pointOnLine in points)
                                        {
                                            tempTexture2.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.red);
                                        }
                                    

                                        tempTexture2.Apply();
                                    #endregion

                                        saveTexture(tempTexture2, "pawnErrorSketch"+fileNameCount);
                                    }
                                    Debug.Log("Too big contour detected inside camera view, probably hand. Area: "+ newPawn.getMinRectArea());
                                    return false;
                                }
                                else
                                {
                                    if (savePawnsTextureToFile)
                                    {
                                        Vector2[] corners = newPawn.getMinEnclosingRect().getMinRectCorners();
                                        List<Vector2> line = new List<Vector2>();
                                        for (int cornerIndexes = 0; cornerIndexes < corners.Length - 1; cornerIndexes++)
                                        {
                                            line = Assets.Scripts.Functions.detectPixelsOnLine(corners[cornerIndexes], corners[cornerIndexes + 1]);
                                            foreach (Vector2 pointOnLine in line)
                                            {
                                                tooBigPawns.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                                            }

                                        }
                                        line = Assets.Scripts.Functions.detectPixelsOnLine(corners[0], corners[corners.Length - 1]);
                                        tooBigPawns.Apply();
                                    }
                                }
                            }
                            
                        }
                        /*if (approxContour.Size == 4)
                        {
                            Debug.Log("Calibration Successful");

                        }*/

                    }
                }
                if (savePawnsTextureToFile)
                {
                    saveTexture(tooBigPawns, "TooBigPawns" + fileNameCount);
                }
                /*ApproxcontoursData = ApproxcontoursTexture.EncodeToPNG();
            
            File.WriteAllBytes(Application.dataPath + "/../Approxcontours.png", ApproxcontoursData);*/
            }
            
            if (detectingPawns)
            {

                float maxdistance = Mathf.Sqrt(contourAreaMax);

                //TEST IF SUSPICIOUS PIECES FORM BIGGER AREAS ----- IF SO SUM THEM
                for (int index = 0; index < suspiciousPieces.Count; index++)
                {
                    for (int tempIndex = index + 1; tempIndex < suspiciousPieces.Count; tempIndex++)
                    {
                        if (Assets.Scripts.Functions.testRectsForIntersection(suspiciousPieces[index].getMinEnclosingRect(), suspiciousPieces[tempIndex].getMinEnclosingRect()) ||
                            Functions.distanceAB(suspiciousPieces[index].getPawnPosition(), suspiciousPieces[tempIndex].getPawnPosition()) < maxdistance)
                        {
                            //Debug.Log("2 PossiblePawns in near area");
                            List<Vector2> newVErtices = suspiciousPieces[index].getAllConvexHullPoints();
                            newVErtices.AddRange(suspiciousPieces[tempIndex].getAllConvexHullPoints());
                            Pawn tempPawn = new Pawn(newVErtices, camera);

                            if (tempPawn.getMinRectArea() < contourAreaMax + contourAreaMax * getMaxContourSizePropToHeight(tempPawn.getPawnPosition().y) && tempPawn.isRectangular())
                            {
                                //Debug.Log("possiblePawns form bigger area");
                                suspiciousPieces[index] = tempPawn;
                                suspiciousPieces.Remove(suspiciousPieces[tempIndex]);
                                //index--;
                                //break;
                                tempIndex--;
                            }
                        }

                    }
                }

                //TEST IF SUSPICIOUS PIECE INTERSECTS ANY PAWN OR ARE CLOSE ENOUGH TO OTHER EXISTING PAWN TO EXTEND HIM
                for (int index = 0; index < suspiciousPieces.Count; index++)
                {
                    double minDistance = CameraControler.cameraCaptureWidth;
                    int minMatchDistanceIndex = -1;
                    maxdistance =  Mathf.Sqrt(contourAreaMax) + Mathf.Sqrt(contourAreaMax) * getMaxContourSizePropToHeight(suspiciousPieces[index].getPawnPosition().y);
                    for (int j = 0; j < result.Count; j++)
                    {
                        double currentMinDistance = Functions.distanceAB(suspiciousPieces[index].getPawnPosition(), result[j].getPawnPosition());
                        if (Assets.Scripts.Functions.testRectsForIntersection(suspiciousPieces[index].getMinEnclosingRect(), result[j].getMinEnclosingRect())
                            ||
                            currentMinDistance < maxdistance)
                        {
                            //Debug.Log("PossiblePawn intersects existing pawn");
                            List<Vector2> newVErtices = suspiciousPieces[index].getAllConvexHullPoints();
                            newVErtices.AddRange(result[j].getAllConvexHullPoints());
                            Pawn tempPawn = new Pawn(newVErtices, camera);

                            if (tempPawn.getMinRectArea() < contourAreaMax + contourAreaMax * getMaxContourSizePropToHeight(tempPawn.getPawnPosition().y) && tempPawn.isRectangular())
                            {
                                if (currentMinDistance < minDistance)
                                {
                                    minMatchDistanceIndex = j;
                                    minDistance = currentMinDistance;
                                }
                            }
                            else
                            {
                                //Debug.Log("Pawn Too BIG");
                            }
                        }
                    }
                    if (minMatchDistanceIndex != -1)
                    {
                        List<Vector2> newVErtices = suspiciousPieces[index].getAllConvexHullPoints();
                        newVErtices.AddRange(result[minMatchDistanceIndex].getAllConvexHullPoints());
                        Pawn tempPawn = new Pawn(newVErtices, camera);
                        result[minMatchDistanceIndex] = tempPawn;
                        suspiciousPieces.Remove(suspiciousPieces[index]);
                        index--;
                    }
                    else
                    {
                        if (suspiciousPieces[index].getMinRectArea() < contourAreaMax + contourAreaMax * getMaxContourSizePropToHeight(suspiciousPieces[index].getPawnPosition().y)
                        && suspiciousPieces[index].getMinRectArea() > contourAreaMin + contourAreaMin * getMinContourSizePropToHeight(suspiciousPieces[index].getPawnPosition().y)
                        && suspiciousPieces[index].isRectangular())


                        {
                            //Debug.Log("Adding sus pawn to results. Results count: " + result.Count + "  susp count: " + suspiciousPieces.Count);
                            result.Add(suspiciousPieces[index]);
                            //Debug.Log("Removing sus pawn from suspList. Results count: " + result.Count + "  susp count: " + suspiciousPieces.Count);
                            suspiciousPieces.Remove(suspiciousPieces[index]);
                            //Debug.Log("Results count: " + result.Count + "  susp count: " + suspiciousPieces.Count);
                            index--;
                        }
                    }
                }

               


                
                //TEST IF SUSPICIOUS PAWNS MIGHT BE A PAWN ALREADY
                //Debug.Log("Sus list count: " + suspiciousPieces.Count);
                /*for (int index = 0; index < suspiciousPieces.Count; index++)
                {
                    //Debug.Log((suspiciousPieces[index].getMinRectArea() < contourAreaMax * 1.3f) + "   " + (suspiciousPieces[index].getMinRectArea() > contourAreaMin * 0.8f) + "   " + suspiciousPieces[index].isRectangular());
                    if (suspiciousPieces[index].getMinRectArea() < contourAreaMax + contourAreaMax * getMaxContourSizePropToHeight(suspiciousPieces[index].getPawnPosition().y)
                        && suspiciousPieces[index].getMinRectArea() > contourAreaMin + contourAreaMin * getMinContourSizePropToHeight(suspiciousPieces[index].getPawnPosition().y)
                        && suspiciousPieces[index].isRectangular())


                    {
                        //Debug.Log("Adding sus pawn to results. Results count: " + result.Count + "  susp count: " + suspiciousPieces.Count);
                        result.Add(suspiciousPieces[index]);
                        //Debug.Log("Removing sus pawn from suspList. Results count: " + result.Count + "  susp count: " + suspiciousPieces.Count);
                        suspiciousPieces.Remove(suspiciousPieces[index]);
                        //Debug.Log("Results count: " + result.Count + "  susp count: " + suspiciousPieces.Count);
                        index--;
                    }
                }*/
                
                /*//TEST IF ANY OF SUSPICIOUS PAWNS ARE CLOSE ENOUGH TO OTHER EXISTING PAWN TO EXTEND HIM
                for (int index = 0; index < result.Count; index++)
                {
                    for (int indexP = 0; indexP < suspiciousPieces.Count; indexP++)
                    {
                        if (Functions.distanceAB(suspiciousPieces[indexP].getPawnPosition(), suspiciousPieces[index].getPawnPosition()) < maxdistance)
                        {
                            List<Vector2> newVErtices = suspiciousPieces[indexP].getAllConvexHullPoints();
                            newVErtices.AddRange(result[index].getAllConvexHullPoints());
                            Pawn tempPawn = new Pawn(newVErtices);

                            if (tempPawn.getMinRectArea() < contourAreaMax * 1.4f && tempPawn.isRectangular())
                            {
                                //Debug.Log("PossiblePawn extends pawn");
                                result[index] = tempPawn;
                                suspiciousPieces.Remove(suspiciousPieces[indexP]);
                                indexP--;
                            }
                        }
                    }
                }*/
            }
            Debug.Log("ColorRange: "+ colorRange+"   NORMAL PAWNS : " + result.Count + "        possible pawns  " + suspiciousPieces.Count);
           
            if (castPawnPosition) { 
                foreach (Pawn p in result)
                {
                    p.getMinEnclosingRect().checkCorners();
                    //p.castCentrePositionToScreen(camera);
                    
                }
            }

            #region debugTextureTruePawns
            if (savePawnsTextureToFile)
            {
                Texture2D tempTexture = convertImageToTexture(img);
                foreach (Pawn foundPawn in result)
                {
                    Vector2[] corners = foundPawn.getMinEnclosingRect().getMinRectCorners();
                    List<Vector2> line = new List<Vector2>();
                    for (int z = 0; z < corners.Length - 1; z++)
                    {
                        line = Assets.Scripts.Functions.detectPixelsOnLine(corners[z], corners[z + 1]);
                        foreach (Vector2 pointOnLine in line)
                        {
                            tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                        }

                    }
                    line = Assets.Scripts.Functions.detectPixelsOnLine(corners[0], corners[corners.Length - 1]);
                    foreach (Vector2 pointOnLine in line)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                    }

                    List<Vector2> points = foundPawn.getMinEnclosingRect().getConvexHull().getConvexHullPoints();

                    for (int z = 0; z < points.Count - 1; z++)
                    {
                        line = Assets.Scripts.Functions.detectPixelsOnLine(points[z], points[z + 1]);
                        foreach (Vector2 pointOnLine in line)
                        {
                            tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.green);
                        }

                    }
                    line = Assets.Scripts.Functions.detectPixelsOnLine(points[0], points[points.Count - 1]);
                    foreach (Vector2 pointOnLine in line)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.green);
                    }

                    points = foundPawn.getMinEnclosingRect().getConvexHull().getAllConvexPoints();
                    foreach (Vector2 pointOnLine in points)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.red);
                    }

                    line = Assets.Scripts.Functions.detectPixelsOnLine(foundPawn.getMinEnclosingRect().debugMostLeft, foundPawn.getMinEnclosingRect().debugMostRight);
                    foreach (Vector2 pointOnLine in line)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.cyan);
                    }

                }
                tempTexture.Apply();

                saveTexture(tempTexture, "pawnDetectionSketch"+ fileNameCount);
                #endregion

                #region debugTextureSuspiciousPawns
                tempTexture = convertImageToTexture(img);
                foreach (Pawn foundPawn in suspiciousPieces)
                {
                    Vector2[] corners = foundPawn.getMinEnclosingRect().getMinRectCorners();
                    List<Vector2> line = new List<Vector2>();
                    for (int z = 0; z < corners.Length - 1; z++)
                    {
                        line = Assets.Scripts.Functions.detectPixelsOnLine(corners[z], corners[z + 1]);
                        foreach (Vector2 pointOnLine in line)
                        {
                            tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                        }

                    }
                    line = Assets.Scripts.Functions.detectPixelsOnLine(corners[0], corners[corners.Length - 1]);
                    foreach (Vector2 pointOnLine in line)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                    }

                    List<Vector2> points = foundPawn.getMinEnclosingRect().getConvexHull().getConvexHullPoints();

                    for (int z = 0; z < points.Count - 1; z++)
                    {
                        line = Assets.Scripts.Functions.detectPixelsOnLine(points[z], points[z + 1]);
                        foreach (Vector2 pointOnLine in line)
                        {
                            tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.green);
                        }

                    }
                    line = Assets.Scripts.Functions.detectPixelsOnLine(points[0], points[points.Count - 1]);
                    foreach (Vector2 pointOnLine in line)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.green);
                    }

                    points = foundPawn.getMinEnclosingRect().getConvexHull().getAllConvexPoints();
                    foreach (Vector2 pointOnLine in points)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.red);
                    }

                    line = Assets.Scripts.Functions.detectPixelsOnLine(foundPawn.getMinEnclosingRect().debugMostLeft, foundPawn.getMinEnclosingRect().debugMostRight);
                    foreach (Vector2 pointOnLine in line)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.cyan);
                    }

                }
                tempTexture.Apply();

                saveTexture(tempTexture, "SuspiciousPawnsDetectionSketch"+ fileNameCount);
            }
            #endregion

            return true;
        }
        public static List<Pawn> myDetectFromTextureFunction(Texture2D colorTexture, CameraControler camera, bool detectingPawns, bool castPawnPosition = true, Texture2D backgroundTexture = null)
        {
            List<Pawn> result = new List<Pawn>();
            List<Pawn> suspiciousPieces = new List<Pawn>();
            //saveTexture(colorTexture, "SavedScreen3", "");
            byte[] imageData;
            if (backgroundTexture != null)
            {
                Texture2D substractedBackground = substractImages(colorTexture, backgroundTexture, 25);
                imageData = substractedBackground.EncodeToPNG();
            }
            else
            {
                imageData = colorTexture.EncodeToPNG();
            }
            /*File.WriteAllBytes(Application.dataPath + "/../SavedScreen3.png", imageData);*/

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }

            Image<Bgr, byte> img = new Image<Bgr, byte>(bitmapData);
            Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);


            CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);
            //Img_Source_Gray.Save(Application.dataPath + "/../GreyScale.png");

            //double threshValue = 180;

            img = img.ThresholdBinary(new Bgr(colorRange, colorRange, colorRange), new Bgr(255, 255, 255));
            if (detectingPawns)
            {
                img.Save(Application.dataPath + "/../ImageBinary.png");
            }


            Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(colorRange), new Gray(255));

            Img_Source_Gray.Save(Application.dataPath + "/../GreyScaleBinary.png");



            UMat cannyEdges = new UMat();

            double cannyThreshold = 240;
            double cannyThresholdLinking = 120;
            CvInvoke.Canny(Img_Source_Gray, cannyEdges, cannyThreshold, cannyThresholdLinking);


            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple, new Point());



                for (int i = 0; i < contours.Size; i++)
                {
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.025, true);


                        float colorVal = Random.value * 255;
                        //Debug.Log(colorVal);

                        if (approxContour.Size > 3)
                        {

                            Pawn newPawn = new Pawn(EmguPointToUnityPoint(approxContour, camera), camera);

                            if (newPawn.getMinRectArea() > contourAreaMin && newPawn.getMinRectArea() < contourAreaMax)
                            //if (Functions.CircleArea(newPawn.myCollider) > contourAreaMin && Functions.CircleArea(newPawn.myCollider) < contourAreaMax*1.8f)
                            {

                                if (newPawn.getPawnPosition().x > 10 && newPawn.getPawnPosition().x < CameraControler.cameraCaptureWidth - 10 && newPawn.getPawnPosition().y > 5 && newPawn.getPawnPosition().y < CameraControler.cameraCaptureHeight - 5)
                                {
                                    //Debug.Log("Contour: " + i + " has desired position");
                                    if (result.Count > 0)
                                    {
                                        bool flager = false;
                                        for (int z = 0; z < result.Count; z++)
                                        {

                                            if (Assets.Scripts.Functions.testRectsForIntersection(result[z].getMinEnclosingRect(), newPawn.getMinEnclosingRect()))
                                            {
                                                //Debug.Log("NEW SPOT OVERLAPS PREAVIOUS PAWN: "+z);
                                                //Pawn tempPawn = new Pawn(createBoundsFromPoints(approxContour));
                                                List<Vector2> newVErtices = EmguPointToUnityPoint(approxContour, camera);
                                                newVErtices.AddRange(result[z].getAllConvexHullPoints());
                                                Pawn tempPawn = new Pawn(newVErtices, camera);
                                                //tempPawn.setMinRectFromPoints(newVErtices);

                                                if (tempPawn.getMinRectArea() > contourAreaMax * 1.5f)
                                                {
                                                    //Debug.Log("Pawn Too Big!! Slicing.");
                                                    result.Add(newPawn);
                                                }
                                                else
                                                {
                                                    //Debug.Log("Pawn isn't too BIG. ReWriting with new data");
                                                    result[z] = tempPawn;
                                                }

                                                flager = true;
                                                break;
                                            }
                                        }
                                        if (!flager)
                                        {
                                            //Debug.Log("Adding to result List Contour: "+ i + " no overlaping item");
                                            result.Add(newPawn);
                                        }
                                    }
                                    else
                                    {
                                        //Debug.Log("Adding to result List Contour: " + i + ": first item");
                                        result.Add(newPawn);
                                    }
                                }
                            }
                            else if (newPawn.getMinRectArea() > contourAreaMin * 0.1f && newPawn.getMinRectArea() < contourAreaMax)
                            {
                                //Debug.Log("Possible Pawn");
                                suspiciousPieces.Add(newPawn);
                            }
                        }
                        /*if (approxContour.Size == 4)
                        {
                            Debug.Log("Calibration Successful");

                        }*/

                    }
                }
                /*ApproxcontoursData = ApproxcontoursTexture.EncodeToPNG();
                File.WriteAllBytes(Application.dataPath + "/../Approxcontours.png", ApproxcontoursData);*/
            }
            if (detectingPawns)
            {
                //TEST IF SUSPICIOUS PIECES FORM BIGGER AREAS ----- IF SO SUM THEM
                float maxdistance = Mathf.Sqrt(contourAreaMax) * 2;
                for (int index = 0; index < suspiciousPieces.Count; index++)
                {
                    int tempIndex = index + 1;

                    for (tempIndex = index + 1; tempIndex < suspiciousPieces.Count; tempIndex++)
                    {
                        if (Assets.Scripts.Functions.testRectsForIntersection(suspiciousPieces[index].getMinEnclosingRect(), suspiciousPieces[tempIndex].getMinEnclosingRect()) ||
                            Functions.distanceAB(suspiciousPieces[index].getPawnPosition(), suspiciousPieces[tempIndex].getPawnPosition()) < maxdistance)
                        {
                            //Debug.Log("2 PossiblePawns in near area");
                            List<Vector2> newVErtices = suspiciousPieces[index].getAllConvexHullPoints();
                            newVErtices.AddRange(suspiciousPieces[tempIndex].getAllConvexHullPoints());
                            Pawn tempPawn = new Pawn(newVErtices, camera);

                            if (tempPawn.getMinRectArea() < contourAreaMax * 2.0f)
                            {
                                //Debug.Log("possiblePawns form bigger area");
                                suspiciousPieces[index] = tempPawn;
                                suspiciousPieces.Remove(suspiciousPieces[tempIndex]);
                                index--;
                                break;
                                //tempIndex--;
                            }
                        }

                    }
                }


                //TEST IF SUSPICIOUS PIECE INTERSECTS ANY PAWN
                for (int index = 0; index < suspiciousPieces.Count; index++)
                {
                    for (int j = 0; j < result.Count; j++)
                    {
                        if (Assets.Scripts.Functions.testRectsForIntersection(suspiciousPieces[index].getMinEnclosingRect(), result[j].getMinEnclosingRect()))
                        {
                            //Debug.Log("PossiblePawn intersects existing pawn");
                            List<Vector2> newVErtices = suspiciousPieces[index].getAllConvexHullPoints();
                            newVErtices.AddRange(result[j].getAllConvexHullPoints());
                            Pawn tempPawn = new Pawn(newVErtices, camera);

                            if (tempPawn.getMinRectArea() < contourAreaMax * 2.0f)
                            {
                                //Debug.Log("PossiblePawn extends pawn");
                                result[j] = tempPawn;
                                suspiciousPieces.Remove(suspiciousPieces[index]);
                                index--;
                                break;
                            }
                            else
                            {
                                //Debug.Log("Pawn Too BIG");
                            }
                        }
                    }
                }
                //TEST IF SUSPICIOUS PAWNS MIGHT BE A PAWN ALREADY
                for (int index = 0; index < suspiciousPieces.Count; index++)
                {
                    if (suspiciousPieces[index].getMinRectArea() < contourAreaMax * 2.0f && suspiciousPieces[index].getMinRectArea() > contourAreaMin)
                    {
                        result.Add(suspiciousPieces[index]);
                    }
                }
            }
            Debug.Log("ColorRange: " + colorRange + "   NORMAL PAWNS : " + result.Count + "        possible pawns  " + suspiciousPieces.Count);

            if (castPawnPosition)
            {
                foreach (Pawn p in result)
                {
                    p.getMinEnclosingRect().checkCorners();
                    //p.castCentrePositionToScreen(camera);
                }
            }

            Texture2D tempTexture = convertImageToTexture(img);
            foreach (Pawn foundPawn in result)
            {
                Vector2[] corners = foundPawn.getMinEnclosingRect().getMinRectCorners();
                List<Vector2> line = new List<Vector2>();
                for (int z = 0; z < corners.Length - 1; z++)
                {
                    line = Assets.Scripts.Functions.detectPixelsOnLine(corners[z], corners[z + 1]);
                    foreach (Vector2 pointOnLine in line)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                    }

                }
                line = Assets.Scripts.Functions.detectPixelsOnLine(corners[0], corners[corners.Length - 1]);
                foreach (Vector2 pointOnLine in line)
                {
                    tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.magenta);
                }

                List<Vector2> points = foundPawn.getMinEnclosingRect().getConvexHull().getConvexHullPoints();

                for (int z = 0; z < points.Count - 1; z++)
                {
                    line = Assets.Scripts.Functions.detectPixelsOnLine(points[z], points[z + 1]);
                    foreach (Vector2 pointOnLine in line)
                    {
                        tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.green);
                    }

                }
                line = Assets.Scripts.Functions.detectPixelsOnLine(points[0], points[points.Count - 1]);
                foreach (Vector2 pointOnLine in line)
                {
                    tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.green);
                }

                points = foundPawn.getMinEnclosingRect().getConvexHull().getAllConvexPoints();
                foreach (Vector2 pointOnLine in points)
                {
                    tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.red);
                }

                line = Assets.Scripts.Functions.detectPixelsOnLine(foundPawn.getMinEnclosingRect().debugMostLeft, foundPawn.getMinEnclosingRect().debugMostRight);
                foreach (Vector2 pointOnLine in line)
                {
                    tempTexture.SetPixel((int)pointOnLine.x, CameraControler.cameraCaptureHeight - (int)pointOnLine.y, UnityEngine.Color.cyan);
                }

            }
            tempTexture.Apply();

            saveTexture(tempTexture, "calibrationPawnTexture");

            return result;
        }
        static List<Vector2> EmguPointToUnityPoint(VectorOfPoint input, CameraControler camera)
        {
            //Debug.Log("RECTANGLEDETECTION CONVERT LISTS size: " + input.Size);

            List<Vector2> output = new List<Vector2>();
            for (int i = 0; i < input.Size; i++)
            {
                output.Add(new Vector2(input[i].X, input[i].Y));
            }
            return output;
        }
        static List<Vector2> EmguPointToUnityPoint(VectorOfPoint input)
        {
            //Debug.Log("RECTANGLEDETECTION CONVERT LISTS size: " + input.Size);

            List<Vector2> output = new List<Vector2>();
            for (int i = 0; i < input.Size; i++)
            {
                output.Add(new Vector2(input[i].X, input[i].Y));
            }
            return output;

        }
        static List<Pawn> checkPawns(List<Pawn> inputList)
        {
            List<Pawn> outputList = new List<Pawn>();
            int i=0;
            while(i<inputList.Count){

                //if(inputList[i].boundsSize() >)
                i++;
            }

            return outputList;
        }
        static void saveTexture(Texture2D textureToSave, string fileName, string optionalFolder = "")
        {
            if (saveTextures)
            {
                byte[] textureByteData = textureToSave.EncodeToPNG();
                if (!string.IsNullOrEmpty(optionalFolder))
                {
                    File.WriteAllBytes(Application.dataPath + "/../" + optionalFolder +"/" +fileName + ".png", textureByteData);
                }
                else
                {
                    File.WriteAllBytes(Application.dataPath + "/../"+fileName + ".png", textureByteData);

                }
            }

        }
        static Bounds createBoundsFromPoints(VectorOfPoint input)
        {
            Vector3[] newVertices = new Vector3[input.Size];
            Vector2[] newTriangleVertices = new Vector2[input.Size];
            for (int pointNumber = 0; pointNumber < input.Size; pointNumber++)
            {
                newVertices[pointNumber] = new Vector3(input[pointNumber].X, input[pointNumber].Y, 0);
                newTriangleVertices[pointNumber] = new Vector2(input[pointNumber].X, input[pointNumber].Y);
            }
            Assets.Scripts.Usefull.Triangulator triangulator = new Scripts.Usefull.Triangulator(newTriangleVertices);
            int[] newTriangles = triangulator.Triangulate();

            Mesh mesh = new Mesh();
            mesh.vertices = newVertices;
            mesh.triangles = newTriangles;

            return mesh.bounds;
        }
        static Rect extendRectByAnother(Rect A, Rect B)
        {
            if(A.xMin > B.xMin)
            {
                A.xMin = B.xMin;
            }
            if(A.xMax < B.xMax)
            {
                A.xMax = B.xMax;
            }
            if(A.yMin > B.yMin)
            {
                A.yMin = B.yMin;
            }
            if(A.yMax < B.yMax)
            {
                A.yMax = B.yMax;
            }

            return A;
        }
        static void splitRect(Rect input, out Rect A, out Rect B)
        {
            A = new Rect();
            B = new Rect();
            
        }
        static Rect findRectByEdgePoints(VectorOfPoint input)
        {

            Vector2 topLeft = new Vector2(input[0].X, input[0].Y);
            Vector2 bottomRight = new Vector2(input[0].X, input[0].Y);

            for (int i = 1; i < input.Size; i++)
            {
                if (input[i].X < topLeft.x)
                {
                    topLeft.x = input[i].X;
                } 
                if (input[i].X > bottomRight.x)
                {
                    bottomRight.x = input[i].X;
                } 
                if (input[i].Y < topLeft.y)
                {
                    topLeft.y = input[i].Y;
                }
                if (input[i].Y > bottomRight.y)
                {
                    bottomRight.y = input[i].Y;
                }
            }
           //Log(new Rect(topLeft.x, topLeft.y, bottomRight.x-topLeft.x, bottomRight.y-topLeft.y));
            return new Rect(topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);
        }
        public static List<Rect> detectFromTexture(Texture2D colorTexture)
        {
            List<Rect> result = new List<Rect>();
            //Load the image from file and resize it for display
           // Image<Bgr, byte> img = new Image<Bgr, byte>("Assets/Models/kwadraty.png");

            byte[] imageData = colorTexture.EncodeToPNG();
            //byte[] imageData = colorTexture.EncodeToJPG();
            //byte[] imageData = colorTexture.GetRawTextureData();
            File.WriteAllBytes(Application.dataPath + "/../SavedScreen2.png", imageData);

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }

            Image<Bgr, byte> img = new Image<Bgr, byte>(bitmapData);
            

            //Convert the image to grayscale and filter out the noise
            UMat uimage = new UMat();
            CvInvoke.CvtColor(img, uimage, ColorConversion.Bgr2Gray);
            //CvInvoke.CvtColor(img, uimage, ColorConversion.Rgba2Gray);

            //use image pyr to remove noise
            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(uimage, pyrDown);
            CvInvoke.PyrUp(pyrDown, uimage);

            //region circle detection
            double cannyThreshold = 240;
            double circleAccumulatorThreshold = 120;
            CircleF[] circles = CvInvoke.HoughCircles(uimage, HoughType.Gradient, 2.0, 20.0, cannyThreshold, circleAccumulatorThreshold, 5);

            //region Canny and edge detection
            double cannyThresholdLinking = 5.0;
            UMat cannyEdges = new UMat();
            CvInvoke.Canny(uimage, cannyEdges, cannyThreshold, cannyThresholdLinking);

            LineSegment2D[] lines = CvInvoke.HoughLinesP(
               cannyEdges,
               1, //Distance resolution in pixel-related units
               Mathf.PI / 45.0, //Angle resolution measured in radians.
               20, //threshold
               30, //min Line width
               10); //gap between lines


            //#region Find triangles and rectangles
            List<Triangle2DF> triangleList = new List<Triangle2DF>();
            List<RotatedRect> boxList = new List<RotatedRect>();

            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple, new Point());
                int count = contours.Size;
                for (int i = 0; i < count; i++)
                {
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.025, true);
                        if (CvInvoke.ContourArea(approxContour, false) > 75 && CvInvoke.ContourArea(approxContour, false) < 150) //only consider contours with area greater than 250
                        {
                            /*if (approxContour.Size == 3) //The contour has 3 vertices, it is a triangle
                            {
                                Point[] pts = approxContour.ToArray();
                                triangleList.Add(new Triangle2DF(
                                   pts[0],
                                   pts[1],
                                   pts[2]
                                   ));
                            }
                            else if (approxContour.Size == 4) //The contour has 4 vertices.
                            {*/
                                #region determine if all the angles in the contour are within [80, 100] degree
                                bool isRectangle = true;
                                Point[] pts = approxContour.ToArray();
                                LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                                /*for (int j = 0; j < edges.Length; j++)
                                {
                                    double angle = edges[(j + 1) % edges.Length].GetExteriorAngleDegree(edges[j]);
                                    if (angle < 0)
                                    {
                                        angle = -angle;
                                    }
                                    if (angle < 50 || angle > 130)
                                    {
                                        isRectangle = false;
                                        break;
                                    }
                                }*/
                                #endregion

                                if (isRectangle) boxList.Add(CvInvoke.MinAreaRect(approxContour));
                           // }
                        }
                    }
                }
            }

            for (int i = 0; i < boxList.Count; i++)
            {
                if(result.Count >0){
                    bool duplicate = false;
                    for (int j = 0; j < result.Count; j++)
                    {
                        if (result[j].Contains(new Vector3(boxList[i].Center.X,boxList[i].Center.Y,0)))
                        {
                            duplicate = true;
                        }
                    }
                    if (!duplicate)
                    {
                        result.Add(new Rect(
                                new Vector2(boxList[i].Center.X - boxList[i].Size.Width / 2, boxList[i].Center.Y - boxList[i].Size.Height / 2),
                                new Vector2(boxList[i].Size.Width, boxList[i].Size.Height)
                                ));
                    }
                }
                else{
                    result.Add(new Rect(
                                new Vector2(boxList[i].Center.X-boxList[i].Size.Width/2,boxList[i].Center.Y-boxList[i].Size.Height/2),
                                new Vector2(boxList[i].Size.Width,boxList[i].Size.Height)
                                ));
                }
            }
                return result;
        }

        public static List<Rect> detectFromFile(string filePath)
        {
            List<Rect> result = new List<Rect>();

            //Load the image from file and resize it for display
            Image<Bgr, byte> img = new Image<Bgr, byte>(filePath);

            //Convert the image to grayscale and filter out the noise
            UMat uimage = new UMat();
            CvInvoke.CvtColor(img, uimage, ColorConversion.Bgr2Gray);

            //use image pyr to remove noise
            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(uimage, pyrDown);
            CvInvoke.PyrUp(pyrDown, uimage);

            //region circle detection
            double cannyThreshold = 180.0;
            double circleAccumulatorThreshold = 120;
            CircleF[] circles = CvInvoke.HoughCircles(uimage, HoughType.Gradient, 2.0, 20.0, cannyThreshold, circleAccumulatorThreshold, 5);

            //region Canny and edge detection
            double cannyThresholdLinking = 120.0;
            UMat cannyEdges = new UMat();
            CvInvoke.Canny(uimage, cannyEdges, cannyThreshold, cannyThresholdLinking);

            LineSegment2D[] lines = CvInvoke.HoughLinesP(
               cannyEdges,
               1, //Distance resolution in pixel-related units
               Mathf.PI / 45.0, //Angle resolution measured in radians.
               20, //threshold
               30, //min Line width
               10); //gap between lines


            //#region Find triangles and rectangles
            List<Triangle2DF> triangleList = new List<Triangle2DF>();
            List<RotatedRect> boxList = new List<RotatedRect>();

            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple, new Point());
                int count = contours.Size;
                for (int i = 0; i < count; i++)
                {
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.05, true);
                        if (CvInvoke.ContourArea(approxContour, false) > 75) //only consider contours with area greater than 250
                        {
                            if (approxContour.Size == 3) //The contour has 3 vertices, it is a triangle
                            {
                                Point[] pts = approxContour.ToArray();
                                triangleList.Add(new Triangle2DF(
                                   pts[0],
                                   pts[1],
                                   pts[2]
                                   ));
                            }
                            else if (approxContour.Size == 4) //The contour has 4 vertices.
                            {
                                #region determine if all the angles in the contour are within [80, 100] degree
                                bool isRectangle = true;
                                Point[] pts = approxContour.ToArray();
                                LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                                for (int j = 0; j < edges.Length; j++)
                                {
                                    double angle = edges[(j + 1) % edges.Length].GetExteriorAngleDegree(edges[j]);
                                    if (angle < 0)
                                    {
                                        angle = -angle;
                                    }
                                    if (angle < 80 || angle > 100)
                                    {
                                        isRectangle = false;
                                        break;
                                    }
                                }
                                #endregion

                                if (isRectangle) boxList.Add(CvInvoke.MinAreaRect(approxContour));
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < boxList.Count; i++)
            {
                if (result.Count > 0)
                {
                    bool duplicate = false;
                    for (int j = 0; j < result.Count; j++)
                    {
                        if (result[j].Contains(new Vector3(boxList[i].Center.X, boxList[i].Center.Y, 0)))
                        {
                            duplicate = true;
                        }
                    }
                    if (!duplicate)
                    {
                        result.Add(new Rect(
                                new Vector2(boxList[i].Center.X - boxList[i].Size.Width / 2, boxList[i].Center.Y - boxList[i].Size.Height / 2),
                                new Vector2(boxList[i].Size.Width, boxList[i].Size.Height)
                                ));
                    }
                }
                else
                {
                    result.Add(new Rect(
                                new Vector2(boxList[i].Center.X - boxList[i].Size.Width / 2, boxList[i].Center.Y - boxList[i].Size.Height / 2),
                                new Vector2(boxList[i].Size.Width, boxList[i].Size.Height)
                                ));
                }
            }
            return result;
        }

        public static Enums.HandDetectionState detectHand( Texture2D colorTexture)
        {
            byte[] imageData = colorTexture.EncodeToPNG();

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }

            Image<Bgr, byte> img = new Image<Bgr, byte>(bitmapData);
            Image<Gray, byte> Img_Source_Gray = new Image<Gray, byte>(img.Size);

            img = img.ThresholdBinary(new Bgr(colorRange, colorRange, colorRange), new Bgr(64, 186, 255));
            img.Save(Application.dataPath + "/../dupadupa.png");

            return Enums.HandDetectionState.HandInCamera;

            CvInvoke.CvtColor(img, Img_Source_Gray, ColorConversion.Bgr2Gray);


            Img_Source_Gray = Img_Source_Gray.ThresholdBinary(new Gray(colorRange), new Gray(255));

           

            UMat cannyEdges = new UMat();

            double cannyThreshold = 120;
            double cannyThresholdLinking = 15;
            CvInvoke.Canny(Img_Source_Gray, cannyEdges, cannyThreshold, cannyThresholdLinking);
           

            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(cannyEdges, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple, new Point());
                Debug.Log("Number of contours found: " + contours.Size);

                //Save contours
                List<Texture2D> contoursTextureList = new List<Texture2D>();
                List<Texture2D> contoursApproxTextureList = new List<Texture2D>();
                for (int i = 0; i < contours.Size; i++)
                {
                    contoursTextureList.Add(new Texture2D(colorTexture.width, colorTexture.height));
                    contoursApproxTextureList.Add(new Texture2D(colorTexture.width, colorTexture.height));
                }


                for (int i = 0; i < contours.Size; i++)
                {
                    float colorVal = Random.value * 255;
                    for (int j = 0; j < contours[i].Size; j++)
                    {
                        contoursTextureList[i].SetPixel(contours[i][j].X, colorTexture.height - contours[i][j].Y, new UnityEngine.Color(0, 0, 0));

                    }
                    contoursTextureList[i].Apply();
                }

                for (int i = 0; i < contours.Size; i++)
                {
                    using (VectorOfPoint contour = contours[i])
                    using (VectorOfPoint approxContour = new VectorOfPoint())
                    {
                        CvInvoke.ApproxPolyDP(contour, approxContour, CvInvoke.ArcLength(contour, true) * 0.025, true);

                        }
                    }
                }
            return Enums.HandDetectionState.HandInCamera;
        }
    }
}
