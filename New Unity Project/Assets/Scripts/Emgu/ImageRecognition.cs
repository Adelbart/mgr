﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.VideoSurveillance;
using Emgu.CV.XFeatures2D;
using UnityEngine;
using Assets.Scripts;
using Assets.Scripts.Structures;

namespace Assets.Scripts.Emgu
{
    class ImageRecognition
    {
        public static double compareHistogramswithEmgu(Texture2D tex1, Texture2D tex2)
        {

            Image<Rgb, byte> img1 = convertTextureToEmguImage(tex1);
            Mat hist11 = convertTextureToCVMat(tex1);
             Image<Rgb, byte> img2 = convertTextureToEmguImage(tex2);
            Mat hist22 = convertTextureToCVMat(tex2);

            //DenseHistogram denseHist1 = new DenseHistogram();
            double result = 0;

            result = CvInvoke.CompareHist(img1, img2, HistogramCompMethod.Correl);

            return result;
        }
        public static void findMatchWithEmguWithRotation(Texture2D templateTexture, Texture2D observedTexture)
        {
            
            Image<Rgb, byte> source = convertTextureToEmguImage(observedTexture); // Image B
            Image<Rgb, byte> template = convertTextureToEmguImage(templateTexture); // Image A
            Image<Rgb, byte> imageToShow = source.Copy();

            using (Image<Gray, float> result = source.MatchTemplate(template, TemplateMatchingType.CcoeffNormed))
            {
                double[] minValues, maxValues;
                Point[] minLocations, maxLocations;
                result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);

                // You can try different values of the threshold. I guess somewhere between 0.75 and 0.95 would be good.
                UnityEngine.Debug.Log(maxValues[0]);

                if (maxValues[0] > 0.9)
                {
                    // This is a match. Do something with it, for example draw a rectangle around it.
                    Rectangle match = new Rectangle(maxLocations[0], template.Size);
                    //imageToShow.Draw(match, new Bgr(255, 255, 255), 3);
                }
            }

            // Show imageToShow in an ImageBox (here assumed to be called imageBox1)
            //imageBox1.Image = imageToShow;
        }
        public static double findMatchWithEmguNoScale(Texture2D templateTexture, Texture2D observedTexture)
        {
            double resultValue = 0;

            Image<Rgb, byte> source = convertTextureToEmguImage(observedTexture); // Image B
            Image<Rgb, byte> template = convertTextureToEmguImage(templateTexture); // Image A
            Image<Rgb, byte> imageToShow = source.Copy();

            using (Image<Gray, float> result = source.MatchTemplate(template, TemplateMatchingType.CcorrNormed))
            {
                double[] minValues, maxValues;
                Point[] minLocations, maxLocations;
                result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);
                // You can try different values of the threshold. I guess somewhere between 0.75 and 0.95 would be good.
                //UnityEngine.Debug.Log(maxValues[0]+"    "+minValues[0]);

                if (maxValues[0] > 0.9)
                {
                    // This is a match. Do something with it, for example draw a rectangle around it.
                    Rectangle match = new Rectangle(maxLocations[0], template.Size);
                    //imageToShow.Draw(match, new Bgr(255, 255, 255), 3);
                }
                resultValue = maxValues[0];

            }

            return resultValue;
            // Show imageToShow in an ImageBox (here assumed to be called imageBox1)
            //imageBox1.Image = imageToShow;
        }
        public static double findMatchWithEmgu(Texture2D templateTexture, Texture2D observedTexture)
        {
            double resultValue = 0;
            Texture2D resized = Assets.Scripts.Usefull.TextureProcessing.scaled(templateTexture, observedTexture.width, observedTexture.height);

            Image<Rgb, byte> source = convertTextureToEmguImage(observedTexture); // Image B
            Image<Rgb, byte> template = convertTextureToEmguImage(resized); // Image A
            Image<Rgb, byte> imageToShow = source.Copy();

            using (Image<Gray, float> result = source.MatchTemplate(template, TemplateMatchingType.CcorrNormed))
            {
                double[] minValues, maxValues;
                Point[] minLocations, maxLocations;
                result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);
                // You can try different values of the threshold. I guess somewhere between 0.75 and 0.95 would be good.
                //UnityEngine.Debug.Log(maxValues[0]+"    "+minValues[0]);

                if (maxValues[0] > 0.9)
                {    
                    // This is a match. Do something with it, for example draw a rectangle around it.
                    Rectangle match = new Rectangle(maxLocations[0], template.Size);
                    //imageToShow.Draw(match, new Bgr(255, 255, 255), 3);
                }
                resultValue = maxValues[0];

            }

            return resultValue;
            // Show imageToShow in an ImageBox (here assumed to be called imageBox1)
            //imageBox1.Image = imageToShow;
        }
        /*public static double findMatchWithAForge(Texture2D templateTexture, Texture2D observedTexture)
        {
            // create template matching algorithm's instance
            // use zero similarity to make sure algorithm will provide anything
            ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching(0);
            Texture2D resized = Assets.Scripts.Usefull.TextureProcessing.scaled(templateTexture, observedTexture.width, observedTexture.height);


            Bitmap image1Bitmap = convertTextureToBitmap(observedTexture);
            Bitmap image2Bitmap = convertTextureToBitmap(resized);

            Rectangle rect1 = new Rectangle(0, 0, image1Bitmap.Width, image1Bitmap.Height);
            Rectangle rect2 = new Rectangle(0, 0, image2Bitmap.Width, image2Bitmap.Height);

            UnmanagedImage image1 = new UnmanagedImage(image1Bitmap.LockBits(rect1, System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format24bppRgb)); //image1Bitmap.PixelFormat
            UnmanagedImage image2 = new UnmanagedImage(image2Bitmap.LockBits(rect2, System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format24bppRgb)); //image2Bitmap.PixelFormat

            // compare two images
            TemplateMatch[] matchings = tm.ProcessImage(image1, image2);
            // check similarity level

            if (matchings[0].Similarity > 0.95f)
            {
                // do something with quite similar images
            }

            return matchings[0].Similarity;
        }*/
        public static Vector2[] DrawFeature()
        {
            long matchTime;
            using(Mat modelImage = CvInvoke.Imread("black.png", LoadImageType.Grayscale))
            using (Mat observedImage = CvInvoke.Imread("ImageBinary.png", LoadImageType.Grayscale))
            {
            Vector2[] result = Draw(modelImage, observedImage, out matchTime);
            UnityEngine.Debug.Log("MatchTime:" + matchTime);
            return result;
            //ImageViewer.Show(result, String.Format("Matched using {0} in {1} milliseconds", CudaInvoke.HasCuda ? "GPU" : "CPU", matchTime));
            }
        }
        public static void FindMatch(Texture2D templateTexture, Texture2D observedTexture, out long matchTime, out VectorOfKeyPoint modelKeyPoints, out VectorOfKeyPoint observedKeyPoints, VectorOfVectorOfDMatch matches, out Mat mask, out Mat homography)
        {
            Mat modelImage = convertTextureToCVMat(templateTexture,"templateTexture");
            Mat observedImage = convertTextureToCVMat(observedTexture,"Image0");
            int k = 2;
            double uniquenessThreshold = 0.8;
            double hessianThresh = 300;

            Stopwatch watch;
            homography = null;

            modelKeyPoints = new VectorOfKeyPoint();
            observedKeyPoints = new VectorOfKeyPoint();   
           
            using (UMat uModelImage = modelImage.ToUMat(AccessType.Read))
            using (UMat uObservedImage = observedImage.ToUMat(AccessType.Read))
            {
                SURF surfCPU = new SURF(hessianThresh);
                //extract features from the object image
                UMat modelDescriptors = new UMat();
                surfCPU.DetectAndCompute(uModelImage, null, modelKeyPoints, modelDescriptors, false);

                watch = Stopwatch.StartNew();

                // extract features from the observed image
                UMat observedDescriptors = new UMat();
                surfCPU.DetectAndCompute(uObservedImage, null, observedKeyPoints, observedDescriptors, false);
                BFMatcher matcher = new BFMatcher(DistanceType.L2);
                matcher.Add(modelDescriptors);

                matcher.KnnMatch(observedDescriptors, matches, k, null);
                mask = new Mat(matches.Size, 1, DepthType.Cv8U, 1);
                mask.SetTo(new MCvScalar(255));
                Features2DToolbox.VoteForUniqueness(matches, uniquenessThreshold, mask);

                int nonZeroCount = CvInvoke.CountNonZero(mask);
                if (nonZeroCount >= 4)
                {
                    nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints,
                        matches, mask, 1.5, 20);
                    if (nonZeroCount >= 4)
                        homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints,
                            observedKeyPoints, matches, mask, 2);
                }

                watch.Stop();
            }
            if (homography != null)
            {
                UnityEngine.Debug.Log("MatchFound");
            }
            matchTime = watch.ElapsedMilliseconds;
        }

        public static Enums.HandDetectionState detectMotion(Image<Rgb, byte> inputImage, MotionHistory motionHistory, Mat forgroundMask)
        {
            
            Mat segMask = new Mat();


            motionHistory.Update(forgroundMask);

            //inputImage.Save(Application.dataPath + "/../motion.png");


            #region get a copy of the motion mask and enhance its color
            double[] minValues, maxValues;
            Point[] minLoc, maxLoc;
            motionHistory.Mask.MinMax(out minValues, out maxValues, out minLoc, out maxLoc);
            Mat motionMask = new Mat();
            using (ScalarArray sa = new ScalarArray(255.0 / maxValues[0]))
                CvInvoke.Multiply(motionHistory.Mask, sa, motionMask, 1, DepthType.Cv8U);
            //Image<Gray, Byte> motionMask = _motionHistory.Mask.Mul(255.0 / maxValues[0]);
            #endregion

            //create the motion image 
            Mat motionImage = new Mat(motionMask.Size.Height, motionMask.Size.Width, DepthType.Cv8U, 3);
            //display the motion pixels in blue (first channel)
            //motionImage[0] = motionMask;
            CvInvoke.InsertChannel(motionMask, motionImage, 0);

            //Threshold to define a motion area, reduce the value to detect smaller motion
            double minArea = CameraControler.cameraCaptureHeight*CameraControler.cameraCaptureWidth*0.05;
            UnityEngine.Debug.Log("min Area: " + minArea);

            //storage.Clear(); //clear the storage
            Rectangle[] rects;
            using (VectorOfRect boundingRect = new VectorOfRect())
            {
                motionHistory.GetMotionComponents(segMask, boundingRect);
                rects = boundingRect.ToArray();
            }

            List<Rectangle> rectsWithArea = new List<Rectangle>();
             //iterate through each of the motion component
            foreach (Rectangle comp in rects)
            {
                int area = comp.Width * comp.Height;
                //UnityEngine.Debug.Log(area);
                //reject the components that have small area;
                if (area < minArea) continue;

                UnityEngine.Debug.Log("Area: " + area);
                //UnityEngine.Debug.Log("Area:" +area);
                // find the angle and motion pixel count of the specific area
                double angle, motionPixelCount;
                motionHistory.MotionInfo(forgroundMask, comp, out angle, out motionPixelCount);

                //reject the area that contains too few motion
                if (motionPixelCount < area * 0.8) continue;
                UnityEngine.Debug.Log("motionPixelCount: " + motionPixelCount);
                //UnityEngine.Debug.Log("motionPixelCount:" + motionPixelCount);

                rectsWithArea.Add(comp);
            }


            //UnityEngine.Debug.Log(rectsWithArea.Count);

            if (rectsWithArea.Count> 0)
            {
                return Enums.HandDetectionState.HandInCamera;
            }

            return Enums.HandDetectionState.CameraClear;
        }
        public static bool FindMatch(Texture2D modelTexture, Texture2D observedTexture)
        {
            if (modelTexture == null || observedTexture == null)
            {
                UnityEngine.Debug.Log("NULL KURCZE");
            }
            Mat modelImage = convertTextureToCVMat(modelTexture, "FindMatchTemplate");
            Mat observedImage = convertTextureToCVMat(observedTexture, "FindMatchLookingFor");
            int k = 5;
            double uniquenessThreshold = 0.99;
            double hessianThresh = 700;

            Stopwatch watch;
            //Mat homography = null;
            Mat homography2 = null;
            //Mat mask = null;
            Mat mask2 = null;
            long matchTime = 0;
            //VectorOfKeyPoint modelKeyPoints = new VectorOfKeyPoint();
            //VectorOfKeyPoint observedKeyPoints = new VectorOfKeyPoint();
            VectorOfKeyPoint modelKeyPoints2 = new VectorOfKeyPoint();
            VectorOfKeyPoint observedKeyPoints2 = new VectorOfKeyPoint();
            //VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch();
            VectorOfVectorOfDMatch matches2 = new VectorOfVectorOfDMatch();
            using (UMat uModelImage = modelImage.ToUMat(AccessType.Read))
            using (UMat uObservedImage = observedImage.ToUMat(AccessType.Read))
            {
                //SURF surfCPU = new SURF(hessianThresh);
                SIFT siftCPU = new SIFT();

                //extract features from the object image
                //UMat modelDescriptors = new UMat();
                UMat modelDescriptors2 = new UMat();
                //surfCPU.DetectAndCompute(uModelImage, null, modelKeyPoints, modelDescriptors, false);
                siftCPU.DetectAndCompute(uModelImage, null, modelKeyPoints2, modelDescriptors2, false);

                //UnityEngine.Debug.Log("modelKeyPoints found: " + modelKeyPoints.Size);
                //UnityEngine.Debug.Log("modelKeyPoints2 found: " + modelKeyPoints2.Size);

                watch = Stopwatch.StartNew();

                //extract features from the observed image
                //UMat observedDescriptors = new UMat();
                UMat observedDescriptors2 = new UMat();
                //surfCPU.DetectAndCompute(uObservedImage, null, observedKeyPoints, observedDescriptors, false);
                siftCPU.DetectAndCompute(uObservedImage, null, observedKeyPoints2, observedDescriptors2, false);

                //BFMatcher matcher = new BFMatcher(DistanceType.L2);
                BFMatcher matcher2 = new BFMatcher(DistanceType.L2);
                //matcher.Add(modelDescriptors);
                matcher2.Add(modelDescriptors2);
                //UnityEngine.Debug.Log("ObservedImageKepoint found: " + (float)observedKeyPoints.Size);
                //UnityEngine.Debug.Log("ObservedImageKepoint2 found: " + observedKeyPoints2.Size*1.0f);

                //matcher.KnnMatch(observedDescriptors, matches, k, null);
                matcher2.KnnMatch(observedDescriptors2, matches2, k, null);
                //mask = new Mat(matches.Size, 1, DepthType.Cv8U, 1);
                mask2 = new Mat(matches2.Size, 1, DepthType.Cv8U, 1);
               // mask.SetTo(new MCvScalar(255));
                mask2.SetTo(new MCvScalar(255));
                //Features2DToolbox.VoteForUniqueness(matches, uniquenessThreshold, mask);
                Features2DToolbox.VoteForUniqueness(matches2, uniquenessThreshold, mask2);

                //UnityEngine.Debug.Log("Matches: " + matches.Size);
                //UnityEngine.Debug.Log("Matches2: " + matches2.Size);

                /*int nonZeroCount = CvInvoke.CountNonZero(mask);
                if (nonZeroCount >= 4)
                {
                    nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints,
                        matches, mask, 1.5, 20);
                    if (nonZeroCount >= 4)
                        homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints,
                            observedKeyPoints, matches, mask, 2);
                }
                */
                int nonZeroCount2 = CvInvoke.CountNonZero(mask2);
                if (nonZeroCount2 >= 4)
                {
                    nonZeroCount2 = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints2, observedKeyPoints2,
                        matches2, mask2, 1.5, 20);
                    if (nonZeroCount2 >= 4)
                        homography2 = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints2,
                            observedKeyPoints2, matches2, mask2, 2);
                }
                watch.Stop();
            }

            /*Mat result = new Mat();
            Features2DToolbox.DrawMatches(modelImage, modelKeyPoints, observedImage, observedKeyPoints,
               matches, result, new MCvScalar(255, 255, 255), new MCvScalar(255, 255, 255), mask);
            */
            Mat result2 = new Mat();
            Features2DToolbox.DrawMatches(modelImage, modelKeyPoints2, observedImage, observedKeyPoints2,
               matches2, result2, new MCvScalar(255, 255, 255), new MCvScalar(255, 255, 255), mask2);

            /*
            //UnityEngine.Debug.Log("Matches2: " + matches2.Size);
            if (homography != null)
            {
                    //draw a rectangle along the projected model
                    Rectangle rect = new Rectangle(Point.Empty, modelImage.Size);
                    PointF[] pts = new PointF[]
               {
                  new PointF(rect.Left, rect.Bottom),
                  new PointF(rect.Right, rect.Bottom),
                  new PointF(rect.Right, rect.Top),
                  new PointF(rect.Left, rect.Top)
               };
                    pts = CvInvoke.PerspectiveTransform(pts, homography);

                    Point[] points = Array.ConvertAll<PointF, Point>(pts, Point.Round);

                    using (VectorOfPoint vp = new VectorOfPoint(points))
                    {
                        CvInvoke.Polylines(result, vp, true, new MCvScalar(255, 0, 0, 255), 1);
                    }
                    result.Save(Application.dataPath + "/../" + "FindMatchMatched.png");

                matchTime = watch.ElapsedMilliseconds;
                //UnityEngine.Debug.Log("MatchFound! time:" + matchTime);
                return true;
            }
            */
            if (homography2 != null)
            {
                //draw a rectangle along the projected model
                Rectangle rect = new Rectangle(Point.Empty, modelImage.Size);
                PointF[] pts = new PointF[]
               {
                  new PointF(rect.Left, rect.Bottom),
                  new PointF(rect.Right, rect.Bottom),
                  new PointF(rect.Right, rect.Top),
                  new PointF(rect.Left, rect.Top)
               };
                pts = CvInvoke.PerspectiveTransform(pts, homography2);

                Point[] points = Array.ConvertAll<PointF, Point>(pts, Point.Round);

                using (VectorOfPoint vp = new VectorOfPoint(points))
                {
                    CvInvoke.Polylines(result2, vp, true, new MCvScalar(255, 0, 0, 255), 1);
                }
                result2.Save(Application.dataPath + "/../" + "FindMatchMatched2.png");

                matchTime = watch.ElapsedMilliseconds;
                //UnityEngine.Debug.Log("MatchFound 2 ! time:" + matchTime);
                //UnityEngine.Debug.Log((matches2.Size * 1.0f) + " " + (observedKeyPoints2.Size * 1.0f));
                
                return true;
            }

            matchTime = watch.ElapsedMilliseconds;
            return false;
        }



        /// <summary>
        /// Draw the model image and observed image, the matched features and homography projection.
        /// </summary>
        /// <param name="modelImage">The model image</param>
        /// <param name="observedImage">The observed image</param>
        /// <param name="matchTime">The output total time for computing the homography matrix.</param>
        /// <returns>The model image and observed image, the matched features and homography projection.</returns>
        public static Vector2[] Draw(Mat modelImage, Mat observedImage, out long matchTime)
        {
            Mat homography;
            VectorOfKeyPoint modelKeyPoints;
            VectorOfKeyPoint observedKeyPoints;
            using (VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch())
            {
                Vector2[] convertedPoints = new Vector2[4];
                Mat mask;
                FindMatch(modelImage, observedImage, out matchTime, out modelKeyPoints, out observedKeyPoints, matches,
                   out mask, out homography);

                //Draw the matched keypoints
                Mat result = new Mat();
                Features2DToolbox.DrawMatches(modelImage, modelKeyPoints, observedImage, observedKeyPoints, matches, result, new MCvScalar(255, 255, 255), new MCvScalar(255, 255, 255), mask);

                #region draw the projected region on the image

                if (homography != null)
                {
                    UnityEngine.Debug.Log("Found Match");
                    //draw a rectangle along the projected model
                    Rectangle rect = new Rectangle(Point.Empty, modelImage.Size);
                    PointF[] pts = new PointF[]
                   {
                      new PointF(rect.Left, rect.Bottom),
                      new PointF(rect.Right, rect.Bottom),
                      new PointF(rect.Right, rect.Top),
                      new PointF(rect.Left, rect.Top)
                   };
                    pts = CvInvoke.PerspectiveTransform(pts, homography);

                    Point[] points = Array.ConvertAll<PointF, Point>(pts, Point.Round);
                    using (VectorOfPoint vp = new VectorOfPoint(points))
                    {
                        CvInvoke.Polylines(result, vp, true, new MCvScalar(255, 0, 0, 255), 5);
                    }
                    for (int i = 0; i < 4; i++)
                    {
                        convertedPoints[i] = new Vector2(points[i].X, Screen.height- points[i].Y);
                    }
                }

                #endregion
               
                //return result;
                return convertedPoints;
            }
        }

        public static Vector2[] Draw(Texture2D modelImageT, Texture2D observedImageT, out long matchTime)
        {
            Mat modelImage = convertTextureToCVMat(modelImageT, "");
            Mat observedImage = convertTextureToCVMat(observedImageT, "");
            Mat homography;
            VectorOfKeyPoint modelKeyPoints;
            VectorOfKeyPoint observedKeyPoints;
            using (VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch())
            {
                Vector2[] convertedPoints = new Vector2[4];
                Mat mask;
                FindMatch(modelImage, observedImage, out matchTime, out modelKeyPoints, out observedKeyPoints, matches,
                   out mask, out homography);

                //Draw the matched keypoints
                Mat result = new Mat();
                Features2DToolbox.DrawMatches(modelImage, modelKeyPoints, observedImage, observedKeyPoints, matches, result, new MCvScalar(255, 255, 255), new MCvScalar(255, 255, 255), mask);

                #region draw the projected region on the image

                if (homography != null)
                {
                    UnityEngine.Debug.Log("Found Match");
                    //draw a rectangle along the projected model
                    Rectangle rect = new Rectangle(Point.Empty, modelImage.Size);
                    PointF[] pts = new PointF[]
                   {
                      new PointF(rect.Left, rect.Bottom),
                      new PointF(rect.Right, rect.Bottom),
                      new PointF(rect.Right, rect.Top),
                      new PointF(rect.Left, rect.Top)
                   };
                    pts = CvInvoke.PerspectiveTransform(pts, homography);

                    Point[] points = Array.ConvertAll<PointF, Point>(pts, Point.Round);
                    using (VectorOfPoint vp = new VectorOfPoint(points))
                    {
                        CvInvoke.Polylines(result, vp, true, new MCvScalar(255, 0, 0, 255), 5);
                    }
                    for (int i = 0; i < 4; i++)
                    {
                        convertedPoints[i] = new Vector2(points[i].X, Screen.height - points[i].Y);
                    }
                }
                for (int i = 0; i < convertedPoints.Length; i++)
                {
                    UnityEngine.Debug.Log(convertedPoints[i]);
                }
                #endregion

                //return result;
                return convertedPoints;
            }
        }
        public static void FindMatch(Mat modelImage, Mat observedImage, out long matchTime, out VectorOfKeyPoint modelKeyPoints, out VectorOfKeyPoint observedKeyPoints, VectorOfVectorOfDMatch matches, out Mat mask, out Mat homography)
        {
            int k = 2;
            double uniquenessThreshold = 0.8;
            double hessianThresh = 300;

            Stopwatch watch;
            homography = null;

            modelKeyPoints = new VectorOfKeyPoint();
            observedKeyPoints = new VectorOfKeyPoint();

                using (UMat uModelImage = modelImage.ToUMat(AccessType.Read))
                using (UMat uObservedImage = observedImage.ToUMat(AccessType.Read))
                {
                    SURF surfCPU = new SURF(hessianThresh);
                    //extract features from the object image
                    UMat modelDescriptors = new UMat();
                    surfCPU.DetectAndCompute(uModelImage, null, modelKeyPoints, modelDescriptors, false);

                    watch = Stopwatch.StartNew();

                    // extract features from the observed image
                    UMat observedDescriptors = new UMat();
                    surfCPU.DetectAndCompute(uObservedImage, null, observedKeyPoints, observedDescriptors, false);
                    BFMatcher matcher = new BFMatcher(DistanceType.L2);
                    matcher.Add(modelDescriptors);

                    matcher.KnnMatch(observedDescriptors, matches, k, null);
                    mask = new Mat(matches.Size, 1, DepthType.Cv8U, 1);
                    mask.SetTo(new MCvScalar(255));
                    Features2DToolbox.VoteForUniqueness(matches, uniquenessThreshold, mask);

                    int nonZeroCount = CvInvoke.CountNonZero(mask);
                    if (nonZeroCount >= 4)
                    {
                        nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints,
                           matches, mask, 1.5, 20);
                        if (nonZeroCount >= 4)
                            homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints,
                               observedKeyPoints, matches, mask, 2);
                    }

                    watch.Stop();
                }
            matchTime = watch.ElapsedMilliseconds;
        }

        public static Bitmap convertTextureToBitmap(Texture2D texture)
        {
            byte[] imageData = texture.EncodeToPNG();

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }

            return bitmapData;
        }
        public static Image<Rgb, byte> convertTextureToEmguImage(Texture2D texture)
        {
            byte[] imageData = texture.EncodeToPNG();

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }

            Image<Rgb, byte> img = new Image<Rgb, byte>(bitmapData);

            return img;
        }
        public static Mat convertTextureToCVMat(Texture2D texture)
        {
            byte[] imageData = texture.EncodeToPNG();

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }

            Image<Bgr, byte> img = new Image<Bgr, byte>(bitmapData);

            //Convert the image to grayscale and filter out the noise
            Mat modelImage = new Mat();

            CvInvoke.CvtColor(img, modelImage, ColorConversion.Bgr2Hsv);

            return modelImage;
        }

        public static Mat convertTextureToCVMat(Texture2D texture, string opts)
        {
            byte[] imageData = texture.EncodeToPNG();
            File.WriteAllBytes(Application.dataPath + "/../"+opts+".png", imageData);

            Bitmap bitmapData;

            using (var ms = new MemoryStream(imageData))
            {
                bitmapData = new Bitmap(ms);
            }

            Image<Bgr, byte> img = new Image<Bgr, byte>(bitmapData);

            //Convert the image to grayscale and filter out the noise
            Mat modelImage = new Mat();

            CvInvoke.CvtColor(img, modelImage, ColorConversion.Bgr2Gray);

            return modelImage;
        }
    }
}
