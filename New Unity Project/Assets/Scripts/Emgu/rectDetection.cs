﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Assets;
public class rectDetection : MonoBehaviour {

    GUITexture textureHolder;
    public Texture2D texture;
    public Texture2D inputTexture;
    List<Rect> quadriables = new List<Rect>(); //a box is a rotated rectangle

	// Use this for initialization
	void Start () {

        textureHolder = GetComponentInChildren<GUITexture>();

        quadriables = RectangleDetection.detectFromTexture(inputTexture);

        //quadriables = RectangleDetection.detectFromFile("Assets/Models/kwadraty.png");

        Debug.Log(quadriables.Count);

        foreach (Rect rect in quadriables)
        {
            Debug.Log(rect.x + " " + rect.y);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        //GUI.DrawTexture(new Rect(new Vector2(0,0), new Vector2(15,15)), texture);

        if (quadriables.Count > 0)
        {
            foreach (Rect rect in quadriables)
            {
                GUI.DrawTexture(rect, texture);
            }
            
        }

    }
}
