﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Structures;
public class MinEnclosingRect{

    private ConvexHull myConvex = new ConvexHull();
    private Vector2[] rectCornerPoints = new Vector2[4]; // 0 - upper left, 1- upper right , 2- lower right , 3- lower left
    private int[] rectSupportPoints = new int[5]; //0 - edge1 , 1- edge2 ,2 -right, 3- lower, 4-left
    private float minRectArea = 0;
    private Vector2 rectCentre = new Vector2();
    private Vector2 rectCentreOnScreen = new Vector2();
    float width;
    float height;
    float widthOnScreen;
    float heightOnScreen;
    float rotationFromAxis;
    public Vector2 debugMostLeft;
    public Vector2 debugMostRight;
    public List<Vector2> debugAbove;
    public List<Vector2> debugBelow;

    public MinEnclosingRect() { }
    public MinEnclosingRect(List<Vector2> inputPoints, CameraControler camControl)
    {
        //Debug.Log("MinEnclosingRect: " + inputPoints.Count);

        this.myConvex.setPolygonPoints(inputPoints);
        this.myConvex = Assets.Scripts.Usefull.QuickConvexHull.find2DConvexHull(inputPoints,out debugMostLeft, out debugMostRight, out debugAbove,out debugBelow);
        Assets.Scripts.Usefull.RotatingCalipers.findMinSurroundingRect(myConvex, out rectSupportPoints, out rectCornerPoints, out rotationFromAxis);
        calculateCentre(camControl);
        checkCorners();
        calculateMinRectArea(camControl);
    }
    public void castPawnToCamera(CameraControler camControl)
    {
        calculateCentre(camControl);
        checkCorners();
        calculateMinRectArea(camControl);
    }
    void calculateCentre(CameraControler camControl)
    {
        for(int i = 0; i < rectCornerPoints.Length; i++)
        {
            rectCentre.x += rectCornerPoints[i].x;
            rectCentre.y += rectCornerPoints[i].y;
        }
        rectCentre.x /= rectCornerPoints.Length;
        rectCentre.y /= rectCornerPoints.Length;

        rectCentreOnScreen = camControl.castPositionFromCameraToScreenQuadrilateralToRectangle(rectCentre);

  //          rectCentre = new Vector2((rectCornerPoints[0].x + rectCornerPoints[2].x) / 2, (rectCornerPoints[0].y + rectCornerPoints[2].y) / 2);
    }
    public bool isRectangular()
    {
        if (width > height)
        {
            return width / height < 2;
        }
        else
        {
            return height / width < 2;
        }

    }
    public float getWidth()
    {
        return width;
    }
    public float getHeight()
    {
        return height;
    }
    public float getScreenWidth()
    {
        return widthOnScreen;
    }
    public float getScreenHeight()
    {
        return heightOnScreen;
    }
    public float getRotation()
    {
        return rotationFromAxis;
    }
    public void checkCorners()
    {

        //DETERMINE FIRST CORNER
        if (rectCornerPoints.Length == 4)
        {
            float mostLeft = rectCornerPoints[0].x;
            int mostLeftIndex = 0;
            for (int i = 1; i < 4; i++)
            {
                if (rectCornerPoints[i].x <= mostLeft)
                {
                    mostLeftIndex = i;           
                }
            }

            if (mostLeftIndex != 0)
            {
                for (int index = 0; index < mostLeftIndex; index++)
                {
                    Vector2 t = rectCornerPoints[0];
                    rectCornerPoints[0] = rectCornerPoints[1];
                    rectCornerPoints[1] = rectCornerPoints[2];
                    rectCornerPoints[2] = rectCornerPoints[3];
                    rectCornerPoints[3] = t;
                }
            }
            /*
            //List<Vector2> cornersIn2Quarter = new List<Vector2>();
            List<int> cornersIn2Quarter = new List<int>();
            for (int i = 0; i < 4; i++)
            {
                if (rectCornerPoints[i].x <= rectCentre.x && rectCornerPoints[i].y <= rectCentre.y)
                {
                    cornersIn2Quarter.Add(i);
                }
            }
            if (cornersIn2Quarter.Count == 1)
            {
                for (int index = 0; index < cornersIn2Quarter[0]; index++)
                {
                    Vector2 t = rectCornerPoints[0];
                    rectCornerPoints[0] = rectCornerPoints[1];
                    rectCornerPoints[1] = rectCornerPoints[2];
                    rectCornerPoints[2] = rectCornerPoints[3];
                    rectCornerPoints[3] = t;
                }
            }
            else if (cornersIn2Quarter.Count > 1)
            {
                int topLeftIndex = cornersIn2Quarter[0];
                if (rectCornerPoints[cornersIn2Quarter[0]].x < rectCornerPoints[cornersIn2Quarter[1]].x)
                {
                    topLeftIndex = cornersIn2Quarter[0];
                }
                else
                {
                    topLeftIndex = cornersIn2Quarter[1];
                }


                for (int index = 0; index < topLeftIndex; index++)
                {
                    Vector2 t = rectCornerPoints[0];
                    rectCornerPoints[0] = rectCornerPoints[1];
                    rectCornerPoints[1] = rectCornerPoints[2];
                    rectCornerPoints[2] = rectCornerPoints[3];
                    rectCornerPoints[3] = t;
                }
            }
            else
            {
                Vector2[] t1 = new  Vector2[2];
                for (int index = 0; index < 4; index++)
                {
                    if(rectCornerPoints[index].x<rectCentre.x)
                    {

                    }
                }
            }*/
        }

        //DETERMINE CLOCKWISE ORDER
        
        bool testOrder = Assets.Scripts.Functions.isLeft(rectCornerPoints[0], rectCornerPoints[1], rectCornerPoints[2]) > 0;
        if (!testOrder)
        {
            Vector2 t = rectCornerPoints[1];
            rectCornerPoints[1] = rectCornerPoints[3];
            rectCornerPoints[3] = t;
        }
        
    }
    public void setRectCornerPoints(Vector2[] input, CameraControler camControl)
    {
        rectCornerPoints = input;
        calculateMinRectArea(camControl);
    }
    public Vector2 getCorner(int i)
    {
            return rectCornerPoints[i];
    }
    public Vector2 getCornerWithYScreenSubstraction(int i)
    {
            return new Vector2(rectCornerPoints[i].x, Screen.height-rectCornerPoints[i].y);
    }
    public Vector2[] getMinRectCorners()
    {
        return rectCornerPoints;
    }
    public List<Vector2> getMinRectCornersAsList()
    {
        List<Vector2> rectCornerList= new List<Vector2>();
        for(int i=0;i<rectCornerPoints.Length;i++)
        {
            rectCornerList.Add(rectCornerPoints[i]);
        }
        return rectCornerList;
    }
    private void calculateMinRectArea(CameraControler camControl)
    {
        width = Assets.Scripts.Functions.distanceAB_float(rectCornerPoints[0], rectCornerPoints[1]);
        height = Assets.Scripts.Functions.distanceAB_float(rectCornerPoints[1], rectCornerPoints[2]);

        Vector2 UL = camControl.castPositionFromCameraToScreenQuadrilateralToRectangle(rectCornerPoints[0]);
        Vector2 UR = camControl.castPositionFromCameraToScreenQuadrilateralToRectangle(rectCornerPoints[1]);
        Vector2 LR = camControl.castPositionFromCameraToScreenQuadrilateralToRectangle(rectCornerPoints[2]);

        widthOnScreen = Assets.Scripts.Functions.distanceAB_float(UL, UR);
        heightOnScreen = Assets.Scripts.Functions.distanceAB_float(UR, LR);

        minRectArea = width * height;
    }
    public ConvexHull getConvexHull()
    {
        return myConvex;
    }
    public float getArea()
    {
        return minRectArea;
    }
    public Vector2 getCentre()
    {
        return rectCentre;
    }
    public Vector2 getCentreOnScreen()
    {
        return rectCentreOnScreen;
    }
}
