﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Structures
{
    public class CameraControler
    {
        public static int cameraCaptureWidth = 1920;
        public static int cameraCaptureHeight = 1080;
        private List<Vector2> cameraCaptureCorners = new List<Vector2>(); //corners are [0]-upperleft, [1] -upperright, [2] -lowerright, [3] -lowerleft
        private int topWidth;
        private int bottomWidth;
        private int height;

        private int widthDifference;

        public CameraControler() { }
        public CameraControler(List<Vector2> input)
        {
            calculateProperties();
        }

        public bool setCameraCorners(List<Vector2> input)
        {
            if (input.Count == 4)
            {
                cameraCaptureCorners = input;
                //calibrateCorners();
                calculateProperties();
                Debug.Log("Calibration successfull");
                printCornerData();
                return true;
            }
            else
            {
                Debug.Log("Calibration Error");
                return false;
            }
        }
        public string CornersToStringFormat()
        {
            string result = "";

            for (int i = 0; i < cameraCaptureCorners.Count; i++)
            {
                result += cameraCaptureCorners[i].x.ToString()+":"+cameraCaptureCorners[i].y.ToString();
                if (i < cameraCaptureCorners.Count - 1)
                {
                    result += ";";
                }
            }

                return result;
        }
        public void CornersFromStringFormat(string inputLine)
        {
            List<Vector2> tempVertex = new List<Vector2>();
            string[] vectorLines = inputLine.Split(';');
            for (int i = 0; i < vectorLines.Length; i++)
            {
                string[] xANDy = vectorLines[i].Split(':');
                tempVertex.Add(new Vector2(float.Parse(xANDy[0]), float.Parse(xANDy[1])));
            }
            setCameraCorners(tempVertex);
        }
        private void printCornerData()
        {
            foreach (Vector2 corner in cameraCaptureCorners)
            {
                Debug.Log(corner);
            }
            Debug.Log("Widthtop: " + topWidth + "   widthBottom:" + bottomWidth + "     height:" + height);
        }
        public List<Vector2> getCorners()
        {
            return cameraCaptureCorners;
        }

        void calibrateCorners()
        {
            if (cameraCaptureCorners[0].y > cameraCaptureCorners[1].y)
            {
                cameraCaptureCorners[0] = new Vector2(cameraCaptureCorners[0].x, cameraCaptureCorners[1].y);
            }
            else
            {
                cameraCaptureCorners[1] = new Vector2(cameraCaptureCorners[1].x, cameraCaptureCorners[0].y);

            }
            if (cameraCaptureCorners[2].y > cameraCaptureCorners[3].y)
            {
                cameraCaptureCorners[3] = new Vector2(cameraCaptureCorners[3].x, cameraCaptureCorners[2].y);
            }
            else
            {
                cameraCaptureCorners[2] = new Vector2(cameraCaptureCorners[2].x, cameraCaptureCorners[3].y);
            }
        }
        void calibrateCorners2()
        {
            Vector2 centre = Assets.Scripts.Functions.findPolygonCentroid(cameraCaptureCorners);

            for (int i = 0; i < cameraCaptureCorners.Count; i++)
            {
                if (cameraCaptureCorners[i].x < centre.x)
                {
                    if (cameraCaptureCorners[i].y < centre.y)
                    {
                        Vector2 t = cameraCaptureCorners[0];
                        cameraCaptureCorners[0] = cameraCaptureCorners[i];
                        cameraCaptureCorners[i] = t;
                    }
                    else
                    {
                        Vector2 t = cameraCaptureCorners[3];
                        cameraCaptureCorners[3] = cameraCaptureCorners[i];
                        cameraCaptureCorners[i] = t;
                    }
                }
                else
                {
                    if (cameraCaptureCorners[i].y < centre.y)
                    {
                        Vector2 t = cameraCaptureCorners[1];
                        cameraCaptureCorners[1] = cameraCaptureCorners[i];
                        cameraCaptureCorners[i] = t;
                    }
                    else
                    {
                        Vector2 t = cameraCaptureCorners[2];
                        cameraCaptureCorners[2] = cameraCaptureCorners[i];
                        cameraCaptureCorners[i] = t;
                    }
                }
            }

            if (cameraCaptureCorners[0].y > cameraCaptureCorners[1].y)
            {
                cameraCaptureCorners[0] = new Vector2(cameraCaptureCorners[0].x, cameraCaptureCorners[1].y);
            }
            else
            {
                cameraCaptureCorners[1] = new Vector2(cameraCaptureCorners[1].x, cameraCaptureCorners[0].y);

            }

            if (cameraCaptureCorners[2].y > cameraCaptureCorners[3].y)
            {
                cameraCaptureCorners[2] = new Vector2(cameraCaptureCorners[2].x, cameraCaptureCorners[3].y);
            }
            else
            {
                cameraCaptureCorners[3] = new Vector2(cameraCaptureCorners[3].x, cameraCaptureCorners[2].y);
            }
        }

        void calculateProperties()
        {
            topWidth = (int)(cameraCaptureCorners[1].x - cameraCaptureCorners[0].x);//, cornersofCameraTexture[2].x - cornersofCameraTexture[3].x);
            bottomWidth = (int)(cameraCaptureCorners[2].x - cameraCaptureCorners[3].x);//, cornersofCameraTexture[2].x - cornersofCameraTexture[3].x);
            height = (int)(cameraCaptureCorners[3].y - cameraCaptureCorners[0].y);
            widthDifference = bottomWidth - topWidth;
        }

        public Vector2 castPointFromCameraToScreen(Vector2 inputPoint)
        {
            Vector2 result = new Vector2();
            
            result.x = inputPoint.x * Screen.width / cameraCaptureWidth;
            //Debug.Log("Width: "+inputPoint.x + "*" + Screen.width + "/" + cameraCaptureWidth);
            result.y = inputPoint.y * Screen.height/ cameraCaptureHeight;
            //Debug.Log("Height: "+inputPoint.y + "*" + Screen.width + "/" + cameraCaptureWidth);


            return result;
        }
        public Vector2 castPositionFromMapToScreenNew(Vector2 inputPosition)
        {
            Vector2 result = new Vector2();

            float AZLengthOnCamera = Functions.distanceAB_float(cameraCaptureCorners[0], inputPosition);
            float BZLengthOnCamera = Functions.distanceAB_float(cameraCaptureCorners[1], inputPosition);
            float CZLengthOnCamera = Functions.distanceAB_float(cameraCaptureCorners[2], inputPosition);

            return result;
        }
        public Vector2 castPositionFromMapToScreen(Vector2 inputPosition)
        {

            Vector2 result = new Vector2();
            float distanceFromTopOfCameraCapture = inputPosition.y - cameraCaptureCorners[0].y;

            result.y = distanceFromTopOfCameraCapture * Screen.height / (height);

            float cameraProportionAtY = (bottomWidth - topWidth) * result.y / height;
            float cameraScreenWidthAtY = topWidth + cameraProportionAtY;

            //float cameraScreenWidthAtY = ((inputPosition.y - cornersofCameraTexture[0].y) * (bottomWidthOfCameraTextureCrop - topWidthOfCameraTextureCrop)) / leftHeightOfCameraTextureCrop;
            //cameraScreenWidthAtY = bottomWidthOfCameraTextureCrop - cameraScreenWidthAtY;

            float distanceFromLeftOfCameraCapture = cameraCaptureCorners[3].x + ((bottomWidth - cameraScreenWidthAtY) / 2);

            result.x = (inputPosition.x - distanceFromLeftOfCameraCapture) * Screen.width / cameraScreenWidthAtY;

            return result;

        }
        public Vector2 castPositionFromCameraToScreenQuadrilateralToRectangle(Vector2 inputPoint)
        {
            Vector2 P = inputPoint;
            Vector2 P0 = cameraCaptureCorners[3];
            Vector2 P1 = cameraCaptureCorners[2];
            Vector2 P2 = cameraCaptureCorners[1];
            Vector2 P3 = cameraCaptureCorners[0];

            Vector2 N0 = new Vector2(P3.y - P0.y, -(P3.x - P0.x)).normalized;
            Vector2 N1 = new Vector2(P1.y - P0.y, -(P1.x - P0.x)).normalized;
            Vector2 N2 = new Vector2(-(P2.y - P1.y),P2.x - P1.x).normalized;
            Vector2 N3 = new Vector2(-(P2.y - P3.y), P2.x - P3.x).normalized;

            float u = Vector2.Dot((P-P0),N0) / (Vector2.Dot((P - P0), N0) + Vector2.Dot(P-P2, N2));
            float v = Vector2.Dot((P - P0), N1) / (Vector2.Dot((P - P0), N1) + Vector2.Dot(P - P3, N3));

            //Debug.Log("UV: "+new Vector2(u, v));
            float ScH = Screen.height;
            float ScW = Screen.width;
            //Debug.Log(v * (ScH / ScW) * ScH);
            //Debug.Log(new Vector2(u * Screen.width, v * Screen.height));

            float ySubstraction = ScW * (1 - (ScH / ScW)) / 3.0f;
            float fractionValue = 0;
            if (v > 0.4f)
            {
                fractionValue = -3.2f * Mathf.Pow(v, 2) + 3.2f * v;
            }
            else
            {
                fractionValue = -2.2f * Mathf.Pow(v, 2) + 2.2f * v;
            }
            return new Vector2(u* ScW, v * ScH - (ySubstraction* fractionValue));
        }

        public Vector2 castPositionFromScreenToMap(Vector2 inputPosition)
        {
            Vector2 result = new Vector2();

            result.y = cameraCaptureCorners[0].y + height * inputPosition.y / Screen.height;

            float cameraProportionAtY = (bottomWidth - topWidth) * result.y / height;
            float cameraScreenWidthAtY = topWidth + cameraProportionAtY;

            result.x = cameraProportionAtY/2 + cameraScreenWidthAtY * inputPosition.x / Screen.width;

            return result;
        }
        
    }
}
