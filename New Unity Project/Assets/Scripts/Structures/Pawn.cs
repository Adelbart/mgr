﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Structures;
using Emgu.CV;
using Emgu.CV.VideoSurveillance;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using UnityEngine.UI;

public class Pawn {

    public Rect myRect;
    public MyCircleCollider myCollider;
    public Vector2 positionOfCentreOnCameraCapture;
    public Bounds myBounds;
    private MinEnclosingRect myEnclosingRect;
    private Enums.RaceType raceType = Enums.RaceType.Null;
    private Enums.PawnState pawnState = Enums.PawnState.acceptedNew;
    private List<Vector2> positionOnMap = new List<Vector2>();
    private int pawnRegionIndex = 0;
    private bool ownedByPlayer = false;

    public Pawn(Bounds newBounds)
    {
        myBounds = newBounds;
        positionOfCentreOnCameraCapture  = myBounds.center;
        myRect = new Rect(myBounds.min, myBounds.size);
        myCollider = new MyCircleCollider(myRect.center, myRect.width > myRect.height ? myRect.width : myRect.height);
    }
    public Pawn(Enums.RaceType rType)
    {
        raceType = rType;
    }
    public Pawn(List<Vector2> inputPoints, CameraControler camC)
    {
        myEnclosingRect = new MinEnclosingRect(inputPoints, camC);
    }
    public bool isRectangular()
    {
        return myEnclosingRect.isRectangular();
    }
    public void castPawnToScreen(CameraControler cam)
    {
        myEnclosingRect.castPawnToCamera(cam);
    }
    public void castLightOnPawn(GameObject UIelem, GameObject UIelemParent, AssetManager aM)
    {
        GameObject obj = GameObject.Instantiate(UIelem);
        Debug.Log("Creating pawn transparency at: " + getPawnPositionOnScreen());
        obj.transform.SetParent(UIelemParent.transform);
        Debug.Log("pre pos set:" + obj.transform.position);
        obj.transform.localPosition = getPawnPositionOnScreen();
        Debug.Log("post pos set:" + obj.transform.position);
        Image objImg = obj.GetComponent<Image>();
        if (getPawnState() == Enums.PawnState.toRemove)
        {
            objImg.sprite = aM.UIElementsTextures[0];
        }
        else
        {
            objImg.sprite = aM.whiteSprite;
        }
        float imageSize = getPawnWidth();
        objImg.rectTransform.sizeDelta = new Vector2(imageSize, imageSize);
    }
    public void setPawnRegionIndex(int rIndex)
    {
        pawnRegionIndex = rIndex;
    }
    public int getPawnRegionIndex()
    {
        return pawnRegionIndex;
    }
    public float getPawnWidth()
    {
        return myEnclosingRect.getWidth();
    }
    public float getPawnHeight()
    {
        return myEnclosingRect.getHeight();
    }
    public float getPawnScreenWidth()
    {
        return myEnclosingRect.getScreenWidth();
    }
    public float getPawnScreenHeight()
    {
        return myEnclosingRect.getScreenHeight();
    }
    public float getPawnRotation()
    {
        return myEnclosingRect.getRotation();
    }
    public Pawn(Rect rect)
    {
        myRect = rect;
        myCollider = new MyCircleCollider(myRect.center, myRect.width > myRect.height ? myRect.width : myRect.height);
    }
    public bool intersectsBounds(Bounds other)
    {
        return myBounds.Intersects(other);
    }
    public void setMinRectFromPoints(List<Vector2> inputPoints, CameraControler camC)
    {
        //Debug.Log("setMinRectFromPoints size: " + inputPoints.Count);

        myEnclosingRect = new MinEnclosingRect(inputPoints, camC);
    }
    public Enums.PawnState getPawnState()
    {
        return pawnState;
    }
    public void setPawnToRemove()
    {
        pawnState = Enums.PawnState.toRemove;
    }
    public MinEnclosingRect getMinEnclosingRect()
    {
        return myEnclosingRect;
    }
    public float getMinRectArea()
    {
        return myEnclosingRect.getArea();
    }
    public Vector2 getPawnPosition()
    {
        if (myEnclosingRect != null)
        {
            return myEnclosingRect.getCentre();
        }
        else
        {
            return positionOfCentreOnCameraCapture;
        }
    }
    public Vector2 getPawnPositionOnScreen()
    {
        return myEnclosingRect.getCentreOnScreen();
        //return positionOfCentreOnScreen;
    }
    public bool intersectsCircle(MyCircleCollider other)
    {
        return myCollider.intersectsCircle(other);
    }
    public ConvexHull getConvexHull()
    {
        return myEnclosingRect.getConvexHull();
    }

    public double boundsSize()
    {
        return Assets.Scripts.Functions.BoundsArea(myBounds);
    }
    public List<Vector2> getConvexHullPoints()
    {
        return myEnclosingRect.getConvexHull().getConvexHullPoints();
    }
    public bool isSimilar(Pawn other)
    {
        if (other.raceType == raceType && Assets.Scripts.Functions.PointInRectangle(myEnclosingRect, other.myEnclosingRect.getCentre()))
        {
            return true;
        }
        return false;
    }
    public Vector2 getCovexHullPointWithScreenSubstraction(int index)
    {
        return myEnclosingRect.getConvexHull().getCovexHullPointWithScreenSubstraction(index);
    }
    public List<Vector2> getAllConvexHullPoints()
    {
        return myEnclosingRect.getConvexHull().getAllConvexPoints();
    }
    public Enums.RaceType getRaceType()
    {
        return raceType;
    }
    public void setRaceType(int newRace)
    {
        //raceType = (Enums.RaceType) Mathf.CeilToInt(newRace/cameraObscura3.racesTemplatesPerDetection);
        //Debug.Log("Setting PawnRace to :" + newRace);
        raceType =  (Enums.RaceType)newRace;
        //Debug.Log(newRace);
    }
    public void setRaceType(Enums.RaceType newRace)
    {
        raceType = newRace;
        //Debug.Log("Setting PawnRace to :" + newRace);
        //Debug.Log(newRace);
    }
    public ColorHistogram cutPawnHistogram(Image<Bgr, byte> inputTexture)
    {
        Vector2 topLeft = myEnclosingRect.getCorner(0);
        Vector2 topRight = myEnclosingRect.getCorner(1);
        Vector2 bottomRight = myEnclosingRect.getCorner(2);
        Vector2 bottomLeft = myEnclosingRect.getCorner(3);

        List<Vector2> top = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, topRight);
        List<Vector2> left = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, bottomLeft);
        List<Vector2> right = Assets.Scripts.Functions.detectPixelsOnLine(topRight, bottomRight);



        int pixelNum = left.Count <= right.Count ? left.Count : right.Count;

        //Debug.Log(pixelNum);
        ColorHistogram extractedImageS = new ColorHistogram();

        //Texture2D newTex = new Texture2D(top.Count, left.Count);
        for (int i = 0; i < pixelNum; i++)
        {

            Vector2 startPixel = left[i];
            Vector2 endPixel = right[i];

            List<Vector2> linePixels = Assets.Scripts.Functions.detectPixelsOnLine(startPixel, endPixel);
            //Debug.Log(linePixels.Count);
            for (int j = 0; j < linePixels.Count; j++)
            {
                //extractedImageS.addPixel(Assets.Scripts.Functions.getColorAroundPixel(inputTexture, (int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y));
                //Debug.Log((int)linePixels[j].x+"    "+( CameraControler.cameraCaptureHeight - (int)linePixels[j].y));
                //Debug.Log(CameraControler.cameraCaptureHeight + "    " + (int)linePixels[j].y);
                Bgr pixel = inputTexture[(int)linePixels[j].y, (int)linePixels[j].x];
                
                /*pixel = new Bgr(pixel.Blue + pixel.Blue * 0.3 <= 255 ? pixel.Blue + pixel.Blue * 0.3 : 255, 
                                pixel.Green + pixel.Green * 0.3 <= 255 ? pixel.Green + pixel.Green * 0.3 : 255,
                                pixel.Red + pixel.Red * 0.3 <= 255 ? pixel.Red + pixel.Red * 0.3 : 255);
                                */
                //Debug.Log(pixel.Red + "     "+pixel.Green + "     " + pixel.Blue);
                //Debug.Log(new Color32((byte)pixel.Red, (byte)pixel.Green, (byte)pixel.Blue, (byte)255));

                //byte byCurrent = inputTexture.Data[(int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y, 0];

                //Debug.Log((byte)pixel.Red + "     " + (byte)pixel.Green + "     " + (byte)pixel.Blue + "     " + (byte)255);
                extractedImageS.addPixel(new Color32((byte)pixel.Red, (byte)pixel.Green, (byte)pixel.Blue, (byte)255));
                
                    //newTex.SetPixel(j, i, new Color32((byte)pixel.Red, (byte)pixel.Green, (byte)pixel.Blue, (byte)255));
                    //extractedImageS.addPixel(new Color32());
                


            }
        }
            //newTex.Apply();
            //Assets.Scripts.Functions.saveTexture(newTex, "extractedPawn" + AssetManager.getNextPawnIndex());
        
        extractedImageS.normalizeHistogramAndCalculateBuckets();
        //Assets.Scripts.Functions.saveTexture(extractedImageS, "observedTexture1", "");

        return extractedImageS;
    }
    public ColorHistogram cutPawnHistogram(Texture2D inputTexture)
    {
        Vector2 topLeft = myEnclosingRect.getCorner(0);
        Vector2 topRight = myEnclosingRect.getCorner(1);
        Vector2 bottomRight = myEnclosingRect.getCorner(2);
        Vector2 bottomLeft = myEnclosingRect.getCorner(3);

        List<Vector2> top = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, topRight);
        List<Vector2> left = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, bottomLeft);
        List<Vector2> right = Assets.Scripts.Functions.detectPixelsOnLine(topRight, bottomRight);



        int pixelNum = left.Count <= right.Count ? left.Count : right.Count;

        //Debug.Log(pixelNum);
        ColorHistogram extractedImageS = new ColorHistogram();

        for (int i = 0; i < pixelNum; i++)
        {

            Vector2 startPixel = left[i];
            Vector2 endPixel = right[i];

            List<Vector2> linePixels = Assets.Scripts.Functions.detectPixelsOnLine(startPixel, endPixel);
            //Debug.Log(linePixels.Count);
            for (int j = 0; j < linePixels.Count; j++)
            {
                extractedImageS.addPixel(Assets.Scripts.Functions.getColorAroundPixel(inputTexture, (int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y));


            }
        }
        //Assets.Scripts.Functions.saveTexture(extractedImageS, "observedTexture1", "");

        return extractedImageS;
    }
    public Texture2D cutPawnFromImage(Texture2D inputTexture)
    {
        Vector2 topLeft = myEnclosingRect.getCorner(0);
        Vector2 topRight = myEnclosingRect.getCorner(1);
        Vector2 bottomRight = myEnclosingRect.getCorner(2);
        Vector2 bottomLeft = myEnclosingRect.getCorner(3);

        List<Vector2> top = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, topRight);
        List<Vector2> left = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, bottomLeft);
        List<Vector2> right = Assets.Scripts.Functions.detectPixelsOnLine(topRight, bottomRight);



        int pixelNum = left.Count <= right.Count ? left.Count : right.Count;

        //Debug.Log(pixelNum);
        Texture2D extractedImageS = new Texture2D(top.Count, pixelNum);

        for (int i = 0; i < pixelNum; i++)
        {

            Vector2 startPixel = left[i];
            Vector2 endPixel = right[i];

            List<Vector2> linePixels = Assets.Scripts.Functions.detectPixelsOnLine(startPixel, endPixel);
            //Debug.Log(linePixels.Count);
            for (int j = 0; j < linePixels.Count; j++)
            {
                extractedImageS.SetPixel(j, i, Assets.Scripts.Functions.getColorAroundPixel(inputTexture, (int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y));
                extractedImageS.Apply();

            }
        }
        Assets.Scripts.Functions.saveTexture(extractedImageS,"observedTexture1",""); 

        return extractedImageS;
    }
    public void movePawn()
    {

    }
    public void detectPawnRace(Texture2D inputT, List<Texture2D> raceTemplateTextures) 
    {
        Vector2 topLeft = myEnclosingRect.getCorner(0);
        Vector2 topRight = myEnclosingRect.getCorner(1);
        Vector2 bottomRight = myEnclosingRect.getCorner(2);
        Vector2 bottomLeft = myEnclosingRect.getCorner(3);

        //Debug.Log(topLeft + "     " + topRight + "       " + bottomRight + "     " + bottomLeft);

        List<Vector2> top = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, topRight);
        List<Vector2> left = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, bottomLeft);
        List<Vector2> right = Assets.Scripts.Functions.detectPixelsOnLine(topRight, bottomRight);

        //Debug.Log(topLeft+"     "+ topRight+"       "+bottomRight+"     "+bottomLeft);

        Texture2D extractedImage = new Texture2D(top.Count, left.Count);
        Texture2D extractedImage1 = new Texture2D(top.Count, left.Count);
        Texture2D extractedImage2 = new Texture2D(top.Count, left.Count);
        Texture2D extractedImage3 = new Texture2D(top.Count, left.Count);

        int pixelNum = left.Count <= right.Count ? left.Count : right.Count;
        for (int i = 0; i < pixelNum; i++)
        {

            Vector2 startPixel = left[i];
            Vector2 endPixel = right[i];

            List<Vector2> linePixels = Assets.Scripts.Functions.detectPixelsOnLine(startPixel, endPixel);

            for (int j = 0; j < linePixels.Count; j++)
            {
                //extractedImage.SetPixel(i, j, Assets.Scripts.Functions.getColorAroundPixel(colorTexture, (int)linePixels[j].x, Screen.height - (int)linePixels[j].y));
                extractedImage.SetPixel(i, j, inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y));
                extractedImage1.SetPixel(i, j, inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y) * 2);
                extractedImage3.SetPixel(i, j, inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y) * 1.2f);
                extractedImage2.SetPixel(i, j, 20* new Color(
                    Mathf.Exp(inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y).r / 100),
                    Mathf.Exp(inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y).g / 100),
                    Mathf.Exp(inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y).b / 100)
                    ));
                extractedImage.Apply();
                extractedImage1.Apply();
                extractedImage2.Apply();
                extractedImage3.Apply();

            }
        }
        Assets.Scripts.Functions.saveTexture(extractedImage, "extractedImage", "");
        Assets.Scripts.Functions.saveTexture(extractedImage1, "extractedImage1", "");
        Assets.Scripts.Functions.saveTexture(extractedImage2, "extractedImage2", "");
        Assets.Scripts.Functions.saveTexture(extractedImage3, "extractedImage3", "");


        /*int templateIndex = 0;
        double matchValue = 0;
        int matchedTemplateIndex = 0;
        foreach (Texture2D template in raceTemplateTextures)
        {
            templateIndex++;
            Texture2D temp = extractedImage;
            for (int i = 0; i < 4; i++)
            {
                //double matchValueT = Assets.Scripts.Emgu.ImageRecognition.findMatchWithEmgu(template, temp);

                
                if (matchValue < matchValueT)
                {
                    matchedTemplateIndex = templateIndex;
                    matchValue = matchValueT;
                }
                temp = Assets.Scripts.Functions.rotateTexture90DegreesLeft(temp);
            }
            //Debug.Log("Best math with template: " + templateIndex + "     " + matchValue);
        }
        */
            int[] sourceHistogram = Assets.Scripts.Usefull.TextureProcessing.getImageHistogram64Colors(extractedImage);

            int templateIndex = 0;
            float matchValue = 1000000;
            int matchedTemplateIndex = 0;

        foreach (Texture2D template in raceTemplateTextures)
        {
            

            Texture2D temp = extractedImage;
            Texture2D resized = Assets.Scripts.Usefull.TextureProcessing.scaled(template, extractedImage.width, extractedImage.height);

            Assets.Scripts.Functions.saveTexture(resized,"Resized","");
            //Debug.Log(extractedImage.width + "    " + extractedImage.height);

            int[] templateHistogram = Assets.Scripts.Usefull.TextureProcessing.getImageHistogram64Colors(resized);

            float matchValueT = Assets.Scripts.Usefull.TextureProcessing.createAndCompareHistograms(resized, template);

            //int matchValueT = Assets.Scripts.Usefull.TextureProcessing.compareColorHistograms(sourceHistogram,templateHistogram);

            Debug.Log("match with template: " + templateIndex + "     " + matchValueT);

            if (matchValue > matchValueT)
            {
                matchedTemplateIndex = templateIndex;
                matchValue = matchValueT;
            }

            /*if (Assets.Scripts.Emgu.ImageRecognition.FindMatch(template, extractedImage))
            {
                Debug.Log("MAtch found with emgu" + templateIndex);
            }
            if (Assets.Scripts.Emgu.ImageRecognition.FindMatch(extractedImage, template))
            {
                Debug.Log("MAtch found with emgu" + templateIndex);
            }*/
            templateIndex++;
        }

        setRaceType((Enums.RaceType)matchedTemplateIndex);
    }
    public void detectPawnRace4(Image<Bgr, byte> inputT, List<Texture2D> raceTemplateTextures, /*List<ColorHistogram>*/ ColorHistogram[] raceTemplateHistograms, int numberOfRegionHistogramsComparison)
    {
        bool moreThanOneEmguMatch = true;
        ColorHistogram histogram = cutPawnHistogram(inputT);
        /*Debug.Log("Histogram number of pixels: "+histogram.getNumberOfPixels());
        Debug.Log("4");
        histogram.print4Buckets();
        Debug.Log("8");
        histogram.print6Buckets();
        Debug.Log("12");
        histogram.print12Buckets();*/
        /*for(int i=0;i<raceTemplateTextures.Count;i++)
        {
            emguMatches[i] = Assets.Scripts.Emgu.ImageRecognition.FindMatch(raceTemplateTextures[i], extractedTexture);
            Debug.Log("SIFT match value: " + emguMatches[i] + "  with template" + (Enums.RaceType)(i+1));

            if (!moreThanOneEmguMatch)
            {
                for (int j = 0; j < i; j++)
                {
                    if (emguMatches[j] && emguMatches[i])
                    {
                        moreThanOneEmguMatch = true;
                    }
                }
            }
        }*/
        //Debug.Log("SIFT Best match value: " + bestMatch + "  with template" + (Enums.RaceType)bestMatchId);

        float bucket4Sum = 0;
        float bucket6Sum = 0;
        float bucket12Sum = 0;

        float best4bucketMatch = 100;
        float best6bucketMatch = 100;
        float best12bucketMatch = 100;
        float[] bestMatches = new float[raceTemplateHistograms.Length / numberOfRegionHistogramsComparison];
        float[] sum12buckets = new float[raceTemplateHistograms.Length / numberOfRegionHistogramsComparison];
        float[] sum6buckets = new float[raceTemplateHistograms.Length / numberOfRegionHistogramsComparison];
        float[] sum4buckets = new float[raceTemplateHistograms.Length / numberOfRegionHistogramsComparison];
        //float lowest12BucketMatch = 10;
        int best4BucketMatchIndex = 0;
        int best6BucketMatchIndex = 0;
        int best12BucketMatchIndex = 0;

        for (int i = 0; i < raceTemplateHistograms.Length / numberOfRegionHistogramsComparison; i++)
        {
            float worse12BucketMatch = 0;
            float worse4BucketMatch = 0;
            float worse6BucketMatch = 0;

            float better12BucketMatch = 10;
            float better4BucketMatch = 10;
            float better6BucketMatch = 10;
            bucket6Sum = 0;
            bucket12Sum = 0;
            bucket4Sum = 0;
            for (int j = 0; j < numberOfRegionHistogramsComparison; j++)
            {
                //Debug.Log();
                int currentIndex = numberOfRegionHistogramsComparison * i + j;
                float tMatch6noGrey = ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistance(histogram, raceTemplateHistograms[currentIndex], false);
                /*ColorHistogram.compareColorFactorWithHalfChiSquaredDistance(ColorHistogram.get6BucketFactorHistogram(histogram.greyScale),
                ColorHistogram.get6BucketFactorHistogram(raceTemplateHistograms[i].greyScale));*/
                float tMatch4noGrey = ColorHistogram.compareColorHistogramsBy4BucketsWithChiSquaredDistance(histogram, raceTemplateHistograms[currentIndex], false);
                /*ColorHistogram.compareColorFactorWithHalfChiSquaredDistance(ColorHistogram.get4BucketFactorHistogram(histogram.greyScale),
                 ColorHistogram.get4BucketFactorHistogram(raceTemplateHistograms[i].greyScale));*/
                float tMatch12noGrey = ColorHistogram.compareColorHistogramsBy12BucketsWithChiSquaredDistance(histogram, raceTemplateHistograms[currentIndex], false);

                //float tMatch6 = tMatch6noGrey;
                float tMatch6 = ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistance(histogram, raceTemplateHistograms[currentIndex], true);
                /*ColorHistogram.compareColorFactorWithHalfChiSquaredDistance(ColorHistogram.get6BucketFactorHistogram(histogram.greyScale),
                ColorHistogram.get6BucketFactorHistogram(raceTemplateHistograms[i].greyScale));*/
                //float tMatch4 = tMatch4noGrey;
                float tMatch4 = ColorHistogram.compareColorHistogramsBy4BucketsWithChiSquaredDistance(histogram, raceTemplateHistograms[currentIndex], true);
                /*ColorHistogram.compareColorFactorWithHalfChiSquaredDistance(ColorHistogram.get4BucketFactorHistogram(histogram.greyScale),
                 ColorHistogram.get4BucketFactorHistogram(raceTemplateHistograms[i].greyScale));*/
                //float tMatch12 = tMatch12noGrey;
                float tMatch12 = ColorHistogram.compareColorHistogramsBy12BucketsWithChiSquaredDistance(histogram, raceTemplateHistograms[currentIndex], true);

                if (tMatch12 > worse12BucketMatch)
                {
                    worse12BucketMatch = tMatch12;
                }
                if (tMatch6 > worse6BucketMatch)
                {
                    worse6BucketMatch = tMatch6;
                }
                if (tMatch4 > worse4BucketMatch)
                {
                    worse4BucketMatch = tMatch4;
                }

                if (tMatch12 < better12BucketMatch)
                {
                    better12BucketMatch = tMatch12;
                }
                if (tMatch6 < better6BucketMatch)
                {
                    better6BucketMatch = tMatch6;
                }
                if (tMatch4 < better4BucketMatch)
                {
                    better4BucketMatch = tMatch4;
                }
                /*
                    ColorHistogram.compareColorFactorWithHalfChiSquaredDistance(ColorHistogram.get12BucketFactorHistogram(histogram.greyScale),
                 ColorHistogram.get12BucketFactorHistogram(raceTemplateHistograms[i].greyScale));*/
                //float tMatch6NoGrey = ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistanceWithoutGreyscale(histogram, raceTemplateHistograms[i]);
                //Debug.Log("Comparing with: " + (Enums.RaceType)((i / cameraObscura3.racesTemplatesPerDetection) + 1) + " value: " + tMatch6);
                //Debug.Log("Comparing with: " + (Enums.RaceType)((i / numberOfRegionHistogramsComparison) + 1));
                /*Debug.Log(
                    "value6: " + tMatch6 +
                    " value4: " + tMatch4 +
                    " value12: " + tMatch12 +
                    " value4NoGrey: " + tMatch4noGrey +
                    " value6NoGrey: " + tMatch6noGrey +
                    " value12NoGrey: " + tMatch12noGrey
                    );*/
                bucket4Sum += tMatch4;
                bucket6Sum += tMatch6;
                bucket12Sum += tMatch12;
            }
            bestMatches[i] = better12BucketMatch;
            //Discard the worst and best match for every racetype
            /*bucket12Sum -= worse12BucketMatch;
            bucket4Sum -= worse4BucketMatch;
            bucket6Sum -= worse6BucketMatch;
            bucket12Sum -= better12BucketMatch;
            bucket4Sum -= better4BucketMatch;
            bucket6Sum -= better6BucketMatch;*/

            bucket12Sum /= (numberOfRegionHistogramsComparison);
            bucket4Sum /= (numberOfRegionHistogramsComparison);
            bucket6Sum /= (numberOfRegionHistogramsComparison);

            /*bucket12Sum /= (numberOfRegionHistogramsComparison - 2);
            bucket4Sum /= (numberOfRegionHistogramsComparison - 2);
            bucket6Sum /= (numberOfRegionHistogramsComparison - 2);*/

            sum12buckets[i] = bucket12Sum;
            sum6buckets[i] = bucket6Sum;
            sum4buckets[i] = bucket4Sum;

            if (bucket12Sum < best12bucketMatch)
            {
                best12bucketMatch = bucket12Sum;
                best12BucketMatchIndex = i;
            }
            if (bucket4Sum < best4bucketMatch)
            {
                best4bucketMatch = bucket4Sum;
                best4BucketMatchIndex = i;
            } 
            if (bucket6Sum < best6bucketMatch)
            {
                best6bucketMatch = bucket6Sum;
                best6BucketMatchIndex = i;
            }
            /*Debug.Log("Race: " + (Enums.RaceType)(i + 1) +
                    "   sum4: " + bucket4Sum +
                    "   sum6: " + bucket6Sum +
                    "   sum12: " + bucket12Sum);*/

            /*if (tMatch6 < matchValue6)
            {
                matchedTemplateIndex6 = i;
                matchValue6 = tMatch6;
            }*/
        }
        int matchIndex = best12BucketMatchIndex;
        //Debug.Log((best12BucketMatchIndex != best4BucketMatchIndex)+ "    " +(best12BucketMatchIndex != best6BucketMatchIndex));
        if (best12BucketMatchIndex != best4BucketMatchIndex || best12BucketMatchIndex != best6BucketMatchIndex)
        {
            //Debug.Log((best6BucketMatchIndex == best12BucketMatchIndex) +"   "+ (Mathf.Abs(best12bucketMatch - best6bucketMatch) < 0.02f));
            if (best6BucketMatchIndex == best12BucketMatchIndex && best12bucketMatch - best6bucketMatch < 0.02f)
            {
                matchIndex = best12BucketMatchIndex;
            }
            /*else if(best4BucketMatchIndex == best12BucketMatchIndex)
            {
                matchIndex = best12BucketMatchIndex;
            }*/
            else
            {
                float[] diffResult = new float[sum12buckets.Length];
                int bestDiffResultIndex = 0;
                for (int w = 0; w < diffResult.Length; w++)
                {
                    float sub12_6 = sum12buckets[w] - sum6buckets[w];
                    float sub6_4 = sum6buckets[w] - sum4buckets[w];
                    if (sub12_6 > 0.35f || sub6_4 > 0.35f)
                    {
                        diffResult[w] = 1;
                    }
                    else
                    {

                        diffResult[w] = Mathf.Abs(1 - ((sub12_6) / sub6_4));
                    }
                    //Debug.Log("Diff result: " + w + ":" + bestDiffResultIndex + "      " + (diffResult[w] < diffResult[bestDiffResultIndex]));
                    if (diffResult[w] < diffResult[bestDiffResultIndex])
                    {
                        bestDiffResultIndex = w;
                    }
                }
                matchIndex = bestDiffResultIndex;
                /*float lowestSumDiff = sum12buckets[0] - sum6buckets[0];
                int lowestIndex = 0;
                for (int w = 1; w < sum12buckets.Length; w++)
                {
                    float diff = sum12buckets[w]-sum6buckets[w];
                    if(lowestSumDiff> diff)
                    {
                        lowestSumDiff = diff;
                        lowestIndex = w;
                    }
                }
                matchIndex = lowestIndex + 1;*/
                //}
            }
        }
//        int matchIndex = (matchedTemplateIndex6 / numberOfRegionHistogramsComparison) + 1;
        //Debug.Log("Histogram Best match with template" + ((Enums.RaceType)matchIndex+1) + " in Region: " + pawnRegionIndex);

        if (moreThanOneEmguMatch)
        {
            setRaceType(matchIndex+1);
        }
    }
    public void detectPawnRace3(Texture2D inputT, List<Texture2D> raceTemplateTextures, ColorHistogram[] raceTemplateHistograms)
    {
        bool[] emguMatches = new bool[raceTemplateTextures.Count];
        bool moreThanOneEmguMatch = true;
        ColorHistogram histogram = cutPawnHistogram(inputT);
        /*for(int i=0;i<raceTemplateTextures.Count;i++)
        {
            emguMatches[i] = Assets.Scripts.Emgu.ImageRecognition.FindMatch(raceTemplateTextures[i], extractedTexture);
            Debug.Log("SIFT match value: " + emguMatches[i] + "  with template" + (Enums.RaceType)(i+1));

            if (!moreThanOneEmguMatch)
            {
                for (int j = 0; j < i; j++)
                {
                    if (emguMatches[j] && emguMatches[i])
                    {
                        moreThanOneEmguMatch = true;
                    }
                }
            }
        }*/
        //Debug.Log("SIFT Best match value: " + bestMatch + "  with template" + (Enums.RaceType)bestMatchId);

        int matchedTemplateIndex6 = 0;

        float matchValue6 = 100000;
        for (int i = 0; i < raceTemplateHistograms.Length; i++)
        {
            //Debug.Log();
            float tMatch6 = ColorHistogram.compareColorFactorWithHalfChiSquaredDistance(ColorHistogram.get6BucketFactorHistogram(histogram.greyScale),
            ColorHistogram.get6BucketFactorHistogram(raceTemplateHistograms[i].greyScale));
            //float tMatch6 = ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistanceWithoutGreyscale(histogram, raceTemplateHistograms[i]);
            //Debug.Log("Comparing with: " + (Enums.RaceType)(i + 1) +" value: "+ tMatch6);
            if (tMatch6 < matchValue6)
            {
                matchedTemplateIndex6 = i;
                matchValue6 = tMatch6;
            }
        }
        int matchIndex = (matchedTemplateIndex6 / cameraObscura3.racesTemplatesPerDetection) + 1;
        //Debug.Log("Histogram Best match value: " + matchIndex + "  with template" + ((Enums.RaceType)matchIndex));

        if (moreThanOneEmguMatch)
        {
            setRaceType(matchIndex);
        }
        else
        {
            for (int i = 0; i < emguMatches.Length; i++)
            {
                if (emguMatches[i])
                {
                    setRaceType(i + 1);
                }
            }
        }
    }
    
    public void detectPawnRace2(Texture2D inputT, List<Texture2D> raceTemplateTextures, List<ColorHistogram> raceTemplateHistograms)
    {
        bool[] emguMatches = new bool[raceTemplateTextures.Count];
        bool moreThanOneEmguMatch = true;
        Texture2D extractedTexture = cutPawnFromImage(inputT);
        /*for(int i=0;i<raceTemplateTextures.Count;i++)
        {
            emguMatches[i] = Assets.Scripts.Emgu.ImageRecognition.FindMatch(raceTemplateTextures[i], extractedTexture);
            Debug.Log("SIFT match value: " + emguMatches[i] + "  with template" + (Enums.RaceType)(i+1));

            if (!moreThanOneEmguMatch)
            {
                for (int j = 0; j < i; j++)
                {
                    if (emguMatches[j] && emguMatches[i])
                    {
                        moreThanOneEmguMatch = true;
                    }
                }
            }
        }*/
        //Debug.Log("SIFT Best match value: " + bestMatch + "  with template" + (Enums.RaceType)bestMatchId);

        int matchedTemplateIndex6 = 0;
        ColorHistogram histogram = new ColorHistogram(extractedTexture);

        float matchValue6 = 100000;
        for (int i = 0; i < raceTemplateHistograms.Count; i++)
        {
            //Debug.Log();
            float tMatch6 = ColorHistogram.compareColorFactorWithHalfChiSquaredDistance(ColorHistogram.get6BucketFactorHistogram(histogram.greyScale),
            ColorHistogram.get6BucketFactorHistogram(raceTemplateHistograms[i].greyScale));
            //float tMatch6 = ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistanceWithoutGreyscale(histogram, raceTemplateHistograms[i]);
            //Debug.Log("Comparing with: " + (Enums.RaceType)(i + 1) +" value: "+ tMatch6);
            if (tMatch6 < matchValue6)
            {
                matchedTemplateIndex6 = i;
                matchValue6 = tMatch6;
            }
        }
        int matchIndex = (matchedTemplateIndex6/cameraObscura3.racesTemplatesPerDetection)+1;
        //Debug.Log("Histogram Best match value: " + matchIndex + "  with template" + ((Enums.RaceType)matchIndex));

        if (moreThanOneEmguMatch)
        {
            setRaceType(matchIndex);
        }
        else
        {
            for (int i = 0; i < emguMatches.Length; i++)
            {
                if (emguMatches[i])
                {
                    setRaceType(i+1);
                }
            }
        }
    }
    public void detectPawnRace(Texture2D inputT, ColorHistogram[] raceTemplateHistograms)
    {
        Texture2D extractedTexture = cutPawnFromImage(inputT);

       /* Vector2 topLeft = myEnclosingRect.getCorner(0);
        Vector2 topRight = myEnclosingRect.getCorner(1);
        Vector2 bottomRight = myEnclosingRect.getCorner(2);
        Vector2 bottomLeft = myEnclosingRect.getCorner(3);

        //Debug.Log(topLeft + "     " + topRight + "       " + bottomRight + "     " + bottomLeft);

        List<Vector2> top = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, topRight);
        List<Vector2> left = Assets.Scripts.Functions.detectPixelsOnLine(topLeft, bottomLeft);
        List<Vector2> right = Assets.Scripts.Functions.detectPixelsOnLine(topRight, bottomRight);

        //Debug.Log(topLeft+"     "+ topRight+"       "+bottomRight+"     "+bottomLeft);

        Texture2D extractedImage = new Texture2D(top.Count, left.Count);
        Texture2D extractedImageS = new Texture2D(top.Count, left.Count);
        //Texture2D extractedImage1 = new Texture2D(top.Count, left.Count);
        //Texture2D extractedImage2 = new Texture2D(top.Count, left.Count);
        //Texture2D extractedImage3 = new Texture2D(top.Count, left.Count);

        int pixelNum = left.Count <= right.Count ? left.Count : right.Count;
        for (int i = 0; i < pixelNum; i++)
        {

            Vector2 startPixel = left[i];
            Vector2 endPixel = right[i];

            List<Vector2> linePixels = Assets.Scripts.Functions.detectPixelsOnLine(startPixel, endPixel);

            for (int j = 0; j < linePixels.Count; j++)
            {
                extractedImageS.SetPixel(i, j, Assets.Scripts.Functions.getColorAroundPixel(inputT, (int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y));
                extractedImage.SetPixel(i, j, inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y));
                //extractedImage1.SetPixel(i, j, inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y) / 2);
                //extractedImage3.SetPixel(i, j, inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y) * 1.2f);
                /*extractedImage2.SetPixel(i, j, 20 * new Color(
                    Mathf.Exp(inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y).r / 100),
                    Mathf.Exp(inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y).g / 100),
                    Mathf.Exp(inputT.GetPixel((int)linePixels[j].x, CameraControler.cameraCaptureHeight - (int)linePixels[j].y).b / 100)
                    ));
                */
               /* extractedImage.Apply();
                //extractedImage1.Apply();
                //extractedImage2.Apply();
                //extractedImage3.Apply();
                extractedImageS.Apply();

            }
        }
        Assets.Scripts.Functions.saveTexture(extractedImage, "extractedImage", "");
        Assets.Scripts.Functions.saveTexture(extractedImageS, "extractedImageS", "");*/
        //Assets.Scripts.Functions.saveTexture(extractedImage1, "extractedImage1", "");
        //Assets.Scripts.Functions.saveTexture(extractedImage2, "extractedImage2", "");
        //Assets.Scripts.Functions.saveTexture(extractedImage3, "extractedImage3", "");

        int matchedTemplateIndex6 = 0;
        int matchedTemplateIndex4 = 0;
        ColorHistogram histogram = new ColorHistogram(extractedTexture);
        //ColorHistogram histogram1 = new ColorHistogram(extractedImage1);
        //ColorHistogram histogram2 = new ColorHistogram(extractedImage2);
        //ColorHistogram histogram3 = new ColorHistogram(extractedImage3);
        //ColorHistogram histogramS = new ColorHistogram(extractedImageS);

        float matchValue6 = 100000;
        float matchValue4 = 100000;
        for (int i = 0; i < raceTemplateHistograms.Length; i++)
        {
            //Debug.Log("Comparing with: " + (Enums.RaceType)(i+1));
            float tMatch6 = ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistance(histogram, raceTemplateHistograms[i],true);
            //Debug.Log(tMatch6);
            if (tMatch6 < matchValue6)
            {
                matchedTemplateIndex6 = i;
                matchValue6 = tMatch6;
            }
            //Debug.Log(ColorHistogram.compareColorHistogramsBy6BucketsWithChiSquaredDistance(histogramS, raceTemplateHistograms[i]));
            //Debug.Log(ColorHistogram.compareColorFactorWithChiSquaredDistance(ColorHistogram.get6BucketFactorHistogram(histogram.greyScale), ColorHistogram.get6BucketFactorHistogram(raceTemplateHistograms[i].greyScale)));
            //Debug.Log("bucket4");
            float tMatch4 = ColorHistogram.compareColorHistogramsBy4BucketsWithChiSquaredDistance(histogram, raceTemplateHistograms[i],true);
            //Debug.Log("Match4: "+tMatch4);
            if (tMatch4 < matchValue4)
            {
                matchedTemplateIndex4 = i;
                matchValue4 = tMatch4;
            }
            //Debug.Log(ColorHistogram.compareColorHistogramsBy4BucketsWithChiSquaredDistance(histogramS, raceTemplateHistograms[i]));
            //Debug.Log(ColorHistogram.compareColorFactorWithChiSquaredDistance(ColorHistogram.get4BucketFactorHistogram(histogram.greyScale), ColorHistogram.get4BucketFactorHistogram(raceTemplateHistograms[i].greyScale)));

        }

        if (matchedTemplateIndex6 == matchedTemplateIndex4)
        {
            int matchIndex = (matchedTemplateIndex6+1);
            setRaceType((Enums.RaceType)matchIndex);
        }
        else
        {
            if (matchValue4 <= matchValue6)
            {
                int matchIndex = (matchedTemplateIndex4 + 1);
                setRaceType((Enums.RaceType)matchIndex);
            }
            else
            {
                int matchIndex = (matchedTemplateIndex6 + 1);
                setRaceType((Enums.RaceType)matchIndex);
            }

        }
    }
}
