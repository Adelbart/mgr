﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Emgu.CV.Util;

namespace Assets.Scripts.Structures
{
    public class MyCircleCollider
    {
        private Vector2 center;
        private float radius;

        public MyCircleCollider(Vector2 c, float r)
        {
            center = c;
            radius = r;
        }

        public bool intersectsCircle(MyCircleCollider other)
        {
            float centreDistance = Mathf.Sqrt(Mathf.Pow(center.x-other.center.x,2)+Mathf.Pow(center.y-other.center.y,2));
            float radiusSum = radius + other.radius;
            return centreDistance <= radiusSum;
        }

        public static bool intersectsCircle(VectorOfPoint A, VectorOfPoint B)
        {
            Vector2 centreA = Vector2.zero;
            for (int i = 0; i < A.Size; i++)
            {
                centreA.x += A[i].X;
                centreA.y += A[i].Y;
            }
            centreA /= A.Size;
            
            Vector2 centreB = Vector2.zero;
            for (int i = 0; i < B.Size; i++)
            {
                centreB.x += B[i].X;
                centreB.y += B[i].Y;
            }
            centreB /= B.Size;
            
            float radiusA = 0;
            /*for (int i = 0; i < A.Size; i++)
            {
                float tRA = Functions.distanceAB_float(centreA, new Vector2(A[i].X, A[i].Y));
                if (tRA > radiusA)
                {
                    radiusA = tRA;
                }
            }
            */
            float radiusB = 0;
           /* for (int i = 0; i < B.Size; i++)
            {
                float tRB = Functions.distanceAB_float(centreB, new Vector2(B[i].X, B[i].Y));
                if (tRB > radiusB)
                {
                    radiusB = tRB;
                }
            }*/

            float centreDistance = Mathf.Sqrt(Mathf.Pow(centreA.x-centreB.x,2)+Mathf.Pow(centreA.y-centreB.y,2));
            float radiusSum = radiusA + radiusB;
            return centreDistance <= radiusSum;
        }
        public Vector2 getCenterPosition()
        {
            return center;
        }
        public float getRadius()
        {
            return radius;
        }
    }
}
