﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Emgu.CV.Structure;
using Emgu.CV;

public class AssetManager : MonoBehaviour {

    public string playersScoreFile = "players_score.txt";
    private static AssetManager assetManagerSingleton;
    public Texture2D winTexture;
    public Texture2D histogramColorMap;
    public Texture2D amazonTexture;
    public Texture2D orcTexture;
    public Texture2D skeletonTexture;
    public Sprite[] raceTexturesSprites;
    public Sprite troopSprite;
    public Texture2D[] pawnTextures;
    public Texture2D[] regionsNeutralTextures;
    public Texture2D[] regionsOwnedTextures;
    public Texture2D[] regionsConquarableTextures;
    public Texture2D[] regionsNotVisibleTextures;
    public Texture2D[] linesConquaredTextures;
    public Texture2D[] linesVisibleTexture;
    public Texture2D[] linesEnemyVisibleTexture;
    public Texture2D[] linesEnemyNotVisibleTexture;
    public Texture2D[,] regionsValidForExtraIncomeNormal;
    public Texture2D[] regionsValidForExtraIncomeNormalInputTextures;
    public Texture2D[,] regionsValidForExtraIncomeFaded;
    public Texture2D[] regionsValidForExtraIncomeFadedInputTextures;
    public Texture2D[] regionExtraFeatureCaves;
    public List<Texture2D> pawnTemplates;
    public Texture2D[] racePatternTextures;
    public Texture2D textureTemp;
    public Texture2D textureTemp2;
    public Texture2D errorCalibTexture;
    public Texture2D calibTexture;
    public Texture2D colorTexture;
    public Texture2D lastColorTexture;
    public Texture2D pawnTexture;
    public Texture2D whiteTexture;
    public Texture2D mapTexture;
    public Texture2D raceTexture;
    public Texture2D errorTexture;
    public Texture2D tickTexture;
    public Texture2D caveFeatureJoinedTexture;
    public Texture2D backgroundImageCaptured;
    public Texture2D whiteBackground;
    public Image<Gray,byte> whiteBackgroundEmguImage;
    public Texture2D match1;
    public Texture2D match2;
    public Texture2D match3;
    public Texture2D match4;
    public Sprite gearbox;
    public Sprite textureTempSprite;
    public Sprite blueSprite;
    public Sprite hand;
    public Texture2D mapRegionColor;
    public MovieTexture movieTexture;
    public GameObject imageHolder;
    public GameObject regionTextureHolderInstance;
    public GameObject lineTextureHolderInstance;
    public GameObject UIElementPrefab;
    public Sprite[] UIElementsTextures;
    public Sprite circleGreen;
    public Sprite circleRed;
    public Sprite whiteSprite;
    public Sprite pawnRemoval;
    public int _uvTieX = 4;
    public int _uvTieY = 1;
    public int _uvTieX2 = 5;
    public int _uvTieY2 = 4;
    public Texture2D[] calibrationTextures = new Texture2D[3];         //Textures to show during Calibration  /0-functionInProcess /1- pawnSize /2-patternTraining

    public static int pawnExtractionDebugIndex = 0;
    void Awake()
    {
        regionsValidForExtraIncomeFaded = new Texture2D[regionsValidForExtraIncomeFadedInputTextures.Length, _uvTieX * _uvTieY];
        regionsValidForExtraIncomeNormal = new Texture2D[regionsValidForExtraIncomeNormalInputTextures.Length, _uvTieX * _uvTieY];
        regionExtraFeatureCaves = new Texture2D[_uvTieX2 * _uvTieY2]; 
        assetManagerSingleton = this;
        for(int i =0;i<regionsValidForExtraIncomeFadedInputTextures.Length;i++)
        {
            if (regionsValidForExtraIncomeFadedInputTextures[i] != null)
            {
                for (int j = 0; j < _uvTieX * _uvTieY; j++)
                {
                    regionsValidForExtraIncomeFaded[i, j] = Assets.Scripts.Functions.cutSpriteFromAnimatedTexture(regionsValidForExtraIncomeFadedInputTextures[i], _uvTieX, _uvTieY, j);
                }
            regionsValidForExtraIncomeFadedInputTextures[i] = null;
            }

        } 
        for (int i = 0; i < regionsValidForExtraIncomeNormalInputTextures.Length; i++)
        {
            if (regionsValidForExtraIncomeNormalInputTextures[i] != null)
            {
                for (int j = 0; j < _uvTieX * _uvTieY; j++)
                {
                    regionsValidForExtraIncomeNormal[i, j] = Assets.Scripts.Functions.cutSpriteFromAnimatedTexture(regionsValidForExtraIncomeNormalInputTextures[i], _uvTieX, _uvTieY, j);
                }
                regionsValidForExtraIncomeNormalInputTextures[i] = null;
            }
        }
        for (int i = 0; i < _uvTieX2 * _uvTieY2; i++)
        {
            regionExtraFeatureCaves[i] = Assets.Scripts.Functions.cutSpriteFromAnimatedTexture(caveFeatureJoinedTexture, _uvTieX2, _uvTieY2, i);
            //Assets.Scripts.Functions.saveTexture(regionExtraFeatureCaves[i], "cave" + i);
        }
    }
    static AssetManager getAssetManagerSingleton()
    {
        if (assetManagerSingleton != null)
        {
            return assetManagerSingleton;
        }
        else
        {
            assetManagerSingleton = new AssetManager();
            return assetManagerSingleton;
        }
    }
    public Texture2D getPawnTemplate(int index)
    {
        return pawnTemplates[index - 1];
    }
    public static string getNextPawnIndex()
    {
        pawnExtractionDebugIndex++;
        return pawnExtractionDebugIndex.ToString();
    }
}
