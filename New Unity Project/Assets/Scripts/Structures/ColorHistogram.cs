﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Structures
{
    public class ColorHistogram
    {
        int numberofPixels = 0;
        float[] redHistogram = new float[256];
        float[] red12Bucket = new float[12];
        float[] red6Bucket = new float[8];
        float[] red4Bucket = new float[4];
        float[] greenHistogram = new float[256];
        float[] green12Bucket = new float[12];
        float[] green6Bucket = new float[8];
        float[] green4Bucket = new float[4];
        float[] blueHistogram = new float[256];
        float[] blue12Bucket = new float[12];
        float[] blue6Bucket = new float[8];
        float[] blue4Bucket = new float[4];
        public float[] greyScale = new float[256];
        float[] grey12Bucket = new float[12];
        float[] grey6Bucket = new float[8];
        float[] grey4Bucket = new float[4];

        public ColorHistogram() { }
        public ColorHistogram(string inputLine)
        {
            string[] histogramsLines = inputLine.Split(';');
            Debug.Log("Number of bucket histograms: " + histogramsLines.Length);
            numberofPixels = int.Parse(histogramsLines[0]);
            redHistogram = histogramFromFile(histogramsLines[1]);
            greenHistogram = histogramFromFile(histogramsLines[2]);
            blueHistogram = histogramFromFile(histogramsLines[3]);
            greyScale = histogramFromFile(histogramsLines[4]);
            calculateBuckets();
        }
        
        public ColorHistogram(Texture2D inputTexture)
        {
            Color32[] colors = inputTexture.GetPixels32();
            numberofPixels = inputTexture.width * inputTexture.height;
            for (int i = 0; i < colors.Length; i++)
            {
                if (!Usefull.TextureProcessing.colorCloseToWhite(colors[i]))
                {
                    //Debug.Log(colors[i]);
                    blueHistogram[colors[i].b] += 1;
                    //Debug.Log(colors[i].b);
                    greenHistogram[colors[i].g] += 1;
                    //Debug.Log(colors[i].g);
                    redHistogram[colors[i].r] += 1;
                    //Debug.Log(colors[i].r);

                    greyScale[(int)(colors[i].r * 0.3f + colors[i].g * 0.59f + colors[i].b * 0.11f)] += 1;
                }
            } 
            greyScale = normalizeHistogram(greyScale);
            blueHistogram = normalizeHistogram(blueHistogram);
            greenHistogram = normalizeHistogram(greenHistogram);
            redHistogram = normalizeHistogram(redHistogram);

            calculateBuckets();
            
        }
        public void addPixel(Color32 inputColor)
        {
            if (!Usefull.TextureProcessing.colorCloseToWhite(inputColor))
            {
                numberofPixels += 1;
                //Debug.Log(colors[i]);
                blueHistogram[inputColor.b] += 1;
                //Debug.Log(colors[i].b);
                greenHistogram[inputColor.g] += 1;
                //Debug.Log(colors[i].g);
                redHistogram[inputColor.r] += 1;
                //Debug.Log(colors[i].r);

                greyScale[(int)(inputColor.r * 0.3f + inputColor.g * 0.59f + inputColor.b * 0.11f)] += 1;
                //Debug.Log(inputColor);
                //Debug.Log("New pixel:" + inputColor.r + "  " + inputColor.g + "  " + inputColor.b);
            }
        }
        public void print4Buckets()
        {
            Debug.Log(red4Bucket[0] + "   " + red4Bucket[1] + "   " + red4Bucket[2] + "   " + red4Bucket[3]);
            Debug.Log(green4Bucket[0] + "   " + green4Bucket[1] + "   " + green4Bucket[2] + "   " + green4Bucket[3]);
            Debug.Log(blue4Bucket[0] + "   " + blue4Bucket[1] + "   " + blue4Bucket[2] + "   " + blue4Bucket[3]);
            Debug.Log(grey4Bucket[0] + "   " + grey4Bucket[1] + "   " + grey4Bucket[2] + "   " + grey4Bucket[3]);
        }
        public void print6Buckets()
        {
            string printBucket = "";
            for(int i = 0; i < red6Bucket.Length; i++)
            {
                printBucket += red6Bucket[i] + "   ";  
            }
            Debug.Log(printBucket);
            printBucket = "";
            for (int i = 0; i < green6Bucket.Length; i++)
            {
                printBucket += green6Bucket[i] + "   ";
            }
            Debug.Log(printBucket);
            printBucket = "";
            for (int i = 0; i < blue6Bucket.Length; i++)
            {
                printBucket += blue6Bucket[i] + "   ";
            }
            Debug.Log(printBucket);
            printBucket = "";
            for (int i = 0; i < grey6Bucket.Length; i++)
            {
                printBucket += grey6Bucket[i] + "   ";
            }
            Debug.Log(printBucket);
        }
        public void print12Buckets()
        {
            string printBucket = "";
            for (int i = 0; i < red12Bucket.Length; i++)
            {
                printBucket += red12Bucket[i] + "   ";
            }
            Debug.Log(printBucket);
            printBucket = "";
            for (int i = 0; i < green12Bucket.Length; i++)
            {
                printBucket += green12Bucket[i] + "   ";
            }
            Debug.Log(printBucket);
            printBucket = "";
            for (int i = 0; i < blue12Bucket.Length; i++)
            {
                printBucket += blue12Bucket[i] + "   ";
            }
            Debug.Log(printBucket);
            printBucket = "";
            for (int i = 0; i < grey12Bucket.Length; i++)
            {
                printBucket += grey12Bucket[i] + "   ";
            }
            Debug.Log(printBucket);
        }
        public int getNumberOfPixels()
        {
            return numberofPixels;
        }
        public float[] equalizeHistogram(float[] inputHistogram)
        {
            float currentCDFvalue = 0;
            float minCDFvalue = 0;
            float[] resultHistogram = new float[inputHistogram.Length];
            for (int i = 0; i < inputHistogram.Length; i++)
            {
                if (inputHistogram[i] != 0)
                {
                    if(currentCDFvalue == 0)
                    {
                        currentCDFvalue += inputHistogram[i];
                        minCDFvalue = currentCDFvalue;

                        resultHistogram[i] = 0;
                    }
                    else
                    {
                        currentCDFvalue += inputHistogram[i];
                        resultHistogram[i] = Mathf.Floor(((currentCDFvalue - minCDFvalue) / (numberofPixels - 1)) * 255);
                    }
                }
            }
            return inputHistogram;
        }
        public void normalizeHistogramAndCalculateBuckets()
        {

            greyScale = normalizeHistogram(equalizeHistogram(greyScale));
            blueHistogram = normalizeHistogram(equalizeHistogram(blueHistogram));
            greenHistogram = normalizeHistogram(equalizeHistogram(greenHistogram));
            redHistogram = normalizeHistogram(equalizeHistogram(redHistogram));

            calculateBuckets();
        }
        void calculateBuckets()
        {
            red12Bucket = get12BucketFactorHistogram(redHistogram);
            green12Bucket = get12BucketFactorHistogram(greenHistogram);
            blue12Bucket = get12BucketFactorHistogram(blueHistogram);
            grey12Bucket = get12BucketFactorHistogram(greyScale);

            red6Bucket = get6BucketFactorHistogram(redHistogram);
            green6Bucket = get6BucketFactorHistogram(greenHistogram);
            blue6Bucket = get6BucketFactorHistogram(blueHistogram);
            grey6Bucket = get6BucketFactorHistogram(greyScale);

            red4Bucket = get4BucketFactorHistogram(redHistogram);
            green4Bucket = get4BucketFactorHistogram(greenHistogram);
            blue4Bucket = get4BucketFactorHistogram(blueHistogram);
            grey4Bucket = get4BucketFactorHistogram(greyScale);
        }
        public float[] histogramFromFile(string line)
        {
            float[] result = new float[256];
            string[] particleInts = line.Split(':');
            Debug.Log("Histogram length: " + particleInts.Length);
            for (int i = 0; i < particleInts.Length; i++)
            {
                result[i] = float.Parse(particleInts[i]);
            }
            
                return result;
        }
        public string HistogramToString()
        {
            string result = numberofPixels.ToString();
            result += ";";
            for (int i = 0; i < redHistogram.Length; i++)
            {
                result+=redHistogram[i];
                if(i<redHistogram.Length-1)
                {
                    result+=":";
                }
            }
            result += ";";
            for (int i = 0; i < greenHistogram.Length; i++)
            {
                result += greenHistogram[i];
                if (i < greenHistogram.Length - 1)
                {
                    result += ":";
                }
            }
            result += ";";
            for (int i = 0; i < blueHistogram.Length; i++)
            {
                result += blueHistogram[i];
                if (i < blueHistogram.Length - 1)
                {
                    result += ":";
                }
            }
            result += ";";
            for (int i = 0; i < greyScale.Length; i++)
            {
                result += greyScale[i];
                if (i < greyScale.Length - 1)
                {
                    result += ":";
                }
            }

            return result;
        }
        public static float compareColorFactorWithChiSquaredDistance(float[] A, float[] B)
        {
            float result = 0;

            if (A.Length == B.Length)
            {
                for (int i = 0; i < A.Length; i++)
                {
                    if (A[i] != 0 || B[i] != 0)
                    {
                        result += Mathf.Pow((A[i] - B[i]), 2) / (A[i] + B[i]);
                    }
                }
            }
            return result;
        }
        public float[] normalizeHistogram(float[] inputHistogram)
        {
            float[] result = new float[inputHistogram.Length];
            for (int i = 0; i < inputHistogram.Length; i++)
            {
                result[i] = inputHistogram[i] / numberofPixels;
                //result[i] = 255 * (inputHistogram[i] - lowestValue) / (diff);
            }
            return result;
        }
        public static float[] normalizeHistogram(float[] inputHistogram, int numberOfPixels)
        {
            float[] result = new float[inputHistogram.Length];
            for (int i = 0; i < inputHistogram.Length; i++)
            {
                result[i] = inputHistogram[i] / numberOfPixels;
                //result[i] = 255 * (inputHistogram[i] - lowestValue) / (diff);
            }
            return result;
        }
        public static float[] normalizeHistogram2(float[] inputHistogram)
        {
            float lowestValue = 1000;
            float highestValue = 0;
            float[] result = new float[inputHistogram.Length];

            for (int i = 0; i < inputHistogram.Length; i++)
            {
                if (inputHistogram[i] != 0 && inputHistogram[i] < lowestValue)
                {
                    lowestValue = inputHistogram[i];
                }
                if (inputHistogram[i] != 0 && inputHistogram[i] > highestValue)
                {
                    highestValue = inputHistogram[i];
                }
            }
            float diff = highestValue - lowestValue;
            for (int i = 0; i < inputHistogram.Length; i++)
            {
                result[i] = inputHistogram[i] * diff / 255;
                //result[i] = 255 * (inputHistogram[i] - lowestValue) / (diff);
            }
            return result;
        }
        public static float compareColorFactorWithHalfChiSquaredDistance(float[] A, float[] B)
        {
            float result = 0;

            if (A.Length == B.Length)
            {
                for (int i = 0; i < A.Length; i++)
                {
                    if (A[i] != 0 || B[i] != 0)
                    {
                        result += Mathf.Pow((A[i] - B[i]), 2) / (A[i] + B[i]);
                        //Debug.Log(A[i] + "  "  +B[i]);
                    }
                }
            }
            return result*0.5f;
        }
        public static float compareColorHistogramsByGrayscale(ColorHistogram A, ColorHistogram B)
        {
            float result = 0;
            result += compareColorFactorWithChiSquaredDistance(A.greyScale, B.greyScale);

            return result;

        }
        public static float compareColorHistogramsBy12BucketsWithChiSquaredDistance(ColorHistogram A, ColorHistogram B, bool useGreyScale)
        {
            float result = 0;

            result += compareColorFactorWithHalfChiSquaredDistance(A.red12Bucket, B.red12Bucket);
            result += compareColorFactorWithHalfChiSquaredDistance(A.green12Bucket, B.green12Bucket);
            result += compareColorFactorWithHalfChiSquaredDistance(A.blue12Bucket, B.blue12Bucket);
            if (useGreyScale)
            {
                result += compareColorFactorWithHalfChiSquaredDistance(A.grey12Bucket, B.grey12Bucket);
            }

            return result;
        }

        public static float compareColorHistogramsBy6BucketsWithChiSquaredDistance(ColorHistogram A, ColorHistogram B, bool useGreyScale)
        {
            float result = 0;

            result += compareColorFactorWithHalfChiSquaredDistance(A.red6Bucket, B.red6Bucket);
            result += compareColorFactorWithHalfChiSquaredDistance(A.green6Bucket,B.green6Bucket);
            result += compareColorFactorWithHalfChiSquaredDistance(A.blue6Bucket, B.blue6Bucket);
            if (useGreyScale)
            {
                result += compareColorFactorWithHalfChiSquaredDistance(A.grey6Bucket, B.grey6Bucket);
            }

            return result;
        }
       
        public static float compareColorHistogramsBy4BucketsWithChiSquaredDistance(ColorHistogram A, ColorHistogram B, bool useGreyScale)
        {
            float result = 0;

            result += compareColorFactorWithHalfChiSquaredDistance(A.red4Bucket, B.red4Bucket);
            result += compareColorFactorWithHalfChiSquaredDistance(A.green4Bucket,B.green4Bucket);
            result += compareColorFactorWithHalfChiSquaredDistance(A.blue4Bucket,B.blue4Bucket);
            if (useGreyScale)
            {
                result += compareColorFactorWithHalfChiSquaredDistance(A.grey4Bucket, B.grey4Bucket);
            }
            return result;
        }
        public static float compareColorHistogramsBy6BucketsWithChiSquaredDistanceWithoutGreyscale(ColorHistogram A, ColorHistogram B)
        {
            float result = 0;

            result += compareColorFactorWithHalfChiSquaredDistance(get6BucketFactorHistogram(A.redHistogram), get6BucketFactorHistogram(B.redHistogram));
            result += compareColorFactorWithHalfChiSquaredDistance(get6BucketFactorHistogram(A.greenHistogram), get6BucketFactorHistogram(B.greenHistogram));
            result += compareColorFactorWithHalfChiSquaredDistance(get6BucketFactorHistogram(A.blueHistogram), get6BucketFactorHistogram(B.blueHistogram));
            //result += compareColorFactorWithHalfChiSquaredDistance(get6BucketFactorHistogram(A.greyScale), get6BucketFactorHistogram(B.greyScale));

            return result;
        }
        public static float[] get12BucketFactorHistogram(float[] inputHistogram)
        {
            float[] result = new float[12];
            for (int i = 0; i < inputHistogram.Length; i++)
            {
                //Debug.Log(i / 20);
                result[i/22] += inputHistogram[i];
            }

            return result;
        }
        public float[] getRed6Bucket()
        {
            return red6Bucket;
        }
        public float[] getGreen6Bucket()
        {
            return green6Bucket;
        }
        public float[] getBlue6Bucket()
        {
            return blue6Bucket;
        }
        public static float[] get6BucketFactorHistogram(float[] inputHistogram)
        {
            float[] result = new float[8];
            for (int i = 0; i < inputHistogram.Length; i++)
            {
                //Debug.Log(i / 20);
                result[i / 32] += inputHistogram[i];
            }

            return result;
        }
        public float[] getRed4Bucket()
        {
            return red4Bucket;
        }
        public float[] getGreen4Bucket()
        {
            return green4Bucket;
        }
        public float[] getBlue4Bucket()
        {
            return blue4Bucket;
        }
        public static float[] get4BucketFactorHistogram(float[] inputHistogram)
        {
            float[] result = new float[4];
            for (int i = 0; i < inputHistogram.Length; i++)
            {
                //Debug.Log(i / 20);
                result[i / 64] += inputHistogram[i];
            }

            return result;
        }
        public static float compareColorHistograms(ColorHistogram A, ColorHistogram B)
        {
            float result = 0;
            //Debug.Log("A:"+result);
            result += compareColorFactorWithChiSquaredDistance(A.redHistogram, B.redHistogram);
            //Debug.Log("B:" + result);
            result += compareColorFactorWithChiSquaredDistance(A.greenHistogram, B.greenHistogram);
            //Debug.Log("C:" + result);
            result += compareColorFactorWithChiSquaredDistance(A.blueHistogram, B.blueHistogram);
            //Debug.Log("D:" + result);

            return result;
        }
        public float compareColorHistograms(ColorHistogram other)
        {
            float result = 0;

            result += compareColorFactorWithChiSquaredDistance(redHistogram, other.redHistogram);
            result += compareColorFactorWithChiSquaredDistance(greenHistogram, other.greenHistogram);
            result += compareColorFactorWithChiSquaredDistance(blueHistogram, other.blueHistogram);

            return result;
        }
        void setHistogram(float[] newHistogram, int number)
        {
            if (number == 0)
            {
                redHistogram = newHistogram;
            }
            else if (number == 1)
            {
                greenHistogram = newHistogram;

            }
            else if (number == 2)
            {
                blueHistogram = newHistogram;
            }
        }
    }
}
