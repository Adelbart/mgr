﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Structures
{
    struct EventInfo
    {
        public Enums.RegionEvents eventType;
        public int regionIndex;
    }
    class MapControler
    {
        private PanelManager panelManager;
        private AssetManager assetManager;
        private Texture2D regionsColorTexture;
        private Player player = new Player();
        private static string mapInfoFolder = "/MapsInfo/";
        private List<Region> regions = new List<Region>();
        private int[] regionsColorValue;
        private EventInfo currentEventShown = new EventInfo();
        private List<EventInfo> EventsQueue = new List<EventInfo>();
        private List<EventInfo> panelsEventsQueue = new List<EventInfo>();
        private int startingRegionIndex = 0;
        public static int numberOfRegions = 28;
        public bool isReadyForNextMove = true;
        GUITexture[] regionsStateTextures;
        GUITexture[] lineStateTextures;
        GUITexture[] regionExtraFeatureTexture;
        private int senarioLoaded = 0;
        private bool scenarioFinished = false;
        private bool eventResultIsBeingShown = false;
        private bool theShowingOfEventsHasEnded = false;
        private bool eventEnded = false;
        private float accumulatedEventTime = 0;
        private float eventupdateTime = 0;
        private float eventMaxTime = 2.0f;
        private float lastMoveTime = 0f;
        private GameObject UIElementsParent = null;

        public MapControler(Texture2D newTexture, PanelManager pM, AssetManager aS, GUITexture[] regionTextures, GUITexture[] lineTextures,GUITexture[] extraF, GameObject newUIElementsParent)
        {
            regionsColorTexture = newTexture;
            assetManager = aS;
            regionsStateTextures = regionTextures;
            lineStateTextures = lineTextures;
            regionExtraFeatureTexture = extraF;
            for (int i = 0; i < regionTextures.Length; i++)
            {
                regionTextures[i].texture = assetManager.regionsNotVisibleTextures[i];
            }

                panelManager = pM;
            UIElementsParent = newUIElementsParent;
        }
        public bool hadThePresentationOfEventsEndedInLastFrame()
        {
            return theShowingOfEventsHasEnded;
        }
        public void startScenario()
        {
            lastMoveTime = Time.realtimeSinceStartup;
            for (int i = 0; i < regions.Count; i++)
            {
                panelManager.changePanelTexture(i, assetManager.troopSprite);
            }
        }
        public void showTipsAfterEventsPresentation(GameObject UIElementsParent)
        {
            theShowingOfEventsHasEnded = false;
            Debug.Log("SHOWING PAWN HINTS!!");
            foreach (Region r in regions)
            {
                r.showPawnsHints(assetManager.UIElementPrefab, UIElementsParent,assetManager);
            }
        }
        public bool proceedToNextEvent()
        {
            eventEnded = false;
            if (EventsQueue.Count > 0)
            {

                Debug.Log("Next Event exists");
                eventResultIsBeingShown = true;
                currentEventShown = EventsQueue[0];
                Debug.Log(currentEventShown.regionIndex + "   " + currentEventShown.eventType);
                EventsQueue.RemoveAt(0);
                return true;
            }

            Debug.Log("Next Event do not exist");
            currentEventShown.eventType = Enums.RegionEvents.nullEvent;
            currentEventShown.regionIndex = -1;
            accumulatedEventTime = 0;
            eventupdateTime = 0;
            eventResultIsBeingShown = false;
            theShowingOfEventsHasEnded = true;
            return false;

        }
        void calculateEventStep(float deltaTime)
        {
            accumulatedEventTime += deltaTime;
            if (currentEventShown.eventType == Enums.RegionEvents.regionConquered)
            {
                //Debug.Log(accumulatedEventTime +"   "+ eventMaxTime);
                if (accumulatedEventTime > eventMaxTime)
                {
                    setRegionTextureToOwned(currentEventShown.regionIndex);
                    setRegionLinesToOwned(currentEventShown.regionIndex);
                    eventEnded = true;
                    Debug.Log("Event ready to be ended");
                }
                else
                {
                    if (eventupdateTime > 0.5f)
                    {
                        eventHandlerChangeRegionTextureDuringConquer(currentEventShown.regionIndex);
                        eventupdateTime = 0;
                    }
                    else
                    {
                        eventupdateTime += deltaTime;
                    }
                }
            }
            else if (currentEventShown.eventType == Enums.RegionEvents.regionDeinforced)
            {

                if (accumulatedEventTime > eventMaxTime)
                {
                    eventEnded = true;
                    Debug.Log("Event ready to be ended");
                }
            }
            else if (currentEventShown.eventType == Enums.RegionEvents.regionLeft)
            {

                if (accumulatedEventTime > eventMaxTime)
                {
                    eventEnded = true;
                    Debug.Log("Event ready to be ended");
                }
            }
            else if (currentEventShown.eventType == Enums.RegionEvents.regionReinforced)
            {

                if (accumulatedEventTime > eventMaxTime)
                {
                    eventEnded = true;
                    Debug.Log("Event ready to be ended");
                }
            }
            else if (currentEventShown.eventType == Enums.RegionEvents.returningToPreviousState)
            {
                if (accumulatedEventTime > eventMaxTime)
                {
                    eventEnded = true;
                    Debug.Log("Event ready to be ended");
                }
            }
            if (eventEnded)
            {
                accumulatedEventTime = 0;
                eventupdateTime = 0;

                if (!proceedToNextEvent())
                {
                    Debug.Log("Events ended, progressing to next move");
                    //panelManager.showPanel();
                    clearHints();
                    showHintsForNextUser();
                }
            }
        }
        public void progressEventResult(float deltaTime)
        {
            //Debug.Log("EventProgression");
            if (eventResultIsBeingShown)
            {
                //Debug.Log(currentEventShown.regionIndex+"   "+ currentEventShown.eventType);
                if (currentEventShown.eventType == Enums.RegionEvents.nullEvent)
                {
                    bool tmp = proceedToNextEvent();
                    if (tmp)
                    {
                        calculateEventStep(deltaTime);
                    }
                }
                else
                {
                    calculateEventStep(deltaTime);
                }
            }
            else
            {
                proceedToNextEvent();
            }
        }

        public bool isEventBeingShown()
        {
            return eventResultIsBeingShown;
        }
        public bool shouldEventbeShowed()
        {
            scenarioFinished = checkIfScenarioIsFinished();
            return EventsQueue.Count > 0;
        }
        public int getEventsCount()
        {
            return EventsQueue.Count;
        }
        public Player getPlayer()
        {
            return player;
        }
        public bool loadScenarioInfo(string path, out List<Region> desiredSetup)
        {
            desiredSetup = new List<Region>();
            string folderPath = Application.dataPath + mapInfoFolder + path;
            if (!File.Exists(folderPath))
            {
               
                return false;
            }


            StreamReader infoFile = File.OpenText(folderPath);

            bool firstLineRead = false;
            while (!infoFile.EndOfStream)
            {

                string line = infoFile.ReadLine();
                if (!firstLineRead)
                {
                    string[] scenraioInfo = line.Split(';');
                    player.setPlayerRace((Enums.RaceType)Int32.Parse(scenraioInfo[0]));
                    player.setMaxPawns(Int32.Parse(scenraioInfo[1]));
                    player.setRegionFeature((Enums.PlayerRegionFeature)Int32.Parse(scenraioInfo[2]));

                    firstLineRead = true;
                }
                else
                {
                    //NUMER REGIONU; NUMER RASY; ILOSC PIONOW
                    string[] scenarioInfo = line.Split(';');
                    desiredSetup.Add(new Region(Int32.Parse(scenarioInfo[0])));
                    regions[Int32.Parse(scenarioInfo[0])].setNewOwner((Enums.RaceType)Int32.Parse(scenarioInfo[1]));
                    regions[Int32.Parse(scenarioInfo[0])].setPreStartDesiredDistribution(Int32.Parse(scenarioInfo[2]));

                    for (int i = 0; i < Int32.Parse(scenarioInfo[2]);i++ )
                    {
                        desiredSetup[desiredSetup.Count - 1].addPawnToRegion(new Pawn((Enums.RaceType)Int32.Parse(scenarioInfo[1])));
                    }
                    Debug.Log("Region " + (desiredSetup.Count - 1) + "Owner: " + ((Enums.RaceType)Int32.Parse(scenarioInfo[1])) + " no of Pawns: " + desiredSetup[desiredSetup.Count - 1].getCurrentNumberOfPawnsInRegion());
                }
            }
            Debug.Log("scenario loaded");
            scenarioFinished = false;
            return true;
        }
        public void clearScenario()
        {
            foreach(Region r in regions)
            {
                r.clearRegionForNewPlay();
            }
        }
        public void setScenario(int index)
        {
            senarioLoaded = index;
        }
        public int getScenario()
        {
            return senarioLoaded;
        }
        public void clearScenarioInfo()
        {
            foreach(Region r in regions)
            {
                r.clearRegionForNewPlay();
            }
        }
        public void finishScenario()
        {
            scenarioFinished = true;
        }
        public bool areAllRegionsCleared()
        {
            foreach(Region r in regions)
            {
                if (r.getOwner() == player.getPlayerRace())
                {
                    if(r.getCurrentEvent() != Enums.RegionEvents.regionConquered)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public int getPlayerRegionsCount()
        {
            return player.getConqauredRegions().Count();
        }
        public int getPlayerIncome()
        {
            return player.getIncome();
        }
        public bool canAnotherRegionBeConquared()
        {
            if (!player.playerHasPawns())
            {
                Debug.Log("Pawns left: " + (player.playerMaxPawns() -player.pawnsLeft()));
                return false;
            }
            else
            {
                foreach(Region r in regions)
                {
                    Debug.Log("Region " + r.getRegionIndex() + "    "+ r.getIsValidForAttack() + "    " + (r.getPawnsRequiredToConquare() <= player.pawnsLeft()) + "    " + (r.getOwner() != player.getPlayerRace()));
                    if(r.getIsValidForAttack() && r.getPawnsRequiredToConquare() <= player.pawnsLeft() && r.getOwner() != player.getPlayerRace())
                    {
                        return true;
                    }
                }
            }
            return false;

        }
        public bool isScenarioFinished()
        {
            return scenarioFinished;
        }
        public bool checkIfScenarioIsFinished()
        {
            if (scenarioFinished)
            {

                Debug.Log("Scenario is Finished");
                return true;
            }
            else
            {
                Debug.Log(!canAnotherRegionBeConquared() + "   " + areAllRegionsCleared());
                if (!canAnotherRegionBeConquared() && areAllRegionsCleared())
                {

                    Debug.Log("Scenario is Finished");
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool isScenarioLoaded()
        {
            return senarioLoaded != 0;
        }
        public bool loadMapInfo(string path)
        {
            bool firstLine = false;
            string folderPath = Application.dataPath + mapInfoFolder + path;
            if (!File.Exists(folderPath))
            {
                Debug.Log("Wrong map File");
                return false;
            }


            StreamReader infoFile = File.OpenText(folderPath);

            while (!infoFile.EndOfStream)
            {
                string line = infoFile.ReadLine();

                if (!string.IsNullOrEmpty(line))
                {
                    if (!firstLine)
                    {
                        numberOfRegions = Int32.Parse(line);
                        regionsColorValue = new int[numberOfRegions];
                        firstLine = true;
                    }
                    else
                    {
                        string[] regionIndexes = line.Split(';');

                        int index = Int32.Parse(regionIndexes[0]);
                        //Debug.Log(index);
                        int regionColorValue = Int32.Parse(regionIndexes[1]);
                        int isOuter = Int32.Parse(regionIndexes[2]);
                        string[] neighbouringRegions = regionIndexes[3].Split(':');
                        int regionType = Int32.Parse(regionIndexes[4]);
                        int regionExtraFeature = Int32.Parse(regionIndexes[5]);
                        string[] histogramIndexes = regionIndexes[6].Split(':');

                        regionsColorValue[index] = regionColorValue;
                        regions.Add(new Region(index));
                        regions[index].addSurroundingRegions(neighbouringRegions);
                        regions[index].setisOuter(isOuter);
                        regions[index].setRegionType((Enums.RegionType)regionType);
                        regions[index].setRegionExtraFeature((Enums.RegionExtraFeature)regionExtraFeature);
                        regions[index].addhistogramComparisonIndexes(histogramIndexes);
                    }
                }
            }
            Debug.Log("map loaded");
            return true;
        }
        public bool checkRegionsPreStartReadiness()
        {
            bool testResult = true;
            for (int i = 0; i < regions.Count; i++)
            {
                regions[i].showPawnHints(UIElementsParent, assetManager, player);
                if (!regions[i].checkPreStartPawnDistribution())
                {
                    Debug.Log("INVALID SETUP IN REGION: " + regions[i].getRegionIndex());
                    testResult = false;
                }
                regions[i].calculateEstimatedIncome(player);
                regions[i].calculateEstimatedForceRequired(player);
                Debug.Log(i + "      " + regions[i].getRegionIncome());
            }
            return testResult;
        }
        public static void showPawnHint(Pawn p, GameObject UIelem, GameObject UIelemParent, Sprite UIelemTexture, bool minSize = false)
        {
            //Debug.Log("Instantiating pawn transparency");
            GameObject obj = GameObject.Instantiate(UIelem);
            obj.transform.SetParent(UIelemParent.transform, true);
            //Debug.Log("Creating pawn transparency at: " + p.getPawnPositionOnScreen());
            //Debug.Log("position" + obj.transform.position);
            //obj.transform.localPosition = Vector3.zero;
            //Debug.Log("pre locpos set zero:" + obj.transform.localPosition);
            //Debug.Log("pre pos set:" + obj.transform.position);
            //obj.transform.position = Vector3.zero;
            //Debug.Log("pre pos set zero :" + obj.transform.position);
            //Debug.Log("pre locpos set zero:" + obj.transform.localPosition);
            Vector3 tempPos = obj.transform.localPosition;
            //obj.transform.localPosition = Vector3.zero;
            Vector3 objWantedPosition = p.getPawnPositionOnScreen();
            //objWantedPosition.y = Screen.height - objWantedPosition.y;
            float pawnWidth = p.getPawnWidth();
            float pawnHeight = p.getPawnHeight();
            obj.transform.position = objWantedPosition;
            //Debug.Log("position:" + obj.transform.position);
            //Debug.Log("position:" + obj.transform.position);
            if (!minSize)
            {
                obj.transform.position += new Vector3(pawnWidth / 4, pawnHeight / 4, 0);
                obj.transform.RotateAround(obj.transform.position - new Vector3(pawnWidth / 4, pawnHeight / 4, 0),new Vector3(0,0,1),-p.getPawnRotation());
                //Debug.Log("position:" + obj.transform.position);
            }
            //obj.transform.localPosition += tempPos;
            //Debug.Log("post locpos set:" + obj.transform.localPosition);
            //Debug.Log("post pos set:" + obj.transform.position);
            Image objImg = obj.GetComponent<Image>();
            
            objImg.sprite = UIelemTexture;
            if (!minSize)
            {
                objImg.rectTransform.sizeDelta = new Vector2(pawnWidth / 2, pawnHeight / 2);
            }
            else
            {
                objImg.rectTransform.sizeDelta = new Vector2(3,3);
            }
        }
        public void addPawnToRegion(Pawn p, GameObject UIElementsParent)
        {

            regions[p.getPawnRegionIndex()].addPawnToRegion(p);
            
            //showPawnHint(p,assetManager.UIElementPrefab,UIElementsParent,assetManager.textureTempSprite,true);
            //Debug.Log("Adding pawn of " + p.getRaceType() + " to region " + p.getPawnRegionIndex()+"    "+regions[p.getPawnRegionIndex()].getRegionIndex());
        }
        public void addPawnToRegion(Pawn p, GameObject UIElementsParent, int regionIndex)
        {

            //regions[getRegionFromPosition(p.getPawnPositionOnScreen())].addPawnToRegion(p);
            //showPawnHint(p, assetManager.UIElementPrefab, UIElementsParent, assetManager.whiteSprite);
            regions[regionIndex].addPawnToRegion(p);
           // Debug.Log("Adding pawn of " + p.getRaceType() + " to region " + regionIndex);
        }
        public int[] getRegionHistogramTemplates(int regionIndex)
        {
            return regions[regionIndex].getHistogramComaprisonIndexes();
        }
        void clearRegionsInfo()
        {
            foreach (Region r in regions)
            {
                r.prepareRegionInfo();
            }
        }
        public void calculateMoveResult()
        {
            if (EventsQueue.Count > 0)
            {
                Debug.Log("Initiating events presentation: "+EventsQueue.Count);
                eventResultIsBeingShown = true;
                proceedToNextEvent();
                panelManager.hidePanel();
            }
        }
        public void clearHints()
        {
            Debug.Log("Clearing hints");
            panelManager.clearAllPanels();
            foreach (Region r in regions)
            {
                r.litRegionAsUnavailableToConquere();
                setRegionTextureToNotVisible(r.getRegionIndex());
                if (r.getOwner() != player.getPlayerRace() && r.getOwner() != Enums.RaceType.Null)
                {
                    setRegionLinesToEnemyNotVisible(r.getRegionIndex());
                }
                else
                {
                    setRegionLinesToNull(r.getRegionIndex());
                }
                r.clearRegionForNextEvent();
            }
        }
        public void showHintsForNextUser()
        {
            //Debug.Log("Showing Info for next move");
            panelManager.showPanel();
            if (isAnyRegionOccupiedByUser())
            {
                //Debug.Log("At least one region occupied by user");
                highliteRegions(false);
            }
            else
            {
                //Debug.Log("No regions occupied by user");
                highliteRegions(true);
            }
        }
        public void updateExtraTextures()
        {
            if (player.getPlayersRegionFeature() == Enums.PlayerRegionFeature.Caveman)
            {
                for (int i = 0; i < regionExtraFeatureTexture.Length; i++)
                {
                    setRegionTextureExtraFeatureActive(i);
                }
            }
            foreach (Region r in regions)
            {
                if (r.isRegionValidForExtraIncome(player) && r.getOwner() != player.getPlayerRace())
                {
                    if (r.hasRegionBeenAlreadyHighlited())
                    {
                        setRegionTextureToVisibleWithExtraIncome(r.getRegionIndex());
                    }
                    else
                    {
                        setRegionTextureToNotVisibleWithExtraIncome(r.getRegionIndex());
                    }
                }
            }
        }
        private void highliteRegions(bool outerRegions)
        {
            if (outerRegions)
            {
                foreach (Region r in regions)
                {
                    //setRegionLinesToNull(r.getRegionIndex());
                    if (r.isOuter)
                    {
                        if (r.getRegionType() == Enums.RegionType.Water)
                        {
                            #region setWaterSurrodingToAvailable
                            foreach (int i in r.getSurroundingRegions())
                            {
                                if (regions[i].isConquerable(player))
                                {
                                    regions[i].litRegionAsAvailableToConquere();
                                    if (regions[i].isRegionValidForExtraIncome(player))
                                    {
                                        setRegionTextureToVisibleWithExtraIncome(regions[i].getRegionIndex());
                                    }
                                    else
                                    {
                                        setRegionTextureToConquarable(regions[i].getRegionIndex());
                                    }
                                    if (regions[i].getOwner() != Enums.RaceType.Null && regions[i].getOwner() != player.getPlayerRace())
                                    {
                                        setRegionLinesToEnemy(regions[i].getRegionIndex());
                                    }
                                    else
                                    {
                                        setRegionLinesToVisible(regions[i].getRegionIndex());
                                    }
                                    panelManager.showPanelInfo(regions[i].getRegionIndex(), regions[i].getRegionIncome(), regions[i].getPawnsRequiredToConquare());
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            r.litRegionAsAvailableToConquere();
                            setRegionTextureToConquarable(r.getRegionIndex());
                            if (r.getOwner() != Enums.RaceType.Null && r.getOwner() != player.getPlayerRace())
                            {
                                setRegionLinesToEnemy(r.getRegionIndex());
                            }
                            else
                            {
                                setRegionLinesToVisible(r.getRegionIndex());
                            }
                            panelManager.showPanelInfo(r.getRegionIndex(), r.getRegionIncome(), r.getPawnsRequiredToConquare());
                        }
                    }
                    else
                    {
                        if (!r.hasRegionBeenAlreadyHighlited())
                        {
                            if (r.isRegionValidForExtraIncome(player))
                            {
                                setRegionTextureToNotVisibleWithExtraIncome(r.getRegionIndex());
                            }
                            else
                            {
                            setRegionTextureToNotVisible(r.getRegionIndex());
                            }
                            r.litRegionAsUnavailableToConquere();
                            if (r.getOwner() != Enums.RaceType.Null && r.getOwner() != player.getPlayerRace())
                            {
                                setRegionLinesToEnemyNotVisible(r.getRegionIndex());
                            }
                            else
                            {
                                setRegionLinesToNull(r.getRegionIndex());
                            }
                            //setRegionTextureToNull(r.getRegionIndex());
                        }
                    }
                }
            }
            else
            {
                foreach (Region r in regions)
                {
                    if (r.getOwner() == player.getPlayerRace())
                    {
                        r.litRegionAsOwnedByPlayer();
                        setRegionTextureToOwned(r.getRegionIndex());
                        setRegionLinesToOwned(r.getRegionIndex());
                        panelManager.showPanelInfo(r.getRegionIndex(), r.getRegionIncome(), r.getPawnsRequiredToConquare());

                        if (r.isExtraFeatureActive(player))
                        {
                            foreach (Region rE in regions)
                            {
                                if (rE.isExtraFeatureActive(player) && rE != r && !rE.hasRegionBeenAlreadyHighlited())
                                {
                                    if (rE.getOwner() != player.getPlayerRace())
                                    {
                                        rE.litRegionAsAvailableToConquere();
                                        if (rE.isRegionValidForExtraIncome(player))
                                        {
                                            setRegionTextureToVisibleWithExtraIncome(rE.getRegionIndex());
                                        }
                                        else
                                        {
                                            setRegionTextureToConquarable(rE.getRegionIndex());
                                        }
                                        if (rE.getOwner() != Enums.RaceType.Null)
                                        {
                                            setRegionLinesToEnemy(rE.getRegionIndex());
                                        }
                                        else
                                        {
                                            setRegionLinesToVisible(rE.getRegionIndex());
                                        }
                                        panelManager.showPanelInfo(rE.getRegionIndex(), rE.getRegionIncome(), rE.getPawnsRequiredToConquare());
                                    }
                                }
                            }
                        }
                        foreach (int i in r.getSurroundingRegions())
                        {
                            if (regions[i].getOwner() != player.getPlayerRace() && regions[i].isConquerable(player) && !regions[i].hasRegionBeenAlreadyHighlited())
                            {
                                //Debug.Log((regions[i].getOwner() != player.getPlayerRace()) +" "+ (r.isConquerable(player) == true));
                                regions[i].litRegionAsAvailableToConquere();
                                setRegionTextureToConquarable(regions[i].getRegionIndex());
                                if (regions[i].getOwner() != Enums.RaceType.Null)
                                {
                                    setRegionLinesToEnemy(regions[i].getRegionIndex());
                                }
                                else
                                {
                                    setRegionLinesToVisible(regions[i].getRegionIndex());
                                }
                                panelManager.showPanelInfo(regions[i].getRegionIndex(), regions[i].getRegionIncome(), regions[i].getPawnsRequiredToConquare());
                            }
                        }
                    }
                }
            }
        }
        public void showSetupPanels(AssetManager aS)
        {
            Player tempP = new Player(Enums.RaceType.Null, 0);
            foreach (Region r in regions)
            {
                if (r.getOwner() != Enums.RaceType.Null)
                {
                    panelManager.changePanelTexture(r.getRegionIndex(), assetManager.raceTexturesSprites[(int)r.getOwner() - 1]);
                    panelManager.showPanelInfo(r.getRegionIndex(), 0, r.getPreStartPawnNumber());
                }
            }
        }
        void eventHandlerChangeRegionTextureDuringConquer(int regionIndex)
        {
            if (regionsStateTextures[regionIndex].texture == assetManager.regionsNeutralTextures[regionIndex])
            {
                setRegionTextureToOwned(regionIndex);
                setRegionLinesToOwned(regionIndex);
            }
            else
            {
                setRegionTextureToConquarable(regionIndex);
                setRegionLinesToNull(regionIndex);
            }
        }
        void setRegionLinesToNull(int regionIndex)
        {
            //Debug.Log("Changing Region lines " + regionIndex + " to null");
            lineStateTextures[regionIndex].texture = null;
        }
        void setRegionLinesToOwned(int regionIndex)
        {
            //Debug.Log("Changing Region lines" + regionIndex + " to owned");
            lineStateTextures[regionIndex].texture = assetManager.linesConquaredTextures[regionIndex];
            lineStateTextures[regionIndex].transform.localPosition = new Vector3(0, 0, 2);
        }
        void setRegionLinesToEnemy(int regionIndex)
        {
            //Debug.Log("Changing Region lines" + regionIndex + " to visible");
            lineStateTextures[regionIndex].texture = assetManager.linesEnemyVisibleTexture[regionIndex];
            lineStateTextures[regionIndex].transform.localPosition = new Vector3(0, 0, 1);
        }
        void setRegionLinesToEnemyNotVisible(int regionIndex)
        {
            //Debug.Log("Changing Region lines" + regionIndex + " to visible");
            lineStateTextures[regionIndex].texture = assetManager.linesEnemyNotVisibleTexture[regionIndex];
            lineStateTextures[regionIndex].transform.localPosition = new Vector3(0, 0, 1);
        }
        void setRegionLinesToVisible(int regionIndex)
        {
            //Debug.Log("Changing Region lines" + regionIndex + " to visible");
            lineStateTextures[regionIndex].texture = assetManager.linesVisibleTexture[regionIndex];
            lineStateTextures[regionIndex].transform.localPosition = new Vector3(0,0,1);
        }
        void setRegionTextureToNull(int regionIndex)
        {
            //Debug.Log("Changing Region " + regionIndex + " to null");
            regionsStateTextures[regionIndex].texture = null;
        }
        void setRegionTextureToVisibleWithExtraIncome(int regionIndex)
        {
            int desiredFPS = assetManager._uvTieX * assetManager._uvTieY;
            int index = (int)(Time.timeSinceLevelLoad * desiredFPS) % (assetManager._uvTieX * assetManager._uvTieY);
            regionsStateTextures[regionIndex].texture = assetManager.regionsValidForExtraIncomeNormal[regionIndex, index];
        }
        void setRegionTextureToNotVisibleWithExtraIncome(int regionIndex)
        {
            int desiredFPS = assetManager._uvTieX * assetManager._uvTieY;
            int index = (int)(Time.timeSinceLevelLoad * desiredFPS) % (assetManager._uvTieX * assetManager._uvTieY);
            regionsStateTextures[regionIndex].texture = assetManager.regionsValidForExtraIncomeFaded[regionIndex, index];
        }
        void setRegionTextureToOwned(int regionIndex)
        {
            //Debug.Log("Changing Region " + regionIndex + " to Owned");
            regionsStateTextures[regionIndex].texture = assetManager.regionsOwnedTextures[regionIndex];
        }
        void setRegionTextureToConquarable(int regionIndex)
        {
            //Debug.Log("Changing Region " + regionIndex + " to Conquarable");
            //regionsStateTextures[regionIndex].texture = assetManager.regionsConquarableTextures[regionIndex];
            regionsStateTextures[regionIndex].texture = assetManager.regionsNeutralTextures[regionIndex];

        }
        void setRegionTextureExtraFeatureActive(int regionIndex)
        {
            int desiredFPS = assetManager._uvTieX2 * assetManager._uvTieY2;
            int index = (int)(Time.timeSinceLevelLoad * desiredFPS) % (assetManager._uvTieX2 * assetManager._uvTieY2);
            //Debug.Log(index);
            regionExtraFeatureTexture[regionIndex].texture = assetManager.regionExtraFeatureCaves[index];
        }
        void setRegionTextureToNotVisible(int regionIndex)
        {
            //Debug.Log("Changing Region " + regionIndex + " to not visible");
            regionsStateTextures[regionIndex].texture = assetManager.regionsNotVisibleTextures[regionIndex];
        }
        public bool isAnyRegionOccupiedByUser()
        {
            //Debug.Log(player.getPlayerRace());
            for (int i = 0; i < regions.Count; i++)
            {
                if (regions[i].getOwner() == player.getPlayerRace())
                {
                    return true;
                }
            }
            return false;
        }
        private bool checkIfRegionCanBeLeft(int regionIndex)
        {
            List<int> playerRegions = player.getConqauredRegions();
            if (regions[regionIndex].isOuter)
            {
                bool hasOtherOuterRegion = false;

                foreach(int playerRegionIndex in playerRegions)
                {
                    if (regions[playerRegionIndex].isOuter && playerRegionIndex != regionIndex)
                    {
                        hasOtherOuterRegion = true;
                    }
                }
                if (!hasOtherOuterRegion)
                {
                    return false;
                }

                for(int i = 0; i < playerRegions.Count; i++)
                {
                    if (playerRegions[i] != regionIndex)
                    {
                        if (regions[playerRegions[i]].isOuter)
                        {
                            bool regionCanActAsStarting = true;
                            foreach (int playerRegionIndex in playerRegions)
                            {
                                if (playerRegionIndex != playerRegions[i])
                                {
                                    List<int> visited = new List<int>();
                                    visited.Add(regionIndex);
                                    if (!pathBetweenRegionsExist(playerRegionIndex, playerRegions[i], visited))
                                    {
                                        regionCanActAsStarting = false;
                                    }
                                }
                            }
                            if (regionCanActAsStarting)
                            {
                                startingRegionIndex = playerRegions[i];
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            else
            {
                //Region is not outer region

                bool routeBetweenEverySurrAndTheStartingRegExists = true;
                foreach (int surroundingRegionIndex in regions[regionIndex].getSurroundingRegions())
                {
                    if (regions[surroundingRegionIndex].getOwner() == player.getPlayerRace())
                    {
                        List<int> visited = new List<int>();
                        visited.Add(regionIndex);
                        if (!pathBetweenRegionsExist(surroundingRegionIndex, startingRegionIndex, visited))
                        {
                            routeBetweenEverySurrAndTheStartingRegExists = false;
                        }
                    }
                }
                if (routeBetweenEverySurrAndTheStartingRegExists)
                {
                    return true;
                }
            }

            return false;

        }
        private bool pathBetweenRegionsExist(int startIndex, int endIndex, List<int> visitedIndexes)
        {
            if (regions[startIndex].getSurroundingRegions().Contains(endIndex))
            {
                return true;
            }
            else
            {
                visitedIndexes.Add(startIndex);
                int[] surrRegions = regions[startIndex].getSurroundingRegions();
                for (int i = 0; i < surrRegions.Count(); i++)
                {
                    if (!visitedIndexes.Contains(surrRegions[i]) && regions[surrRegions[i]].getOwner() == player.getPlayerRace())
                    {
                        if (pathBetweenRegionsExist(surrRegions[i], endIndex, visitedIndexes))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        private bool regionHasConectionWithFirstOne(int startingRegionIndex)
        {
            if(regions[startingRegionIndex].getSurroundingRegions().Contains(startingRegionIndex))
            {
                return true;
            }
            else
            {
            List<int> regionsToSearch = new List<int>();
            List<int> regionsAlreadySearched = new List<int>();

                for(int surrRegionIndex=0; surrRegionIndex< regions[startingRegionIndex].getSurroundingRegions().Length;surrRegionIndex++)
                {
                    if(regions[surrRegionIndex].getOwner() == player.getPlayerRace()){
                        
                        if(regionHasConectionWithFirstOne(surrRegionIndex))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        public void printPawns()
        {
            foreach (Region r in regions)
            {
                r.printPawns();
            }
        }
        public void LookForBestResult(bool cauntingRegions, List<Region> inputList)
        {
            List<int> regionsConquaredIndexes = new List<int>();
            List<int> regionsAvailableIndexes = new List<int>();
            int troopsUsed = 0;
            int finalIncome = 0;
            int uniqueResults = 0;
            int BestIncome = 0;
            int BestTroopsUsed = 0;
            List<int> bestRegionsConquaredSequence = new List<int>();


                StreamWriter sW = File.CreateText(Application.dataPath + "/bestScenarios.txt");
                sW.Close();

            for(int i=0;i< inputList.Count;i++)
            {
                List<int> bestRegionsConquaredSequenceOutput = new List<int>();
                int uniqueResultsOutput = 0;
                int BestIncomeOutput = 0;
                int BestTroopsUsedOutput = 0;
                regionsConquaredIndexes = new List<int>();
                regionsAvailableIndexes = new List<int>();
                troopsUsed = 0;
                finalIncome = 0;
                
                if (inputList[i].isConquerable(player) && !regionsConquaredIndexes.Contains(inputList[i].getRegionIndex()) &&
                    (inputList[i].isOuter || inputList[i].getSurroundingRegions().Contains(0) || inputList[i].getSurroundingRegions().Contains(27))
                    )
                {
                    
                    if (inputList[i].getPawnsRequiredToConquare() <= (player.playerMaxPawns() - troopsUsed))
                    {
                        regionsConquaredIndexes.Add(inputList[i].getRegionIndex());
                        foreach (int reIndex in inputList[i].getSurroundingRegions())
                        {
                            if (inputList[reIndex].isConquerable(player))
                            {
                                regionsAvailableIndexes.Add(reIndex);
                            }
                        }
                        if (inputList[i].isExtraFeatureActive(player))
                        {
                            foreach (Region rE in inputList)
                            {
                                if (rE.isExtraFeatureActive(player) && !regionsAvailableIndexes.Contains(rE.getRegionIndex()))
                                {
                                    regionsAvailableIndexes.Add(rE.getRegionIndex());
                                }
                            }
                        }
                        troopsUsed += inputList[i].getPawnsRequiredToConquare();
                        finalIncome += inputList[i].getRegionIncome();
                        /*StreamWriter sWW = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
                        sWW.WriteLine("Starting New Conquare");
                        sWW.WriteLine("Conquaring region " + regions[i].getRegionIndex() + " using pawns: " + regions[i].calculateEstimatedForceRequired(player) + " totalPawnsUsed:" + troopsUsed);
                        sWW.Close();*/
                        enterRegion(regionsConquaredIndexes, regionsAvailableIndexes, out bestRegionsConquaredSequenceOutput,
                            troopsUsed, finalIncome, out BestIncomeOutput, out BestTroopsUsedOutput, out uniqueResultsOutput,
                            cauntingRegions, inputList);
                    }

                }
                StreamWriter sWW = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
                sWW.WriteLine();
                sWW.WriteLine();
                sWW.WriteLine("END of Path");
                sWW.WriteLine("RegionsConqared:" + bestRegionsConquaredSequenceOutput.Count + "    Troops Used: " + BestTroopsUsedOutput +
                    "     IncomeGenerated: " + BestIncomeOutput + "     uniqueResults: " + uniqueResultsOutput);
                sWW.WriteLine("Region order:");
                for (int j = 0; j < bestRegionsConquaredSequenceOutput.Count; j++)
                {
                    sWW.WriteLine("Conquaring: " + bestRegionsConquaredSequenceOutput[j] +
                        "using " + inputList[bestRegionsConquaredSequenceOutput[j]].getPawnsRequiredToConquare() + " troops, generating "+
                        inputList[bestRegionsConquaredSequenceOutput[j]].getRegionIncome() + " income");
                }
                sWW.WriteLine();
                sWW.WriteLine();
                sWW.Close();

                if (cauntingRegions)
                {
                    if (bestRegionsConquaredSequenceOutput.Count > bestRegionsConquaredSequence.Count)
                    {
                        uniqueResults = uniqueResultsOutput;
                        bestRegionsConquaredSequence = bestRegionsConquaredSequenceOutput;
                        BestIncome = BestIncomeOutput;
                        BestTroopsUsed = BestTroopsUsedOutput;
                    }
                    else if (bestRegionsConquaredSequenceOutput.Count == bestRegionsConquaredSequence.Count)
                    {
                        for (int k = 0; k < bestRegionsConquaredSequenceOutput.Count; k++)
                        {
                            if (!bestRegionsConquaredSequenceOutput.Contains(bestRegionsConquaredSequence[k]))
                            {
                                uniqueResults += uniqueResultsOutput;
                            }
                        }
                    }
                }
                else
                {
                    if (BestIncomeOutput > BestIncome)
                    {
                        uniqueResults = uniqueResultsOutput;
                        bestRegionsConquaredSequence = bestRegionsConquaredSequenceOutput;
                        BestIncome = BestIncomeOutput;
                        BestTroopsUsed = BestTroopsUsedOutput;
                    }
                    else if (BestIncomeOutput == BestIncome)
                    {
                        for (int k = 0; k < bestRegionsConquaredSequenceOutput.Count; k++)
                        {
                            if (!bestRegionsConquaredSequenceOutput.Contains(bestRegionsConquaredSequence[k]))
                            {
                                uniqueResults += uniqueResultsOutput;
                            }
                        }
                    }
                }
            }

            StreamWriter sWW2 = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
            sWW2.WriteLine();
            sWW2.WriteLine();
            sWW2.WriteLine("END of SEARCH");
            sWW2.WriteLine("Best Result");
            sWW2.WriteLine("RegionsConqared:" + bestRegionsConquaredSequence.Count + "    Troops Used: " + BestTroopsUsed + "     IncomeGenerated: " + BestIncome + "     uniqueResults: " + uniqueResults);
            sWW2.Write("Region order:");
            for (int j = 0; j < bestRegionsConquaredSequence.Count; j++)
            {
                sWW2.Write("   " + bestRegionsConquaredSequence[j]);
            }
            sWW2.WriteLine();
            sWW2.WriteLine();
            sWW2.Close();
        }
        public void enterRegion(List<int> regionsConquared, List<int> regionsAvailableToConquare, out List<int> bestSequence,
            int troopsUsed, int currentIncome, out int bestIncome, out int bestTroopsUsed, out int uniqueResults,
            bool cauntingRegions/*if false caunting income*/, List<Region> inputList)
        {
            List<int> regionsConquaredScope = regionsConquared;
            List<int> regionsAvailableToConquareScope = regionsAvailableToConquare;
            int troopsUsedScope = troopsUsed;
            int currentIncomeScope = currentIncome;

            bestSequence = new List<int>();
            bestIncome = 0;
            bestTroopsUsed = 0;
            uniqueResults = 1;

            int smallestForceRequired = player.playerMaxPawns() + 1;
            int biggestIncome = 0;
            int smallestIndex = 0;
            bool moreThanOneLowestMatch = false;
            List<int> lowestMatches = new List<int>();
            for (int i = 0; i < regionsAvailableToConquareScope.Count; i++)
            {
                if (inputList[regionsAvailableToConquareScope[i]].isConquerable(player) && !regionsConquaredScope.Contains(regionsAvailableToConquareScope[i]))
                {
                    int forceReq = inputList[regionsAvailableToConquareScope[i]].getPawnsRequiredToConquare();
                    if ((player.playerMaxPawns() - troopsUsedScope) >= forceReq)
                    {
                        if (cauntingRegions)
                        {
                            if (forceReq < smallestForceRequired)
                            {
                                lowestMatches = new List<int>();
                                moreThanOneLowestMatch = false;
                                smallestForceRequired = forceReq;
                                smallestIndex = regionsAvailableToConquareScope[i];
                                lowestMatches.Add(smallestIndex);
                            }
                            else if (forceReq == smallestForceRequired)
                            {
                                moreThanOneLowestMatch = true;
                                lowestMatches.Add(regionsAvailableToConquareScope[i]);
                            }
                        }
                        else
                        {
                            int income = inputList[regionsAvailableToConquareScope[i]].getRegionIncome();
                            if (income > biggestIncome)
                            {
                                lowestMatches = new List<int>();
                                moreThanOneLowestMatch = false;
                                biggestIncome = income;
                                smallestIndex = regionsAvailableToConquareScope[i];
                                lowestMatches.Add(smallestIndex);
                                smallestForceRequired = forceReq;
                            }
                            else if (income == biggestIncome)
                            {
                                moreThanOneLowestMatch = true;
                                lowestMatches.Add(regionsAvailableToConquareScope[i]);
                            }
                        }
                    }
                }
            }

            if (smallestForceRequired <= (player.playerMaxPawns() - troopsUsedScope))
            {
                if (moreThanOneLowestMatch)
                {
                    for (int index = 0; index < lowestMatches.Count; index++)
                    {
                        /*StreamWriter sWW = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
                        sWW.Write("New PATH: Previously Conquared regions:");
                        foreach (int prevIndex in regionsConquared)
                        {
                            sWW.Write(" " + prevIndex);
                        }
                        sWW.WriteLine();*/
                        List<int> regionsConquared2 = new List<int>();
                        regionsConquared2.AddRange(regionsConquared);
                        regionsConquared2.Add(lowestMatches[index]);
                        List<int> regionsAvailableToConquare2 = new List<int>();
                        regionsAvailableToConquare2.AddRange(regionsAvailableToConquare);
                        regionsAvailableToConquare2.Remove(lowestMatches[index]);
                        int troopsUsed2 = troopsUsed + smallestForceRequired;
                        int currentIncome2 = currentIncome + inputList[lowestMatches[index]].getRegionIncome();
                        foreach (int rE in inputList[lowestMatches[index]].getSurroundingRegions())
                        {
                            if (!regionsAvailableToConquare2.Contains(inputList[rE].getRegionIndex()) && !regionsConquared2.Contains(inputList[rE].getRegionIndex()))
                            {
                                regionsAvailableToConquare2.Add(rE);
                            }
                        }
                        if (inputList[lowestMatches[index]].isExtraFeatureActive(player))
                        {
                            foreach (Region rE in inputList)
                            {
                                if (rE.isExtraFeatureActive(player) && !regionsAvailableToConquare2.Contains(rE.getRegionIndex()) && !regionsConquared2.Contains(rE.getRegionIndex()))
                                {
                                    regionsAvailableToConquare2.Add(rE.getRegionIndex());
                                }
                            }
                        }
                        /*sWW.WriteLine("Conquaring region " + lowestMatches[index] + " using pawns: " +
                            smallestForceRequired + " totalPawnsUsed:" + troopsUsed2);
                        sWW.Close();*/

                        List<int> bestSequence2 = new List<int>();
                        int bestIncome2 = 0;
                        int bestTroopsUsed2 = 0;
                        int uniqueResults2 = 0;

                        enterRegion(regionsConquared2, regionsAvailableToConquare2, out bestSequence2,
                            troopsUsed2, currentIncome2, out bestIncome2, out bestTroopsUsed2, out uniqueResults2, cauntingRegions);

                        if (cauntingRegions)
                        {
                            if (bestSequence2.Count > bestSequence.Count)
                            {
                                uniqueResults = uniqueResults2;
                                bestSequence = bestSequence2;
                                bestIncome = bestIncome2;
                                bestTroopsUsed = bestTroopsUsed2;
                            }
                            else if (bestSequence2.Count == bestSequence.Count)
                            {
                                for (int k = 0; k < bestSequence2.Count; k++)
                                {
                                    if (!bestSequence.Contains(bestSequence2[k]))
                                    {
                                        uniqueResults += uniqueResults2;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (bestIncome2 > bestIncome)
                            {
                                uniqueResults = uniqueResults2;
                                bestSequence = bestSequence2;
                                bestIncome = bestIncome2;
                                bestTroopsUsed = bestTroopsUsed2;
                            }
                            else if (bestIncome2 == bestIncome)
                            {
                                for (int k = 0; k < bestSequence2.Count; k++)
                                {
                                    if (!bestSequence.Contains(bestSequence2[k]))
                                    {
                                        uniqueResults += uniqueResults2;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    regionsConquaredScope.Add(smallestIndex);
                    regionsAvailableToConquareScope.Remove(smallestIndex);
                    troopsUsedScope += smallestForceRequired;
                    currentIncomeScope += inputList[smallestIndex].getRegionIncome();
                    foreach (int rE in inputList[smallestIndex].getSurroundingRegions())
                    {
                        if (!regionsAvailableToConquareScope.Contains(rE) && !regionsConquaredScope.Contains(rE) && inputList[rE].isConquerable(player))
                        {
                            regionsAvailableToConquareScope.Add(rE);
                        }
                    }
                    if (inputList[smallestIndex].isExtraFeatureActive(player))
                    {
                        foreach (Region rE in inputList)
                        {
                            if (rE.isExtraFeatureActive(player) && !regionsAvailableToConquareScope.Contains(rE.getRegionIndex()) && !regionsConquaredScope.Contains(rE.getRegionIndex()))
                            {
                                regionsAvailableToConquareScope.Add(rE.getRegionIndex());
                            }
                        }
                    }
                    /*StreamWriter sWW = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
                    sWW.WriteLine("Conquaring region " + smallestIndex + " using pawns: " +
                        smallestForceRequired + " totalPawnsUsed:" + troopsUsed);
                    sWW.Close();
                    */
                    List<int> bestSequence2 = new List<int>();
                    int bestIncome2;
                    int bestTroopsUsed2;
                    int uniqueResults2;

                    enterRegion(regionsConquaredScope, regionsAvailableToConquareScope, out bestSequence2,
                        troopsUsedScope, currentIncomeScope, out bestIncome2, out bestTroopsUsed2, out uniqueResults2, cauntingRegions, inputList);

                    bestIncome = bestIncome2;
                    bestSequence = bestSequence2;
                    bestTroopsUsed = bestTroopsUsed2;
                    uniqueResults = uniqueResults2;
                }
            }
            else
            {
                bestIncome = currentIncomeScope;
                bestSequence = regionsConquaredScope;
                bestTroopsUsed = troopsUsedScope;
                /*StreamWriter sWW2 = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
                sWW2.WriteLine("END of TURN");
                sWW2.WriteLine("RegionsConqared:" + regionsConquaredScope.Count + "    Troops Used: " + troopsUsedScope + "     IncomeGenerated: " + currentIncomeScope);
                sWW2.Write("Region order:");
                for (int j = 0; j < regionsConquaredScope.Count; j++)
                {
                    sWW2.Write("   " + regionsConquaredScope[j]);
                }
                sWW2.WriteLine();
                sWW2.WriteLine();
                sWW2.Close();*/

                //if(regions[regionsAvailableToConquare[i]].calculateEstimatedForceRequired)


            }
        }
        public void LookForBestResult(bool cauntingRegions)
        {
            List<int> regionsConquaredIndexes = new List<int>();
            List<int> regionsAvailableIndexes = new List<int>();
            int troopsUsed = 0;
            int finalIncome = 0;
            int uniqueResults = 0;
            int BestIncome = 0;
            int BestTroopsUsed = 0;
            List<int> bestRegionsConquaredSequence = new List<int>();


                StreamWriter sW = File.CreateText(Application.dataPath + "/bestScenarios.txt");
                sW.Close();

            for(int i=0;i<regions.Count;i++)
            {
                List<int> bestRegionsConquaredSequenceOutput = new List<int>();
                int uniqueResultsOutput = 0;
                int BestIncomeOutput = 0;
                int BestTroopsUsedOutput = 0;
                regionsConquaredIndexes = new List<int>();
                regionsAvailableIndexes = new List<int>();
                troopsUsed = 0;
                finalIncome = 0;
                
                if (regions[i].isConquerable(player) && !regionsConquaredIndexes.Contains(regions[i].getRegionIndex()) &&
                    (regions[i].isOuter || regions[i].getSurroundingRegions().Contains(0) || regions[i].getSurroundingRegions().Contains(27))
                    )
                {
                    
                    if (regions[i].getPawnsRequiredToConquare() <= (player.playerMaxPawns() - troopsUsed))
                    {
                        regionsConquaredIndexes.Add(regions[i].getRegionIndex());
                        foreach (int reIndex in regions[i].getSurroundingRegions())
                        {
                            if (regions[reIndex].isConquerable(player))
                            {
                                regionsAvailableIndexes.Add(reIndex);
                            }
                        }
                        if (regions[i].isExtraFeatureActive(player))
                        {
                            foreach (Region rE in regions)
                            {
                                if (rE.isExtraFeatureActive(player) && !regionsAvailableIndexes.Contains(rE.getRegionIndex()))
                                {
                                    regionsAvailableIndexes.Add(rE.getRegionIndex());
                                }
                            }
                        }
                        troopsUsed += regions[i].getPawnsRequiredToConquare();
                        finalIncome += regions[i].getRegionIncome();
                        /*StreamWriter sWW = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
                        sWW.WriteLine("Starting New Conquare");
                        sWW.WriteLine("Conquaring region " + regions[i].getRegionIndex() + " using pawns: " + regions[i].calculateEstimatedForceRequired(player) + " totalPawnsUsed:" + troopsUsed);
                        sWW.Close();*/
                        enterRegion(regionsConquaredIndexes, regionsAvailableIndexes, out bestRegionsConquaredSequenceOutput,
                            troopsUsed, finalIncome, out BestIncomeOutput, out BestTroopsUsedOutput, out uniqueResultsOutput,
                            cauntingRegions);
                    }

                }
                StreamWriter sWW = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
                sWW.WriteLine();
                sWW.WriteLine();
                sWW.WriteLine("END of Path");
                sWW.WriteLine("RegionsConqared:" + bestRegionsConquaredSequenceOutput.Count + "    Troops Used: " + BestTroopsUsedOutput +
                    "     IncomeGenerated: " + BestIncomeOutput + "     uniqueResults: " + uniqueResultsOutput);
                sWW.WriteLine("Region order:");
                for (int j = 0; j < bestRegionsConquaredSequenceOutput.Count; j++)
                {
                    sWW.WriteLine("Conquaring: " + bestRegionsConquaredSequenceOutput[j] +
                        "using " + regions[bestRegionsConquaredSequenceOutput[j]].getPawnsRequiredToConquare() + " troops, generating "+
                        regions[bestRegionsConquaredSequenceOutput[j]].getRegionIncome() + " income");
                }
                sWW.WriteLine();
                sWW.WriteLine();
                sWW.Close();

                if (cauntingRegions)
                {
                    if (bestRegionsConquaredSequenceOutput.Count > bestRegionsConquaredSequence.Count)
                    {
                        uniqueResults = uniqueResultsOutput;
                        bestRegionsConquaredSequence = bestRegionsConquaredSequenceOutput;
                        BestIncome = BestIncomeOutput;
                        BestTroopsUsed = BestTroopsUsedOutput;
                    }
                    else if (bestRegionsConquaredSequenceOutput.Count == bestRegionsConquaredSequence.Count)
                    {
                        for (int k = 0; k < bestRegionsConquaredSequenceOutput.Count; k++)
                        {
                            if (!bestRegionsConquaredSequenceOutput.Contains(bestRegionsConquaredSequence[k]))
                            {
                                uniqueResults += uniqueResultsOutput;
                            }
                        }
                    }
                }
                else
                {
                    if (BestIncomeOutput > BestIncome)
                    {
                        uniqueResults = uniqueResultsOutput;
                        bestRegionsConquaredSequence = bestRegionsConquaredSequenceOutput;
                        BestIncome = BestIncomeOutput;
                        BestTroopsUsed = BestTroopsUsedOutput;
                    }
                    else if (BestIncomeOutput == BestIncome)
                    {
                        for (int k = 0; k < bestRegionsConquaredSequenceOutput.Count; k++)
                        {
                            if (!bestRegionsConquaredSequenceOutput.Contains(bestRegionsConquaredSequence[k]))
                            {
                                uniqueResults += uniqueResultsOutput;
                            }
                        }
                    }
                }
            }

            StreamWriter sWW2 = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
            sWW2.WriteLine();
            sWW2.WriteLine();
            sWW2.WriteLine("END of SEARCH");
            sWW2.WriteLine("Best Result");
            sWW2.WriteLine("RegionsConqared:" + bestRegionsConquaredSequence.Count + "    Troops Used: " + BestTroopsUsed + "     IncomeGenerated: " + BestIncome + "     uniqueResults: " + uniqueResults);
            sWW2.Write("Region order:");
            for (int j = 0; j < bestRegionsConquaredSequence.Count; j++)
            {
                sWW2.Write("   " + bestRegionsConquaredSequence[j]);
            }
            sWW2.WriteLine();
            sWW2.WriteLine();
            sWW2.Close();

        }
        public void enterRegion(List<int> regionsConquared,List<int> regionsAvailableToConquare,out List<int> bestSequence,
            int troopsUsed, int currentIncome,out int bestIncome, out int bestTroopsUsed,out int uniqueResults,
            bool cauntingRegions/*if false caunting income*/)
        {
            List<int> regionsConquaredScope = regionsConquared;
            List<int> regionsAvailableToConquareScope = regionsAvailableToConquare;
            int troopsUsedScope = troopsUsed;
            int currentIncomeScope = currentIncome;

            bestSequence = new List<int>();
            bestIncome = 0;
            bestTroopsUsed = 0;
            uniqueResults = 1;

            int smallestForceRequired = player.playerMaxPawns()+1;
            int biggestIncome = 0;
            int smallestIndex = 0;
            bool moreThanOneLowestMatch = false;
            List<int> lowestMatches = new List<int>();
            for (int i = 0; i < regionsAvailableToConquareScope.Count; i++)
            {
                if (regions[regionsAvailableToConquareScope[i]].isConquerable(player) && !regionsConquaredScope.Contains(regionsAvailableToConquareScope[i]))
                {
                    int forceReq = regions[regionsAvailableToConquareScope[i]].getPawnsRequiredToConquare();
                    if ((player.playerMaxPawns() - troopsUsedScope) >= forceReq)
                        {
                            if (cauntingRegions)
                            {
                                if (forceReq < smallestForceRequired)
                                {
                                    lowestMatches = new List<int>();
                                    moreThanOneLowestMatch = false;
                                    smallestForceRequired = forceReq;
                                    smallestIndex = regionsAvailableToConquareScope[i];
                                    lowestMatches.Add(smallestIndex);
                                }
                                else if (forceReq == smallestForceRequired)
                                {
                                    moreThanOneLowestMatch = true;
                                    lowestMatches.Add(regionsAvailableToConquareScope[i]);
                                }
                            }
                            else
                            {
                                int income = regions[regionsAvailableToConquareScope[i]].getRegionIncome();
                                if (income > biggestIncome)
                                {
                                    lowestMatches = new List<int>();
                                    moreThanOneLowestMatch = false;
                                    biggestIncome = income;
                                    smallestIndex = regionsAvailableToConquareScope[i];
                                    lowestMatches.Add(smallestIndex);
                                    smallestForceRequired = forceReq;
                                }
                                else if (income == biggestIncome)
                                {
                                    moreThanOneLowestMatch = true;
                                    lowestMatches.Add(regionsAvailableToConquareScope[i]);
                                }
                            }
                        }
                 }
            }

            if (smallestForceRequired <= (player.playerMaxPawns() - troopsUsedScope))
            {
                if (moreThanOneLowestMatch)
                {
                    for (int index = 0; index < lowestMatches.Count; index++)
                    {
                        /*StreamWriter sWW = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
                        sWW.Write("New PATH: Previously Conquared regions:");
                        foreach (int prevIndex in regionsConquared)
                        {
                            sWW.Write(" " + prevIndex);
                        }
                        sWW.WriteLine();*/
                        List<int> regionsConquared2 = new List<int>();
                        regionsConquared2.AddRange(regionsConquared);
                        regionsConquared2.Add(lowestMatches[index]);
                        List<int> regionsAvailableToConquare2 = new List<int>();
                        regionsAvailableToConquare2.AddRange(regionsAvailableToConquare);
                        regionsAvailableToConquare2.Remove(lowestMatches[index]);
                        int troopsUsed2 = troopsUsed + smallestForceRequired;
                        int currentIncome2 = currentIncome + regions[lowestMatches[index]].getRegionIncome();
                        foreach (int rE in regions[lowestMatches[index]].getSurroundingRegions())
                        {
                            if (!regionsAvailableToConquare2.Contains(regions[rE].getRegionIndex()) && !regionsConquared2.Contains(regions[rE].getRegionIndex()))
                            {
                                regionsAvailableToConquare2.Add(rE);
                            }
                        }
                        if (regions[lowestMatches[index]].isExtraFeatureActive(player))
                        {
                            foreach (Region rE in regions)
                            {
                                if (rE.isExtraFeatureActive(player) && !regionsAvailableToConquare2.Contains(rE.getRegionIndex()) && !regionsConquared2.Contains(rE.getRegionIndex()))
                                {
                                    regionsAvailableToConquare2.Add(rE.getRegionIndex());
                                }
                            }
                        }
                        /*sWW.WriteLine("Conquaring region " + lowestMatches[index] + " using pawns: " +
                            smallestForceRequired + " totalPawnsUsed:" + troopsUsed2);
                        sWW.Close();*/

                        List<int> bestSequence2 = new List<int>();
                        int bestIncome2 = 0;
                        int bestTroopsUsed2 = 0;
                        int uniqueResults2 = 0;

                        enterRegion(regionsConquared2, regionsAvailableToConquare2, out bestSequence2,
                            troopsUsed2, currentIncome2, out bestIncome2, out bestTroopsUsed2, out uniqueResults2, cauntingRegions);

                        if (cauntingRegions)
                        {
                            if (bestSequence2.Count > bestSequence.Count)
                            {
                                uniqueResults = uniqueResults2;
                                bestSequence = bestSequence2;
                                bestIncome = bestIncome2;
                                bestTroopsUsed = bestTroopsUsed2;
                            }
                            else if (bestSequence2.Count == bestSequence.Count)
                            {
                                for (int k = 0; k < bestSequence2.Count; k++)
                                {
                                    if (!bestSequence.Contains(bestSequence2[k]))
                                    {
                                        uniqueResults += uniqueResults2;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (bestIncome2 > bestIncome)
                            {
                                uniqueResults = uniqueResults2;
                                bestSequence = bestSequence2;
                                bestIncome = bestIncome2;
                                bestTroopsUsed = bestTroopsUsed2;
                            }
                            else if (bestIncome2 == bestIncome)
                            {
                                for (int k = 0; k < bestSequence2.Count; k++)
                                {
                                    if (!bestSequence.Contains(bestSequence2[k]))
                                    {
                                        uniqueResults += uniqueResults2;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    regionsConquaredScope.Add(smallestIndex);
                    regionsAvailableToConquareScope.Remove(smallestIndex);
                    troopsUsedScope += smallestForceRequired;
                    currentIncomeScope += regions[smallestIndex].getRegionIncome();
                    foreach (int rE in regions[smallestIndex].getSurroundingRegions())
                    {
                        if (!regionsAvailableToConquareScope.Contains(rE) && !regionsConquaredScope.Contains(rE) && regions[rE].isConquerable(player))
                        {
                            regionsAvailableToConquareScope.Add(rE);
                        }
                    }
                    if (regions[smallestIndex].isExtraFeatureActive(player))
                    {
                        foreach (Region rE in regions)
                        {
                            if (rE.isExtraFeatureActive(player) && !regionsAvailableToConquareScope.Contains(rE.getRegionIndex()) && !regionsConquaredScope.Contains(rE.getRegionIndex()))
                            {
                                regionsAvailableToConquareScope.Add(rE.getRegionIndex());
                            }
                        }
                    }
                    /*StreamWriter sWW = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
                    sWW.WriteLine("Conquaring region " + smallestIndex + " using pawns: " +
                        smallestForceRequired + " totalPawnsUsed:" + troopsUsed);
                    sWW.Close();
                    */
                    List<int> bestSequence2 = new List<int>();
                    int bestIncome2;
                    int bestTroopsUsed2;
                    int uniqueResults2;

                    enterRegion(regionsConquaredScope, regionsAvailableToConquareScope, out bestSequence2,
                        troopsUsedScope, currentIncomeScope, out bestIncome2, out bestTroopsUsed2, out uniqueResults2, cauntingRegions);

                    bestIncome = bestIncome2;
                    bestSequence = bestSequence2;
                    bestTroopsUsed = bestTroopsUsed2;
                    uniqueResults = uniqueResults2;
                }
            }
            else
            {
                bestIncome = currentIncomeScope;
                bestSequence = regionsConquaredScope;
                bestTroopsUsed = troopsUsedScope;
                /*StreamWriter sWW2 = new StreamWriter(Application.dataPath + "/bestScenarios.txt", true);
                sWW2.WriteLine("END of TURN");
                sWW2.WriteLine("RegionsConqared:" + regionsConquaredScope.Count + "    Troops Used: " + troopsUsedScope + "     IncomeGenerated: " + currentIncomeScope);
                sWW2.Write("Region order:");
                for (int j = 0; j < regionsConquaredScope.Count; j++)
                {
                    sWW2.Write("   " + regionsConquaredScope[j]);
                }
                sWW2.WriteLine();
                sWW2.WriteLine();
                sWW2.Close();*/
                
                //if(regions[regionsAvailableToConquare[i]].calculateEstimatedForceRequired)


            }
        }
        public void updateRegionsInfo()
        {
            panelsEventsQueue = new List<EventInfo>();
            EventsQueue = new List<EventInfo>();
            Debug.Log("Updating regions");
            foreach (Region r in regions)
            {
                Enums.RegionEvents regionEvent = r.checkPawnsInRegion(player);
                //Debug.Log("Region " + r.getRegionIndex() + "    curr:" + r.getCurrentEvent() + "   prev:" + r.getPreviousEvent()+"  "+ regionEvent);
                if (regionEvent == Enums.RegionEvents.regionConquered)
                {
                    if (r.getOwner() == player.getPlayerRace())
                    {
                        Debug.Log("Event Occured in region: " + r.getRegionIndex() + "    eventType: " + r.getCurrentEvent());
                        EventInfo tempEvent = new EventInfo();
                        tempEvent.eventType = r.getCurrentEvent();
                        tempEvent.regionIndex = r.getRegionIndex();
                        EventsQueue.Add(tempEvent);
                        player.AddRegionAsConquered(r.getRegionIndex(), r.getRegionIncome(), r.getPlayerPawnsInConquaredRegion());
                    }
                }
                else if (regionEvent == Enums.RegionEvents.regionBeingConqueredNotEnoughPawns)
                {
                    Debug.Log("Event Occured in region: " + r.getRegionIndex() + "    eventType: " + r.getCurrentEvent());
                    EventInfo tempEvent = new EventInfo();
                    tempEvent.eventType = r.getCurrentEvent();
                    tempEvent.regionIndex = r.getRegionIndex();
                    panelsEventsQueue.Add(tempEvent);
                }
                else if (regionEvent == Enums.RegionEvents.regionLeft)
                {
                    if (r.getOwner() == player.getPlayerRace())
                    {
                        //CHECK IF REGION CAN BE LEFT IF NOT RETURN TO PREVIOUS
                        if (checkIfRegionCanBeLeft(r.getRegionIndex()))
                        {
                            r.returnToStartingSet();
                            player.RemoveRegionAsConquered(r.getRegionIndex(), r.getRegionIncome(), r.getPlayerPawnsUsedToConquareRegionOnLeaving());
                            Debug.Log("Event Occured in region: " + r.getRegionIndex() + "    eventType: " + r.getCurrentEvent());
                            EventInfo tempEvent = new EventInfo();
                            tempEvent.eventType = r.getCurrentEvent();
                            tempEvent.regionIndex = r.getRegionIndex();
                            panelsEventsQueue.Add(tempEvent);
                        }
                        else
                        {
                            r.setAsCantBeLeft(player);
                            EventInfo tempEvent = new EventInfo();
                            tempEvent.eventType = r.getCurrentEvent();
                            tempEvent.regionIndex = r.getRegionIndex();
                            panelsEventsQueue.Add(tempEvent);
                        }
                    }
                    else
                    {
                        r.returnToStartingSet();

                    }
                }
                else if (regionEvent == Enums.RegionEvents.regionBeingLeftButNotAllPawnsTaken)
                {
                    if (r.getCurrentState() == Enums.RegionState.cantBeLeft)
                    {
                        EventInfo tempEvent = new EventInfo();
                        tempEvent.eventType = r.getCurrentEvent();
                        tempEvent.regionIndex = r.getRegionIndex();
                        panelsEventsQueue.Add(tempEvent);
                    }
                    else
                    {
                        if (checkIfRegionCanBeLeft(r.getRegionIndex()))
                        {
                            r.returnToStartingSet();
                            player.RemoveRegionAsConquered(r.getRegionIndex(), r.getRegionIncome(), r.getPlayerPawnsUsedToConquareRegionOnLeaving());
                            Debug.Log("Event Occured in region: " + r.getRegionIndex() + "    eventType: " + r.getCurrentEvent());
                            EventInfo tempEvent = new EventInfo();
                            tempEvent.eventType = r.getCurrentEvent();
                            tempEvent.regionIndex = r.getRegionIndex();
                            panelsEventsQueue.Add(tempEvent);
                        }
                        else
                        {
                            r.setAsCantBeLeft(player);
                            EventInfo tempEvent = new EventInfo();
                            tempEvent.eventType = r.getCurrentEvent();
                            tempEvent.regionIndex = r.getRegionIndex();
                            panelsEventsQueue.Add(tempEvent);
                        }
                    }
                }
                r.showPawnHints(UIElementsParent, assetManager, player);
                r.clearRegionForNextEvent();
            }
            if (EventsQueue.Count > 0)
            {
                player.addNextMove(Time.realtimeSinceStartup - lastMoveTime);
                lastMoveTime = Time.realtimeSinceStartup;
            }
            //Debug.Log(EventsQueue.Count);
            /*for (int i = 0; i < regions.Count; i++)
            {
                Enums.RegionState currentRegionState = regions[i].checkPawnsInRegion();
                if (regions[i].getCurrentEvent() != regions[i].getPreviousEvent())
                {
                    Debug.Log("Region " + regions[i].getRegionIndex() + "    curr:" + regions[i].getCurrentEvent() + "   prev:" + regions[i].getPreviousEvent() + "    " + currentRegionState);
                }
                
            }*/
               // Debug.Log("Preparing NextMove");
                //calculateMoveResult();
        }
        public int getRegionIndex(int regionIndexInList)
        {
            return regions[regionIndexInList].getRegionIndex();
        }
        public void updatePanels(float deltaTime)
        {
            foreach (EventInfo ev in panelsEventsQueue)
            {
                panelManager.updatePanelInfo(ev.regionIndex,ev.eventType ,deltaTime);
            }
        }
        public int getRegionFromPosition(Vector2 pixelPosition)
        {

            Color color = regionsColorTexture.GetPixel((int)pixelPosition.x, (int)pixelPosition.y);
            
            int colorValue = (int)(color.r*255);
            //Debug.Log(colorValue);
            for (int i = 0; i < regionsColorValue.Length; i++)
            {
                if (colorValue == regionsColorValue[i])
                {
                    //Debug.Log("Pawn with pos: " + new Vector2((int)pixelPosition.x,(int)pixelPosition.y) + "   in region: " + i);
                    return i;
                }
            }
            return 0;
            /*
            //Debug.Log(pixelPosition);
            //Debug.Log(new Vector2((int)pixelPosition.x, Screen.height - (int)pixelPosition.y));
            Color color = regionsColorTexture.GetPixel((int)pixelPosition.x, Screen.height-(int)pixelPosition.y);

            //Debug.Log(color.g*255 + "   " + (int)color.g);
            //Debug.Log((int)(color.g * 255));
            switch ((int)(color.g*255))
            {
                case 255: 
                    return 0;
                case 230:
                    return 1;
                case 210:
                    return 2;
                case 195:
                    return 3;
                case 180:
                    return 4;
                case 170:
                    return 5;
                case 160:
                    return 6;
                case 150:
                    return 7;
                case 135:
                    return 8;
                case 120:
                    return 9;
                case 100:
                    return 10;
            }
            return 0;
            */
        }
    }
}
