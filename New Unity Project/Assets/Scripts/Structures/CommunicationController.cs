﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Structures
{
    class CommunicationController
    {
        private Enums.ImageProcessFunction currentFunction;
        private bool awaitingCorutineEnd = false;
        private bool newDataFromCorutine = false;



        public bool isNewDataReady()
        {
            return newDataFromCorutine;
        }
        public void newDataAccquired()
        {
            newDataFromCorutine = true;
        }
    }
}
