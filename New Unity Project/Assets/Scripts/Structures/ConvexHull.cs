﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConvexHull {

    private List<Vector2> polygonPointsALL = new List<Vector2>();
    private List<Vector2> convexHull = new List<Vector2>();
    private Vector2 centroid = new Vector2();

    public ConvexHull() { }
    public ConvexHull(List<Vector2> inputConvex){
        convexHull = inputConvex;
        calculateCentroid();
    }
    public ConvexHull(List<Vector2> inputConvex, List<Vector2> inputPolygonPoints)
    {
        convexHull = inputConvex;
        polygonPointsALL = inputPolygonPoints;
        calculateCentroid();
    }
    public void setPolygonPoints(List<Vector2> newPoints)
    {
        polygonPointsALL = newPoints;
    }
    public List<Vector2> getConvexHullPoints()
    {
            return convexHull;
    }
    private void calculateCentroid()
    {
        centroid = Assets.Scripts.Functions.findPolygonCentroid(convexHull);
    }
    public Vector2 getConvexCentroid()
    {
        return centroid;
    }
    public List<Vector2> getAllConvexPoints()
    {
        if (polygonPointsALL.Count > 0)
        {
            return polygonPointsALL;
        }
        else
        {
            return convexHull;
        }
    }
    public Vector2 getCovexHullPointWithScreenSubstraction(int index)
    {
        return new Vector2(convexHull[index].x, Screen.height - convexHull[index].y);
    }
}
