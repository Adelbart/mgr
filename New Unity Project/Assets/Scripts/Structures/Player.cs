﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
namespace Assets.Scripts.Structures
{
    class Player
    {
        private Enums.RaceType myRaceType = Enums.RaceType.Null;
        private Enums.PlayerRegionFeature myRegionFeature = Enums.PlayerRegionFeature.None;
        private List<int> conqureQueue = new List<int>();
        private List<float> consecutiveMovesTimes = new List<float>();
        private int maxNumberOfPawns = 0;
        private int pawnsUsed = 0;
        private int numberOfConquaredRegions = 0;
        private int currentPlayerIncome = 0;
        public Player() { }
        public Player(Enums.RaceType playerRaceType, int numberOfPawns)
        {
            myRaceType = playerRaceType;
            maxNumberOfPawns = numberOfPawns;
        }
        public void AddRegionAsConquered(int rIndex, int rIncome, int numberOfPlayerPawnsUsed)
        {
            Debug.Log("Trying to add region form conquared: " + !conqureQueue.Contains(rIndex));
            if (!conqureQueue.Contains(rIndex))
            {
                Debug.Log(conqureQueue.Count);
                conqureQueue.Add(rIndex);
                Debug.Log(conqureQueue.Count);

                Debug.Log(currentPlayerIncome);
                currentPlayerIncome += rIncome;
                Debug.Log(currentPlayerIncome);


                Debug.Log(pawnsUsed);
                pawnsUsed += numberOfPlayerPawnsUsed;
                Debug.Log(pawnsUsed);
            }
        }
        public void RemoveRegionAsConquered(int rIndex, int rIncome, int numberOfPlayerPawnsUsed)
        {
            Debug.Log("Trying to remove region form conquared: "+ conqureQueue.Contains(rIndex));
            if (conqureQueue.Contains(rIndex))
            {
                Debug.Log(conqureQueue.Count);
                conqureQueue.Remove(rIndex);
                Debug.Log(conqureQueue.Count);

                Debug.Log(currentPlayerIncome);
                currentPlayerIncome -= rIncome;
                Debug.Log(currentPlayerIncome);

                Debug.Log(pawnsUsed);
                pawnsUsed -= numberOfPlayerPawnsUsed;
                Debug.Log(pawnsUsed);
            }
        }
        public int getIncome()
        {
            return currentPlayerIncome;
        }
        public List<int> getConqauredRegions()
        {
            return conqureQueue;
        }
        public void setRegionFeature(Enums.PlayerRegionFeature newF)
        {
            myRegionFeature = newF;
            //Debug.Log("Setting players region Feature to: " + newF);
        }
        public Enums.PlayerRegionFeature getPlayersRegionFeature()
        {
            return myRegionFeature;
        }
        public float getLastMoveTime()
        {
            if (consecutiveMovesTimes.Count > 0)
            {
                return consecutiveMovesTimes[consecutiveMovesTimes.Count - 1];
            }
            return 0;
        }
        public void addNextMove(float moveTime)
        {
            consecutiveMovesTimes.Add(moveTime);
        }
        public List<float> getMoveSTimes()
        {
            return consecutiveMovesTimes;
        }
        public int getNumberOfMoves()
        {
            return consecutiveMovesTimes.Count;
        }
        public void setPlayerRace(Enums.RaceType newRace)
        {
            myRaceType = newRace;
        }
        public void setMaxPawns(int newMax)
        {
            maxNumberOfPawns = newMax;
        }
        public Enums.RaceType getPlayerRace()
        {
            return myRaceType;
        }
        public bool isRaceSameAsPlayers(Enums.RaceType otherRace)
        {
            return otherRace == myRaceType;
        }
        public void playerUsedSomePawns(int numberOfPawnsUSed)
        {
            pawnsUsed += numberOfPawnsUSed;
        }
        public int playerMaxPawns()
        {
            return maxNumberOfPawns;
        }
        public int pawnsLeft()
        {
            return maxNumberOfPawns - pawnsUsed;
        }
        public bool playerHasPawns()
        {
            //Debug.Log(maxNumberOfPawns +"   "+ pawnsUsed);
            //Debug.Log((maxNumberOfPawns - pawnsUsed) == 0);
            return (maxNumberOfPawns - pawnsUsed) > 0;
        }
    }
}
