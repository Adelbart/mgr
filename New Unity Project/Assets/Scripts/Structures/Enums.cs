﻿using UnityEngine;
using System.Collections;

public static class Enums {

    public enum RaceType
    {
        Null = 0,
        Amazon = 1,
        Orc = 2,                //GETS 1 COIN MORE FOR ATTACKING OCCUPIED LANDS
        Skeleton = 3,
        numberOfTypes = 4,
    }
    public enum RegionType
    {
        Normal = 0,
        GreenLand = 1,
        Mountain = 2,
        Farm = 3,
        Water = 4,
        Forest = 5,
        Swamp = 6

    }
    public enum RegionExtraFeature
    {
        None = 0,
        Cave = 1,

    }
    public enum PlayerRegionFeature
    {
        None = 0,
        Greenlander = 1,      //GETS 1 COIN MORE FOR GREENLAND LANDS
        Mountaineer = 2,      //GETS 1 COIN MORE FOR MOUNTAIN LANDS 
        Attacker = 3,        //NEEDS 1 PAWN LESS TO ATTACK
        Caveman = 4           //NEEDS 1 PAWN LESS TO ATTACK CAVES
    }
    public enum PawnState
    {
        neutral = 0,
        acceptedNew = 1,
        toRemove = 2,

    }
    public enum ImageProcessFunction
    {
        detectCorners = 0,
        detectPawnSize = 1,
        detectPawns = 2,
        detectTreshold = 3,
        trainPatterns = 4,
        manualTreshold = 5,
        saveScreen = 6,
        detectHand = 7,
        testTreshold = 8,
        testSetup = 9,
        testPhotoDelay = 10,
        none = 11,
    }
    public enum SlopeDirection
    {
        Steep,
        Flat,
        Horizontal,
        Vertical
    }

    public enum HandDetectionState
    {
        HandInCamera = 0,
        CameraClear = 1,

    }
    public enum RegionEvents
    {
        regionUnchanged = 0,                            // ---> REGION STATE CLEAR
        regionClearForNextEvent = 1,                    // ---> REGION STATE CLEAR
        regionConquered = 2,                            // ---> REGION STATE pawnsToBeCollected
        regionLeft = 3,                                 // ---> REGION STATE CLEAR
        regionBeingConqueredNotEnoughPawns = 4,         // ---> REGION STATE AWAITINGPAWNCHANGE
        returningToPreviousState = 5,                   // ---> REGION STATE AWAITINGPAWNCHANGE
        regionReinforced = 6,                           // ---> REGION STATE CLEAR
        regionDeinforced = 7,                           // ---> REGION STATE CLEAR
        regionBeingLeftButNotAllPawnsTaken = 8,
        nullEvent = 9,

    }
    public enum RegionState
    {
        regionClear = 0,
        pawnsToBeCollected = 1,
        awaitingPawnChange = 2,
        ownerPawnShouldBeAdded = 3,
        regionEventEnded = 4,
        regionEventOccured = 5,
        cantBeLeft =6,
        noEvent = 7,



    }

}
