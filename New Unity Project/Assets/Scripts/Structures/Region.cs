﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
namespace Assets.Scripts.Structures
{
    class Region
    {
        private int regionIndex;

        private Enums.RegionType regionType = Enums.RegionType.Normal;
        private Enums.RegionExtraFeature regionExtraFeature = Enums.RegionExtraFeature.None;
        private Enums.RegionState currentRegionState = Enums.RegionState.regionClear;
        private Enums.RegionEvents currentRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
        private Enums.RegionEvents previousRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
        private Enums.RegionState previousRegionState = Enums.RegionState.regionClear;
        private int regionIncome = 0;
        private int pawnsUsedToConquareRegion = 0;
        private int pawnsNeededToConquareRegion = 0;
        private bool eventOccured = false;
        private bool isHighlited = false;
        private bool isValidForAttack = false;
        public bool isOuter = false;

        int[] startingPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
        int[] expectedPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
        int[] previousPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
        int[] currentPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
        int lastNumberOfPawnsNeededToConquareRegion = 0;
        private int numberOfPawnsThatShouldBeInTheRegion = 0;

        private List<Pawn> pawnsInRegion = new List<Pawn>();
        private List<Pawn> pawnsInRegionDuringLastUpdate = new List<Pawn>();
        private Enums.RaceType regionOccupiedBy = Enums.RaceType.Null;
        private int[] indexNumberOfSurroundingRegions;
        private int[] indexesOfHistogramComaprison;
        public Region() { }
        public Region(int index)
        {
            regionIndex = index;
        }
        public void addPawnToRegion(Pawn newPawn)
        {
            pawnsInRegion.Add(newPawn);
            currentPawnDistribution[(int)newPawn.getRaceType()] += 1;
        }
        public void clearRegionForNewPlay()
        {
            pawnsInRegion = new List<Pawn>();
            pawnsInRegionDuringLastUpdate = new List<Pawn>();
            regionOccupiedBy = Enums.RaceType.Null;
            startingPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
            expectedPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
            previousPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
            currentPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
            currentRegionState = Enums.RegionState.regionClear;
            currentRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
            previousRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
            previousRegionState = Enums.RegionState.regionClear;

            eventOccured = false;
            isHighlited = false;
            isValidForAttack = false;
    }
    public void addhistogramComparisonIndexes(string[] histogramIndexes)
        {
            indexesOfHistogramComaprison = new int[histogramIndexes.Length];
            for (int i = 0; i < histogramIndexes.Length; i++)
            {
                indexesOfHistogramComaprison[i] = Int32.Parse(histogramIndexes[i]);
            }
        }
        public void addSurroundingRegions(string[] regionIndexes)
        {
            indexNumberOfSurroundingRegions = new int[regionIndexes.Length];
            for (int i = 0; i < regionIndexes.Length; i++)
            {
                indexNumberOfSurroundingRegions[i] = Int32.Parse(regionIndexes[i]);
            }
        }
        public void setRegionType(Enums.RegionType rType)
        {
            regionType = rType;
            //Debug.Log("Setting region " + regionIndex + " to: " + rType);
        }
        public void setPreStartDesiredDistribution(int pawnCount)
        {
            startingPawnDistribution[(int)regionOccupiedBy] = pawnCount;
        }
        public int getPreStartPawnNumber()
        {
            return startingPawnDistribution[(int)regionOccupiedBy];
        }
        public bool checkPreStartPawnDistribution()
        {
            bool result = true;
            Debug.Log("Region no: " + regionIndex + " Owner: " + regionOccupiedBy + " expected No: " + startingPawnDistribution[(int)regionOccupiedBy] + " current No: " + pawnsInRegion.Count);
            if (pawnsInRegion.Count != startingPawnDistribution[(int)regionOccupiedBy])
            {
                Debug.Log("Different Number of pawns in Region");
                result = false;
            }
            for (int i = 0; i < pawnsInRegion.Count; i++)
            {
                if (pawnsInRegion[i].getRaceType() != regionOccupiedBy)
                {
                    Debug.Log("Pawn of inproper Race detected in region. Detected: "+pawnsInRegion[i].getRaceType());
                    result = false;
                }
            }
            pawnsInRegionDuringLastUpdate = pawnsInRegion;
            previousPawnDistribution = currentPawnDistribution;
            pawnsInRegion = new List<Pawn>();
            currentPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
            return result;
        }
        public void setRegionExtraFeature(Enums.RegionExtraFeature rExtraFeature)
        {
            regionExtraFeature = rExtraFeature;
            //Debug.Log("Setting region " + regionIndex + " extra feature to: " + rExtraFeature);
        }
        public Enums.RegionType getRegionType()
        {
            return regionType;
        }
        public int getCurrentNumberOfPawnsInRegion()
        {
            return pawnsInRegion.Count;
        }
        public Pawn getCurrentPawn(int indes)
        {
            return pawnsInRegion[indes];
        }
        public int[] getHistogramComaprisonIndexes()
        {
            return indexesOfHistogramComaprison;
        }
        public int[] getSurroundingRegions()
        {
            return indexNumberOfSurroundingRegions;
        }
        public void prepareEvent()
        {
            eventOccured = true;
        }
        public bool didAnEventOccur()
        {
            return eventOccured;
        }
        public void regionEevntAddedToQueue()
        {
            eventOccured = false;
        }
        public int getPlayerPawnsUsedToConquareRegionOnLeaving()
        {
            int temp = pawnsUsedToConquareRegion;
            pawnsUsedToConquareRegion = 0;
            return temp;
        }
        public int getPlayerPawnsInConquaredRegion()
        {
            pawnsUsedToConquareRegion = currentPawnDistribution[(int)regionOccupiedBy];
            return pawnsUsedToConquareRegion;
        }
        public int calculateEstimatedIncome(Player p)
        {
            Enums.PlayerRegionFeature pFeature = p.getPlayersRegionFeature();
            Enums.RaceType pRaceType = p.getPlayerRace();
            //CALCULATE INCOME
            int givenIncome = 1;
            if (pFeature == Enums.PlayerRegionFeature.Greenlander && regionType == Enums.RegionType.GreenLand)
            {
                givenIncome += 1;
            }
            else
            {
                if (pFeature == Enums.PlayerRegionFeature.Mountaineer && regionType == Enums.RegionType.Mountain)
                {
                    givenIncome += 1;
                }
            }
            if (pRaceType == Enums.RaceType.Orc && regionOccupiedBy != Enums.RaceType.Null && regionOccupiedBy != pRaceType)
            {
                givenIncome += 1;
            }
            //Debug.Log("Calculated income: "+regionIncome);
            regionIncome = givenIncome;
            //Debug.Log("Calculated income2: " + regionIncome);
            return regionIncome;

        }
        public bool isExtraFeatureActive(Player p)
        {
            if (p.getPlayersRegionFeature() == Enums.PlayerRegionFeature.Caveman && regionExtraFeature == Enums.RegionExtraFeature.Cave)
            {
                return true;
            }
            return false;
        }
        public int calculateEstimatedForceRequired(Player p)
        {
            Enums.PlayerRegionFeature pFeature = p.getPlayersRegionFeature();
            Enums.RaceType pRaceType = p.getPlayerRace();
            int requiredForce = 0;
            //CALCULATE FORCE
            if (regionOccupiedBy != pRaceType)
            {
                requiredForce = 2;
                for (int i = 0; i < pawnsInRegionDuringLastUpdate.Count; i++)
                {
                    if (pawnsInRegionDuringLastUpdate[i].getRaceType() != pRaceType)
                    {
                        requiredForce += 1;
                    }
                }
                if (pFeature == Enums.PlayerRegionFeature.Attacker)
                {
                    requiredForce -= 1; ;
                }
                if (pFeature == Enums.PlayerRegionFeature.Caveman && regionExtraFeature == Enums.RegionExtraFeature.Cave)
                {
                    requiredForce -= 1;
                }

                if (requiredForce < 1)
                {
                    requiredForce = 1;
                }
            }
            pawnsNeededToConquareRegion = requiredForce;
            return requiredForce;
        }
        public int getPawnsRequiredToConquare()
        {
            return pawnsNeededToConquareRegion;
        }
        public void calculateRegionValues(Player p, out int requiredForce, out int givenIncome)
        {
            Enums.PlayerRegionFeature pFeature = p.getPlayersRegionFeature();
            Enums.RaceType pRaceType = p.getPlayerRace();
            requiredForce = getPawnsRequiredToConquare();
            givenIncome = getRegionIncome();

        }
        public void showPawnHints(GameObject UIelemParent, AssetManager aM, Player player)
        {
            foreach(Pawn p in pawnsInRegion)
            {
                GameObject obj = GameObject.Instantiate(aM.UIElementPrefab);
                obj.transform.SetParent(UIelemParent.transform, true);
                //Debug.Log("Creating pawn transparency at: " + p.getPawnPositionOnScreen());
                //Debug.Log("position" + obj.transform.position);
                //obj.transform.localPosition = Vector3.zero;
                //Debug.Log("pre locpos set zero:" + obj.transform.localPosition);
                //Debug.Log("pre pos set:" + obj.transform.position);
                //obj.transform.position = Vector3.zero;
                //Debug.Log("pre pos set zero :" + obj.transform.position);
                //Debug.Log("pre locpos set zero:" + obj.transform.localPosition);
                Vector3 tempPos = obj.transform.localPosition;
                //obj.transform.localPosition = Vector3.zero;
                Vector3 objWantedPosition = p.getPawnPositionOnScreen();
                //objWantedPosition.y = Screen.height - objWantedPosition.y;
                float pawnWidth = p.getPawnWidth();
                float pawnHeight = p.getPawnHeight();
                obj.transform.position = objWantedPosition;
                //Debug.Log("position:" + obj.transform.position);
                //Debug.Log("position:" + obj.transform.position);
                    obj.transform.position += new Vector3(pawnWidth / 3, pawnHeight / 3, 0);
                    obj.transform.RotateAround(obj.transform.position - new Vector3(pawnWidth / 3, pawnHeight / 3, 0), new Vector3(0, 0, 1), -p.getPawnRotation());
                    //Debug.Log("position:" + obj.transform.position);
                //obj.transform.localPosition += tempPos;
                //Debug.Log("post locpos set:" + obj.transform.localPosition);
                //Debug.Log("post pos set:" + obj.transform.position);

                Image objImg = obj.GetComponent<Image>();

                if (p.getRaceType() == player.getPlayerRace() && p.getPawnState() == Enums.PawnState.acceptedNew)
                {
                    objImg.sprite = aM.circleGreen;
                }
                else if(p.getPawnState() == Enums.PawnState.toRemove)
                {
                    objImg.sprite = aM.pawnRemoval;
                }
                else
                {
                    objImg.sprite = aM.circleRed;
                }
                objImg.rectTransform.sizeDelta = new Vector2(pawnWidth / 1.5f, pawnHeight / 1.5f);
            }
        }
        public void showPawnsHints(GameObject UIelem, GameObject UIelemParent, AssetManager aM)
        {
            foreach (Pawn p in pawnsInRegion)
            {
                
                GameObject obj = GameObject.Instantiate(UIelem);
                Debug.Log("Creating pawn transparency at: " + p.getPawnPositionOnScreen());
                obj.transform.SetParent(UIelemParent.transform);
                Debug.Log("pre pos set:"+ obj.transform.position);
                obj.transform.localPosition = p.getPawnPositionOnScreen();
                Debug.Log("post pos set:" +obj.transform.position);
                Image objImg = obj.GetComponent<Image>();
                if (p.getPawnState() == Enums.PawnState.toRemove)
                {
                    objImg.sprite = aM.UIElementsTextures[0];
                }
                else
                {
                    objImg.sprite = aM.whiteSprite;
                }
                float imageSizeW = p.getPawnScreenWidth();
                float imageSizeH = p.getPawnScreenHeight();
                objImg.rectTransform.sizeDelta = new Vector2(imageSizeW, imageSizeH);
            }
        }
        public bool getIsValidForAttack()
        {
            return isValidForAttack;
        }
        public bool isConquerable(Player p)
        {
            if (regionType == Enums.RegionType.Water)
            {
                return false;
            }
            return true;
        }
        public bool hasRegionBeenAlreadyHighlited()
        {
            return isHighlited;
        }
        public bool isRegionValidForExtraIncome(Player p)
        {
            if(
                (p.getPlayersRegionFeature() == Enums.PlayerRegionFeature.Greenlander && regionType == Enums.RegionType.GreenLand) ||
                (p.getPlayersRegionFeature() == Enums.PlayerRegionFeature.Mountaineer && regionType == Enums.RegionType.Mountain))
            {
                return true;
            }
            else{
                return false;
            }
        }
        public void litRegionAsAvailableToConquere()
        {
            isValidForAttack = true;
            if (isHighlited)
                return;
            //CHANGE REGION TEXTURE

            isHighlited = true;
        }
        public void litRegionAsOwnedByPlayer()
        {
            isHighlited = true;
            isValidForAttack = false;
        }
        public void litRegionAsUnavailableToConquere()
        {
            isHighlited = false;
            isValidForAttack = false;
        }
        public void setisOuter(bool newValue)
        {
            isOuter = newValue;
        }
        public void setisOuter(int newValue)
        {
            if (newValue == 1)
            {
                isOuter = true;
            }
            else
            {
                isOuter = false;
            }
        }
        public void setNewOwner(Enums.RaceType newOwner)
        {
            Debug.Log("Setting owner of region: " + regionIndex + " to: " + newOwner);
            regionOccupiedBy = newOwner;
        }
        public Enums.RaceType getStartingRegionOwner()
        {
            for(int i = 0; i < startingPawnDistribution.Length; i++)
            {
                if(startingPawnDistribution[i] != 0)
                {
                    return (Enums.RaceType)(i);
                }
            }
            return Enums.RaceType.Null;
        }
        public Enums.RaceType getOwner()
        {
            return regionOccupiedBy;
        }
        public Enums.RegionEvents getCurrentEvent()
        {
            return currentRegionEvent;
        }
        public Enums.RegionState getCurrentState()
        {
            return currentRegionState;
        }
        public Enums.RegionEvents getPreviousEvent()
        {
            return previousRegionEvent;
        }
        public int getRegionIndex()
        {
            return regionIndex;
        }
        public Enums.RegionState getRegionState()
        {
            return currentRegionState;
        }
        public void clearRegionForNextEvent()
        {
            pawnsInRegion = new List<Pawn>();
            currentPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
            //currentRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
            //currentRegionState = Enums.RegionState.regionClear;
        }
        public void prepareRegionInfo()
        {
            currentRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
            currentRegionState = Enums.RegionState.regionClear;
            previousRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
            previousRegionState = Enums.RegionState.regionClear;
        }
        bool pawnsOfTheSameRaceInRegionDuringPreviousFrame()
        {
            Enums.RaceType basicRace = pawnsInRegionDuringLastUpdate[0].getRaceType();
            if (pawnsInRegionDuringLastUpdate.Count > 1)
                for (int i = 1; i < pawnsInRegionDuringLastUpdate.Count; i++)
                {
                    if (!(basicRace == pawnsInRegionDuringLastUpdate[i].getRaceType()))
                    {
                        return false;
                    }
                }
            return true;
        }
        bool pawnsOfTheSameRaceInRegion()
        {
            Enums.RaceType basicRace = pawnsInRegion[0].getRaceType();
            if(pawnsInRegion.Count >1)
            for (int i = 1; i < pawnsInRegion.Count; i++)
            {
                if (!(basicRace == pawnsInRegion[i].getRaceType()))
                {
                    return false;
                }
            }
            return true;
        }
        void conquereBlankRegion()
        {
            Debug.Log("Conquering blank region no: " + regionIndex);
            setNewOwner(pawnsInRegion[0].getRaceType());
            numberOfPawnsThatShouldBeInTheRegion = pawnsInRegion.Count;
            currentRegionEvent = Enums.RegionEvents.regionConquered;
            currentRegionState = Enums.RegionState.regionClear;
        }

        void conquereRegion(Enums.RaceType newRace)
        {
            Debug.Log("Conquering region no: " + regionIndex);
            setNewOwner(newRace);
            numberOfPawnsThatShouldBeInTheRegion = 0;
            for (int i = 0; i < pawnsInRegion.Count; i++)
            {
                if (pawnsInRegion[i].getRaceType() == regionOccupiedBy)
                {
                    numberOfPawnsThatShouldBeInTheRegion += 1;
                }
            }
            expectedPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
            expectedPawnDistribution[(int)getOwner()] = numberOfPawnsThatShouldBeInTheRegion;
            currentRegionEvent = Enums.RegionEvents.regionConquered;
            currentRegionState = Enums.RegionState.pawnsToBeCollected;
        }
        void setStateToAwaitToPreviousState()
        {
            previousRegionEvent = currentRegionEvent;
            currentRegionEvent = Enums.RegionEvents.returningToPreviousState;
            previousRegionState = currentRegionState;
            currentRegionState = Enums.RegionState.awaitingPawnChange;

            numberOfPawnsThatShouldBeInTheRegion = pawnsInRegionDuringLastUpdate.Count;
            expectedPawnDistribution = distributePawns(pawnsInRegionDuringLastUpdate);
            markPawnsToBeTakenOff(expectedPawnDistribution);
        }
        void markPawnsOfDefeatedRace()
        {
            for (int i = 0; i < pawnsInRegion.Count; i++)
            {
                if (pawnsInRegion[i].getRaceType() != getOwner())
                {
                    pawnsInRegion[i].setPawnToRemove();
                    

                }
            }
        }
        void markPawnsToBeTakenOff()
        {
            int numberOfOwnerPawnsInRegion = 0;
            Debug.Log("Setting pawn to be removed");
            foreach(Pawn p in pawnsInRegion)
            {
                if(p.getRaceType() != regionOccupiedBy)
                {
                    p.setPawnToRemove();
                }
                else
                {
                    if (currentRegionEvent != Enums.RegionEvents.regionConquered) {
                        numberOfOwnerPawnsInRegion += 1;
                        if (numberOfOwnerPawnsInRegion > startingPawnDistribution[(int)regionOccupiedBy])
                        {
                            p.setPawnToRemove();
                        }
                    }
                }
            }
        }
      void markPawnsToBeTakenOff(int[] pawnsDistributionPattern)
        {
            int[] markedPawns = new int[(int)Enums.RaceType.numberOfTypes];

            for (int i = 0; i < pawnsInRegion.Count; i++)
            {
                int pawnRaceIndex = (int)pawnsInRegion[i].getRaceType();
                if (markedPawns[pawnRaceIndex] < pawnsDistributionPattern[pawnRaceIndex])
                {
                    markedPawns[pawnRaceIndex] += 1;
                }
                else
                {
                    pawnsInRegion[i].setPawnToRemove();
                  
                }
            }
        }
        void regionDidNotChange()
        {
            currentRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
            currentRegionState = Enums.RegionState.regionClear;
        }
        void regionLeft()
        {
            numberOfPawnsThatShouldBeInTheRegion = 0;
            currentRegionEvent = Enums.RegionEvents.regionLeft;
            currentRegionState = Enums.RegionState.regionClear;
        }
        void regionTroopChange(bool isReinforced)
        {
            currentRegionState = Enums.RegionState.regionClear;
            if (isReinforced)
            {
                currentRegionEvent = Enums.RegionEvents.regionReinforced;
            }
            else
            {
                currentRegionEvent = Enums.RegionEvents.regionDeinforced;
            }
            numberOfPawnsThatShouldBeInTheRegion = pawnsInRegion.Count;
        }
        float calculateRaceFactor(out Enums.RaceType offensiveRace)
        {
            offensiveRace = Enums.RaceType.Null;
            float pawnsOfDefense = 0;
            float pawnsOfOffense = 0;
            for (int i = 0; i < pawnsInRegion.Count; i++)
            {
                if (pawnsInRegion[i].getRaceType() != regionOccupiedBy)
                {
                    offensiveRace = pawnsInRegion[i].getRaceType();
                    pawnsOfOffense += 1;
                }
                else
                {
                    pawnsOfDefense += 1;
                }

            }
            return pawnsOfDefense/pawnsOfOffense;
        }
        bool areTheExpectedPawnsInRegion(int[] currentPawnDistribution)
        //RETURNS TRUE IF SIMILAR PAWNS WHERE IN THE REGION, FALSE IF SOMETHING NEW HAS OCCURED
        {

            for (int i = 0; i < currentPawnDistribution.Length; i++)
            {
                if (currentPawnDistribution[i] != expectedPawnDistribution[i])
                {
                    return false;
                }
            }
            return true;
        }
        private int[] distributePawns(List<Pawn> inputList)
        {
            int[] distributionOfPawns = new int[(int)Enums.RaceType.numberOfTypes];


            for (int i = 0; i < inputList.Count; i++)
            {
                distributionOfPawns[(int)inputList[i].getRaceType()] += 1;
            }
            return distributionOfPawns;
        }
        private void printPawnDistribution(int[] inputdistribution)
        {
            for(int i = 0; i < inputdistribution.Length; i++)
            {
                Debug.Log((Enums.RaceType)i + ": " + inputdistribution[i]);
            }
        }
        private void printPawnDistribution(List<Pawn> inputList)
        {
            int[] pawnList = distributePawns(inputList);

            /*for (int i = 0; i < pawnList.Length; i++)
            {
                Debug.Log("Number of "+(Enums.RaceType)i+" pawns in region: "+pawnList[i]);
            }*/
        }
        public int getRegionIncome()
        {
            //Debug.Log(regionIncome);
            return regionIncome;
        }
        public void setAsCantBeLeft(Player p)
        {
            Debug.Log("setting region " + regionIndex + " to cantBeLeFT");
            setNewOwner(p.getPlayerRace());
            currentRegionEvent = Enums.RegionEvents.regionBeingLeftButNotAllPawnsTaken;
            currentRegionState = Enums.RegionState.cantBeLeft;
            expectedPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
            expectedPawnDistribution[(int)p.getPlayerRace()] = pawnsNeededToConquareRegion;
        }
        public void returnToStartingSet()
        {
            regionOccupiedBy = Enums.RaceType.Null;
            for (int i = 0; i < startingPawnDistribution.Length; i++)
            {
                if (startingPawnDistribution[i] != 0)
                {
                    regionOccupiedBy = (Enums.RaceType)startingPawnDistribution[i];
                }
            }
            currentRegionState = Enums.RegionState.awaitingPawnChange;
            currentRegionEvent = Enums.RegionEvents.returningToPreviousState;
            previousRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
            previousRegionState = Enums.RegionState.regionClear;
            expectedPawnDistribution = startingPawnDistribution;
        }
        private bool regionWasBlankAtStart()
        {
            for (int i = 0; i < startingPawnDistribution.Length; i++)
            {
                if (startingPawnDistribution[i] != 0)
                {
                    return false;
                }
            }
            return true;
        }
        private void updateRegionPawns()
        {
            //Debug.Log("Updating region no: " + regionIndex + " pawns");
            pawnsInRegionDuringLastUpdate = pawnsInRegion;
        }
        private void calculateExpectedPawnDistribution(List<Pawn> inputList)
        {
            expectedPawnDistribution = distributePawns(inputList);
        }
        private int getEnemyStrengthInDistribution(Enums.RaceType pRAceType, int[] pDistr)
        {
            int result = 0;
            if (pRAceType != Enums.RaceType.Amazon)
            {
                result += pDistr[(int)Enums.RaceType.Amazon];
            } 
            if (pRAceType != Enums.RaceType.Skeleton)
            {
                result += pDistr[(int)Enums.RaceType.Skeleton];
            } 
            if (pRAceType != Enums.RaceType.Orc)
            {
                result += pDistr[(int)Enums.RaceType.Orc];
            }
            return result;
        }
        public void printPawns()
        {
            Debug.Log("Region " + regionIndex + "   number of pawns: " +
                Enums.RaceType.Amazon + " " + currentPawnDistribution[(int)Enums.RaceType.Amazon] +"  "+
                Enums.RaceType.Orc + " " + currentPawnDistribution[(int)Enums.RaceType.Orc] + "  " +
                Enums.RaceType.Skeleton + " " + currentPawnDistribution[(int)Enums.RaceType.Skeleton]);
        }
        public Enums.RegionEvents checkPawnsInRegion(Player p)
        {

            //Exactly same pawns
            //Debug.Log("Region  "+regionIndex+"  pawns:" + (pawnsInRegion.Count + "    " + pawnsInRegionDuringLastUpdate.Count));
            if (pawnsInRegion.Count == pawnsInRegionDuringLastUpdate.Count && currentRegionState == Enums.RegionState.regionClear)
            {
                if (checkPawnDistributionEquality(currentPawnDistribution))
                {
                    //NIC SIE NIE ZADZIALO
                    return Enums.RegionEvents.nullEvent;
                }
                else
                {
                    if (currentPawnDistribution[(int)p.getPlayerRace()] >= getPawnsRequiredToConquare())
                    {
                        currentRegionEvent = Enums.RegionEvents.regionConquered;
                        currentRegionState = Enums.RegionState.awaitingPawnChange;
                        markPawnsToBeTakenOff();
                        previousPawnDistribution = currentPawnDistribution;
                        pawnsInRegionDuringLastUpdate = pawnsInRegion;
                        return currentRegionEvent;
                    }
                    else if (currentPawnDistribution[(int)p.getPlayerRace()] > 0 && currentPawnDistribution[(int)p.getPlayerRace()] < getPawnsRequiredToConquare())
                    {
                        currentRegionEvent = Enums.RegionEvents.regionBeingConqueredNotEnoughPawns;
                        currentRegionState = Enums.RegionState.awaitingPawnChange;
                        previousPawnDistribution = currentPawnDistribution;
                        pawnsInRegionDuringLastUpdate = pawnsInRegion;
                        return currentRegionEvent;
                    }
                    else
                    {
                        //BLEDNY RUCH - sprawdz podpowiedzi i pokaz co jest zle
                        expectedPawnDistribution = previousPawnDistribution;
                        previousPawnDistribution = currentPawnDistribution;
                        previousRegionState = currentRegionState;
                        previousRegionEvent = currentRegionEvent;
                        currentRegionEvent = Enums.RegionEvents.returningToPreviousState;
                        currentRegionState = Enums.RegionState.awaitingPawnChange;
                        pawnsInRegionDuringLastUpdate = pawnsInRegion;
                        return Enums.RegionEvents.returningToPreviousState;
                    }
                }
            }
            //NEW ALGORITHM
            #region newPawnCheckAlgorithm
            if (!isConquerable(p) && !hasRegionBeenAlreadyHighlited() && pawnsInRegion.Count>0)
            {
                expectedPawnDistribution = previousPawnDistribution;
                previousPawnDistribution = currentPawnDistribution;
                previousRegionEvent = currentRegionEvent;
                previousRegionState = currentRegionState;
                currentRegionEvent = Enums.RegionEvents.returningToPreviousState;
                currentRegionState = Enums.RegionState.awaitingPawnChange;
                pawnsInRegionDuringLastUpdate = pawnsInRegion;
                return Enums.RegionEvents.returningToPreviousState;
            }

            //ROZNA ILOSC PIONKOW MIEDZY PRZEJSCIAMI}
            if (currentRegionEvent == Enums.RegionEvents.regionClearForNextEvent ) //Check if region is ready for the next event
            {
                if (!isValidForAttack)
                {
                    markPawnsToBeTakenOff();
                    return Enums.RegionEvents.nullEvent;

                }
                Debug.Log("Region "+regionIndex +" ready fort new event");
                //Region Left - uproszczony check
                if (pawnsInRegion.Count == 0 && pawnsInRegionDuringLastUpdate.Count > 0 && regionOccupiedBy == p.getPlayerRace())
                {
                    setNewOwner(getStartingRegionOwner());
                    currentRegionEvent = Enums.RegionEvents.regionLeft;
                    currentRegionState = Enums.RegionState.awaitingPawnChange;
                    previousPawnDistribution = currentPawnDistribution;
                    expectedPawnDistribution = startingPawnDistribution;
                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                    return Enums.RegionEvents.regionLeft;
                }

                int estimatedForceRequired = 0;
                int estimatedIncome = 0;
                int playerPawnsInRegionInCurrentFrame = currentPawnDistribution[(int)p.getPlayerRace()];
                int playerPawnsInRegionInPreviousFrame = previousPawnDistribution[(int)p.getPlayerRace()];

                int oppPawnsInRegionInCurrentFrame = getEnemyStrengthInDistribution(p.getPlayerRace(), currentPawnDistribution);
                int oppPawnsInRegionInPreviousFrame = getEnemyStrengthInDistribution(p.getPlayerRace(), previousPawnDistribution);

                calculateRegionValues(p, out estimatedForceRequired, out estimatedIncome);

                int[] estimatedForceDistribution = new int[(int)Enums.RaceType.numberOfTypes];
                estimatedForceDistribution[(int)p.getPlayerRace()] = estimatedForceRequired;


                //CONQARE BLANK REGION
                if (pawnsInRegionDuringLastUpdate.Count == 0 && playerPawnsInRegionInCurrentFrame >= estimatedForceRequired && isValidForAttack)
                {
                    setNewOwner(p.getPlayerRace());
                    currentRegionEvent = Enums.RegionEvents.regionConquered;
                    currentRegionState = Enums.RegionState.regionClear;
                    previousPawnDistribution = currentPawnDistribution;
                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                    return currentRegionEvent;
                }
                //NOT ENOUGH PAWNS TO CONQUARE BLANK REGION
                if (pawnsInRegionDuringLastUpdate.Count == 0 && playerPawnsInRegionInCurrentFrame>0 &&
                    playerPawnsInRegionInCurrentFrame == pawnsInRegion.Count && playerPawnsInRegionInCurrentFrame < estimatedForceRequired && isValidForAttack)
                {

                    currentRegionEvent = Enums.RegionEvents.regionBeingConqueredNotEnoughPawns;
                    currentRegionState = Enums.RegionState.awaitingPawnChange;
                    previousPawnDistribution = currentPawnDistribution;
                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                    return currentRegionEvent;
                }

                //LEAVEREGION
                if (pawnsInRegion.Count == 0 && playerPawnsInRegionInPreviousFrame > 0 && regionOccupiedBy == p.getPlayerRace())
                {
                    setNewOwner(getStartingRegionOwner());
                    currentRegionEvent = Enums.RegionEvents.regionLeft;
                    currentRegionState = Enums.RegionState.awaitingPawnChange;
                    expectedPawnDistribution = startingPawnDistribution;
                    previousPawnDistribution = currentPawnDistribution;
                    pawnsInRegionDuringLastUpdate = pawnsInRegion;

                    return currentRegionEvent;
                }

                    //DEINFORCE REGION -- w tej wersji illegal move! GRACZ PROBUJE OPUSCIC SWOJ/PDOBITY REGION
                if (regionOccupiedBy == p.getPlayerRace() && playerPawnsInRegionInCurrentFrame < playerPawnsInRegionInPreviousFrame && playerPawnsInRegionInCurrentFrame == pawnsInRegion.Count)
                {
                /*if (pawnsInRegion.Count > 0 && playerPawnsInRegionInCurrentFrame > 0 &&
                    playerPawnsInRegionInCurrentFrame < playerPawnsInRegionInPreviousFrame &&
                    playerPawnsInRegionInCurrentFrame == pawnsInRegion.Count)
                {*/

                    /*currentRegionEvent = Enums.RegionEvents.regionDeinforced;
                    currentRegionState = Enums.RegionState.regionClear;
                    return Enums.RegionEvents.regionDeinforced;*/
                    currentRegionEvent = Enums.RegionEvents.regionBeingLeftButNotAllPawnsTaken;
                    markPawnsToBeTakenOff();
                    currentRegionState = Enums.RegionState.awaitingPawnChange;
                    setNewOwner(getStartingRegionOwner());
                    expectedPawnDistribution = startingPawnDistribution;
                    lastNumberOfPawnsNeededToConquareRegion = playerPawnsInRegionInPreviousFrame;
                    pawnsInRegionDuringLastUpdate = pawnsInRegion;

                    return currentRegionEvent;
                }

                //CONQUARE OCCUPIED REGION
                //FIRST INITIALIZE CONQUARE BY CHECKING IF THERE ARE PAWNS OF DIFFERENT RACES
                if (playerPawnsInRegionInCurrentFrame != 0 && oppPawnsInRegionInCurrentFrame != 0 && isValidForAttack)
                {
                    //Debug.Log("Trying to conquare an occupied region");
                    if (regionOccupiedBy != p.getPlayerRace())
                    {
                        if (playerPawnsInRegionInCurrentFrame >= estimatedForceRequired)
                        {
                            currentRegionEvent = Enums.RegionEvents.regionConquered;
                            currentRegionState = Enums.RegionState.pawnsToBeCollected;
                            expectedPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
                            expectedPawnDistribution[(int)p.getPlayerRace()] = currentPawnDistribution[(int)p.getPlayerRace()];
                            previousPawnDistribution = currentPawnDistribution;
                            pawnsInRegionDuringLastUpdate = pawnsInRegion;
                            setNewOwner(p.getPlayerRace());
                            //MARK PAWNS TO BE COLLECTED
                            markPawnsToBeTakenOff();
                            return currentRegionEvent;
                        }
                        else
                        {
                            previousPawnDistribution = currentPawnDistribution;
                            pawnsInRegionDuringLastUpdate = pawnsInRegion;
                            currentRegionEvent = Enums.RegionEvents.regionBeingConqueredNotEnoughPawns;
                            currentRegionState = Enums.RegionState.awaitingPawnChange;
                            return currentRegionEvent;
                        }
                    }
                    else
                    {
                        //POLOZONY ZOSTAL PIONEK W REJONIE GRACZA INVALID MOVE
                        //SET PREVIOUS 
                        //NIE POWINNO TUTAJ NIGDY WEJSC
                        Debug.Log("Unreachable code");
                        return Enums.RegionEvents.nullEvent;
                    }
                }
                markPawnsToBeTakenOff();
                /*else
                {
                    //POLOZONY ZOSTAL PIONEK W REJONIE GRACZA INVALID MOVE
                    //SET PREVIOUS STATE
                    lastNumberOfPawnsNeededToConquareRegion = calculateEstimatedForceRequired(p);
                    currentRegionEvent = Enums.RegionEvents.returningToPreviousState;
                    currentRegionState = Enums.RegionState.awaitingPawnChange;
                    markPawnsToBeTakenOff();
                    return currentRegionEvent;
                }*/
            }
            else
            {
                Debug.Log("Region already had an event");
                if (currentRegionEvent == Enums.RegionEvents.regionConquered)
                {
                    Debug.Log("Region has already been Conquared By Player");
                    Debug.Log(pawnsInRegion.Count + "    " + pawnsInRegionDuringLastUpdate.Count);
                    if (pawnsInRegion.Count != pawnsInRegionDuringLastUpdate.Count && pawnsInRegion.Count > 0)
                    {
                        if (pawnsInRegion.Count < pawnsInRegionDuringLastUpdate.Count && currentRegionState != Enums.RegionState.pawnsToBeCollected)
                        {
                            previousRegionState = currentRegionState;
                            previousRegionEvent = currentRegionEvent;
                            lastNumberOfPawnsNeededToConquareRegion = pawnsInRegionDuringLastUpdate.Count;

                            previousPawnDistribution = currentPawnDistribution;
                            expectedPawnDistribution = startingPawnDistribution;
                            setNewOwner(getStartingRegionOwner());
                            currentRegionEvent = Enums.RegionEvents.regionBeingLeftButNotAllPawnsTaken;
                            markPawnsToBeTakenOff();
                            currentRegionState = Enums.RegionState.awaitingPawnChange;

                            return currentRegionEvent;
                        }
                        else if (pawnsInRegion.Count < pawnsInRegionDuringLastUpdate.Count && currentRegionState == Enums.RegionState.pawnsToBeCollected)
                        {
                            int playerPawnsInRegionInCurrentFrame = currentPawnDistribution[(int)p.getPlayerRace()];
                            int playerPawnsInRegionInPreviousFrame = previousPawnDistribution[(int)p.getPlayerRace()];
                            if (playerPawnsInRegionInCurrentFrame == playerPawnsInRegionInPreviousFrame)
                            {
                                if (playerPawnsInRegionInCurrentFrame == pawnsInRegion.Count)
                                {
                                    currentRegionState = Enums.RegionState.regionClear;
                                    previousPawnDistribution = currentPawnDistribution;
                                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                    return Enums.RegionEvents.nullEvent;
                                }
                                else
                                {
                                    markPawnsToBeTakenOff();
                                    previousPawnDistribution = currentPawnDistribution;
                                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                    return Enums.RegionEvents.nullEvent;
                                }
                            }
                            else
                            {
                                //Gracz zdjal swoj pionek
                                if (!checkPawnDistributionEquality(currentPawnDistribution, startingPawnDistribution))
                                {
                                    lastNumberOfPawnsNeededToConquareRegion = previousPawnDistribution[(int)p.getPlayerRace()];
                                    previousPawnDistribution = currentPawnDistribution;
                                    currentRegionEvent = Enums.RegionEvents.regionBeingLeftButNotAllPawnsTaken;
                                    markPawnsToBeTakenOff();
                                    setNewOwner(getStartingRegionOwner());
                                    currentRegionState = Enums.RegionState.awaitingPawnChange;
                                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                    return currentRegionEvent;
                                }
                                else
                                {
                                    //GRACZ ZREZYGNOWAL Z PODBIJANIA REGIONU region w stanie startowym
                                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                    previousPawnDistribution = currentPawnDistribution;
                                    currentRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
                                    currentRegionState = Enums.RegionState.regionClear;
                                    return currentRegionEvent;

                                }

                            }

                        }
                        else if (pawnsInRegion.Count > pawnsInRegionDuringLastUpdate.Count)
                        {
                            if (currentPawnDistribution[(int)p.getPlayerRace()] > previousPawnDistribution[(int)p.getPlayerRace()])
                            {
                                //SPRAWDZ CZY ZWIEKSZYLA SIE TYLKO LIOSC PIONKOW GRACZA
                                if (currentPawnDistribution[(int)p.getPlayerRace()] - previousPawnDistribution[(int)p.getPlayerRace()] + pawnsInRegionDuringLastUpdate.Count == pawnsInRegion.Count)
                                {
                                    //Tylko doszedl pionek gracza
                                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                    previousPawnDistribution = currentPawnDistribution;
                                    return Enums.RegionEvents.nullEvent;
                                }
                                else
                                {
                                    //Dolozono pionek przeciwnika
                                    markPawnsToBeTakenOff();
                                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                    previousPawnDistribution = currentPawnDistribution;
                                    return Enums.RegionEvents.nullEvent;

                                }
                            }
                            else if (currentPawnDistribution[(int)p.getPlayerRace()] == previousPawnDistribution[(int)p.getPlayerRace()])
                            {
                                //ZWIEKSZYLA SIE ILOSC PIONKOW PRZECIWNIKA W REJONIE
                                markPawnsToBeTakenOff();
                                pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                previousPawnDistribution = currentPawnDistribution;
                                return Enums.RegionEvents.nullEvent;
                            }
                            else
                            {
                                //ZMNIEJSZYLA SIE ILOSC PIONKOW GRACZA sprawdz czy region jest startowy
                                if (checkPawnDistributionEquality(currentPawnDistribution, startingPawnDistribution))
                                {
                                    currentRegionEvent = Enums.RegionEvents.regionLeft;
                                    currentRegionState = Enums.RegionState.regionClear;
                                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                    previousPawnDistribution = currentPawnDistribution;
                                    return currentRegionEvent;
                                }
                                else
                                {
                                    currentRegionEvent = Enums.RegionEvents.regionBeingLeftButNotAllPawnsTaken;
                                    markPawnsToBeTakenOff();
                                    setNewOwner(getStartingRegionOwner());
                                    currentRegionState = Enums.RegionState.awaitingPawnChange;
                                    lastNumberOfPawnsNeededToConquareRegion = previousPawnDistribution[(int)p.getPlayerRace()];
                                    pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                    expectedPawnDistribution = previousPawnDistribution;
                                    previousPawnDistribution = currentPawnDistribution;
                                    return currentRegionEvent;
                                }

                            }
                        }
                        return Enums.RegionEvents.nullEvent;
                    }
                    else
                    {
                        if (pawnsInRegion.Count == 0)
                        {
                            //Region was left
                            if (checkPawnDistributionEquality(currentPawnDistribution, startingPawnDistribution))
                            {
                                setNewOwner(getStartingRegionOwner());
                                currentRegionEvent = Enums.RegionEvents.regionLeft;
                                currentRegionState = Enums.RegionState.regionClear;
                                pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                previousPawnDistribution = currentPawnDistribution;
                            }
                            else
                            {
                                currentRegionEvent = Enums.RegionEvents.regionLeft;
                                currentRegionState = Enums.RegionState.awaitingPawnChange;
                                pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                expectedPawnDistribution = startingPawnDistribution;
                                previousPawnDistribution = currentPawnDistribution;
                            }
                            return currentRegionEvent;
                        }
                        else
                        {
                            Debug.Log("Region pereviously conquared no changes applied");
                            markPawnsToBeTakenOff();
                            return Enums.RegionEvents.nullEvent;
                        }
                    }
                }
                else if (currentRegionEvent == Enums.RegionEvents.regionBeingConqueredNotEnoughPawns)
                {
                    if (pawnsInRegion.Count != pawnsInRegionDuringLastUpdate.Count)
                    {
                        int estimatedForceRequired = 0;
                        int estimatedIncome = 0;
                        int playerPawnsInRegionInCurrentFrame = currentPawnDistribution[(int)p.getPlayerRace()];
                        int playerPawnsInRegionInPreviousFrame = previousPawnDistribution[(int)p.getPlayerRace()];

                        int oppPawnsInRegionInCurrentFrame = getEnemyStrengthInDistribution(p.getPlayerRace(), currentPawnDistribution);
                        int oppPawnsInRegionInPreviousFrame = getEnemyStrengthInDistribution(p.getPlayerRace(), previousPawnDistribution);

                        calculateRegionValues(p, out estimatedForceRequired, out estimatedIncome);
                        if (playerPawnsInRegionInCurrentFrame >= estimatedForceRequired)
                        {
                            setNewOwner(p.getPlayerRace());
                            currentRegionEvent = Enums.RegionEvents.regionConquered;
                            currentRegionState = Enums.RegionState.pawnsToBeCollected;
                            expectedPawnDistribution = new int[(int)Enums.RaceType.numberOfTypes];
                            expectedPawnDistribution[(int)p.getPlayerRace()] = currentPawnDistribution[(int)p.getPlayerRace()];
                            previousPawnDistribution = currentPawnDistribution;
                            pawnsInRegionDuringLastUpdate = pawnsInRegion;
                            markPawnsToBeTakenOff();
                            //MARK PAWNS TO BE COLLECTED
                            return currentRegionEvent;
                        }
                        else if (playerPawnsInRegionInCurrentFrame == 0)
                        {
                            //Gracz zrezygnowal z podbijania regionu
                            if (checkPawnDistributionEquality(currentPawnDistribution, startingPawnDistribution))
                            {

                                currentRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
                                currentRegionState = Enums.RegionState.regionClear;
                                previousPawnDistribution = currentPawnDistribution;
                                pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                return currentRegionEvent;
                            }
                            else
                            {
                                markPawnsToBeTakenOff();
                                previousPawnDistribution = currentPawnDistribution;
                                previousRegionEvent = currentRegionEvent;
                                previousRegionState = currentRegionState;
                                currentRegionEvent = Enums.RegionEvents.returningToPreviousState;
                                currentRegionState = Enums.RegionState.awaitingPawnChange;
                                expectedPawnDistribution = startingPawnDistribution;
                                pawnsInRegionDuringLastUpdate = pawnsInRegion;
                                return currentRegionEvent;
                            }
                        }
                        else
                        {
                            markPawnsToBeTakenOff();
                            lastNumberOfPawnsNeededToConquareRegion = startingPawnDistribution[(int)getStartingRegionOwner()];
                            pawnsInRegionDuringLastUpdate = pawnsInRegion;
                            expectedPawnDistribution = previousPawnDistribution;
                            previousPawnDistribution = currentPawnDistribution;
                            previousRegionEvent = currentRegionEvent;
                            previousRegionState = currentRegionState;
                            currentRegionEvent = Enums.RegionEvents.returningToPreviousState;
                            currentRegionState = Enums.RegionState.awaitingPawnChange;
                        }
                    }
                    else
                    {
                        if(currentPawnDistribution[(int)p.getPlayerRace()] > 0)
                        {
                            if(currentPawnDistribution[(int)p.getPlayerRace()] >= pawnsNeededToConquareRegion)
                            {
                                currentRegionEvent = Enums.RegionEvents.regionConquered;
                                currentRegionState = Enums.RegionState.awaitingPawnChange;

                            }
                            previousPawnDistribution = currentPawnDistribution;
                            pawnsInRegionDuringLastUpdate = pawnsInRegion;
                            return currentRegionEvent;
                        }
                        else
                        {
                            markPawnsToBeTakenOff();
                            previousPawnDistribution = currentPawnDistribution;
                            pawnsInRegionDuringLastUpdate = pawnsInRegion;
                            return Enums.RegionEvents.nullEvent;
                        }
                    }
                }
                else if (currentRegionEvent == Enums.RegionEvents.returningToPreviousState)
                {
                    //Debug.Log("Returning to prev state:");
                    //Debug.Log("Pawns curr: "+pawnsInRegion.Count + "    Pawns prev: "+pawnsInRegionDuringLastUpdate.Count);
                    //Debug.Log("Needed pawns: "+ pawnsNeededToConquareRegion + "    player pawns: "+ currentPawnDistribution[(int)p.getPlayerRace()]);
                    //Debug.Log(currentPawnDistribution == expectedPawnDistribution);
                    printPawnDistribution(currentPawnDistribution);
                    printPawnDistribution(expectedPawnDistribution);
                    if (currentPawnDistribution[(int)p.getPlayerRace()] >= pawnsNeededToConquareRegion)
                    {
                        //GRACZ PODBIL REGION
                        markPawnsToBeTakenOff();
                        setNewOwner(p.getPlayerRace());
                        pawnsInRegionDuringLastUpdate = pawnsInRegion;
                        previousPawnDistribution = currentPawnDistribution;
                        currentRegionEvent = Enums.RegionEvents.regionConquered;
                        if (pawnsInRegion.Count > currentPawnDistribution[(int)p.getPlayerRace()])
                        {
                            currentRegionState = Enums.RegionState.regionClear;
                        }
                        else
                        {
                            currentRegionState = Enums.RegionState.awaitingPawnChange;
                        }
                        if (previousRegionEvent == currentRegionEvent)
                        {
                            return Enums.RegionEvents.nullEvent;
                        }
                        else
                        {
                            return currentRegionEvent;
                        }
                    }
                    else if (checkPawnDistributionEquality(currentPawnDistribution, expectedPawnDistribution))
                    {
                        pawnsInRegionDuringLastUpdate = pawnsInRegion;
                        previousPawnDistribution = currentPawnDistribution;

                        currentRegionEvent = previousRegionEvent;
                        currentRegionState = previousRegionState;
                        return currentRegionEvent;
                    }
                    //else if (currentPawnDistribution[(int)p.getPlayerRace()] >= lastNumberOfPawnsNeededToConquareRegion && lastNumberOfPawnsNeededToConquareRegion != 0)
                    
                    else
                    {
                        markPawnsToBeTakenOff();
                        pawnsInRegionDuringLastUpdate = pawnsInRegion;
                        previousPawnDistribution = currentPawnDistribution;

                        return Enums.RegionEvents.nullEvent;
                    }

                }
                else if (currentRegionEvent == Enums.RegionEvents.regionBeingLeftButNotAllPawnsTaken)
                {
                    if(currentRegionState == Enums.RegionState.cantBeLeft)
                    {
                        if (currentPawnDistribution[(int)p.getPlayerRace()] ==  expectedPawnDistribution[(int)p.getPlayerRace()])
                        {
                            currentRegionEvent = Enums.RegionEvents.regionConquered;
                            if (pawnsInRegion.Count == currentPawnDistribution[(int)p.getPlayerRace()])
                            {
                                currentRegionState = Enums.RegionState.regionClear;
                            }
                            else
                            {
                                currentRegionState = Enums.RegionState.awaitingPawnChange;
                            }
                            previousPawnDistribution = currentPawnDistribution;
                            pawnsInRegionDuringLastUpdate = pawnsInRegion;

                            return Enums.RegionEvents.nullEvent;
                        }
                        else
                        {
                            markPawnsToBeTakenOff();
                            previousPawnDistribution = currentPawnDistribution;
                            pawnsInRegionDuringLastUpdate = pawnsInRegion;
                            return currentRegionEvent;
                        }
                    }
                    else if (checkPawnDistributionEquality(expectedPawnDistribution, currentPawnDistribution))
                    {

                    }
                    else if (currentPawnDistribution[(int)p.getPlayerRace()] >= lastNumberOfPawnsNeededToConquareRegion)
                    {
                        //REGIONPODBITY
                        setNewOwner(p.getPlayerRace());
                        currentRegionEvent = Enums.RegionEvents.regionConquered;
                        if (currentPawnDistribution[(int)p.getPlayerRace()] == pawnsInRegion.Count)
                        {
                            currentRegionState = Enums.RegionState.regionClear;
                        }
                        else
                        {
                            currentRegionState = Enums.RegionState.awaitingPawnChange;
                        }
                        previousPawnDistribution = currentPawnDistribution;
                        pawnsInRegionDuringLastUpdate = pawnsInRegion;

                        return Enums.RegionEvents.nullEvent;
                    }
                    else
                    {
                        previousPawnDistribution = currentPawnDistribution;
                        pawnsInRegionDuringLastUpdate = pawnsInRegion;
                        return currentRegionEvent;
                    }
                }
                else if (currentRegionEvent == Enums.RegionEvents.regionLeft)
                {
                    if (checkPawnDistributionEquality(currentPawnDistribution, startingPawnDistribution))
                    {
                        currentRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
                        currentRegionState = Enums.RegionState.regionClear;
                        pawnsInRegionDuringLastUpdate = pawnsInRegion;
                        previousPawnDistribution = currentPawnDistribution;

                        return currentRegionEvent;
                    }
                }

            }
            #endregion

            //pawnsInRegion = new List<Pawn>();
            pawnsInRegionDuringLastUpdate = pawnsInRegion;
            previousPawnDistribution = currentPawnDistribution;
            return Enums.RegionEvents.nullEvent;
            //Debug.Log("Region " + regionIndex + " pawn check");
            //Debug.Log("Was: " + pawnsInRegionDuringLastUpdate.Count + "Is: " + pawnsInRegion.Count);
            //printPawnDistribution(pawnsInRegionDuringLastUpdate);
            //printPawnDistribution(pawnsInRegion);
            /*if (currentRegionEvent == Enums.RegionEvents.regionClearForNextEvent)
            {

                //EXPECT THAT THERE WERE NO CHANGES
                if(checkIfSimilarPawnIsInRegion())
                {
                    regionDidNotChange();
                }
                 //EXPECT THAT THERE WERE SOME PAWNS BUT NOW THERE ARE NONE
                else if (pawnsInRegionDuringLastUpdate.Count > 0 && pawnsInRegion.Count == 0)
                {
                    regionLeft();
                    prepareEvent();
                }
                 //EXPECT THAT ALL NEW PAWNS ARE THE SAME AND THERE WERE NO PAWNS EARLIER   
                else if(pawnsOfTheSameRaceInRegion() && pawnsInRegionDuringLastUpdate.Count == 0 && pawnsInRegion.Count > 0)
                {
                    int playerPawns = 0;
                    if (pawnsInRegion.Count >= calculateEstimatedForceRequired(p, out playerPawns, out playerPawns))
                    {
                        conquereBlankRegion();
                        prepareEvent();
                    }
                    else
                    {
                        currentRegionEvent = Enums.RegionEvents.regionBeingConqueredNotEnoughPawns;
                        currentRegionState = Enums.RegionState.awaitingPawnChange;
                    }
                }
                //EXPECT THAT WERE SOME PAWNS AND NOW THERE ARE SOME TOO
                else if (pawnsInRegionDuringLastUpdate.Count > 0 && pawnsInRegion.Count > 0)
                {
                    //EXPECT THAT ONLY THE NUMBER OF OWNER PAWNS WAS CHANGED
                    if (pawnsOfTheSameRaceInRegion() && pawnsInRegion[0].getRaceType() == regionOccupiedBy)
                    {
                        if (pawnsInRegion.Count > pawnsInRegionDuringLastUpdate.Count)
                        {
                            regionTroopChange(true);
                            prepareEvent();
                        }
                        else
                        {
                            regionTroopChange(false);
                            prepareEvent();
                        }
                    }
                        //EXPECT THAT THERE ARE PAWNS OF DIFFERENT RACES -------- BATTLE
                    else if (!pawnsOfTheSameRaceInRegion())
                    {
                        //DIFFERENT RACES -------> BATTLLE
                        Enums.RaceType offenseRace;
                        float raceDiffFactor = calculateRaceFactor(out offenseRace);
                        if (raceDiffFactor >= 1.0f)
                        {
                            //NOT ENOUGH PAWNS ON OFFENSE
                            currentRegionEvent = Enums.RegionEvents.regionBeingConqueredNotEnoughPawns;
                            currentRegionState = Enums.RegionState.awaitingPawnChange;

                        }
                        else
                        {
                            // ------------REGION CONQUERED
                            conquereRegion(offenseRace);
                            markPawnsOfDefeatedRace();
                            prepareEvent();
                        }
                    }
                    else
                    {
                        //UNEXPECTED MOVE
                        Debug.Log("Unexpexted Move");
                        setStateToAwaitToPreviousState();
                    }
                }
                else
                {   //UNEXPECTED MOVE
                    setStateToAwaitToPreviousState();
                }
            }
            else
            {
                if (currentRegionEvent == Enums.RegionEvents.regionBeingConqueredNotEnoughPawns)
                {
                    //state ----> AWAITINGPAWNCHANGE
                    #region regionBeingConqueredNotEnoughPawns
                        
                        if (pawnsInRegion.Count != pawnsInRegionDuringLastUpdate.Count)
                        {
                            if (!pawnsOfTheSameRaceInRegion())
                            {
                                //NUMBER OF PAWNS HAS CHANGED
                                Enums.RaceType offenseRace;
                                float factor = calculateRaceFactor(out offenseRace);
                                if (factor < 1.0f)
                                {
                                    //---------REGION CONQUERED------
                                    conquereRegion(offenseRace);
                                    markPawnsOfDefeatedRace();
                                    prepareEvent();
                                }
                                //IF STILL NOT ENOUGH POWER TO CONQUERE DO NOTHING
                            }
                            else
                            {
                                setStateToAwaitToPreviousState();
                            }
                        }
                        else
                        {
                            //NUMBER OF PAWNS DIDN'T CHANGE CHECK IF PAWNS TYPE CHANGED
                            if (!checkIfSimilarPawnIsInRegion())
                            {
                                //ERROR INVALID MOVE ----- PROCEED TO PREVIOUS STATE
                                setStateToAwaitToPreviousState();

                            }
                            //NOTHING CHANGED ->>> STILL WAIT FOR PAWN CHANGE

                        }
                    #endregion
                }
                else if (currentRegionEvent == Enums.RegionEvents.regionConquered)
                {
                    #region regionConquered
                    if (areTheExpectedPawnsInRegion(distributePawns(pawnsInRegion)))
                    {
                        currentRegionState = Enums.RegionState.regionClear;
                        currentRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
                    }
                    else
                    {
                        markPawnsOfDefeatedRace();
                    }
                    #endregion
                }
                else if (currentRegionEvent == Enums.RegionEvents.returningToPreviousState)
                {
                    #region returningToPreviousState
                    if (pawnsInRegion.Count == pawnsInRegionDuringLastUpdate.Count && areTheExpectedPawnsInRegion(distributePawns(pawnsInRegion)))
                    {
                        currentRegionEvent = previousRegionEvent;
                        previousRegionEvent = Enums.RegionEvents.regionClearForNextEvent;
                        currentRegionState = previousRegionState;
                        previousRegionState = Enums.RegionState.regionClear;

                    }
                    #endregion
                }
                
                //REGION SHOULD BE CLEAR CHECK IF ANYTHING CHANGED
                else if (!(pawnsInRegion.Count == pawnsInRegionDuringLastUpdate.Count && checkIfSimilarPawnIsInRegion()))
                {
                    //IF CURRENT STATE IS DIFFERENT THAN PREVIOUS ----> SET STATE TO AWAIT FOR PREVIOUS STATE
                    setStateToAwaitToPreviousState();

                }
                
            }

            if (currentRegionEvent != Enums.RegionEvents.returningToPreviousState)
            {
                updateRegionPawns();
            }
            pawnsInRegion = new List<Pawn>();
            return Enums.RegionEvents.;
             * */
        }
         //RETURNS TRUE IF SIMILAR PAWNs WERE IN REGION DURING LAST UPDATE
        private bool checkIfSimilarPawnIsInRegion(int[] lastFrame, int[] current)
        {
            if (lastFrame.Length == current.Length)
            {
                for (int i = 0; i < lastFrame.Length; i++)
                {
                    if (lastFrame[i] != current[i])
                    {
                        return false;
                    }
                }
            }
            else{
                return false;
            }
            return true;
        }
        private bool checkPawnDistributionEquality(int[] distributionA, int[] distributionB)
        {
            if (distributionA.Length == distributionB.Length)
            {


                for (int i = 0; i < distributionA.Length; i++)
                {
                    if (distributionA[i] != distributionB[i])
                    {
                        //DODAJ PIONKOWI DO USUNIECIA
                        //Debug.Log((Enums.RaceType)i + "    " + distributionA[i] + "    " + distributionB[i]);
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool checkPawnDistributionEquality(int[] currentPawnDistirbution)
        {
            for (int i = 0; i < currentPawnDistirbution.Length; i++)
            {
                if (currentPawnDistirbution[i] != previousPawnDistribution[i])
                {
                    Debug.Log((Enums.RaceType)i + "    " + currentPawnDistirbution[i] + "    " + previousPawnDistribution[i]);
                    return false;
                }
            }
            return true;
        }
        //RETURNS TRUE IF SIMILAR PAWNs WERE IN REGION DURING LAST UPDATE
        private bool checkIfSimilarPawnIsInRegion()
        {
            if (pawnsInRegion.Count == pawnsInRegionDuringLastUpdate.Count)
            {

                int[] numberOfPawnRacesCurrent = new int[(int)Enums.RaceType.numberOfTypes];
                int[] numberOfPawnRacesPrevious = new int[(int)Enums.RaceType.numberOfTypes];

           
                for (int i = 0; i < pawnsInRegion.Count; i++)
                {
                    numberOfPawnRacesCurrent[(int)pawnsInRegion[i].getRaceType()] += 1;
                    numberOfPawnRacesPrevious[(int)pawnsInRegionDuringLastUpdate[i].getRaceType()] += 1;
                }

                for (int i = 0; i < numberOfPawnRacesCurrent.Length; i++)
                {
                    if (numberOfPawnRacesCurrent[i] != numberOfPawnRacesPrevious[i])
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
            return true;
        }
    }
}
